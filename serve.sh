#!/bin/bash

if [ -z "$1" ]; then
  echo "Parameter missing: language"
  exit 1
fi

langdir=content/$1
cp mkdocs_base.yml $langdir/mkdocs.yml
cp -r vendor $langdir/docs/

pushd $langdir > /dev/null
cat config.yml >> mkdocs.yml
#mkdocs serve --config-file mkdocs.yml
docker run --rm -v $(readlink -f .):/mkdocs/ --expose 8000 -p 127.0.0.1:8000:8000 metasolutions/mkdocs mkdocs serve --livereload -a 0.0.0.0:8000
rm mkdocs.yml
rm -r material docs/vendor/
popd > /dev/null
