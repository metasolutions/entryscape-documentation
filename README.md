# ENTRYSCAPE Documentation

## Prerequisites

The scripts use Docker. Follow the [official instructions](https://docs.docker.com/engine/install/ubuntu/) to install Docker on Ubuntu.

## Build and browse with all languages

Run `./build.sh _site` to build the documentation in all languages. The output directory is `_site`.

If you an error such as  `Parameter missing: directory to deploy to` then run

`./build.sh _site` and enter your site via your browser and local file directory e.g. `file:///path/to/your/built/mkdocs`.

Please note that this not allow for navigating top menu files.

## Serve locally one language at a time

If you want to serve the site locally in one language per time then run serve with a specific language code you want to use, currently en, de and sv.

Serving the site locally in Swedish is e.g. done by running `./serve.sh sv`.

If it works you will see something like: `Serving on http://127.0.0.1:8000
`. Enter this link and you can browse the latest changes by saving your code and refreshing your pages.

## License

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
