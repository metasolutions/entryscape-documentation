# Objekte

Ein Projekt enthält **Objekte** verschiedener **Objekttypen**, wie z. B. Dokumente, Veranstaltungen, Grafiken, Fotos, Autoren usw. Sie werden in der Projektübersicht zusammen mit der Anzahl der einzelnen Typen aufgelistet. Die aufgelisteten Objekttypen werden durch den ausgewählten Projekttyp bestimmt und sind oft vordefiniert, können aber in einigen Fällen vom Benutzer [konfiguriert](projekte.md#configure-entity-types) werden.

Das folgende Beispiel zeigt ein Tourismusprojekt mit vordefinierten Objekttypen in Form von Aktivitäten, Unterkünften, Essen, Fotos, Wanderungen usw.

![Ein Tourismusprojekt mit zugehörigen Objekten](img/workbench_entities_overview.png)

## Neues Objekt erstellen
<a id="create-new"></a>

Um ein neues Objekt zu erstellen, klicken Sie auf den Objekttyp und dann auf die Schaltfläche **"Erstellen"**. Das folgende Beispiel zeigt den Objekttyp "Unterkunft" im Projekt "Tourismus", aber es könnte sich genauso gut um Broschüren, Aktivitäten, Künstler oder etwas anderes handeln. Hier können Sie die Metadaten eingeben und Beziehungen zu anderen Objekttypen herstellen.

![Neues Objekt erstellen](img/workbench_entities_createnewentity.png)

Die hier angezeigten Informationsfelder hängen auch von der ausgewählten Konfiguration (Projekttyp mit den zugehörigen Objekttypen) ab.

## Beschreiben
<a id="describe"></a>

Denken Sie daran, dass genauere Beschreibungen veröffentlichter Projekte und Objekte es anderen erleichtern, die Metadaten zu finden und wiederzuverwenden.

Das Eingabefenster enthält Eingabefelder auf drei verschiedenen Ebenen: **Pflichtangaben** (müssen ausgefüllt werden), **empfohlene Angaben** (es ist gut, sie auszufüllen) und **freiwillige Angaben**. Sie können mithilfe der Schieberegler oben festlegen, welche Eingabefelder angezeigt werden sollen.

![Neue Aktivität erstellen](img/workbench_entities_man_rec_opt.png)

## Bearbeiten
<a id="edit"></a>

Sie können später zu einem Objekt gehen und **weitere Informationen ändern** oder **hinzufügen**, z.B. Informationen in einer neuen Sprache hinzufügen oder neue Bilder hochladen. Klicken Sie dazu entweder auf den Bleistift ganz rechts oder auf das Objekt, um zur Übersichtsseite zu gelangen, und klicken Sie dann auf die Schaltfläche **"Bearbeiten"**.

![Stift zum Bearbeiten eines Objekts](img/workbench_entities_editentity.png)

![Schaltfläche "Bearbeiten" auf der Übersichtsseite eines Objekts](img/workbench_entities_editbutton.png)

## Dateien und Links verwalten
<a id="manage-files-and-links"></a>

Für Objekte, die mit **Dateien** wie Bildern oder Dokumenten verlinkt sind, wird in diesem Abschnitt beschrieben, wie Sie Dateien in EntryScape Workbench hochladen, herunterladen und ersetzen können. Es ist auch möglich, auf Dateien, Webseiten und andere im Internet verfügbare Ressourcen zu **verlinken**.

### Datei hochladen
<a id="upload-file"></a>

Um **eine Datei hochzuladen**, klicken Sie auf **Datei** und dann auf das Lupensymbol.

![Datei hochladen](img/workbench_entities_uploadfile.png)

### Datei herunterladen
<a id="download-file"></a>

Sie können auch Dateien **herunterladen**, die hochgeladen wurden, indem Sie die Übersichtsseite des Objekts aufrufen und auf die Schaltfläche **"Herunterladen"** klicken.

![Datei herunterladen](img/workbench_entities_downloadfile.png)

### Datei ersetzen
<a id="replace-file"></a>
Bei Objekten, die aus von Ihnen hochgeladenen **Dateien** bestehen (z.B. Bilddateien oder Dokumente), gibt es eine Schaltfläche "Datei ersetzen", mit der Sie die mit der Metadatenbeschreibung verlinkte Datei schnell und einfach ersetzen können.

![Schaltfläche "Datei ersetzen"](img/workbench_entities_replacefilebutton.png)
Suchen Sie die neue Datei und klicken Sie auf **"Datei ersetzen"**.

![Datei ersetzen](img/workbench_entities_replacefile.png)

### Link zu einer Ressource
<a id="link-to-resource"></a>

Um eine **Verlinkung** zu einer Datei, Webseite, einem Bild oder einer anderen Ressource herzustellen, die im Internet verfügbar ist, geben Sie die URL ein oder **fügen Sie sie ein**, und fügen Sie einen beschreibenden Namen hinzu.

![Schaltfläche "Bearbeiten" auf der Übersichtsseite eines Objekts](img/workbench_entities_linktoresource.png)

## Suchen
<a id="search"></a>

Wenn Sie viele Objekte haben, z.B. eine große Sammlung von Bildern oder Dokumenten, und nach einem bestimmten Dokument suchen, können Sie das **Suchfeld** oben verwenden. **Geben Sie mindestens 3 Zeichen ein**, um ein Suchresultat zu erhalten.

![Suchfeld in der Workbench](img/workbench_entities_search.png)

## Tabellenansicht und Listenansicht
<a id="table-view-and-list-view"></a>

In der Standardansicht von EntryScape Workbench werden Daten in einer **Listenansicht** angezeigt. Wenn Sie jedoch viele Objekte gleichzeitig auf derselben Seite anzeigen und bearbeiten möchten, können Sie die **Tabellenansicht** verwenden. Sie können zur Tabellenansicht wechseln, indem Sie oben auf das **Tabellensymbol** klicken.

![Tabellensymbol zum Wechseln in die Tabellenansicht](img/workbench_entities_tableview1.png)

In der Tabellenansicht können Sie auch festlegen, wie viele Spalten Sie gleichzeitig anzeigen möchten. Durch Klicken auf **"Spalten"** können Sie auswählen, welche Spalten Sie anzeigen oder ausblenden möchten.

![Spaltenauswahl](img/workbench_entities_columns.png)

Um eine Tabellenzelle zu bearbeiten, klicken Sie auf den Text in der Zelle. Klicken Sie auf das **obere X** oder außerhalb des Bearbeitungsfelds, um es **zu schließen**. Vergessen Sie nicht, unten auf die Schaltfläche **Speichern** zu klicken, nachdem Sie alle Tabellenzellen bearbeitet haben.

![Tabellenansicht mit Bearbeitungsoptionen](img/workbench_entities_tableview2.png)

Um zur Listenansicht zurückzukehren, klicken Sie auf das Listensymbol oben rechts.

![Symbol für Listenansicht](img/workbench_entities_listview.png)

## Detaillierte Informationen anzeigen
<a id="see-detailed-information"></a>

Wenn Sie detailliertere Informationen zu einem Projekt oder einem Objekt anzeigen möchten, klicken Sie auf das Informationssymbol.

![Informationssymbol](img/workbench_informationicon.png)

Dadurch wird ein Dialogfenster geöffnet, in dem Sie die Metadaten für das Objekt und alle anderen Objekte sehen können, die mit Ihrem Objekt verknüpft sind. Weitere Informationen finden Sie auf der Seite [Detaillierte Informationen](information.md).

## Entfernen
<a id="remove"></a>

Um ein Objekt **zu entfernen**, gehen Sie zur Übersichtsseite des Objekts und klicken Sie auf die Schaltfläche **Entfernen**.

![Menüpunkt "Objekt entfernen"](img/workbench_entities_removeentity.png)

Beachten Sie, dass Sie beim Versuch, ein Objekt zu löschen, das von einer anderen Stelle aus **verlinkt ist**, z.B. ein Hauptbild, das zur Darstellung eines Hotels verwendet wird, eine Fehlermeldung erhalten. Sie müssen zuerst das Hauptbild durch ein anderes Bild auf der Hotelseite ersetzen, bevor Sie das alte Bild löschen können.

![Fehlermeldung zum Löschen eines verlinkten Objekts](img/workbench_entities_removelinkedfile.png)