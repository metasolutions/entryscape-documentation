Arbeitsbereiche in Workbench werden als **Projekte** bezeichnet und enthalten wiederum verschiedene Typen von **Objekten**.

Auf der Hauptseite sehen Sie Projekte, die Sie bereits erstellt haben oder auf die Sie Zugriff haben. Es wird auch angezeigt, ob das Projekt **öffentlich** ist oder nicht, d.h. ob es von anderen gefunden und durchsucht werden kann. Wenn Sie hier kein Projekt sehen, müssen Sie zunächst eines erstellen oder den Gruppenmanager um [Zugriffsberechtigung](#permissions-and-sharing-settings) für das Projekt bitten.

![Übersicht der Projekte](img/workbench_projects_listprojects.png)

## Ein Projekt erstellen
<a id="create-a-project"></a>

Um **ein neues Projekt zu erstellen**, klicken Sie auf die Schaltfläche **"Erstellen"**.

![Schaltfläche "Erstellen"](img/workbench_projects_createbutton.png)

Wenn Ihre Organisation mehrere verschiedene Projekttypen zur Auswahl hat, müssen Sie zunächst den **Projekttyp** für Ihr Projekt auswählen. Der Projekttyp bestimmt auch, welche **Objekttypen** in Ihrem Projekt enthalten sein werden.

![Dialogfeld „Projekt erstellen“](img/workbench_projects_createproject.png)

Geben Sie als Nächstes den **Projektnamen** ein, d.h. einen Titel wie "Dokumentenverwaltung". Beachten Sie, dass der Titel kurz sein sollte und dass alle Wörter durchsuchbar sein müssen. Geben Sie außerdem eine kurze **Projektbeschreibung** ein.

Ein neu erstelltes Projekt ist nicht öffentlich. Der Ersteller des Projekts wird automatisch zum **Gruppenmanager** für das Projekt, d.h., Sie verwalten den Zugriff anderer Benutzer auf das Projekt.

## Berechtigungen und Freigabeeinstellungen
<a id="permissions-and-sharing-settings"></a>

In EntryScape Workbench kann ein Benutzer im Allgemeinen entweder ein **Gruppenmanager** oder ein **Mitglied** mit Lese- und Schreibberechtigungen sein. Der Gruppenmanager kann anderen Benutzern Zugriff auf ein Projekt gewähren und das Projekt veröffentlichen. Mitglieder können die Metadaten des Projekts bearbeiten und Objekte für das Projekt erstellen und verwalten, jedoch nicht veröffentlichen. Je nach Ihrer Arbeitsrolle und der Datenorganisation können Sie Mitglied eines oder mehrerer Projekte sein.

Bei Bedarf ist es möglich, in bestimmten Fällen eine höhere Berechtigungsstufe zu erhalten, d.h. Administratorberechtigungen, wenn Sie sich an den MetaSolutions-Support wenden. In solchen Fällen muss Ihre Organisation bereits eine Arbeitsroutine, eine Arbeitsgruppe und eine Metadatenverwaltung oder eine offene Datenverwaltung in der Verwaltungsorganisation eingerichtet haben.

Wenn Sie keinen Zugriff auf das Projekt haben, mit dem Sie arbeiten sollen, müssen Sie den Gruppenmanager (oder Administrator) um Zugriff auf dieses Projekt bitten. Die verantwortliche Person ist wahrscheinlich der Abteilungsleiter, Manager oder eine ähnliche Person in Ihrer Organisation.

Gruppenmanager können die **Freigabeeinstellungen** für andere Benutzer ändern, indem sie auf das Dreipunktmenü klicken und **"Freigabeeinstellungen"** auswählen.

![Menüpunkt Freigabeeinstellungen](img/workbench_projects_menusharingsettings.png)

Dort können Sie **Benutzer zur Gruppe hinzufügen oder entfernen**, die Zugriff auf ein Projekt haben. Um einen Benutzer zur Gruppe hinzuzufügen, klicken Sie auf "Benutzer zur Gruppe hinzufügen". Sie können auch auswählen, **welche Berechtigungsstufe** der Benutzer haben soll (Gruppenmanager oder Mitglied), indem Sie auf das Symbol vor dem Namen klicken.

![Berechtigungseinstellungen für Benutzer eines Katalogs](img/workbench_projects_sharingsettings2.png)

Um einen Benutzer aus der Gruppe **zu entfernen**, klicken Sie auf das Dreipunktmenü und wählen Sie **"Entfernen"** aus.

![Menüpunkt „Benutzer aus Gruppe entfernen“](img/workbench_projects_sharingsettings3.png)

## Projekt bearbeiten
<a id="edit-project"></a>

Sie können die Informationen zu Ihrem Projekt bearbeiten, indem Sie auf das **Dreipunktmenü** klicken und "Bearbeiten" auswählen.

![Menüpunkt "Projekt bearbeiten"](img/workbench_projects_menueditproject.png)

Im Bearbeitungsmodus können Sie Informationen ändern oder Informationen in **verschiedenen Sprachen** hinzufügen.

![Projekt bearbeiten](img/workbench_projects_editproject.png)

## Objekttypen konfigurieren
<a id="configure-entity-types"></a>

Sie müssen nur in **besonderen Fällen** Objekttypen hinzufügen oder entfernen, da Objekttypen in den meisten Fällen bereits im ausgewählten Projekttyp für das Projekt vordefiniert sind. Wenn Sie **Objekttypen für ein Projekt ändern** müssen, können Sie auf das Dreipunktmenü für ein Projekt klicken und **"Objekttypen konfigurieren"** auswählen.

![Menüpunkt "Objekttypen konfigurieren"](img/workbench_projects_menuconfigureentitytypes.png)

Wenn Sie bereits einen vordefinierten Projekttyp haben, von dem aus Sie starten können, können die Objekttypen nicht geändert werden. Sehen dazu das Beispiel des Projekttyps "Tourismus" unten, bei dem die Objekttypen ausgegraut und somit gesperrt sind.

![Objekttypen konfigurieren, gesperrt](img/workbench_projects_manageentitytypes_tourism.png)

In anderen Fällen, in denen Sie die Objekttypen ändern müssen, können Sie auswählen, welche Objekttypen Sie im Projekt haben möchten.

![Objekttypen konfigurieren, auswählbar](img/workbench_projects_manageentitytypes_default.png)

## Projekt entfernen
<a id="remove-project"></a>

Um ein Projekt **zu entfernen**, klicken Sie auf das Dreipunktmenü des Projekts und wählen Sie **"Entfernen"** aus.
![Menüpunkt Projekt entfernen](img/workbench_projects_menuremoveproject.png)