Wenn Sie auf das **Informationssymbol** klicken, wird ein Dialogfeld mit **detaillierteren Metadaten** für ein Dokument, ein Ereignis, ein Kunstwerk oder ein anderes Objekt Ihrer Wahl geöffnet.

![Informationssymbol in der Liste](img/workbench_detailedinformation_icon1.png)

Das Informationssymbol finden Sie sowohl in der Liste der Objekte als auch auf der Übersichtsseite für ein einzelnes Objekt.

![Informationssymbol auf der Übersichtsseite](img/workbench_detailedinformation_icon2.png)

Im daraufhin angezeigten Dialogfenster sehen Sie drei Registerkarten: **"Information"**, **"Über"** und **"Referenzen"**.

![Informationsdialog, drei Registerkarten](img/workbench_detailedinformation_tabs.png)

## Informationen
<a id="information"></a>

Auf der ersten Registerkarte, **"Information"**, finden Sie Metadaten für das von Ihnen ausgewählte Objekt, einschließlich aller anderen Objekte, mit denen **Ihr Objekt verlinkt ist**.

![Informationsdialog, Registerkarte "Informationen"](img/workbench_detailedinformation_information.png)

## Über
<a id="about"></a>

Im zweiten Reiter, **"Über"**, finden Sie den **URI** zu Ihrem Objekt. So können Sie den URI schnell finden und **kopieren**, wenn Sie an anderer Stelle darauf verweisen müssen.

![Informationsdialog, Reiter "Über"](img/workbench_detailedinformation_about.png)

## Referenzen
<a id="references"></a>

Im dritten Reiter, **"Referenzen"**, finden Sie alle Objekte, die **auf Ihr Objekt verweisen**.

![Informationsdialog, Reiter "Referenzen"](img/workbench_detailedinformation_references.png)

## Sprachen
<a id="languages"></a>

In der oberen rechten Ecke befinden sich zwei **Sprachsymbole**, mit denen Sie die Informationen in den drei Reitern in **verschiedenen Sprachen** anzeigen können.

![Informationsdialog, Sprachen](img/workbench_detailedinformation_languageicons.png)

Wenn Sie auf das **Globussymbol** klicken, werden alle Metadaten zu Ihrem Objekt in **allen eingegebenen Sprachen gleichzeitig** angezeigt. Um alle Sprachen zu deaktivieren und zur ursprünglichen Sprache zurückzukehren, klicken Sie erneut auf das Globussymbol.

Sie können Ihre Metadaten auch in **jeweils einer Sprache anzeigen lassen**, indem Sie auf **das Symbol für die richtige Sprache** klicken und eine Sprache aus einer langen Liste auswählen.

![Informationsdialog, Sprachenliste](img/workbench_detailedinformation_languagelist.png)