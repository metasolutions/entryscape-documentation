**EntryScape Workbench** ist ein generisches **Metadaten-Tool**, das an Informationsmodelle von Organisationen angepasst werden kann. Dabei kann es sich um Publikationen, Tourismusdaten, Kunstwerke, Kreditorenbuchhaltung oder andere Datensammlungen handeln, die als offene oder gemeinsam genutzte Daten veröffentlicht werden sollen.

Arbeitsbereiche in Workbench sind in **Projekten** organisiert und enthalten wiederum verschiedene **Objekte**, die mit Metadaten und Beziehungen bzw. Verweisen beschrieben werden.

![Projektbeispiele in Workbench](img/workbench_overview_projectexamples.png)

## Projektbeispiel

Das folgende Beispiel zeigt ein Tourismusprojekt, das eine Reihe verschiedener **Objekttypen** enthält, z.B. Unterkünfte, Aktivitäten, Veranstaltungen, etc. Wir haben uns entschieden, Objekte vom Typ **Aktivitäten** aufzulisten.

Durch Klicken auf eine Aktivität können Sie deren Metadaten wie Name, Beschreibung, Webseite usw. bearbeiten und die Aktivität mit anderen Objekten wie Fotos, nahe gelegenen Unterkünften, Restaurants usw. verlinken.

![EntryScape Workbench, Aktivitäten](img/workbench_overview_editgavleborg.png)

Die gesammelten Informationen können dann **auf einer Webseite** potenziellen Besuchern der Region präsentiert werden. In diesem Beispiel werden verschiedene Aktivitäten für Besucher der Region Gävleborg mithilfe unserer Präsentationsbibliothek **EntryScape Blocks** präsentiert. Besucher können sich von den Aktivitäten zu Restaurants oder Unterkünften in der Nähe durchklicken.

![Präsentation von EntryScape Workbench-Aktivitäten mit EntryScape Blocks](img/workbench_overview_presentationgavleborg.png)