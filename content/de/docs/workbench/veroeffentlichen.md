## Veröffentlichungsprojekte und Objekte
<a id="publishing-projects-and-entities"></a>

Alle neu erstellten Projekte sind standardmäßig unveröffentlicht, sodass nur Benutzer mit den entsprechenden Berechtigungen das Projekt anzeigen und verwalten können. Wenn der Veröffentlichungsstatus des Projekts grün ist, bedeutet dies, dass die Metadaten des Projekts öffentlich lesbar sind und z. B. von Open-Data-Portalen erfasst werden können.
Über die Schaltfläche "Veröffentlichen" können Sie Ihr Projekt **veröffentlichen** oder **die Veröffentlichung aufheben**.

![Schaltfläche "Veröffentlichen" für ein Projekt](img/workbench_publishing_publishproject.png)

Wenn Ihre Projekt- und Objekttypen so konfiguriert sind, dass sie das **Veröffentlichen** und **Zurückziehen der Veröffentlichung** von Objekten zulassen, können Sie dies auf der Übersichtsseite eines Objekts festlegen. Klicken Sie auf die Schaltfläche **"Veröffentlichen"** für das Objekt, um es zu veröffentlichen. Beachten Sie, dass das Projekt, unter dem sich das Objekt befindet, **veröffentlicht** sein muss, damit auch das Objekt veröffentlicht wird.

![Schaltfläche "Veröffentlichen" für ein Objekt](img/workbench_publishing_publishentity.png)

Objekte mit einem **grünen Veröffentlichungssymbol** (Weltkugel) sind veröffentlicht, wie im folgenden Beispiel.

![Veröffentlichungssymbol für ein Objekt](img/workbench_publishing_publishedentity.png)

Wenn Sie kein Veröffentlichungssymbol (oder keine Veröffentlichungstaste) für Ihre Objekte sehen, bedeutet dies, dass sie **automatisch mit dem Projekt veröffentlicht/deaktiviert werden**, wenn es veröffentlicht/deaktiviert wird.

## Versionsverlauf
<a id="revision-history"></a>

Entsprechend konfigurierte EntryScape-Instanzen verfügen über eine automatische **Versionshistorie** von Projekten und Objekten. Die Versionshistorie finden Sie unter dem Menü mit den drei Punkten für Projekte und auf der Übersichtsseite für Objekte.

![Menüpunkt Versionen für ein Projekt](img/workbench_publishing_revisionsproject.png)

Für Objekte gibt es eine blaue Schaltfläche, um auf die Versionshistorie zuzugreifen.

![Versionsschaltfläche für Objekte](img/workbench_publishing_revisionsentities.png)

Wenn Sie Gruppenmanager oder Administrator sind, können Sie zu früheren Versionen von Metadaten für das Objekt Ihrer Wahl **zurückkehren**. Normale Mitglieder haben die Berechtigung, verschiedene Versionen anzuzeigen, können jedoch nicht zu früheren Versionen zurückkehren.

![Versionsverlauf](img/workbench_publishing_revisionhistory.png)

## Veröffentlichen mit EntryScape Blocks
<a id="publish-with-entryscape-blocks"></a>

Viele Benutzer, die Metadaten in die EntryScape Workbench eingeben, verwenden auch die Präsentationsbibliothek **EntryScape Blocks**, um die Informationen auf einer Webseite anzuzeigen. Lesen Sie mehr über EntryScape Blocks unter [entryscape.com](https://entryscape.com/sv/produkter/blocks/) oder hier in der [Dokumentation](../blocks/index.md).

### Beispiel

Unten sehen Sie die **Bearbeitungsansicht in EntryScape Workbench** für ein Reiseziel in einem Tourismusprojekt. Hier hat der Benutzer Texte, Bilder und andere Metadaten über das Reiseziel eingegeben.

![EntryScape Workbench Bearbeitungsmodus, Beispiel](img/workbench_publishing_editexample.png)

Der Text und die Bilder werden dann in einem übersichtlichen Layout auf einer Webseite mithilfe der **EntryScape Blocks**-Präsentationsbibliothek dargestellt, mit Links zu verwandten Organisationen und verwandten Objekten, die über gemeinsam genutzte Daten in EntryScape verbunden sind.

![Beispiel für eine Präsentation mit EntryScape Blocks](img/workbench_publishing_blocksexample.png)