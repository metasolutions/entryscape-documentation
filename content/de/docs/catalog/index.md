## Überblick
<a id="overview"></a>

**EntryScape Catalog** verwaltet und veröffentlicht Kataloge mit Datensätzen, Datendiensten und Distributionen gemäß empfohlener Metadatenstandards wie DCAT-AP. Es ermöglicht allen Arten von Organisationen, die gleichen Daten auf die gleiche Weise zu beschreiben und Daten auf standardisierte Weise zu teilen, sodass andere sie leicht suchen, finden und nutzen können.

![Hierarchische Übersicht über den EntryScape-Katalog](img/catalog_1_hierarchicaloverview.png)

Beispiel für einen Katalog mit Datensätzen, Distributionen, APIs, Dateien und Visualisierungen

Auf der Startseite finden Sie die Kataloge, die Sie bereits erstellt haben oder auf die Sie Zugriff haben. Sie können auch sehen, ob die Kataloge öffentlich sind oder nicht, d.h. ob sie von anderen eingesehen werden können. Wenn Sie hier keinen Katalog sehen, müssen Sie zunächst einen erstellen oder den Gruppenadministrator um [Zugriffsberechtigungen](#berechtigungen-und-freigabeeinstellungen) bitten.

![Übersicht der Kataloge](img/catalog_1_catalogs_datacatalog.png)

## Erstellen Sie einen neuen Katalog
<a id="create-a-new-catalog"></a>

Klicken Sie auf die Schaltfläche "Erstellen", um einen neuen Katalog zu erstellen.

![Schaltfläche „Erstellen”](img/catalog_1_catalogs_createbutton.png)

Wenn Ihre Organisation verschiedenen Projektarten verwendet, können Sie zunächst den **Projekttyp** für Ihren Katalog auswählen (z.B. einen Metadatenstandard). Der Standard ist DCAT-AP und für Geodaten GeoDCAT-AP.

![Dialogfeld zum Erstellen eines Katalogs](img/catalog_1_catalogs_create_catalog_popup.png)

Geben Sie dann den **Namen** des Katalogs und einen Titel ein, z.B. "Open Data der Gemeinde Hedestad". Der Titel sollte kurz sein, und alle Wörter im Titel sollten suchbar sein. Geben Sie eine kurze **Beschreibung** und einen **Herausgeber** ein, z.B. "Gemeinde Hedestad".

Ein Katalog ist nach der Erstellung immer unveröffentlicht. Benutzer die einen Katalog erstellen werden automatisch zum **Gruppenmanager**, d.h. sie verwalten den Zugriff auf diesen Katalog für andere Benutzer.

Viele Datenportale erwarten nur einen Datenkatalog pro Organisation, aber in manchen Situationen kann es besser sein, wenn verschiedene Abteilungen/Einheiten ihre eigenen Kataloge erstellen und verwalten.

## Berechtigungen und Freigabeeinstellungen
<a id="permissions-and-sharing-settings"></a>

In EntryScape Catalog kann ein Benutzer im Allgemeinen entweder ein **Gruppenmanager** oder ein **Mitglied** mit Lese- und Schreibberechtigungen sein. Der Gruppenmanager kann anderen Benutzern Zugriff auf einen Katalog gewähren und den Katalog veröffentlichen. Mitglieder können die Metadaten des Katalogs bearbeiten und Datensätze, Distributionen, Vorschläge usw. für den Katalog erstellen und verwalten, aber keine Kataloge veröffentlichen. Sie können Mitglied in einem oder mehreren Katalogen sein, je nach Ihrer Arbeitsrolle und der Organisation der Daten.

Bei Bedarf können Sie in bestimmten Fällen eine höhere Berechtigungsstufe erhalten, d.h. Administratorberechtigungen, wenn Sie sich an den MetaSolutions-Support wenden. In solchen Fällen muss Ihre Organisation bereits eine Arbeitsroutine, eine Arbeitsgruppe und eine Metadatenverwaltung oder eine offene Datenverwaltung in der Verwaltungsorganisation eingerichtet haben.

Wenn Sie keinen Zugriff auf einen Katalog haben, müssen Sie sich an den Gruppenmanager (oder Administrator) für diesen Katalog in Ihrer Organisation wenden. (Die zuständige Person ist wahrscheinlich der Abteilungsleiter, Manager oder eine ähnliche Person.)

Gruppenmanager können die **Freigabeeinstellungen** für andere Benutzer ändern, indem sie auf das Dreipunktmenü klicken und **"Freigabeeinstellungen"** auswählen.

![Menüoption Freigabeeinstellungen](img/catalog_1_catalogs_sharingsettings1.png)

Hier können Sie Benutzer zur Gruppe hinzufügen oder aus der Gruppe entfernen, die Zugriff auf einen Katalog haben. Um einen Benutzer zur Gruppe hinzuzufügen, klicken Sie auf "Benutzer zur Gruppe hinzufügen". Sie können auch auswählen, welche Berechtigungsstufe der Benutzer haben soll (Gruppenmanager oder Mitglied), indem Sie auf das Symbol vor dem Namen klicken.

![Berechtigungseinstellungen für Benutzer eines Katalogs](img/catalog_1_catalogs_sharingsettings2.png)

Um einen Benutzer aus der Gruppe zu entfernen, klicken Sie auf das Dreipunktmenü und wählen Sie "Entfernen".

![Menüoption „Benutzer aus Gruppe entfernen”](img/catalog_1_catalogs_sharingsettings3.png)

## Geben Sie weitere Informationen zum Katalog ein
<a id="enter-more-information-about-the-catalog"></a>

Wenn Sie einen Katalog erstellt haben, sollten Sie ihn mit detaillierteren Informationen beschreiben. Klicken Sie auf das **Dreipunktmenü** und wählen Sie **"Bearbeiten"** aus.

![Menüoption Katalog bearbeiten Katalog bearbeiten](img/catalog_1_catalogs_editcatalog.png)

Je detaillierter die Informationen sind, die Sie für die veröffentlichten Metadaten bereitstellen, desto einfacher ist es für andere, diese zu finden und wiederzuverwenden.

Der Dialog enthält Eingabefelder auf drei verschiedenen Ebenen: Pflichtfelder (* müssen ausgefüllt werden), empfohlene Felder (empfehlenswert) und optionale Felder. Sie können die Eingabefelder, die Sie sehen möchten, mithilfe der Schieberegler oben ein- und ausblenden.

Die Felder **Titel**, **Beschreibung** und **Herausgeber** haben Sie bereits beim Erstellen Ihres Katalogs ausgefüllt. Je nach gewähltem Profil werden Ihnen unterschiedliche Eingabefelder angezeigt.

![Buttons zum Ein- und Ausblenden der empfohlenen und optionalen Felder](img/catalog_1_catalogs_man_rec_opt.png)

## Detaillierte Informationen anzeigen
<a id="see-detailed-information"></a>

Wenn Sie detailliertere Informationen zu Ihrem Katalog anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt der Metadaten wie Herausgeber, Identifikatoren, usw. für Ihren Katalog enthält, sowie Information zu allen Objekten die auf Ihren Katalog verweisen oder von ihm ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).

## Katalog entfernen
<a id="remove-catalog"></a>

Um einen Katalog zu entfernen, klicken Sie auf das Dreipunktmenü und wählen Sie „Entfernen“.

![Menüoption Katalog entfernen](img/catalog_1_catalogs_removecatalog.png)