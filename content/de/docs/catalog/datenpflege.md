## Überblick

Je nachdem welche Option Sie im Feld **"Aktualisierungshäufigkeit"** für Ihren **Datensatz** ausgewählt haben, sollten Sie Ihre Daten entsprechend aktualisieren. Es empfiehlt sich, in Ihrem Kalender und in Ihrem Projektmanagement-Tool Erinnerungen für die Aktualisierung einzurichten. Wenn ein Datensatz nicht entsprechend seiner Aktualisierungshäufigkeit aktualisiert wird, kann dies dazu führen dass sich Datennutzer an Sie oder Ihre Organisation wenden und fragen, warum der Datensatz nicht gepflegt wird. Es ist daher besser, ihn proaktiv zu aktualisieren.

![Aktualisierungshäufigkeit für einen Datensatz](img/catalog_12_datamaintenance_updatingfrequency.png)

## Dateien in einer Distribution aktualisieren

Um die zu einer Distribution gehörenden Dateien zu verwalten, klicken Sie unter **Distributionen** auf das Dreipunktmenü und wählen Sie **"Dateien verwalten"** aus.

![Menüoption „Dateien für eine Distribution verwalten”](img/catalog_12_datamaintenance_menumanagefiles.png)

Auf der Übersichtsseite der Distribution können Sie **eine neue Datei** (z.B. Daten für ein neues Jahr) hinzufügen oder eine vorhandene Datei **ersetzen** oder **entfernen**.

![Buttons Datei hinzufügen und Menüoptionen Datei ersetzen und Datei entfernen](img/catalog_12_datamaintenance_managefiles.png)

Vergessen Sie nicht **die Metadatenbeschreibung auf dem neuesten Stand zu halten** damit sie immer mit den hochgeladenen Dateien übereinstimmt. **"Geändert am"** ist ein optionales, aber nützliches Eingabefeld für Distributionen. Sie können die Beschreibung für die Distribution über die Dreipunktmenü "Bearbeiten" bearbeiten.

![Menüoption „Bearbeiten“ für Distributionen](img/catalog_12_datamaintenance_editdistribution.png)

Sie können auch zu "Datensätze" gehen und "Bearbeiten" auswählen um das empfohlene Feld **"Zeitraum"** so zu ändern, dass es dem Zeitraum in den Daten für die neue Datei entspricht.

![Zeitraum für einen Datensatz bearbeiten](img/catalog_12_datamaintenance_timeperiod.png)

## Download

Es gibt mehrere Möglichkeiten, um **Daten** in EntryScape Catalog herunterzuladen. Sie können **einzelne Dateien**, die zu der Distribution gehören, unter "Dateien verwalten" herunterladen.

![Menüoption „Dateien verwalten”](img/catalog_12_datamaintenance_menumanagefiles.png)

![Menüoption „Datei herunterladen”](img/catalog_12_datamaintenance_downloadfile.png)

Sie können auch **Ihren gesamten Katalog** (als Datei) herunterladen, indem Sie mit der rechten Maustaste auf das Dreipunktmenü des Katalogs klicken und "Download" auswählen. Beachten Sie, dass der heruntergeladene Katalog nur **Metadaten** enthält und **nicht** die Dateien oder Visualisierungen, die zu den Distributionen gehören.

![Menüoption Katalog herunterladen](img/catalog_12_datamaintenance_downloadcatalog.png)

Wählen Sie anschließend das Format für Ihre Datei aus: RDF/XML, Turtle, N-Triples oder JSON-LD. Klicken Sie dann auf "Download".

![Dialogfeld zur Auswahl des Download-Formats](img/catalog_12_datamaintenance_downloadpopup.png)

Klicken Sie auf "Schließen", um das Fenster zu schließen.