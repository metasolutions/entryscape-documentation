## Überblick

In EntryScape können Sie **Statistiken zur Nutzung** Ihrer veröffentlichten offenen Daten anzeigen. Dies kann für Ihre Berichterstattung und Entwicklungsarbeit nützlich sein.

Auf der Übersichtsseite Ihres Katalogs finden Sie auf der rechten Seite eine einfache **Vorschau** der Nutzungsaktivitäten. Sie zeigt die Anzahl der Dateidownloads und API-Aufrufe der letzten 7 Tage an.

![Vorschau der Nutzungsaktivitäten](img/catalog_11_statistics_overview.png)

**Detailliertere Statistiken** finden Sie unter **"Statistik"** unten im linken Menü. (Je nach Anzahl der berechneten Datensätze kann das Laden der Statistikseite einige Sekunden dauern.)

![Hauptmenü „Statistiken”](img/catalog_11_statistics_mainmenu.png)

Die detaillierte Statistik zeigt die **Anzahl der Downloads von APIs und Dateien** für verschiedene Datensätze in einem ausgewählten Zeitraum an.

![Ansicht der detaillierten Statistik](img/catalog_11_statistics_detailedview.png)

Sie können die **Statistiken auf verschiedene Weise filtern.** Sie können im Suchfeld oben nach dem Titel Ihres Datensatzes suchen. Sie können auch auswählen, welchen **Zeitraum** Sie anzeigen möchten (z.B. gestern, letzter Monat, dieses Jahr usw.) und welche **Arten von Daten** Sie anzeigen möchten (Dateien, Dokumente, API-Aufrufe).

![Filteroptionen für detaillierte Statistiken](img/catalog_11_statistics_filter.png)

Unterhalb des Diagramms wird **jedes Objekt in einer einzelnen Zeile** angezeigt. Wenn Sie auf eine Zeile klicken, wird nur dieses Objekt im Diagramm angezeigt. Klicken Sie erneut auf die Zeile, um wieder alle Objekte anzuzeigen.

Sie können die Statistik auch als CSV-Datei **herunterladen** oder ausdrucken, indem Sie die Schaltflächen oben rechts verwenden.

![Buttons zum Herunterladen von CSV-Dateien und Drucken](img/catalog_11_statistics_download.png)