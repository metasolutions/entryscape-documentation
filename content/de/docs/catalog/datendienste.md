## Überblick

Ein **Datendienst** ist eine Möglichkeit eine oder mehrere **APIs** zu beschreiben und zu veröffentlichen. 

## Datendienst erstellen

Um einen Datendienst zu erstellen, klicken Sie im Menü auf "**Datendienste**" und dann auf "Erstellen".

![Hauptmenü Datendienst](img/catalog_7_dataservices_menu.png)

Geben Sie dann einen **Titel** und eine **Endpunkt-URL** für den Datendienst ein. Geben Sie im Feld "Endpunkt-URL" die öffentliche Zugriffsadresse für Ihren Datendienst ein. Vermeiden Sie eine tiefere Verknüpfung mit Schnittstellen in der API.

![Dialogfeld zum Erstellen eines Datendienstes](img/catalog_7_dataservices_createdataservice.png)

Wenn Sie die **empfohlenen** und **optionalen** Felder aktivieren, können Sie Ihren Datendienst ausführlicher beschreiben. Klicken Sie dann auf "Erstellen". Ausführlichere Beschreibungen mit zusätzlichen ausgefüllten Metadatenfeldern helfen anderen Benutzern dabei Ihren Datendienst leichter zu finden, zu verstehen und zu nutzen.

Es ist möglich, die Beschreibung eines Datendienstes als Entwurf zu speichern, auch wenn einige Pflichtangaben fehlen. Der Datendienst wird dann in der Liste mit einem gelben Symbol gekennzeichnet, das anzeigt, dass Pflichtangaben fehlen. Es ist nicht möglich den Datendienst zu veröffentlichen bevor nicht alle Pflichtangaben eingegeben wurden.

![Gelbes Entwurfssymbol zeigt an, dass Pflichtangaben fehlen](img/catalog_drafticon.png)

## Datendienst bearbeiten oder entfernen

Sie können die Beschreibung Ihres Datendienstes **bearbeiten** oder den Datendienst **entfernen**, indem Sie auf der Übersichtsseite für den Datendienst auf die Schaltflächen "Bearbeiten" bzw. "Entfernen" klicken. Dort können Sie auch sehen, welche **Datensätze** mit dem Datendienst verknüpft sind.

![Menüoption Bearbeiten](img/catalog_7_dataservices_edit.png)

## Detaillierte Informationen anzeigen

Wenn Sie detailliertere Informationen zu Ihrem Datendienst anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Informationen zu allen Objekten die auf Ihren Datendienst verweisen oder von ihm ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).

## Datendienst veröffentlichen

Wenn Sie die Beschreibung Ihres Datendienstes abgeschlossen haben, können Sie ihn **veröffentlichen**, indem Sie auf der Seite mit der Liste der Datendienste auf die Schaltfläche "Veröffentlichen" klicken, sodass diese grün wird.

![Schaltfläche „Veröffentlichen”](img/catalog_7_dataservices_publishdataservice.png)