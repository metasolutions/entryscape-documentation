## Vorschläge für Datensätze

Es gibt mehrere Möglichkeiten, Datensätze in EntryScape Catalog zu erstellen. Sie können beispielsweise mit der Erstellung eines **Vorschlags** für einen Datensatz beginnen, der einen Entwurf eines in der Erstellung befindlichen Datensatzes darstellt. Es wird empfohlen, mit Vorschlägen zu arbeiten, wenn Sie noch nicht über alle Informationen verfügen, die Sie zur Beschreibung des Datensatzes benötigen, oder wenn Details nachgeschlagen werden müssen, bevor der Datensatz veröffentlicht werden kann.

Zunächst sollten Sie überprüfen, ob der Datensatz oder Vorschlag, den Sie erstellen möchten, bereits existiert oder von jemand anderem initiiert wurde. Verwenden Sie die **Suchfunktion**, die Sie unter "Vorschläge" bzw. "Datensätze" finden. Geben Sie mindestens 3 Zeichen ein, um ein Ergebnis zu erhalten, das dem Titel des Vorschlags oder Datensatzes entspricht.

![Suchfeld für Vorschläge und Datensätze](img/catalog_2_suggestions_searchfield.png)

## Vorschlag erstellen

Unter "Vorschläge" finden Sie eine Liste der bereits erstellten Vorschläge für Datensätze, auf die Sie Zugriff haben. Auf der linken Seite befindet sich eine Statusleiste, die die Anzahl der erfüllten Anforderungen für den Vorschlag anzeigt, um in einen veröffentlichbaren Datensatz hochgestuft zu werden. Weitere Informationen dazu, wie die Checkliste für Anforderungen ausgefüllt werden kann, finden Sie im Abschnitt "Erfüllung der Anforderungen" weiter unten.

![Übersicht der Vorschläge mit Statusbasis](img/catalog_2_suggestions_overview.png)

Neue Vorschläge können auf zwei verschiedene Arten erstellt werden: Sie können entweder einen neuen Vorschlag von Grund auf erstellen oder einen Vorschlag mithilfe einer vorhandenen Vorlage erstellen.

Klicken Sie auf "Erstellen", um einen neuen Vorschlag für einen Datensatz zu erstellen, oder klicken Sie auf "Aus Vorlage erstellen", um von einem vorhandenen Konzept auszugehen.

![Buttons „Vorlage erstellen” und „Erstellen”](img/catalog_2_suggestions_createbuttons.png)

Wenn Sie einen Vorschlag von Grund auf neu erstellen, wird die folgende Ansicht angezeigt:

![Dialogfeld zum Erstellen eines neuen Vorschlags](img/catalog_2_suggestions_createnewsuggestion.png)

Hier beschreiben Sie Ihren Vorschlag für einen Datensatz. Das Pflichtfeld "Titel" ist dasselbe wie für den zukünftigen Datensatz. Es wird empfohlen, dass Sie ihm denselben Namen geben wie der enthaltenen Information oder einer Teilmenge der enthaltenen Information. Denken Sie daran, dass der Titel kurz und präzise sein sollte, damit er für andere leicht zu finden ist. Danach geben Sie das empfohlene Feld "Beschreibung" ein. Die Beschreibung sollte dem Benutzer deutlich machen, was der Datensatz enthält.

Die **URL der Anfrage** wird noch nicht häufig verwendet, kann aber in Zukunft eine wichtige Rolle bei offenen Daten spielen. Hier kann eine URL angegeben werden falls der Vorschlag in einem anderen System entstanden ist, z.B. ein Support-System.

Wenn Sie einen passenden Titel und eine passende Beschreibung eingegeben haben, klicken Sie unten auf "Erstellen".

Wenn Sie einen Vorschlag mithilfe einer vorhandenen **Vorlage** erstellen möchten, klicken Sie auf "Aus Vorlage erstellen", um eine Liste aller verfügbaren Vorlagen zur Auswahl zu erhalten. Sie können weitere **detaillierte Informationen** zur Vorlage anzeigen, indem Sie auf das Informationssymbol klicken. (Lesen Sie mehr darüber, wie der Dialog zu [detaillierten Informationen](information.md) funktioniert.)

![Informationssymbol](img/catalog_informationicon.png)

Wenn Sie eine geeignete Vorlage gefunden haben, klicken Sie auf "Erstellen", um einen neuen Vorschlag basierend auf der ausgewählten Vorlage zu erstellen.

![Dialog Vorschlag aus Vorlage erstellen](img/catalog_2_suggestions_createfromtemplate.png)

## Checkliste für die Erfüllung der Anforderungen

Um Ihren Vorschlag zu einem veröffentlichbaren Datensatz zu machen, sollte er idealerweise eine Reihe von Anforderungen für die Datenveröffentlichung erfüllen. EntryScape Catalog bietet eine Checkliste, die dem Benutzer dabei hilft, sicherzustellen, dass die empfohlenen Anforderungen für den vorgeschlagenen Datensatz erfüllt sind, bevor die Daten veröffentlicht werden, z.B. Urheber- und Eigentumsrechte. Um mit der Checkliste zu arbeiten, klicken Sie auf die Statusleiste unter "Checkliste".

![Checkliste Statusleiste](img/catalog_2_suggestions_checklist1.png)

Die drei wichtigsten Anforderungen in der Checkliste sind mit einem roten Ausrufezeichen versehen. Dies bedeutet, dass diese Punkte **dringend zu überprüfen sind**, bevor Sie mit der Erstellung und Veröffentlichung eines auf dem Vorschlag basierenden Datensatzes fortfahren. Die erste Anforderung besagt, dass der Datensatz **keine vertraulichen oder persönlichen Informationen** enthält. Die zweite Anforderung besagt, dass **Urheber- und Eigentumsrechte geklärt wurden**. Die dritte Anforderung besagt, dass der **Dateneigentümer innerhalb der Organisation identifiziert wurde**.

![Die Checkliste](img/catalog_2_suggestions_checklist2.png)

Es ist gut, auch zusätzliche Anforderungen zu erfüllen, z.B. ob es einen Wartungsplan für den Datensatz gibt oder nicht. Wenn Sie Ihre Antworten auf der Checkliste gespeichert haben, zeigt die Statusleiste in der Liste mit Vorschlägen an, wie viele der Anforderungen erfüllt sind. Wenn die drei wichtigsten Anforderungen erfüllt sind, wird die Statusleiste grün.

![Grüne Statusleiste für die Checkliste](img/catalog_2_suggestions_checklist3.png)

## Kommentare

Wenn Sie das Ausfüllen einiger Informationen zu Ihrem Vorschlag pausieren müssen, können Sie stattdessen einen Kommentar hinzufügen. Dieser kann Ihnen selbst oder Kollegen, die den vorgeschlagenen Datensatz weiter vorbereiten, als Gedächtnisstütze dienen. Klicken Sie auf das Kommentar-Symbol auf der rechten Seite, um Ihrem Vorschlag oder Datensatz einen Kommentar hinzuzufügen.

![Symbol für Kommentare](img/catalog_2_suggestions_comments.png)

Geben Sie einen Titel und eine Beschreibung für Ihren Kommentar ein.

Die Anzahl der Kommentare zu einem Vorschlag wird durch einen blauen Kreis auf dem Kommentar-Symbol in der Liste der Vorschläge angezeigt. Kommentare können einfach bearbeitet oder entfernt werden.

![Ein blauer Kreis zeigt die Anzahl der Kommentare zu einem Vorschlag an](img/catalog_2_suggestions_comments2.png)

## Erstellen Sie einen Datensatz aus einem Vorschlag

Wenn Ihr Vorschlag korrekt beschrieben ist, können Sie **einen Datensatz aus einem Vorschlag erstellen**, um ihn zu veröffentlichen. Klicken Sie auf den Vorschlag und dann auf die Lupe neben "Datensätze".

![Schaltfläche „Datensatz erstellen”](img/catalog_2_suggestions_createdataset.png)

Nun können Sie einen Datensatz aus Ihrem Vorschlag erstellen, indem Sie auf die Schaltfläche "Erstellen" klicken. Sie können auch nach einem bereits vorhandenen Datensatz suchen und diesen mit Ihrem Vorschlag verknüpfen.

![Dialog Datensatz suchen oder erstellen](img/catalog_2_suggestions_createorlinktodataset.png)

Wenn Sie basierend auf Ihrem Vorschlag einen neuen Datensatz erstellen, können Sie den Titel, die Beschreibung und den Herausgeber überprüfen. Sie können auch weitere empfohlene oder optionale Informationen zu Ihrem Datensatz eingeben. Wenn Sie auf "Erstellen" klicken, wird der neue Datensatz automatisch zu Ihrer Liste mit Datensätzen hinzugefügt. Ihr Vorschlag bleibt jedoch weiterhin in der Vorschlagsliste, da es möglich ist, mehrere verschiedene Datensätze auf der Grundlage desselben Vorschlags zu erstellen.

Auf der Übersichtsseite Ihres Vorschlags sehen Sie unten die Liste der verknüpften Datensätze.

![Liste der verknüpften Datensätze unter dem Vorschlag](img/catalog_2_suggestions_linkeddatasets.png)

Wenn Sie die Verknüpfung zwischen einem Datensatz und einem Vorschlag entfernen möchten, klicken Sie auf das Dreipunktmenü für den Datensatz und wählen Sie "Trennen".

![Datensatz von Vorschlag trennen](img/catalog_2_suggestions_disconnectdataset.png)

## Vorschläge archivieren oder entfernen

Vorschläge, an denen nicht mehr aktiv gearbeitet wird, können entweder **archiviert** oder **entfernt** werden. Sie können einen Vorschlag archivieren, indem Sie auf die Schaltfläche "Archivieren" klicken. Durch Klicken auf "Entfernen" können Sie einen Vorschlag auch dauerhaft entfernen. Ein entfernter Vorschlag kann nicht wiederhergestellt werden.

![Buttons Vorschlag archivieren und entfernen](img/catalog_2_suggestions_archiveremovesuggestion.png)

Archivierte Vorschläge können Sie finden, indem Sie oben auf die Schaltfläche "Filter" klicken und "Alle" oder "Archivierte" Vorschläge anzeigen.

![Filter-Schaltfläche](img/catalog_2_suggestions_filter.png)

Sie können einen Vorschlag ganz einfach aus dem Archiv entfernen, indem Sie auf die Schaltfläche "Wiederherstellen" klicken.

![Schaltfläche "Wiederherstellen"](img/catalog_2_suggestions_unarchive.png)