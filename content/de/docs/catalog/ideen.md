## Überblick

Mit dieser Funktion können Organisationen **Ideen zur Nutzung veröffentlichter offener Daten** veröffentlichen, die auf einem oder mehreren Datensätzen in einem Katalog basieren. Diese Funktion kann bei der Planung von Hackathons oder bei der Förderung einer bestimmten Art der Nutzung offener Daten hilfreich sein. Der Unterschied zwischen dieser Funktion und [Showcases](showcases.md) besteht darin, dass Ideen nur Ideen für Dinge sind, die noch nicht existieren. 

## Idee erstellen

Um eine Idee zu erstellen, gehen Sie zu "Ideen" und klicken Sie auf "Erstellen".

![Hauptmenü Ideen](img/catalog_8_ideas_menu.png)

**Titel** und **Beschreibung** sind Pflichtfelder, die ausgefüllt werden müssen. Es ist jedoch auch sinnvoll, anzugeben, auf welchem **Datensatz** die Idee basieren soll.

![Dialogfeld zum Erstellen einer Idee](img/catalog_8_ideas_createidea.png)

## Idee bearbeiten oder entfernen

Sie können die Idee später genauer beschreiben, indem Sie zur Übersichtsseite der Idee gehen und auf "Bearbeiten" klicken. Dort können Sie die Idee auch mit der Schaltfläche "Entfernen" löschen.

![Buttons Bearbeiten und Entfernen für Ideen](img/catalog_8_ideas_editremove.png)

## Idee veröffentlichen

Alle Ideen sind standardmäßig öffentlich und folgen dem Veröffentlichungsstatus des Kataloges sobald sie erstellt wurden. Sie können den Veröffentlichungstatus über die Schaltfläche "Veröffentlichung" ändern.

![Schaltfläche „Veröffentlichung” für Ideen](img/catalog_8_ideas_app.png)

## Detaillierte Informationen anzeigen

Wenn Sie detailliertere Informationen zu Ihrer Idee anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Information zu allen Objekten die auf Ihre Idee verweisen oder von ihr ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).