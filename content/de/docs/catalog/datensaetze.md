## Nach Datensatz suchen
<a id="search-for-dataset"></a>

Bevor Sie einen neuen Datensatz erstellen sollten Sie zunächst prüfen, ob dieser Datensatz bereits vorhanden ist. Sie können im Katalog nach vorhandenen Datensätzen **suchen**, indem Sie mindestens drei Zeichen in das Suchfeld eingeben.

![Suchfeld für Datensätze](img/catalog_3_datasets_search.png)

Wenn Sie auf einen Datensatz klicken, gelangen Sie auf die Übersichtsseite für Datensätze. Dort können Sie den Datensatz bearbeiten, indem Sie auf die Schaltfläche **Bearbeiten** klicken.

![Schaltfläche "Bearbeiten" für einen Datensatz](img/catalog_3_datasets_editbutton.png)

Wenn der Datensatz noch nicht existiert können Sie einen neuen Datensatz erstellen.

## Datensatz aus Vorlage erstellen
<a id="create-dataset-from-template"></a>

Es gibt zwei verschiedene Möglichkeiten einen Datensatz zu erstellen: entweder Sie **erstellen ihn anhand einer Vorlage** die bereits vorhanden ist, oder Sie **erstellen ihn von Grund auf neu**.

![Menüpunkt Datensatz aus Vorlage erstellen](img/catalog_3_datasets_createfromtemplate.png)

Um einen Datensatz aus einer Vorlage zu erstellen, klicken Sie auf die Schaltfläche **"Erstellen"**, wählen anschließend **"Datensatz aus Vorlage"** und suchen sich einen geeigneten Katalog und ggf. eine passende Kategorie (zum Beispiel „Umwelt“) aus, in der sich eine brauchbare Vorlage für den zu erstellenden Datensatz befinden könnte.

![Dialogfeld "Vorlage erstellen"](img/catalog_3_datasets_createfromtemplate1.png)

Um eine Vorschau der Datensatzbeschreibung zu erhalten, klicken Sie auf das runde Informationssymbol. Wenn Sie die ausgewählte Vorlage verwenden möchten, klicken Sie auf "Erstellen".

![Übersicht der Datensatzvorlagen](img/catalog_3_datasets_createfromtemplate2.png)

Sie erhalten eine Seite mit vorab eingegebenen Informationen aus der Vorlage, die Sie bearbeiten können, um sie an Ihren Datensatz anzupassen.

![Beispiel für vorab eingegebene Vorlageninformationen](img/catalog_3_datasets_createfromtemplate3.png)

Durch **Aktivieren der empfohlenen und optionalen Felder** können Sie Ihren Datensatz detaillierter beschreiben. Weitere Informationen zu den einzelnen Eingabefeldern finden Sie unter [Beschreibung eines Datensatzes](#einen-datensatz-beschreiben).

![Buttons zum Aktivieren der empfohlenen und optionalen Eingabefelder](img/catalog_3_datasets_man_rec_opt.png)

Sie können nur einen Datensatz aus einer Vorlage erstellen. Wenn Sie jedoch mehrere ähnliche Datensätze erstellen möchten, lesen Sie den Abschnitt [Datensatz kopieren](#datensatz-kopieren).

## Einen Datensatz von Grund auf neu erstellen
<a id="create-a-dataset-from-scratch"></a>

Wenn Sie **einen Datensatz von Grund auf neu erstellen** möchten, anstatt eine Vorlage zu verwenden, klicken Sie auf die Schaltfläche **"Erstellen"** und wählen Sie dann **"Datensatz"**.

![Menüpunkt Datensatz erstellen](img/catalog_3_datasets_createdataset.png)

Wählen Sie zunächst **ein Profil** aus das zu Ihrem Datensatz passt bevor Sie Informationen in die Eingabefelder eingeben. Wenn Sie keine Profilschaltfläche sehen ist das Profil bereits für Sie ausgewählt. Das von Ihnen ausgewählte Profil bestimmt, welche Eingabefelder Ihr Datensatz enthält. Das am häufigsten verwendete Profil ist in der Regel der voreingestellte Standard.

![Profil-Schaltfläche](img/catalog_3_datasets_profilebutton.png)

Die Organisation kann Daten gemäß verschiedener Anwendungsprofile veröffentlichen. Für PSI-Daten kann dies DCAT-AP und für INSPIRE eine Kombination aus GeoDCAT-AP und DCAT-AP sein. Wenn Sie mit Geodaten arbeiten, sollten Sie das INSPIRE-Profil verwenden.

## Einen Datensatz beschreiben
<a id="describing-a-dataset"></a>

Wenn Sie ein geeignetes Profil ausgewählt haben, können Sie mit der Beschreibung Ihres Datensatzes beginnen. Alle **obligatorischen** und **empfohlenen Eingabefelder** werden standardmäßig angezeigt. Sie können die empfohlenen und optionalen Eingabefelder auf der Bearbeitungsseite mithilfe der Schieberegler oben **anzeigen** oder **ausblenden**. Eingabefelder die bereits Daten enthalten werden immer angezeigt.

Auf der rechten Seite sehen Sie Schnellzugriffslinks zu allen Eingabefeldern. Wenn Sie auf den Titel eines Eingabefelds klicken, wird ein Tooltip mit einer kurzen Information darüber angezeigt, welche Informationen als Eingabe erwartet werden.

Die drei Felder "Titel", "Beschreibung" und "Herausgeber" sind Pflichtfelder für einen Datensatz. Denken Sie daran, dass Ihre Organisation möglicherweise verlangt, dass Sie Informationen in Pflichtfeldern in **mehr als einer Sprache** eingeben. Um einen Titel in einer anderen Sprache hinzuzufügen, klicken Sie auf **"+ Titel"**. Es wird eine neue Zeile mit Eingabefeldern angezeigt, in der Sie andere Sprachen auswählen können. Wenn Sie stattdessen eine Zeile entfernen möchten, klicken Sie auf das Minuszeichen auf der rechten Seite.

![Felder für die Eingabe des Titels](img/catalog_3_datasets_title.png)

Es ist möglich, die Beschreibung eines Datensatzes als Entwurf zu speichern, auch wenn einige Pflichtangaben fehlen. Der Datensatz wird dann in der Liste mit einem **gelben Entwurfssymbol** gekennzeichnet, das anzeigt, dass Pflichtangaben fehlen. Es ist nicht möglich, den Datensatz zu veröffentlichen, bevor alle Pflichtangaben eingegeben wurden.

![Gelbes Entwurfssymbol zeigt an, dass Pflichtangaben fehlen](img/catalog_drafticon.png)

Hinweis! Wenn ein **externer** Teil den Datensatz für eine Organisation bereitstellt, sollte **diese Organisation** als Herausgeber für den Datensatz eingetragen werden. Der Grund dafür ist, dass die ursprüngliche Organisation nur Regeln dafür festlegen kann, wie der externe Bereitsteller mit den Daten umgehen soll, aber in Wirklichkeit keine Kontrolle über die Daten ausüben kann.

![Das Feld "Publisher"](img/catalog_3_datasets_publisher.png)

Für weitere Informationen zu den empfohlenen und optionalen Feldern wenden Sie sich an die Spezifikation der von Ihnen verwendeten DCAT-AP-Variante.

## Datensatzserien

DCAT-AP 3.0 führt **Datensatzserien** ein, die auch in EntryScape 3.14 implementiert sind. Datensatzserien können verwendet werden, um zusammengehörende Datensätze zu gruppieren, beispielsweise in Zeitreihen. Wenn Sie in Ihrer Organisation jedoch noch DCAT-AP 2.x verwenden, werden Datensatzserien in Ihrer EntryScape-Instanz deaktiviert, bis Sie auf DCAT-AP 3.0 oder höher aktualisieren.

Um eine Datensatzserie zu erstellen, gehen Sie zu **"Datensätze"**, klicken Sie auf **"Erstellen"** und wählen Sie dann **"Datensatzserie"**. Geben Sie einen **Titel**, eine **Beschreibung**, einen **Herausgeber** sowie weitere Informationen ein und klicken Sie auf "Erstellen".

![Datensatzserie erstellen](img/catalog_3_datasets_createdatasetseries.png)

Sie sehen Ihre Datensatzserie in der Datensatzliste, gekennzeichnet mit einem Seriensymbol.

![Symbol für Datensatzserien](img/catalog_3_datasets_datasetseriesicon.png)

Das gleiche Symbol finden Sie in der Datensatzserienübersicht. Dort können Sie zu Ihrer Serie **Datensätze hinzufügen**, indem Sie auf die Lupe neben "Datensätze" klicken. Wenn Sie einen Datensatz entfernen möchten, können Sie auf das Dreipunktmenü für einen Datensatz klicken und ihn aus der Reihe **entfernen** oder auch vollständig aus dem Katalog entfernen.

![Datensatz zu einer Datensatzserie hinzufügen](img/catalog_3_datasets_datasetseries.png)

## Kontaktstelle erstellen
<a id="create-contact-point"></a>

Wenn Sie Ihren Datensatz beschreiben, können Sie auch einen neuen **Kontaktpunkt** erstellen, indem Sie auf die Lupe klicken.

![Das Feld Kontaktperson mit Lupe](img/catalog_3_datasets_contactpoint.png)

Wählen Sie zunächst aus, ob Sie eine Organisation (z.B. eine Abteilung/Einheit) oder eine Einzelperson als Kontaktpunkt eingeben möchten. Name und E-Mail-Adresse sind Pflichtfelder. Wenn Sie jedoch die **empfohlenen** Felder (oben) aktivieren können Sie weitere Informationen wie Telefonnummer und Adresse eingeben. Klicken Sie dann unten rechts auf die Schaltfläche **Erstellen**.

![Dialogfeld zum Erstellen eines neuen Kontaktpunkts](img/catalog_3_datasets_createcontactpoint.png)

Beachten Sie, dass Sie hier keine vorhandenen Kontaktstellen bearbeiten oder entfernen können. Gehen Sie stattdessen im Hauptmenü zu **Organisationen**. Dort haben Sie vollen Zugriff auf alle Kontaktstellen.

![Buttons Kontakt bearbeiten und entfernen](img/catalog_3_datasets_editremovecontacts.png)

## Datensatz kopieren
<a id="copy-dataset"></a>

Wenn Sie bereits einen beschriebenen Datensatz haben, den Sie kopieren möchten, gehen Sie zur Übersicht des Datensatzes und klicken Sie auf **"Kopieren"**.

![Schaltfläche "Kopieren" für einen Datensatz](img/catalog_3_datasets_copydataset.png)

Beachten Sie, dass Sie eine Kopie der Datensatzbeschreibung erstellen, nicht jedoch der zugehörigen Distributionen, Visualisierungen oder Kommentare des Originaldatensatzes. Sie werden außerdem gefragt, ob Sie den Vorgang trotzdem fortsetzen möchten.

![Bestätigung für das Kopieren eines Datensatzes](img/catalog_3_datasets_copydataset2.png)

## Tabellenansicht und Listenansicht
<a id="table-view-and-list-view"></a>

Standardmäßig werden die Daten in EntryScape Catalog in einer **Listenansicht** angezeigt. Wenn Sie viele Vorschläge oder Datensätze auf derselben Seite anzeigen und bearbeiten möchten, können Sie die **Tabellenansicht** verwenden. Wechseln Sie zur Tabellenansicht, indem Sie oben auf das **Symbol für die Tabellenansicht** klicken.

![Symbol für die Tabellenansicht](img/catalog_3_datasets_tableview1.png)

In der Tabellenansicht können Sie auch festlegen, wie viele Spalten gleichzeitig angezeigt werden sollen. Klicken Sie auf **"Spalten"**, um festzulegen, welche Spalten angezeigt oder ausgeblendet werden sollen.

![Spalten auswählen](img/catalog_3_datasets_columns.png)

Klicken Sie auf das Feld, das Sie bearbeiten möchten, und geben Sie einen Wert aus der Dropdown-Liste oder einen Freitext ein. Um das Bearbeitungsfeld zu schließen, klicken Sie auf das **obere X** oder außerhalb des Felds. Vergessen Sie nicht, unten auf die Schaltfläche **Speichern** zu klicken, nachdem Sie fertig sind.

![Tabellenansicht zum Bearbeiten](img/catalog_3_datasets_tableview2.png)

Um zur Listenansicht zurückzukehren, klicken Sie oben rechts auf das Symbol für die Listenansicht.

![Symbol für Listenansicht](img/catalog_3_datasets_listview.png)

## Detaillierte Informationen anzeigen
<a id="see-detailed-information"></a>

Wenn Sie detailliertere Informationen zu Ihrem Datensatz anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Information zu allen Objekten die auf Ihren Datensatz verweisen oder von ihm ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).

## Datensatz entfernen
<a id="remove-dataset"></a>

Um einen Datensatz zu entfernen, gehen Sie zur Übersicht für den Datensatz und klicken Sie auf die Schaltfläche **"Entfernen"**. Beachten Sie, dass Sie nur Datensätze entfernen können, die nicht veröffentlicht sind. Wenn Sie also einen veröffentlichten Datensatz entfernen möchten, müssen zuerst durch Klicken auf die grüne Veröffentlichungsschaltfläche die Veröffentlichung rückgängig machen.

![Datensatz entfernen](img/catalog_3_datasets_remove.png)