## Überblick

Eine Distribution ist eine **verfügbare Form** des Datensatzes. Ein Datensatz kann beispielsweise gleichzeitig über eine API und als CSV-Datei verfügbar sein, was bedeutet, dass der Datensatz zwei verschiedene Distributionen aufweist. Es gibt keine Begrenzung für die Anzahl der Distributionen pro Datensatz, aber es sollte **nicht mehr als eine Distribution mit demselben Inhalt und Dateiformat** für einen Datensatz geben. Eine Distribution kann jedoch aus **mehreren Dateien** im gleichen Format bestehen, was sich besonders für Zeitreihen eignet die kontinuierlich aktualisiert werden.

## Distribution erstellen

Um eine Distribution zu erstellen, gehen Sie zur Übersichtsseite des Datensatzes und klicken Sie auf die Schaltfläche "+" neben "Distributionen".

![Plus-Schaltfläche zum Erstellen einer Distribution](img/catalog_4_distributions_plusbutton.png)

Als Nächstes können Sie auswählen, ob Sie **einen Link** (Webadresse) eingeben oder **eine Datei hochladen** möchten.


## Datei hochladen

Um eine Datei **hochzuladen**, klicken Sie auf **"Datei"** und dann auf das Lupensymbol. Die maximale Dateigröße beträgt 24 MB. Wenn Ihre Datei größer ist, empfehlen wir, die Daten in mehrere Dateien aufzuteilen. Es kann Ausnahmen von dieser Größenbeschränkung geben, wenn Sie eine maßgeschneiderte EntryScape Catalog-Lösung haben.

![Dialogfeld zum Hochladen einer Datei](img/catalog_4_distributions_uploadfile.png)

Wenn Sie die empfohlenen und optionalen Felder aktivieren, können Sie die Distribution genauer beschreiben.

![Dialogfeld zum Eingeben weiterer Informationen](img/catalog_4_distributions_createdistribution_file.png)

Sie können Dateien, die zur Distribution gehören, über das Dreipunktemenü bearbeiten und verwalten (z.B. herunterladen, ersetzen und entfernen). Sie können auch eine API aus einer vorhandenen CSV-Datei erstellen (siehe unten).

![Menüoptionen für Distributionen](img/catalog_4_distributions_threepointmenu.png)

## Detaillierte Informationen anzeigen

Wenn Sie detailliertere Informationen zu Ihrer Distribution anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Informationen zu allen Objekten die auf Ihre Distribution verweisen oder von ihr ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).

## Dateien entfernen, ersetzen und aktualisieren

Um die zu einer Distribution gehörenden Dateien zu verwalten, klicken Sie unter **Distributionen** auf das Dreipunktmenü und wählen Sie **"Dateien verwalten"** aus.

![Menüoption "Dateien für eine Distribution verwalten"](img/catalog_12_datamaintenance_menumanagefiles.png)

Auf der Übersichtsseite der Distribution können Sie **eine neue Datei** (z.B. Daten für ein neues Jahr) hinzufügen oder eine vorhandene Datei **ersetzen** oder **entfernen**.

![Buttons Datei hinzufügen und Menüoptionen Datei ersetzen und Datei entfernen](img/catalog_12_datamaintenance_managefiles.png)

Vergessen Sie nicht **die Metadatenbeschreibung auf dem neuesten Stand zu halten** damit sie immer mit den hochgeladenen Dateien übereinstimmt. **"Geändert am"** ist ein optionales, aber nützliches, Eingabefeld für Distributionen. Sie können die Beschreibung für die Distribution über das Dreipunktmenü "Bearbeiten" bearbeiten.

![Menüoption "Bearbeiten" für Distributionen](img/catalog_12_datamaintenance_editdistribution.png)

Sie können auch zu "Datensätze" gehen und "Bearbeiten" auswählen um das empfohlene Feld **"Zeitraum"** so zu ändern, dass es dem Zeitraum der Daten in der neuen Datei entspricht.

![Zeitraum für einen Datensatz bearbeiten](img/catalog_12_datamaintenance_timeperiod.png)


## Erstellen einer API mithilfe von Tabellendaten (CSV)

Eine Distribution mit Tabellendaten (CSV-Dateien) kann zur automatischen Erstellung einer API verwendet werden.

Damit die API-Erstellung funktioniert, müssen einige Bedingungen erfüllt sein:

* Die erste Zeile der Tabelle sollte Titel für jede Spalte enthalten, da diese in der API als Variablenbezeichner verwendet werden. Die Spaltentitel werden beim Import getrimmt und in Kleinbuchstaben umgewandelt.
* Zeichenkettenwerte wie Spaltentitel oder Zellinhalte dürfen nur Unicode-Zeichen enthalten.
* Als Spaltentrennzeichen müssen Kommas (`,`) verwendet werden. CSV-Dateien, die Semikolons (`;`) als Trennzeichen verwenden, können zwar erkannt werden, es wird jedoch empfohlen, Kommas zu verwenden.
* Verwenden Sie Anführungszeichen des Schreibmaschinensatzes (`"`).
* Verwenden Sie den doppelten Backslash (`\\`) als Escape-Zeichen.
* Zeilenvorschub (`\n`) oder Wagenrücklauf gefolgt von Zeilenvorschub (`\r\n`) muss verwendet werden, um eine neue Zeile anzuzeigen.

Eine automatisch generierte API ist über dessen REST-Schnittstelle verfügbar, und über einen Webbrowser kann auf eine einfache Webschnittstelle zugegriffen werden. Die Webschnittstelle enthält Links zu einer ausführlicheren Swagger-basierten API-Dokumentation.

### API für eine Distribution aktivieren

Es ist von Vorteil die API für eine Dateidistribution zu aktivieren wenn dies möglich ist. Wenn Sie eine Datei als Distribution hochgeladen haben, können Sie sie für die Verwendung über die API aktivieren. Gehen Sie dazu zur Übersicht des Datensatzes, klicken Sie auf das Dreipunktmenü auf der rechten Seite der Distribution und dann auf **API aktivieren**.

![Menüoption API aktivieren](img/catalog_4_distributions_activateapi.png)

Wenn der Datensatz noch nicht veröffentlicht wurde, erscheint die Warnung, dass der Datensatz veröffentlicht wird, wenn Sie mit der Aktivierung der API-Erstellung beginnen. Wenn Sie damit einverstanden sind, klicken Sie auf "Ja".

![Bestätigung zur Aktivierung der API](img/catalog_4_distributions_confirmationactivateapi.png)

Anschließend beginnt die API-Generierung.

![Initialisierung der API](img/catalog_4_distributions_initializedapi.png)

Sobald die API generiert wurde, klicken Sie auf "Schließen".

![API wurde erstellt](img/catalog_4_distributions_generatedapi.png)

Ihre neue API ist nun als Distribution mit dem Namen "Automatisch generierte API" im JSON-Format vorhanden. Über das Dreipunktmenü können Sie die API-Informationen anzeigen, z.B. die URL für die API. Sie können die API auch bearbeiten, entfernen und – falls Sie Änderungen in der ursprünglichen Distribution (CSV-Datei) vorgenommen haben – aktualisieren, um die neuen Änderungen zu übernehmen.

![Dreipunktmenü für automatisch generierte API](img/catalog_4_distributions_autogeneratedapi.png)

## Link zu externen APIs oder Dateien

Wenn bereits eine **externe API** oder eine **externe Datei** zu dem in EntryScape beschriebenen Datensatz vorhanden ist, können Sie **als externe Distribution** eine Verknüpfung von Ihrem Datensatz zu der Datei oder API erstellen. Das externe System ist für die Aktualisierung seiner Dateien und APIs verantwortlich, im Gegensatz zu einer Distribution, die durch das Hochladen einer eigenen Datei in EntryScape erstellt wurde.

Um eine Verbindung zu einer API oder externen Datei herzustellen, klicken Sie auf die Plus-Schaltfläche neben "Distributionen".

![Plusbutton zum Erstellen einer Distribution](img/catalog_4_distributions_plusbutton_small.png)

Geben Sie dann die externe Webadresse ein, beginnend mit "http" oder "https" und endend mit beispielsweise ".json". Klicken Sie dann auf "Erstellen".

![Erstellen Sie eine Distribution über einen Link](img/catalog_4_distributions_createdistribution_link.png)

Dadurch wird eine externe Distribution mit dem temporären Namen "Zugangspunkt" erstellt.

![Menüoption für den neuen Zugangspunkt](img/catalog_4_distributions_accesspoint.png)

Im Dreipunktmenü können Sie "Bearbeiten" auswählen, um die Distribution genauer zu beschreiben und sie leichter von anderen Distributionen unterscheiden zu können. Geben Sie einen Titel, eine Beschreibung und vorzugsweise auch das Dateiformat, sowie die Verfügbarkeit und die Lizenz ein.

## Visualisierungen

Sie können Visualisierungen Ihrer Daten in EntryScape in Form von **Balkendiagrammen**, **Kreisdiagrammen**, **Liniendiagrammen**, **Karten** und **Tabellen** erstellen. Wenn Sie die Visualisierungen auch extern in EntryScape Blocks verwenden dann werden Ihre Visualisierungen nach Ihren Änderungen automatisch in Blocks übernommen.

![Beispiel einer Karte mit geeigneten Dächern für Solarzellen in der Altstadt von Stockholm](img/catalog_4_distributions_sunmap.png)

Kartenbeispiel: Altstadt in Stockholm. Dächer mit geeigneter Sonneneinstrahlung für die Installation von Photovoltaik.

Um eine Visualisierung erstellen zu können, muss eine Distribution mit einer hochgeladenen CSV-Datei oder einem Web Map Service (WMS) vorhanden sein. Klicken Sie auf die Schaltfläche "Plus" neben "Visualisierungen", um eine neue Visualisierung zu erstellen.

![Plus-Schaltfläche zum Erstellen einer Visualisierung](img/catalog_4_distributions_visualizationbutton.png)

Wählen Sie dann die Distribution aus, aus der die Visualisierung erstellt werden soll.

![Wählen Sie eine Distribution für die Visualisierung aus](img/catalog_4_distributions_createvisualization1.png)

Anschließend beschreiben Sie die Visualisierung, indem Sie die Pflichtfelder **Titel**, **Diagrammtyp** und **Diagrammachsen** ausfüllen. Um eine Kartenvisualisierung erstellen zu können, müssen in Ihrer Datendatei oder Ihrem WMS-Dienst geografische Koordinaten enthalten sein.

![Eingabefelder für die Visualisierung](img/catalog_4_distributions_createvisualization2.png)

Wenn Sie die Pflichtfelder ausgefüllt haben, wird unten im selben Fenster eine Vorschau der Visualisierung angezeigt.

![Vorschau einer Kartenvisualisierung](img/catalog_4_distributions_preview.png)

!!! info "**Hinweis!** Wenn Sie in Ihrer Visualisierung seltsame Zeichen sehen, liegt wahrscheinlich eine Inkompatibilität zwischen der Kodierung der hochgeladenen Datei und der in EntryScape ausgewählten Kodierung vor. Sie können versuchen, die Kodierung in EntryScape zu ändern, bis die Vorschau in Ordnung ist. Wenn es sich um eine große Datei handelt, sollten Sie mehrere Seiten und nicht nur die erste Seite überprüfen, um einzelne seltsame Zeichen zu bemerken."

Über das Dreipunktmenü können Sie Visualisierungen bearbeiten, in der Vorschau anzeigen und entfernen.

![Menüoption für Visualisierungen](img/catalog_4_distributions_menu_visualizations.png)