## Überblick

Mit Dokumenten können Sie Dateien **importieren oder hochladen** um sie mit z.B. mit einem Datensatz zu verknüpfen (z.B. eine Spezifikation oder eine Lizenzvereinbarung). Derzeit werden die folgenden drei Dokumenttypen unterstützt: **Dokumentation**, **Lizenz** und **Standard/Empfehlung/Spezifikation**. Zunächst wählen Sie aus, ob Sie **eine externe Spezifikation** importieren oder **ein Dokument** erstellen möchten, indem Sie eine Datei verknüpfen oder hochladen.

![Buttons Import und Erstellen](img/catalog_10_documents_createbuttons.png)

## Externe Spezifikation importieren

Wenn Sie auf "Importieren" klicken, wird eine Liste der über **dataportal.se** verfügbaren **Spezifikationen** angezeigt. Ihre EntryScape-Instanz weicht möglicherweise davon ab falls Sie für die Nutzung ausserhalb Schwedens konfiguriert ist. 

**Eine Spezifikation ist eine Anleitung oder eine Anweisung, wie ein bestimmter Datensatz aussehen sollte.** Wenn Sie die Spezifikation befolgen, ist Ihr Datensatz mit den Datensätzen anderer Anbieter kompatibel.

Klicken Sie auf "Importieren" bei der von Ihnen gewählten Spezifikation und dann auf "Schließen", um das Fenster zu schließen.

![Liste der verfügbaren Spezifikationen zum Importieren](img/catalog_10_documents_importspecification.png)

## Dokument erstellen

Um **ein neues Dokument zu erstellen/hinzuzufügen**, gehen Sie zu "Dokumente" und klicken Sie auf die Schaltfläche "Erstellen".

Wenn das Dokument im Internet verfügbar ist, geben Sie die Webadresse (URL) ein. Andernfalls klicken Sie auf "Datei" und laden die Datei hoch. **Dokumenttyp** und **Titel** sind Pflichtfelder, aber es wird empfohlen, auch eine **Beschreibung** des Dokuments einzugeben. Klicken Sie dann unten rechts auf "Erstellen".

![Eingabefelder zum Erstellen eines neuen Dokuments](img/catalog_10_documents_createdocument.png)

Das Dokument wurde nun erstellt, ist jedoch in EntryScape Catalog als einzelne Datei vorhanden, die noch nicht verknüpft ist. Im Folgenden erfahren Sie, wie Sie das Dokument mit einem vorhandenen Datensatz verknüpfen können.

## Ein Dokument mit einem Datensatz oder einer Distribution verknüpfen

In einer Datensatzbeschreibung gibt es zwei optionale Felder, in denen Sie **einen Link zu einem Dokument** einfügen können, das Sie hochgeladen oder über EntryScape verknüpft haben:

**"Entspricht"** bezieht sich auf eine Spezifikation oder Ähnliches, der der Datensatz entspricht, sowie auf die allgemeinere "Dokumentation", wo Sie auf eine Dokumentationsdatei verweisen, die zum Datensatz gehört. Um von einem Datensatz aus auf ein Dokument zu verweisen, wählen Sie unter "Datensätze" den zu verknüpfenden Datensatz aus und klicken auf "Bearbeiten".

Auf der rechten Seite können Sie die Suchfunktion oder das Inhaltsverzeichnis verwenden, um schneller zu den Feldern "Entspricht" und "Dokumentation" zu gelangen.

![Das Feld "Entspricht" für einen Datensatz](img/catalog_10_documents_editconformsto.png)

![Das Feld Dokumentation für einen Datensatz](img/catalog_10_documents_editdocumentation.png)

Sie können auch auf ein Dokument aus einer **Distribution** heraus verweisen. Gehen Sie zur Bearbeitungsansicht für eine Distribution und aktivieren Sie optionale Felder. Beachten Sie, dass das Dokument im Katalog vom Dokumenttyp "Standard/Empfehlung/Spezifikation" sein muss, um hier sichtbar zu sein.

![Distribution bearbeiten](img/catalog_10_documents_editdistribution.png)

## Detaillierte Informationen anzeigen

Wenn Sie detailliertere Informationen zu Ihrem Dokument anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Information zu allen Objekten die auf Ihr Dokument verweisen oder von ihm ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).

## Dokument bearbeiten, ersetzen oder entfernen

Mit den Schaltflächen **"Bearbeiten"**, **"Ersetzen"** oder **"Entfernen"** auf der Übersichtsseite können Sie die Informationen zu Ihrem Dokument bearbeiten, ersetzen oder entfernen.

![Schaltflächen Bearbeiten, Ersetzen und Entfernen](img/catalog_10_documents_editremove.png)

## Dokument herunterladen

Sie können ein Dokument herunterladen, indem Sie auf die Schaltfläche **"Herunterladen"** klicken.

![Schaltfläche Dokument herunterladen](img/catalog_10_documents_download.png)