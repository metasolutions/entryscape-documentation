## Überblick

Um einen **Herausgeber** für einen Katalog oder einen Datensatz auswählen zu können, müssen Sie in EntryScape eine oder mehrere Organisationen hinzufügen. Herausgeber und Kontakte werden in EntryScape als separate Objekte verwaltet. Sie können mehrere Herausgeber hinzufügen, was nützlich ist, wenn Ihre Organisation Datensätze für eine andere Organisation veröffentlicht.

## Herausgeber

Um einen Herausgeber hinzuzufügen, öffnen Sie Ihren Katalog, wählen Sie im linken Menü "Herausgeber" aus, und klicken Sie auf "Erstellen".

![Hauptmenü Herausgeber und Kontakte](img/catalog_6_pubcon_menupublishers.png)

Geben Sie dort den **Namen** der Herausgeberorganisation ein. Es wird außerdem empfohlen, die **Art der Organisation** auszuwählen und nach Möglichkeit eine Homepage und eine E-Mail-Adresse (optional) einzugeben.

![Dialog Herausgeber erstellen](img/catalog_6_pubcon_createpublisher.png)

Sie können jederzeit weitere Details hinzufügen. Klicken Sie dazu einfach auf der Übersichtsseite für den Herausgeber auf die Schaltfläche "Bearbeiten".

![Schaltfläche Herausgeber bearbeiten](img/catalog_6_pubcon_editpublisher.png)

Dort können Sie den Herausgeber auch entfernen, indem Sie auf die Schaltfläche "Entfernen" klicken.

## Kontakte

Ein **Kontakt** wird als Funktionsadresse oder als individueller Kontakt für einen Datensatz angegeben. Er ist für die Verwaltung des Datensatzes verantwortlich und **dafür zuständig, die Metadaten auf dem neuesten Stand und korrekt zu halten**. Verwenden Sie das Suchfeld, um den Kontakt zu finden, den Sie für Ihren Datensatz angeben möchten, oder erstellen Sie einen neuen Kontakt unter Kontakte im linken Menü, indem Sie auf "Erstellen" klicken.

![Hauptmenü Kontakte](img/catalog_6_pubcon_menucontacts.png)

Wählen Sie zunächst aus, ob es sich bei dem Kontakt um eine **Einzelperson** oder eine **Organisation** handelt. Geben Sie anschließend den **Namen** und die **E-Mail-Adresse** ein. Wenn Sie möchten, können Sie auch die empfohlenen Felder aktivieren und Telefonnummer und Adresse eingeben.

![Dialog zum Erstellen eines neuen Kontakts](img/catalog_6_pubcon_createcontact.png)

Wenn Sie zur Übersicht des Kontakts gehen, können Sie Beschreibungen bearbeiten oder den Kontakt entfernen, indem Sie die Schaltflächen auf der rechten Seite verwenden.

![Schaltflächen „Kontakt bearbeiten” und „Kontakt entfernen”](img/catalog_6_pubcon_editcontact.png)

## Detaillierte Informationen anzeigen

Wenn Sie detailliertere Informationen zu Ihrer Organisation anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Informationen zu allen Objekten die auf Ihre Organisation verweisen oder von ihr ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).