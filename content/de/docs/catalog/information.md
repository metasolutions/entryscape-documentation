Wenn Sie auf das **Informationssymbol** klicken, wird ein Dialog mit ausführlichen Informationen für einen Datensatz, einen Kontakt, eine Distribution oder ein anderes Objekt Ihrer Wahl geöffnet.

![Informationssymbol in der Liste](img/catalog_13_detailedinformation_icon1.png)

Sie finden das Informationssymbol sowohl in der Objektliste als auch auf der Übersichtsseite eines einzelnen Objekts.

![Informationssymbol in der Übersicht](img/catalog_13_detailedinformation_icon2.png)

Im angezeigten Dialogfenster werden drei Registerkarten angezeigt: **"Information"**, **"Über"** und **"Referenzen"**.

![information dialog, three tabs](img/catalog_13_detailedinformation_tabs.png)

## Information

Auf der ersten Registerkarte **"Information"** finden Sie die Metadaten zum ausgewählten Objekt, darunter auch alle anderen Objekte, auf die **Ihr Objekt verweist**, wie zum Beispiel Herausgeber und Kontakt.

![Informationsdialog, Registerkarte Information](img/catalog_13_detailedinformation_information.png)

## Über

Auf der zweiten Registerkarte **"Über"**, finden Sie den **Identifikator** zu Ihrem Objekt. So können Sie diesen schnell finden und **kopieren**, wenn Sie ihn an anderer Stelle benötigen.

![Informationsdialog, Registerkarte Über](img/catalog_13_detailedinformation_about.png)

## Referenzen

Auf der dritten Registerkarte **"Referenzen"** finden Sie alle Objekte, die **auf Ihr Objekt verweisen**.

![Informationsdialog, Registerkarte Referenzen](img/catalog_13_detailedinformation_references.png)

## Sprachen

In der oberen rechten Ecke befinden sich zwei **Sprachsymbole**, mit denen Sie die Informationen in den drei Registerkarten in **verschiedenen Sprachen** anzeigen können.

![Informationsdialog, Sprachen](img/catalog_13_detailedinformation_languageicons.png)

Wenn Sie auf das **Globussymbol** klicken, werden Ihnen alle Metadaten zu Ihrem Objekt in **allen eingegebenen Sprachen gleichzeitig** angezeigt. Um alle Sprachen zu deaktivieren und zur ursprünglichen Sprache zurückzukehren, klicken Sie noch einmal auf das Globussymbol.

![Informationsdialog, Liste der Sprachen](img/catalog_13_detailedinformation_alllanguages.png)

Sie können Ihre Metadaten auch **in jeweils einer Sprache** anzeigen, indem Sie auf **das jeweilige Sprachsymbol** klicken und eine Sprache aus einer Liste auswählen.

![Informationsdialog, Liste der Sprachen](img/catalog_13_detailedinformation_languagelist.png)