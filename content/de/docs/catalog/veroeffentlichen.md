## Veröffentlichung von Katalogen und Datensätzen

Alle neu erstellten Kataloge sind standardmäßig privat, sodass nur ausdrücklich zugewiesene Benutzer auf den Katalog zugreifen und ihn verwalten können. Wenn der Status "Öffentlich" grün angezeigt wird, bedeutet dies, dass die Metadaten des Katalogs öffentlich lesbar sind und von Open Data-Portalen erfasst werden können.
Über die **Schaltfläche "Veröffentlichung"** können Sie Ihren Katalog **veröffentlichen** bzw. die Veröffentlichung rückgängig machen.

![Veröffentlichungsschaltfläche für einen Katalog](img/catalog_5_publishing_publishcatalog.png)

Um in einem veröffentlichten Katalog etwas sehen zu können, muss dieser einen **Datensatz** mit **öffentlichem** Status enthalten oder eine **externe Datenerfassung** aktiviert haben. Sie können den Veröffentlichungsstatus verwalten und externe Datenerfassungen auf Datensatzebene aktivieren/deaktivieren. Klicken Sie auf die Status-Schaltfläche für den Datensatz, um den Veröffentlichungsstatus zu ändern.

![Schaltfläche „Veröffentlichen” für Datensatz](img/catalog_5_publishing_publishdataset.png)

## Revisionen

Einige Versionen von EntryScape verfügen über einen automatischen **Revisionsverlauf** von Katalogen, Datensätzen, Distributionen usw. Sie können auf den Revisionsverlauf im Dreipunktmenü für Kataloge und Distributionen zugreifen.

![Menüoption „Revisions“ für Kataloge](img/catalog_5_publishing_revisionscatalog.png)

Für Datensätze, Datendienste, Vorschläge und andere Objekte befindet sich auf der Übersichtsseite eine Schaltfläche "Revisionen".

![Schaltfläche „Revisionen” für Datensätze](img/catalog_5_publishing_revisionsdataset.png)

Wenn Sie Gruppenmanager oder Administrator sind, können Sie über den Revisionsverlauf eine frühere Version der Metadatenbeschreibung für das ausgewählte Objekt wiederherstellen.

Wenn beispielsweise jemand einen Datensatz in einem Katalog hinzugefügt oder entfernt hat, sollte dies im Revisionsverlauf mit Datum, Uhrzeit und Name des Benutzers, der die Änderung vorgenommen hat, angezeigt werden. Normale Mitglieder können die verschiedenen Versionen ebenfalls anzeigen, aber sie können keine älteren Versionen wiederherstellen.

![Revisionsverlauf](img/catalog_5_publishing_revisionhistory.png)

## Einbetten

Wenn Sie einen oder mehrere veröffentlichte Datensätze in einem Katalog haben, können Sie diese **auf einer Webseite** einbetten. Sie können eine Liste mit veröffentlichten Datensätzen auf externen Webseiten anzeigen, indem Sie einen JavaScript-Code einbetten, der auf einen bestimmten Katalog verweist.

Kehren Sie zur Übersicht Ihrer Kataloge zurück. Klicken Sie auf das Dreipunktmenü für den ausgewählten Katalog und wählen Sie "Einbetten" aus.

![Menüoption Einbetten](img/catalog_5_publishing_menu_embed.png)

Als Nächstes wird ein Fenster mit einem Codeausschnitt und der Möglichkeit angezeigt, um das Themenformat auszuwählen und festzulegen, ob die Links zu Ihrem Katalog in einem neuen Fenster geöffnet werden sollen oder nicht. Mit dem Themenformat können Sie auswählen, durch welche Icons Ihre Datensätze symbolisiert werden sollen.

![Dialogfeld mit Code-Ausschnitt und Themenformat](img/catalog_5_publishing_embedding1.png)

Wenn Sie auf die Schaltfläche „Vorschau“ klicken, können Sie sehen, wie Ihr Katalog auf Ihrer Webseite angezeigt wird, und die Symbole vergleichen, die am besten zu Ihren Daten passen. Der JavaScript-Code enthält ansonsten nur minimale Formatierungsanweisungen, sodass Tabellen und Schriftarten die visuellen Eigenschaften der umgebenden externen Webseite übernehmen können.

![Vorschau der ausgewählten Symbole im Themenformat](img/catalog_5_publishing_embedding2.png)

Klicken Sie auf "Schließen". Wählen Sie den JavaScript-Code aus und kopieren Sie ihn in die Zwischenablage. Fügen Sie den Code dann in den Editor Ihrer Homepage ein.

![Dialogfeld mit dem zu kopierenden JavaScript-Code](img/catalog_5_publishing_embedding3.png)

Sie können einen Katalog auch in Ihr Intranet einbetten, was praktisch sein kann, wenn Daten geteilt, aber nicht als offene Daten veröffentlicht werden sollen. Folgen Sie einfach den oben beschriebenen Schritten.

!!! Info "Es gibt noch fortgeschrittenere Möglichkeiten, Webseiten mit Daten aus EntryScape zu erstellen, indem Sie EntryScape Blocks verwenden. Wenden Sie sich an den EntryScape-Support, um weitere Informationen zu Blöcken zu erhalten, oder lesen Sie mehr unter [entryscape.com](https://entryscape.com)"