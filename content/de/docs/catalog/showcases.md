## Überblick

Mit **Showcases** können Sie **konkrete Beispiele** (wie eine Webseite oder eine mobile App) zeigen, die auf der Grundlage eines oder mehrerer veröffentlichter Datensätze in einem Katalog erstellt wurden. Es kann nützlich sein, zu zeigen, wie von einer Organisation veröffentlichte offene Daten von anderen Akteuren genutzt wurden. Im Gegensatz zu Ideen handelt es sich bei einem Showcase um etwas, das bereits existiert und angezeigt bzw. präsentiert werden kann.

![Übersicht der Showcases](img/catalog_9_showcases_overview.png)

## Showcase erstellen

Um einen Showcase zu erstellen, gehen Sie zu Showcases und klicken Sie auf "Erstellen". Geben Sie dann einen **Titel** und eine **Beschreibung** ein (Pflichtfelder) und geben Sie an, auf welchen **Datensätzen** der Showcase basiert.

![Dialogfeld zum Erstellen eines Showcases](img/catalog_9_showcases_createshowcase.png)

## Showcase bearbeiten oder entfernen

Wenn Sie den Showcase später entfernen oder die Beschreibung bearbeiten möchten, verwenden Sie die Schaltflächen "Bearbeiten" oder "Entfernen" auf der Übersichtsseite für Showcases.

![Schaltflächen Bearbeiten und Entfernen für Showcases ](img/catalog_9_showcases_editremove.png)

## Showcase veröffentlichen

Alle Showcases sind standardmäßig öffentlich, sobald sie erstellt wurden. Sie können sie mithilfe der Schaltfläche "Veröffentlichen" wieder entfernen.

![Schaltflächen zum Veröffentlichen von Showcases](img/catalog_9_showcases_publishbuttons.png)

## Detaillierte Informationen anzeigen

Wenn Sie detailliertere Informationen zu Ihrem Showcase anzeigen möchten, klicken Sie auf das Informationssymbol.
![Informationssymbol](img/catalog_informationicon.png)

Es wird ein Dialog angezeigt mit Metadaten, sowie Information zu allen Objekten die auf Ihren Showcase verweisen oder von ihm ausgehen. Lesen Sie mehr über [detaillierte Informationen](information.md).