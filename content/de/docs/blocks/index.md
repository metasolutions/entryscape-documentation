# Einführung in Blocks

Die Dokumentation für EntryScape Blocks ist derzeit nur auf Englisch verfügbar.

Gehen Sie zur [englischen Dokumentation](../../en/blocks/).
