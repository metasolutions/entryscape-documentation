# Willkommen zur EntryScape Dokumentation

!!! info "Diese Dokumentation dient hauptsächlich als Leitfaden für Endbenutzer."

EntryScape ist eine Informationsmanagement-Plattform, die auf dem Linked Data Technologie-Stack aufbaut. Es unterstützt den kompletten Lebenszyklus des Datenmanagements von Import bis Veröffentlichung.

EntryScape besteht aus verschiedenen Anwendungsmodulen, die je nach Aufgabenstellung verwendet werden:

### [Catalog](catalog/index.md)

Verwalten und veröffentlichen Sie Datenkataloge und Datensätze gemäß dem DCAT-AP-Standard. Bei Datensätzen kann es sich um öffentliche APIs, Datendumps, Dokumentensammlungen etc. handeln. Lernen Sie mehr über [Catalog](catalog/index.md).

### [Terms](terms/index.md)

Erstellen und verwalten Sie Terminologien und veröffentlichen Sie diese für die Wiederverwendung gemäß dem SKOS-Standard. Bauen Sie auf vorhandenen Terminologien auf oder erstellen Sie neue. Lernen Sie mehr über [Terms](terms/index.md).

### [Workbench](workbench/index.md)

Verwalten und veröffentlichen Sie benutzerdefinierte Objekttypen aufbauend auf dem Linked Data-Stack. Beschreiben und bereichern Sie Ihre Objekte mit Metadaten gemäß Ihrer eigenen Informationsmodelle, auch in Zusammenarbeit mit anderen Nutzern oder Nutzergruppen. Lernen Sie mehr über [Workbench](workbench/index.md).

### [Registry](registry/index.md)

Verwalten Sie Datenquellen und harvesten Sie inklusive Konvertierung zwischen Metadatenstandards. Die Validierung gemäß Ihrer Metadatenprofile ist automatisiert und der für die Datenquelle Verantwortliche erhält sofort Feedback, um Korrekturmaßnahmen vornehmen zu können. Lernen Sie mehr über [Registry](registry/index.md).

### Blocks

Betten Sie die in EntryScape verwalteten Informationen mit Hilfe von Code-Schnipseln in eine beliebige Webseite oder ein beliebiges CMS ein. Erstellen Sie beispielsweise ein Datenportal basierend auf einem Standard-CMS mit Metadaten von EntryScape. Lernen Sie mehr über [Blocks](blocks/index.md).

***

Lesen Sie mehr über die Möglichkeiten mit EntryScape und die Dienste von MetaSolutions unter [entryscape.com](https://entryscape.com).
