# Erste Schritte mit EntryScape

Sie benötigen ein Benutzerkonto, um EntryScape nutzen zu können. Abhängig von der Konfiguration Ihrer EntryScape-Instanz können Sie entweder selbst eine Anmeldung vornehmen oder ein Administrator muss ein Konto für Sie erstellen. Falls Ihre Organisation Single Sign-On in EntryScape verwendet, können Sie sich mit den üblichen Anmeldeinformationen Ihrer Organisation anmelden.

## Anmelden

Wenn Sie auf EntryScape zugreifen, ohne eingeloggt zu sein, werden Sie automatisch zum Anmeldebildschirm weitergeleitet, auf dem Sie Ihre Anmeldedaten eingeben müssen:

![Screenshot des Anmeldebildschirms](img/login.png)

Nach erfolgreicher Anmeldung werden Sie zur Startansicht weitergeleitet.

## Benutzer erstellen

Wenn Sie noch kein Konto haben, können Sie eines erstellen, indem Sie auf den Link "Konto erstellen" im unteren Bereich des Anmeldedialogs klicken. Dies zeigt Ihnen den folgenden Dialog:

![Screenshot der Benutzererstellung](img/signup.png)

Nachdem Sie die erforderlichen Informationen eingegeben haben müssen Sie Ihre E-Mail-Adresse bestätigen indem Sie die URL aufrufen die Ihnen per E-Mail gesendet wurde damit Ihr Konto erstellt wird.

## Kennwort zurücksetzen

Falls Sie Ihr Kennwort vergessen haben oder Ihr Administrator für Sie ein Konto ohne Kennwort eingerichtet hat müssen Sie Ihr Kennwort zurücksetzen. Sie können den Dialog zum Zurücksetzen des Kennworts öffnen, indem Sie auf den Link "Kennwort vergessen?" im Anmeldebildschirm klicken.

Sie werden aufgefordert direkt ein neues Kennwort anzugeben, es muss jedoch bestätigt werden indem Sie die URL aufrufen die an die E-Mail-Adresse Ihres Kontos gesendet wird.

## Wechseln zwischen Modulen und Ansichten

In der Startansicht können Sie zwischen den verschiedenen EntryScape-Modulen wählen, die für Ihre Organisation und Ihren Benutzer aktiviert sind:

![Screenshot der Startansicht](img/start_view.png)

Die Arbeit mit EntryScape baut auf modulspezifischen Ansichten auf, welche nach der Wahl eines Moduls und eines darin vorhandenen verwaltbaren Objekts (z.B. eine Datenkatalog, eine Terminologie oder ein generisches Projekt) sichtbar werden.

Die Ansichten eines Moduls werden in der Regel nach dem Typ der verwalteten Informationen unterteilt.