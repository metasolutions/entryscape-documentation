# Überblick

EntryScape Registry besteht aus mehreren sich ergänzenden Teilen:

![Startansicht der EntryScape Registry](img/start_view.png)

### Statusbericht

Der Statusbericht bietet einen Überblick über Organisationen die DCAT-AP-Metadaten und eine Webseite bereitstellen, die auf ihre PSI-Daten verweist.

### Suche

Eine Benutzerschnittstelle zum Durchsuchen aller Datenkataloge und Datensätze die von dieser Register-Instanz erfasst und verwaltet werden.

### Organisationen

Eine Schnittstelle zum Verwalten von Organisationen und deren Harvestingquellen.

### Toolkit

Ein DCAT-AP-Toolkit zum Validieren von Metadaten und anderen Werkzeugen.