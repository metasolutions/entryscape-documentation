# Datenkataloge durchsuchen

Die Suchansicht des Registers ermöglicht das Durchsuchen aller Metadaten die von dieser Register-Instanz verwaltet und geharvestet werden.

Im linken Bereich werden alle Organisationen (ein Katalog pro Organisation) aufgelistet und absteigend nach der Anzahl der erfolgreich geharvesteten Datensätze sortiert. Der rechte Bereich enthält eine Liste aller Datensätze.

![Screenshot der Suche](img/search.png)

Wenn eine Organisation ausgewählt ist, werden im rechten Fensterbereich nur die Datensätze dieser Organisation angezeigt.

![Screenshot der Organisationsliste](img/search_detail.png)