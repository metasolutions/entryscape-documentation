# Den Statusbericht verwenden

Der Statusbericht des Registers fungiert als Dashboard, um ein Gesamtbild davon zu erhalten, wie viele Organisationen offene Daten veröffentlichen und/oder eine Webseite über ihre offenen Daten bereitstellen (vgl. Schwedens Empfehlung zur Bereitstellung einer `/psidata`-Seite).

![Screenshot des Statusberichts auf registrera.oppnadata.se](img/status_report.png)