# Das Toolkit verwenden

EntryScape Registrys Toolkit Registrierung wurde speziell für DCAT-AP-Datenkataloge entwickelt.

!!! info "Das Toolkit arbeitet nur mit dem aktuell geladenen Datenkatalog."

## Laden eines Datenkatalogs

Im Reiter "Katalogquelle" können DCAT-AP-Metadaten geladen werden. Diese Metadaten können als Datei hochgeladen, als URL angegeben oder direkt als RDF/XML oder RDF/JSON eingefügt werden. Zu Demonstrations- und Testzwecken kann ein Beispielkatalog über die entsprechende Schaltfläche geladen werden.

![Screenshot des geladenen Beispiels](img/toolkit_example.png)

Der in diesem Reiter geladene Katalog wird dann für die anderen Werkzeuge des Toolkits verwendet.

## Validieren von Katalogmetadaten

Das Validierungswerkzeug des Registers arbeitet mit fünf der wichtigsten Entitätstypen von DCAT-AP: Katalog, Datensatz, Distribution, Herausgeber und Kontaktpunkt.

Das Werkzeug stellt einen Validierungsbericht bereit der eine detaillierte Analyse von DCAT-AP-Metadaten ermöglicht. Probleme werden pro Statement markiert. Zusätzlich zum Validierungsbericht auf oberster Ebene ist es möglich, auf den Validierungsbericht pro Entitätstyp zuzugreifen, indem Sie nach dem Expandieren des Bereichs auf den entsprechenden Link klicken.

![Screenshot der detaillierten Validierungsansicht](img/toolkit_validation.png)

## Mehrere Kataloge zusammenführen

Zusätzliche DCAT-AP-Kataloge können mit dem bereits geladenen Primärkatalog zusammengeführt werden. Die Datensätze und alle anderen Entitäten (außer dem Katalog selbst) der zusätzlich geladenen Kataloge werden in den Primärkatalog zusammengeführt. Auf die resultierenden Metadaten kann über den Reiter "Katalogquelle" zugegriffen und von dort auch heruntergeladen werden.

## Einen Katalog untersuchen

Mit dem Untersuchungstool können die Datensätze des geladenen Katalogs mit einer Liste von Datensätzen durchsucht werden. Durch Klicken auf einen Datensatz werden die Metadaten des Datensatzes in einem Seitendialog angezeigt. Ausgehend von diesem kann auch auf andere verknüpfte Entitäten zugegriffen werden.

![Screenshot des Untersuchungstools](img/toolkit_explore.png)

## Konvertieren von Katalogmetadaten

Das Konvertierungswerkzeug kann verwendet werden um die Metadaten von älteren DCAT-AP-Versionen in eine aktuelle DCAT-AP-Version zu konvertieren. Eine Liste der Änderungen wird vor der Konvertierung zusammenfassend in einer Tabelle angezeigt. Nach dem Klicken auf die Schaltfläche "Konvertieren" werden die Katalogmetadaten im Reiter "Katalogquelle" aktualisiert und können von dort heruntergeladen werden.

![Screenshot der Konvertierungstabelle](img/toolkit_conversion.png)