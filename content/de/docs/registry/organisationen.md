# Verwalten von Organisationen und Harvesting&shy;quellen

In EntryScape Registry ist eine Organisation auch eine Harvestingquelle. Um einen Datenkatalog zu harvesten muss daher eine Organisation erstellt werden.

In den folgenden Abschnitten wird beschrieben wie Harvestingquellen erstellt und verwaltet werden.

## Eine Organisation erstellen

Klicken Sie auf die Schaltfläche <input type="button" value="+"> im oberen rechten Bereich um eine Organisation zu erstellen. Es sind nur wenige Details erforderlich: der Name der Organisation, eine kurze Beschreibung, der Typ des Datenkatalogs (DCAT, CKAN usw.) und die URL der Harvestingquelle.

![Screenshot des Erstellungsdialogs](img/organization_creation.png)

Nachdem die Organisation erstellt wurde kann es einige Zeit dauern bis die URL überprüft und von ihr geharvestet wird.

Durch Klicken auf das Organisationsmenü <input type="button" value="&#9881;"> können Sie auf den Konfigurationsdialog zugreifen um die Details zu Organisation und Harvesting zu bearbeiten.

## Den Harvestingstatus kontrollieren

Der Harvestingstatus der Organisation kann durch Klicken auf den Organisationseintrag oder durch Klicken auf den entsprechenden Menüeintrag nach dem Klicken auf das Zahnrad aufgerufen werden <input type="button" value="&#9881;">.

Die letzten Harvestingversuche werden mit einigen grundlegenden statistischen Information wie z.B. der Menge der geharvesteten Datensätze angezeigt. Wenn eine Harvestingquelle gefunden wurde kann auf einen Validierungsbericht pro Harvestingversuch zugegriffen werden.

Die Schaltfläche "Neu laden" fügt die Organisation der Harvesting-Warteschlange hinzu und es wird innerhalb kurzer Zeit ein erneuter Harvestingversuch auslöst.

![Screenshot des Harvestingstatus](img/harvesting_status.png)