# Einführung in Terms

Die Dokumentation für EntryScape Terms ist derzeit nur auf Englisch verfügbar.

Gehen Sie zur [englischen Dokumentation](../../en/terms/).
