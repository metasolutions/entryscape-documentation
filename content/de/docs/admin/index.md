# Grundlegendes über die Zugriffskontrolle in EntryScape

EntryScape verwaltet zwei Arten von Berechtigungstypen: Benutzer und Gruppen. Gruppen können Zugriffsrechte auf die gleiche Weise wie bei Benutzern zugewiesen werden. Gruppen können Benutzer enthalten, jedoch keine anderen Gruppen (dies kann sich in zukünftigen Versionen ändern).

Jedes Objekt das in EntryScape verwaltet wird besteht aus Verwaltungsinformationen ("Entry") und einem oder mehreren Metadatengrafen. Der Entry verknüpft Metadaten und eine Ressource, die in EntryScape hochgeladen werden kann oder auf eine URI verweist. EntryScape kann eine Zugriffskontrolle für jeden der genannten Teile verwalten, stellt diese erweiterte Funktionalität jedoch nur im Admin-Modul zur Verfügung. Die Module Catalog, Terms und Workbench stellen eine benutzerfreundlichere Benutzerschnittstelle zur Verfügung und unterscheiden nur zwischen "Mitgliedern" und "Managern". Mitglieder können nur Entitäten anzeigen, bearbeiten und erstellen, während Manager die volle Kontrolle über einen Katalog, eine Terminologie oder ein Projekt haben.

Die erweiterte Funktionalität die vom Admin-Modul bereitgestellt wird, wird nur für erweiterte Anwendungsfälle benötigt. Es wird daher empfohlen stattdessen die Teilungseinstellungen der Module zu verwenden.

!!! info "Nur Mitglieder der `admin`-Gruppe können das Admin-Modul sehen und darauf zugreifen"

Funktionen wie die Benutzererstellung, die Zuweisung von Gruppenmitgliedschaften, das Zurücksetzen von Kennwörtern usw. müssen mit dem Admin-Modul ausgeführt werden, da diese Funktionalität in den anderen Modulen nicht enthalten ist.