# Verwalten von Benutzern, Gruppen und Projekten

Das Admin-Modul verwaltet Benutzer, Gruppen und Projekte. Diese können in separaten Registerkarten gefunden werden, welche auch ein Suchfeld und eine Liste der mit der Suche übereinstimmenden Objekte enthalten. Standardmäßig werden alle Objekte angezeigt.

## Erstellen von Benutzern, Gruppen und Projekten

Der Erstellungs-Button <input type="button" value="+"> rechts oben zeigt einen Dialog an, in dem Informationen über das zu erstellende Objekt eingegeben werden können.

Alle anderen Operationen sind über die Liste verfügbar - die Operationen sind spezifisch für die die Objekttypen. Eine Schaltfläche mit einem Zahnrad <input type="button" value="&#9881;"> wird angezeigt, wenn Sie den Mauszeiger über ein Listenelement bewegen.

## Funktionen spezifisch für Benutzer

### Bearbeiten

Ändert die Metadaten des Benutzers (Standardaktion, wenn direkt auf den Listeneintrag geklickt wird).

### Benutzername ändern

### Passwort ändern

### Teilungseinstellungen

Ändert die Zugriffskontrollliste des Benutzers.

### Gruppenmitgliedschaften

Ändert die Gruppenmitgliedschaften des Benutzers.

### Benutzer deaktivieren

Deaktiviert den Benutzer. Ein deaktivierter Benutzer kann sich nicht anmelden, alle Daten des Benutzers bleiben jedoch erhalten.

### Entfernen

Entfernt den Benutzer.

## Funktionen spezifisch für Gruppen

### Bearbeiten

Ändert die Metadaten der Gruppe (Standardaktion, wenn direkt auf den Listeneintrag geklickt wird).

### Teilungseinstellungen

Ändert die Zugriffskontrollliste des Benutzers.

### Mitglieder

Ändert die Mitglieder der Gruppe.

### Entfernen

Entfernt die Gruppe.
  
## Funktionen spezifisch für Projekte

### Bearbeiten

Ändert die Metadaten des Projekts (Standardaktion, wenn direkt auf den Listeneintrag geklickt wird).

### Teilungseinstellungen

Ändert die Zugriffskontrollliste des Benutzers.

### Entfernen

Entfernt das Projekt.