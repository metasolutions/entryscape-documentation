# Managing users, groups and projects

The Admin module manages users, groups and projects. These entities can be found in separate tabs which contain a search field and a list of matching entities. By default all entities are shown.

## Creating a user, group or project

The creation button <input type="button" value="+"> at the top triggers a dialog where information about the entity which is to be created is requested.

All other operations are available through the list - the operations are specific for the entity types. A button with a cog wheel <input type="button" value="&#9881;"> is shown when hovering over a list item.

## Operations specific for users

### Edit

Changes the metadata of the user (default action when clicking directly on the list entry).

### Change username

### Change password

### Sharing settings

Changes the ACL of the user.

### Groups memberships

Changes the the user's group memberships.

### Disable user

Disables the user. A disabled user cannot login, but the user's data remains in the system.

### Remove

Deletes the user. 

## Operations specific for groups

### Edit

Changes the metadata of the group (default action when clicking directly on the list entry).

### Sharing settings

Changes the ACL of the group.

### Members

Changes the members of the group.

### Remove

Deletes the group.
  
## Operations specific for projects

### Edit

Changes the metadata of the project (default action when clicking directly on the list entry).

### Sharing settings

Changes the ACL of the project.

### Remove

Deletes the project.