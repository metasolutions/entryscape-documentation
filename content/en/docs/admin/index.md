# Understanding access control

EntryScape manages two kinds of principals: users and groups. Groups can be assigned access rights the same way as with users. Groups can contain users but no other groups (this might change in the future as groups are improved).

Every entity managed in EntryScape consists of administrative information (the "entry") and one or more metadata graphs. The entry links together metadata and a resource which may be uploaded data in EntryScape or simply a URI that points to an external location. EntryScape can manage ACL for each of the mentioned parts, but exposes this advanced functionality only in the Admin module. The modules Catalog, Terms and Workbench apply a more user-friendly approach and only differentiate between "members" and "managers". Members may only view, edit and create entities whereas managers have full control over a catalog, terminology or project. 

The advanced ACL functionality as exposed by the Admin module is only needed for advanced use cases, it is therefore recommended to use the sharing functionality in the modules instead.

!!! info "Only members of the `admin` group can see and access the Admin module"

Other functionality such as user creation, assignment of group memberships, password resets, etc, need to be performed using the Admin module as such functionality is not included in the other modules.