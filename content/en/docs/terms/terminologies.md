# Terminologies

## Create terminologies

When you log in to EntryScape you come to the main page. To start working with terminologies and concepts, click on **"Terms"**, either in the left menu or in the main view.

![Main page with left menu and main view, Terms](img/terms_startpage.png)

Then you come to the **overview** of the **terminologies** in EntryScape Terms. Here you see all the terminologies already created and you can search (by terminology name), import existing terminologies (RDF/XML files) or create new terminologies. If you don't see a list of terminologies here, none has been created yet, or your organization has not shared them with you. Under [Permissions](permissions.md) you can read more about permissions and sharing settings.

![Overview of terminologies](img/terms_overviewterminologies.png)



There are two ways to create terminologies in EntryScape Terms. Either you **import an existing terminology** or you **create a terminology from scratch**, using the "Import" and "Create" buttons at the top right.

![Import and Create buttons](img/terms_importcreatebuttons.png)


### Import terminology

You can import an existing terminology by clicking on the **"Import"** button. This will open a window where you can choose to either **link** to a file on the web or **upload** a file from a local drive. Terminology files should be in the format RDF/XML, which is the standard format for linked data.

![Import Terminology via Link dialog box](img/terms_importterminologylink.png)

An important thing to decide when importing a terminology is whether you intend to: 

1) only **copy** a terminology from a source that **someone else maintains/updates** (which you can re-import with their updates)

or

2) the new imported terminology should be the **new master source**, meaning that **you/your organization is responsible for updates and maintenance** of the terminology.

If you go for **option 1** and only want to copy an existing terminology and only re-import new updated copies, check the box **"Restrict terminology to allow only extensions of imported concepts"**.

![Restricted import](img/terms_restrictterminology.png)

If you instead go for **option 2** where the imported terminology should **be the new master source**, leave the box "Restrict terminology..." **unchecked**.

### Import via link

If you want to **import a terminology from the web**, paste a **link** to a file in RDF/XML format. When you click on the "Import" button, the process of uploading, analyzing and importing the terminology into EntryScape Terms starts. If there are a lot of terms, it may take a while to finish. (If you need to interrupt the process, just click the refresh button in your browser.)

![Import status window](img/terms_importstatus.png)

### Import via local file

You can also upload a terminology file from a local drive. Click on **"File"** and then the magnifying glass icon to select an RDF/XML file.

![Import terminology from file dialog](img/terms_importterminologyfile.png)

### Update a restricted terminology

For terminologies that have been **imported as "restricted"** and need to be updated from another source, you can update them by clicking on the threepoint-menu, choose **"Update from source"** and provide the link or file to the source (RDF/XML format). Both group admins and members can update a restricted terminology from a source.

![Update terminology from source](img/terms_updatefromsource.png)


### Create new terminology 
To **create a completely new terminology** from scratch, click on the **"Create"** button. Then you enter a name for the terminology and a description. Once you have created your terminology, you can start creating concepts to add to it.

![Create Terminology dialog box](img/terms_createterminology.png)


## Edit terminology

Group managers can easily **edit** a terminology by going back to the list of terminologies (click on Terms in the left menu). There, click on the **three-dot menu** and select **"Edit"**.

![Three-dot menu Edit terminology](img/terms_editterminology1.png)

This will open a dialog where you can **edit all information/metadata** for your terminology: Title, Description, Publisher and Keywords.

![Edit Terminology dialog window](img/terms_editterminology2.png)




## Concepts

### Creating new concepts

You can **create concepts** to add to your terminology by clicking on **"Create"** on the "Hierarchy" page.

![Button Create concept](img/terms_createconcept1.png)

Then you can enter a concept name and add it to your terminology.

![Create Concept dialog](img/terms_createconcept2.png)

Please note that it's not possible to add new concepts to restricted terminologies that can only be updated from another source. Although it is possible to enhance the existing concepts by adding extra information, such as translations to other languages etc.


### Organize concepts

The concepts will automatically sort themselves in alphabetical order. For non-restricted terminologies, it is easy to create **subordinate (narrower) concepts** and move around concepts in the hierarchy. You can create a narrower concept by first selecting the broader concept and then click on the "Create" button.

![Create narrower concept, select broader concept](img/terms_createnarrowerconcept1.png)

The new concept (blue) will be added below the selected parent concept  (black).

![Newly created narrower concept](img/terms_createnarrowerconcept2.png)

It is even easier to **move concepts by dragging and dropping** them where you want them. Click on the concept you want to move with the mouse pointer and hold down the button. Then point at where you want to move it. A small black arrow on the left side shows where the concept will be dropped when you release the mouse button.

![Moving concepts by drag and drop](img/terms_moveconcept.png)

Please note that it's not possible to move around concepts belonging to restricted terminologies that can only be updated from another source.


### Edit concepts

To **edit** a concept, **click on the concept** in the concept hierarchy and then click on the **pen** in the top right corner.


![Edit concept buttons](img/terms_editconcept1.png)


Then you can fill in more information about the concept. The green slide buttons at the top allow you to choose if you want to see  **Recommended fields** and **Optional fields**.

![Edit concept dialog](img/terms_editconcept2.png)


### Recommended fields
**Preferred label:** The main label for the concept. A concept may have a maximum of one preferred label per language.

**Alternative label:** A concept can have several alternative labels.

**Hidden label:** You can also have hidden labels for a concept, e.g. obsolete or outdated names that should be searchable but not displayed.

**Definition:** A short text that describes the concept. It is important to describe the concept so that it cannot be confused with identical words that have a different meaning.

**Scope note:** Help text describing the context/scope of use.

**Example:** A concrete example of what is meant by the concept.

**History note:** A text describing how the term has evolved and possibly whether the meaning has changed over time.

**Has related concept:** Concepts that are in different places in a hierarchy but still have related properties that need to be expressed.

**Has broader concept:** A more generic, broader concept in the same terminology.

**Has narrower concept:** A subordinate, narrower concept in the same terminology.

**In scheme:** The scheme (terminology) the concept belongs to.

**Top concept of:** Is top concept in scheme.

**Has exact matching concept:** Mapping to a concept that has the same definition and boundary but is in a different terminology

**Has close matching concept:** Mapping to a concept that is broadly equivalent to another concept in a different terminology

**Has related matching concept:** Concept with the same scope, but in a different terminology



### Optional fields
**Editorial note:** Information addressed to the editor of the concept.

**Change note:** Note corresponding to the editor's changes over time.

**Note:** Extra field for notes that do not fit in other fields.

**Has broader matching concept:** A parent, broader concept that is in a different terminology.

**Has narrower matching concept:** A subordinate, narrower concept that is in a different terminology.


### Remove concept

To **remove** a concept, select the concept in the hierarchy, click on the three-dot menu and select **"Remove"**. 

![Remove concept](img/terms_removeconcept.png)


## Publish terminology

When your terminology is **ready for publication**, go to your list of terminologies (click on "Terms" in the main menu) and publish it using the green **publish button**. You can just as easily unpublish it with the same button. Only group admins have permissions to publish/unpublish a terminology.


![Publish button](img/terms_publishbutton.png)


## Download terminology

You can **download a terminology** by navigating to the list of terminologies (click on "Terms" in the main menu), click on the threepoint-menu to the right
 and choose **"Download terminology"**. 

 Please note that the download will **include all concepts** belonging to the terminology, but **no collections**.

 ![Menu option download terminology](img/terms_downloadterminology1.png)

 Then you choose format for your downloaded file: RDF/XML, Turtle, N-Triples, or JSON-LD. Click on **"Download terminology"** to save the file locally.

![Choose download format](img/terms_downloadterminology2.png)

## Remove terminology

It is easy to **remove** a terminology. Just click on the three-dot menu and select **"Remove"**. Please note that a removed terminology cannot be restored. Only group admins have permissions to remove a terminology.

![Button Remove terminology](img/terms_removeterminology.png)