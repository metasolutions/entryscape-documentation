
# EntryScape Terms

With **EntryScape Terms** you manage **terms and concepts** related to open and shared data. You can create or import terminologies and taxonomies that can be used by other applications. 

Information managers/data owners will also benefit from reading parts of this documentation. Before you start working with EntryScape Terms, please check existing policies for sharing and re-using data in your organization.

## How concepts are organized in EntryScape Terms

A **terminology** is a set of terms and concepts within a specific field of expertise. In EntryScape Terms you collect concepts that belong together within a certain terminology and structure them in **concept hierarchies**. You can also create **collections** where you collect subsets of concepts from one or more terminologies.

Below is an **example of a terminology**, "Dog breeds", where the concept "Companion and Toy Dogs" serves as the parent concept for several subordinate concepts in a hierarchical structure, allowing you to go down multiple levels.

![Example of hierarchical terminology](img/terms_concepthierarchy.png)


You can also group existing concepts into **collections**. A collection is a selected **subset of concepts** from one or more terminologies grouped together for a specific purpose. 

Below is an **example of a collection**, "Allergy friendly dogs and cats", that collects and points to a subset of concepts from the already existing terminologies "Dog breeds" and "Cat breeds". 

![Example of a collection](img/terms_collection.png)




