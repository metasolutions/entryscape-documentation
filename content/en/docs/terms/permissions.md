# Permissions and sharing settings

In EntryScape Terms, a user can usually be either **group manager** for a terminology or **member** with restricted access. You can be a member or group admin of one or more terminologies. A **group manager** can **give permissions** to other users, **publish/unpublish** a terminology, **download** it, **remove** it, **edit metadata** for a terminology, and **add concepts** and collections to a terminology. When you create or import a terminology for the first time, you automatically become a group manager for that terminology.

**Members** can **download** a terminology, **update a restricted terminology** from a source (see below), and **create concepts and collections** etc. for a terminology. However, **members cannot publish/unpublish** a terminology, **delete** it or **edit its metadata**. 

If needed it's possible to get a higher level of permissions in specific cases, admin permissions, if you contact the MetaSolutions support. In such cases your organization must already have established a work routine, work group and metadata management or open data work in the management organization. 

If you can't access the terminology you want, you need to request access to it from the group manager (or admin) in your organization. (The group manager is most likely head of the department, unit manager or similar.) 

**Group managers** can **change access permissions** for other users by clicking the three-dot menu and select **"Sharing settings"**. 

![Menu item Sharing settings](img/terms_sharingsettings1.png)


There you can **add and remove users** to the group that has access to the terminology. To add a user to a group, click on **"Add user to group"**. You can also choose the **level of permissions** the user should have, **group manager or member**, by clicking on the icon in front of the name.

![Permission settings for users](img/terms_sharingsettings2.png)

To **remove** a user from a group, click on the three-dot menu and select **"Remove"**.

![Menu item Remove user from group](img/terms_sharingsettings3.png)