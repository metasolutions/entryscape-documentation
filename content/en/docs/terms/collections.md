# Collections

If you want to use a **subset** of concepts in a terminology and **combining concepts from different terminologies**, you can create **collections**. 

![Schematic image of a collection](img/collection.png)

## Create a collection

To create a new collection, click on **"Collections"** in the left menu and then **"Create"**. Fill in the **Title** (mandatory) and **Description** of the collection, preferably in more than one language. 

![Create Collection dialog box](img/terms_createcollection.png)

Then click on the "Create" button at the bottom right to save. You will then see your collection in a list. The zero in the black circle indicates that it is doesn't yet contain any concepts. Click on the title of your collection to access the collection overview, where you can add concepts.

![List of collections](img/terms_listcollections.png)

On the overview page, you can click on the **pen** next to **Concepts** to add concepts to your collection. 

![Pen button to add terms](img/terms_addconcepts1.png)

This will open a window where you can either **search for concepts** (via the search box) or **browse concepts** in the terminologies you have access to. 

You choose which concepts you want to add to your collection by **checking the boxes** on the right side.

At the top right you can choose which **source** you want to search and whether you want to select concepts from a **specific terminology**. 


![Find and add terms dialog box](img/terms_addconcepts2.png)

At the bottom, you can see **how many concepts** you can browse through (based on the source and terminologies you selected). If you want to see more than five concepts per page, you can change it at the bottom with **"Rows per page"**.

When you click "Save", your selected concepts will be added to the list of concepts belonging to your collection.


![List of concepts in a collection](img/terms_listedconceptsincollection.png)

## Edit collection

If you want to **edit the title** or **description** of your collection, click on the **"Edit"** button at the top right.

If you want to **add concepts** to the collection, use the **pen** next to "Concepts". To **remove concepts** from the collection, you can click on the three-dot menu to the right of the concept and select **"Remove"**. The concept is then removed from your collection, but remains in its terminology.

![Edit collection](img/terms_editcollection.png)


## Remove collection

To **remove** a collection, click on the **"Remove"** button. A removed collection cannot be restored, but it is of course possible to recreate it by creating a new collection and adding the concepts again, as the concepts remain under their terminologies.

![Remove collection](img/terms_removecollection.png)
