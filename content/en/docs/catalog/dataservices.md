## Overview
A **data service** is a way to describe and publish one or several **API:s**. 

## Create data service

To create a data service, go to **Data services** in the menu and click on "Create".

![Main menu Data service](img/catalog_7_dataservices_menu.png)

Then you can enter a **Title** and **Endpoint URL** for the data service.  For the "Endpoint URL" field, point to the most general open access address to your data service. Avoid linking deeper with intersections in the API. 

![Dialog to create a data service](img/catalog_7_dataservices_createdataservice.png)

If you turn on **recommended and optional fields**, you can describe your data service in more detail. Then click on "Create". More detailed descriptions with additional metadata fields filled in will help othere users to easily find, understand and be able to use the data available from your data service.

It is possible to save a description of a data service as a draft even if some mandatory information is missing. The data service is then marked in the list with a yellow icon indicating that it's missing mandatory information. It's not possible to publish the data service before all mandatory information has been entered.

![Yellow draft icon indicating that mandatory information is missing](img/catalog_drafticon.png)

## Edit or remove data service

You can edit the description about your data service or remove it with the **Edit** and **Remove** buttons on the overview page for the data service. There you can also see which **datasets** that are linked to the data service.

![Menu option Edit](img/catalog_7_dataservices_edit.png)

## See detailed information

If you want to see more detailed information about your data service, click on the information icon. 

![Information icon](img/catalog_informationicon.png)

Then you get a popup dialog where you can see metadata such as endpoint URL and  dataset the data service serves as well as all entities linking to or from your data service. Read more about [detailed information](detailed_information.md). 

## Publish data service

When you are done describing your data service, you can **publish** it by clicking the Publish button to make it green, on the data service list page. 

![Publish button](img/catalog_7_dataservices_publishdataservice.png)
