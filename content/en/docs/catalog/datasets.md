## Search for dataset
<a id="search-for-dataset"></a>

Before you create a new dataset, it's a good idea to first check that it doesn't already exists. You can **search** among existing datasets in the catalog, by entering at least three characters in the search field.

![Search field for datasets](img/catalog_3_datasets_search.png)

By clicking on a dataset you get to the dataset overview page. There you can edit the dataset by clicking on the  **Edit button**.

![Edit button for a dataset](img/catalog_3_datasets_editbutton.png)

If the dataset doesn't already exist, you can create a new one.

## Create dataset from template
<a id="create-dataset-from-template"></a>

There are two different ways to create a dataset, either by **creating it from a template** that already exists or **creating it from scratch**. 

![Menu option Create dataset from template](img/catalog_3_datasets_createfromtemplate.png)

To create a dataset from a template, click on the **”Create”** button, then select **"Dataset from template"** and choose a suitable catalog and possibly a matching category (for example "Environment") where there might be a useful template for the dataset you want to to create. 


![Dialog for Create from template](img/catalog_3_datasets_createfromtemplate1.png)

To preview the dataset description, click on the round information icon. If you want to use the chosen template, click on ”Create”.


![Overview of dataset templates](img/catalog_3_datasets_createfromtemplate2.png)

You will get a page with pre-entered information from the template that you can edit to suite your dataset. 

![Example of pre-entered template information](img/catalog_3_datasets_createfromtemplate3.png)

By **turning on recommended and optional fields** you can describe your dataset in more detail. For more information about each input field, see [Describing a dataset](#describing-a-dataset) below. 

![Buttons to turn on recommended and optional input fields](img/catalog_3_datasets_man_rec_opt.png)

You can only create one dataset from one template, but if you want to create several similar datasets, see [Copy dataset](#copy-dataset).

## Create a dataset from scratch
<a id="create-a-dataset-from-scratch"></a>

If you want to **create a dataset from scratch** instead of using a template, click on the button **"Create"** and then choose **"Dataset"**.

![Menu option Create dataset](img/catalog_3_datasets_createdataset.png)

Start by **choosing a profile** that suits your dataset before you enter information in the input fields. If you can't see a profile button, the profile is already chosen for you. The profile you choose determines which set of input fields your dataset will have. The most common profile is usually the prechosen standard. 

![Profile button](img/catalog_3_datasets_profilebutton.png)

The organization can publish data according to different application profiles. For PSI data it can be DCAT-AP-SE and for INSPIRE it can be a combination of NMDP and DCAT-AP-SE. If you are working with geospatial data, you should use the INSPIRE profile.

## Describing a dataset
<a id="describing-a-dataset"></a>

When you have chosen an appropriate profile you can start describing your dataset. All **mandatory** and **recommended input fields** are shown by default. You can choose to **show** or **hide** the recommended and optional input fields on the edit page by using the slide buttons at the top. Input fields already containing data are always shown. 

On the right side you see quick links to all the input fields. If you click on the title for an input field, you will get a tool tip with short information about what information that is expected as input. 

The three fields: **Title**, **Description** and **Publisher** are **mandatory fields** for a dataset. Remember that your organization may require you to enter information in mandatory fields in **more than one language**. To add a title in another language, click on **"+ Title"**. A new row with input fields will appear where you can choose other languages. If you instead want to remove a row, click on the minus symbol on the right side. 

![Fields for entering Title](img/catalog_3_datasets_title.png)

It is possible to save a description of a dataset as a draft even if some mandatory information is missing. The dataset is then marked in the list with a **yellow draft icon** indicating that it's missing mandatory information. It's not possible to publish the dataset before all mandatory information has been entered.

![Yellow draft icon indicating that mandatory information is missing](img/catalog_drafticon.png)

Note! If an **external part** hosts the dataset for an organization, **that organization** should be entered as publisher for the dataset. The reason for this is that the original organization can only  set up rules on how the external host should handle the data, but in reality can't enforce control over the data.

![The field Publisher](img/catalog_3_datasets_publisher.png)

Some of the recommended fields and the optional fields for the profile DCAT-AP-SE are described below. If you want more information about the fields for DCAT-AP-SE, you can read more about it at [dataportal.se](http://dataportal.se).

### Recommended fields
**Contact point:** A dataset needs a contact point, usually the data owner, in order for the organization to keep track of which department or person that updates and maintains the dataset. It may be an individual or an organization  (department or unit within the organization) with a functional mailbox.

 Choose a contact from the list or create one by clicking on the magnifying glass.  For more information on how to create a contact, se [Create contact point](#create-contact-point).

![The field Contact point](img/catalog_3_datasets_contactpoint.png)

**Keywords:** To make the dataset searchable, and thus discoverable, you need to describe it by defining keywords, categories and subject terms (e.g. via linked or imported terminologies such as GEMET from the EU).
Filling in the various fields may require some thought and work but they are very important to make it possible to find the datasets, both in your own catalog but also in other catalogs that harvest your catalog (e.g. dataportal.se, govdata.de or opendata.swiss).

Describe the dataset with your own **keywords, one per line without commas**. Create a new row with the plus sign (+).

**Special case, Custom fields:**
In some cases there may be custom fields that your organization have in their specific profile. These are only available in your own instance of EntryScape and not in EntryScape Free.

For example, your organization may have chosen to import controlled subject terms from the GEMET (GEneral Multilingual Environmental Thesaurus) glossary into EntryScape. GEMET is an established European thesaurus with over 40 themes (top level) and contains a total of about 5000 terms. As there are so many terms to choose from, the easiest way to select appropriate subject terms is to use the search function.

**Custom field from Inspire application profile:**
If your organization has a custom application profile for e.g. INSPIRE.
If you have selected the Inspire profile, the Inspire Theme and Subject Category fields are also visible to describe the data set.

**Release date:** The formal publication date for the dataset. With the date menu to the right, you can choose if you want to enter only a year, or a date or a date and time. Then you enter the year/date/time in the left fields.

**Named geographical area:** The geographical area(s) for the dataset. Click on the magnifying glass to choose for example country, region, municipality or city. 

**Geographical area, Boundingbox:** Choose area by zooming in and click on the map or write the coordinates, in reference system **WGS 84 (latitude and longitude)**. Zoom in and out with the plus and minus buttons on the left side.

![Zoom buttons on map ](img/catalog_3_datasets_mapzoom.png)

Click on the map marker to choose to pinpoint a location on the map, or click on the rectangle icon to choose to draw a rectangle around the chosen area for the dataset.

![Map markers to pinpoint a location or draw a rectangle on the map](img/catalog_3_datasets_mappoint_mapsquare.png)

To draw a rectangle, click once to choose the upper left corner and click a second time to finish with the right lower corner of your chosen area. The coordinates update automatically in the fields on the right side. 

**Time period, Start and End:** The time period to which the dataset applies, with start and end date. You can choose to add more than one time period. With the date menu to the right, you can choose if you want to enter only a year, or a date or a date and time. Then you enter the year/date/time in the left fields.

**Access rights:** Classification of the dataset, stating if it contains open data, data with access restrictions or non-public data, such as sensitive personal information. Note that descriptive metadata can be published for non-public data, but that distributions with the actual data should not be created for those datasets. 

### Optional fields

**High-value dataset, category:** If the dataset is categorised as a high-value dataset (e.g. "Heritage sites") and which category/categories it belongs to.

**Conforms to:** Refers to an implementing rule or other specification. For example, you can refer to something you have imported or uploaded with the [Document](documents.md) function or a specification you imported from a data portal. Note that the document must be of document type "Standard/Recommendation/Specification" in Catalog to be visible.

**Temporal resolution:** The minimum time period resolvable in the dataset, e.g. "Months: 3”.

**Fee:** If there is a fee associated with using the dataset.	

**Documentation:** A page or a document about this dataset. Note that the document you refer to must be of document type "Documentation" in Catalog if you want it to be visible. You can read more about how to create and link documents to datasets in [Documents](documents.md).

!!! info "**Note!** It can be tempting to work with EntryScape in several tabs open at the same time in a browser, but it is not recommended if you want to edit your datasets. If you accidently edit the same dataset in two different tabs, it's easy to overwrite your own changes in the wrong way and cause error messages in EntryScape."

## Dataset series

DCAT-AP 3.0 introduces **dataset series**, which is also implemented in EntryScape 3.14. Dataset series can be used to group datasets that belong together, in time series for example. However, if you are still using DCAT-AP 2.x, dataset series will be disabled in EntryScape, until you upgrade to DCAT-AP 3.0 or later.

To create a dataset series, go to Datasets, click on **"Create"** and then select **"Dataset series"**. Enter a **Title**, **Description**, **Publisher**, as well as other information and click on "Create".

![Create dataset series](img/catalog_3_datasets_createdatasetseries.png)

You will see your dataset series in the list of datasets, marked with a series icon.

![Dataset series icon](img/catalog_3_datasets_datasetseriesicon.png)

The same icon can be found in the dataset series overview. There you can **add datasets** to your series by clicking the looking glass button next to "Datasets". And if you want to remove a dataset, you can click on the three-point menu for a dataset and choose to **remove** it from the series or remove it completely. 

![Add datasets to a dataset series](img/catalog_3_datasets_datasetseries.png)


## Create contact point
<a id="create-contact-point"></a>

When you describe your dataset, you can also create a new **contact point** by clicking on the magnifying glass. 

![The field Contact point with magnifying glass](img/catalog_3_datasets_contactpoint.png)

First choose whether you want to enter an organization (for example a department/unit) or an individual as contact point. Name and email are mandatory information, but if you choose to turn on **recommended** fields (at the top) you can enter more information such as phone number and address. Then click the **Create button** down to the right.

![Dialog to create a new contact point](img/catalog_3_datasets_createcontactpoint.png)

Note that you can't edit or remove an existing contact point here. Instead, go to  **Contacts** in the main menu to the left. There you have full edit access to all contact points.

![Buttons Edit and Remove contact](img/catalog_3_datasets_editremovecontacts.png)


## Copy dataset
<a id="copy-dataset"></a>

If you already have a described dataset that you would like to copy, go to the overview for the dataset and click on **”Copy”**.

![Copy button for a dataset](img/catalog_3_datasets_copydataset.png)

Note that you create a copy of the dataset description, but not the associated distributions, visualizations or comment of the original dataset. You also get to confirm if you would like to proceed anyway.

![Confirmation for copying dataset](img/catalog_3_datasets_copydataset2.png)


## Table view and list view
<a id="table-view-and-list-view"></a>

The default view in EntryScape Catalog is to show data in a **list view**. If you would prefer to see and edit many suggestions or datasets on the same page, you can use the **table view**. Change to table view by clicking on the **table view icon** at the top.

![Table view icon](img/catalog_3_datasets_tableview1.png)

In table view, you can also decide how many columns you want to see simultaneously. Click on **"Columns"** to choose which columns to hide or show. 

![Choose columns](img/catalog_3_datasets_columns.png)

Click on the field you want to edit and describe it with a value from the drop-down list or free text. To close the edit box, click on the **upper X** or outside of the box. After you are done, don't forget to click on the **Save button** at the bottom. 

![Table view for editing](img/catalog_3_datasets_tableview2.png)

To change back to list view, click on the list view icon up on the right.

![List view icon](img/catalog_3_datasets_listview.png)

## See detailed information
<a id="see-detailed-information"></a>


If you want to see more detailed information about your dataset, click on the information icon. 
![Information icon](img/catalog_informationicon.png)

Then you get a popup dialog where you can see metadata such as publisher, URI etc for your dataset as well as all entities linking to or from your dataset. Read more about [detailed information](detailed_information.md). 

## Remove dataset
<a id="remove-dataset"></a>

To remove a dataset, go to the overview for the dataset and click on the button **"Remove"**. Note that you can only remove datasets that are unpublished, so if you wish to remove a published dataset you have to unpublish it first by click on the green publication slide button. 

![Remove dataset](img/catalog_3_datasets_remove.png)