## Overview
With **Showcases** you can show **concrete examples** (like a webpage or a mobile app) that have been built based on one or several published datasets in a catalog. It can be useful to display how open data published by an organization have been brought to use by other actors. Unlike ideas, a showcase is something that already exists and is possible to display.

![Overview of showcases](img/catalog_9_showcases_overview.png)

## Create showcase

To put up a showcase, go to Showcases and click on "Create". Then enter **Title**, **Description** (mandatory fields) and which **Datasets** the showcase is based on. 

![Dialog for Create a showcase](img/catalog_9_showcases_createshowcase.png)

## Edit or remove showcase

If you later want to remove the showcase or edit the description, use the buttons "Edit" or "Remove" on the showcase overview page. 

![Buttons Edit and Remove for showcases ](img/catalog_9_showcases_editremove.png)

## Publish showcase

All showcases are public by default as sooon as they are created. You can unpublish them by using the publication button.

![Publication buttons for showcases](img/catalog_9_showcases_publishbuttons.png)

## See detailed information

If you want to see more detailed information about your showcase, click on the information icon. 
![Information icon](img/catalog_informationicon.png)

Then you get a popup dialog where you can see metadata as well as all entities linking to or from your showcase. Read more about [detailed information](detailed_information.md). 