## Overview
Depending on what you selected in the field **"Frequency of update"** for your **dataset**, you should update your data accordingly. It's a good idea to set reminders for updating in your calendar and project management tool. A dataset that is not updated according to it's frequency of update may lead to data users contacting you or your organization to ask why the dataset is not maintained. It's better to proactively update it. 

![Update frequency for a dataset](img/catalog_12_datamaintenance_updatingfrequency.png)


## Update files in a distribution
To manage the files belonging to a distribution, go to **Distributions**, click on the three-point menu and select **”Manage Files”**.

![Menu option Manage files for a distribution](img/catalog_12_datamaintenance_menumanagefiles.png)

On the distribution overview page you can choose to **add a new file** (for example data for a new year), or **replace** or **remove** an existing file.

![Buttons Add file and menu options Replace file and Remove file](img/catalog_12_datamaintenance_managefiles.png)

Don't forget to **keep the metadata description updated**, so it always matches the uploaded files. **”Date modified”** is an optional but useful input field for distributions. You can edit the description for the distribution through the three-point menu option "Edit".

![Menu option Edit for distributions](img/catalog_12_datamaintenance_editdistribution.png)

You can also go to Datasets and choose "Edit" to change the recommended field **”Time period”** to match the time period in the data for the new file. 

![Edit time period for a dataset](img/catalog_12_datamaintenance_timeperiod.png)


## Download
There are several ways to **download data** in EntryScape Catalog. You can download **single files** belonging to the distribution below ”Manage files”.

![Menu option Manage files](img/catalog_12_datamaintenance_menumanagefiles.png)

![Menu option Download file](img/catalog_12_datamaintenance_downloadfile.png)

You can also choose to download **your whole catalog** (as a file), by right clicking on the three-point menu for the catalog and choose "Download". Note that the downloaded catalog only contains **metadata** and **not** the files or visualizations belonging to its distributions.

![Menu option Download catalog](img/catalog_12_datamaintenance_downloadcatalog.png)

Next, select format for your file: RDF/XML, Turtle, N-Triples or JSON-LD. Then click on ”Download”.

![Dialog for selecting download format](img/catalog_12_datamaintenance_downloadpopup.png)

Click on ”Close” to close the window.