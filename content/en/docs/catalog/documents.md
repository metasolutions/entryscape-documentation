## Overview
With Documents you can **import, upload or link to files** that should be linked to a dataset, like a specification or a license agreement. At the moment, these three document types are supported: **Documentation**, **License** och **Standard/Recommendation/Specification**. First you select if you want to **import an external specification** or if you want to **create a document**, by linking to or uploading a file.

![Buttons Import and Create](img/catalog_10_documents_createbuttons.png)


## Import external specification
If you click on ”Import” you will see a list of **specifications** available through **dataportal.se**. 

**A specification is a guide or an instruction for how a certain kind of dataset should look.** If you follow the specification your dataset till be compatible with the same kind of datasets from other providers.

Click on ”Import” on the specification you have chosen and then click on "Close" to close the window. 

![List of available specifications to import](img/catalog_10_documents_importspecification.png)


## Create document
To **create/add a new document**, go to Documents and click on the "Create" button.

If the document is available on the web, enter the web address (URL). In other case, click on "File" and upload the file. **Document type** and **Title** are mandatory fields, but it's recommended to also enter a **Description** of the document. Then, click on "Create" down to the right. 

![Input fields to create a new document](img/catalog_10_documents_createdocument.png)

The document is now created but exists in EntryScape Catalog as a loose file not linked to anything yet. See below on how to link the document to an existing dataset. 


## Link a document to a dataset or distribution
In a dataset description there are two optional fields where you can **link a dataset to a document** that you have uploaded or linked to through EntryScape: 

**"Conforms to"** refers to a specification or similar that the dataset conforms to. Note that the document has to be of the document type "Standard/Rekommendation/Specifikation" in Catalog to be visible.

**"Documentation"**, where you refer to a documentation file that belongs to the dataset. To link to a document from a dataset, go to Datasets, select which dataset to link from and choose "Edit". Note that the document has to be of the document type "Documentation" in Catalog to be visible.

On the right side you can use Search property or the Table of contents to faster skip down to the fields "Conforms to" and "Documentation".

![The field Conforms to for a dataset](img/catalog_10_documents_editconformsto.png)

![The field Documentation for a dataset](img/catalog_10_documents_editdocumentation.png)


You may also refer to a documentation file from a **distribution**. Go to the edit view for a distribution and make sure to turn on optional fields. Note that the document must be of document type "Standard/Recommendation/Specification" in Catalog to be visible.

![Edit a distribution](img/catalog_10_documents_editdistribution.png)

## See detailed information

If you want to see more detailed information about your document, click on the information icon. 
![Information icon](img/catalog_informationicon.png)

Then you get a popup dialog where you can see metadata as well as all entities linking to or from your document. Read more about [detailed information](detailed_information.md). 

## Edit, replace or remove document

You can edit the information about your document, replace it or remove it with the buttons **"Edit"**, **"Replace"** or **"Remove"** on the overview page.

![Edit, Replace and Remove buttons](img/catalog_10_documents_editremove.png)

## Download document

You can download a document by clicking the **""Download"** button.

![Download document button](img/catalog_10_documents_download.png)