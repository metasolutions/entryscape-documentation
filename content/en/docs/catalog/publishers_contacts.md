## Overview
To be able to choose a **publisher** for a catalog or a dataset, you need to add publishers in EntryScape. Publishers and Contacts are managed as separate entities in EntryScape. You can add several publishers, which is useful if your organization is publishing datasets for another organization. 


## Publishers
To create a publisher, go to your catalog, then choose Publishers in the left menu and click on "Create".

![Main menu Publishers and Contacts](img/catalog_6_pubcon_menupublishers.png)

There you enter the **Name** of the publishing organization. It is also recommended to choose which **type of organization** it is and preferably enter a homepage and an email adress (optional) .

![Dialog Create Publisher](img/catalog_6_pubcon_createpublisher.png)

You can always fill in more details later. Just click on the "Edit" button on the overview page for the publisher. 

![Button Edit publisher](img/catalog_6_pubcon_editpublisher.png)

There you can also remove the publisher by clicking on the "Remove" button.




## Contacts
A **Contact** is specified as a function address or an individual contact to a dataset. It is a responsible function for the management of the dataset and has the **responsibility to keep the metadata updated and accurate**. Use the search field to find the contact you want to enter in your dataset or create a new contact under Contacts in the left menu, by clicking on "Create".

![Main menu Contacts](img/catalog_6_pubcon_menucontacts.png)

First select if the contact is an **individual** or an **organzation**. Next enter **Name** and **Email**. If you want you can turn on the recommended fields and enter Telephone number och Address as well.

![Dialog to create a new contact](img/catalog_6_pubcon_createcontact.png)

If you go to the overview for the contact you can edit descriptions or remove the contact, using the buttons on the right side. 

![Buttons Edit and Remove contact](img/catalog_6_pubcon_editcontact.png)

## See detailed information

If you want to see more detailed information about a publisher or a contact, click on the information icon.
 
![Information icon](img/catalog_informationicon.png)

Then you get a popup dialog where you can see metadata such as type of publisher or email address to a contact as well as all entities linking to or from your dataset. Read more about [detailed information](detailed_information.md). 