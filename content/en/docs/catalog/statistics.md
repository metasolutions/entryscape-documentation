## Overview
In EntryScape you can se **statistics of the use** of your published open data. This may be useful in your reporting and development work.

On the overview page for your catalog you will find a simple **preview** of usage activity on the right side. It shows the number of file downloads and API calls for the last 7 days.

![Preview of usage activities](img/catalog_11_statistics_overview.png)

For **more detailed statistics**, go to **Statistics** at the bottom of the left menu. (It can take a few seconds for the statistics page to load, depending on the amount of calculated datasets.)

![Main menu Statistics](img/catalog_11_statistics_mainmenu.png)

The detailed statistics shows **number of downloads of APIs and files** for different datasets during a chosen time range.

![Detailed statistics view](img/catalog_11_statistics_detailedview.png)

You can **filter the statistics** in several ways. You can **search on the title** of your dataset in the search box at the top. You can also select which **time range** you want to see (for examplem yesterday, last month, this year etc) and you can select which **types of data** you would like to see (files, documents, API calls). 

![Filter options for detailed statistics](img/catalog_11_statistics_filter.png)


Below the diagram **each entity is displayed in a single row**. If you click on one row, only that entity is showed in the diagram. Click on the row once more to show all entities again. 

You can also **download** the statistics as a CSV file, or **print** it, by using the buttons to your top right.

![Buttons download CSV and Print](img/catalog_11_statistics_download.png)