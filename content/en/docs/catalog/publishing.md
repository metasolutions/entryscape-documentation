## Publishing catalogs and datasets
All newly created catalogs are private by default and therefore only explicitly assigned users may access and manage the catalog. If the Public status is green it means that the catalog's metadata is publicly readable and ready to be harvested by Open Data portals.
Through the **publication slide button** you may **publish** and **unpublish** your catalog.


![Publication slide button for a catalog](img/catalog_5_publishing_publishcatalog.png)

To be able to see anything in a published catalog, it must contain a **dataset** with a **public** status (listed publicly) or have an **external data harvest** activated. You can manage publication status and activate/unactivate external data harvests on dataset level. Click on the **publication button** for the dataset to make it listed publicly. 

![Publishing button for dataset](img/catalog_5_publishing_publishdataset.png)




## Revisions
Some versions of EntryScape have automatic **revision history** of catalogs, datasets, distributions etc. You can access the revision history in the three-point menu for catalogs and distributions.

![Menu option Revisions for catalogs](img/catalog_5_publishing_revisionscatalog.png)

For datasets, data services, suggestions and other entities there is a blue Revisions button on the overview page.

![Revision button for datasets](img/catalog_5_publishing_revisionsdataset.png)

If you are a group manager or admin, you can use the Revision history to roll back/restore and earlier version of metadata description for the chosen entity. 

For example, if someone has added or removed a dataset in a catalog, it should be visible in the revision history with date, time and name of the user who did the change. Ordinary members can see the different versions as well, but they can't do roll backs/restore older versions. 

![Revision history](img/catalog_5_publishing_revisionhistory.png)


## Embedding
When you have one or more published datasets in a catalog, you can **embed them on a webpage**, for example on a blog or a press release. You can display a list with published datasets on external webpages by embedding a snippet of JavaScript code pointing at a specific catalog. 

Go back to the overview of your catalogs. Click on the three-point menu for the chosen catalog and select "Embed”.

![Menu option Embed](img/catalog_5_publishing_menu_embed.png)


Next, you get a window showing a snippet of code and possibility to select Theme format and if the links to your catalog should be opened in a new window or not. The Theme format is just to choose which set of icons your datasets will be symbolized by. 

![Dialog with code snippet and theme format](img/catalog_5_publishing_embedding1.png)

If you click on the "Preview" button you can see how your catalog will display on your webpage and compare which set of icons that suits your data the most. The JavaScript snippet otherwise contains minimal styling, allowing tables and fonts to inherit the visual properties from the surrounding external webpage. 

![Preview of chosen Theme format icons](img/catalog_5_publishing_embedding2.png)

Click on "Close". **Select** and **copy the javascript code** in the box. Then **paste the code** in the editor for your homepage.

![Dialog with javascript code to be copied](img/catalog_5_publishing_embedding3.png)

You may also embed a catalog on your intranet, which could be practical for data that needs to be shared but not published as open data. Just follow the same steps as above. 


!!! info "There are more advanced ways to build webpages with data from EntryScape, by using EntryScape Blocks. Contact EntryScape support for more information on Blocks or read more at [entryscape.com](http://entryscape.com)"