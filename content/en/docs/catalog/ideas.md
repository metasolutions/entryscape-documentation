## Overview
With this feature, organizations can publish **ideas on how published open data can be used**, based on one or several datasets in a catalog. It can be useful when planning hackatons or wanting to encourage to a certain kind of use of open data where you see an opportunity. The difference between this function and [Showcases](showcases.md) is that Ideas are just ideas of some things that don't exist yet. 

## Create idea

To create an idea, go to Ideas and click on "Create".

![Main menu Ideas](img/catalog_8_ideas_menu.png)

**Title** och **Description** are mandatory fields to fill in, but it's good to also state which **dataset** you have in mind for the idea to be based upon.

![Dialog to Create idea](img/catalog_8_ideas_createidea.png)

## Edit or remove idea

You can describe the idea in more detail later, by going to the overview page for the idea and click on Edit. There you can also remove the idea with the button Remove.

![Buttons Edit and Remove for ideas](img/catalog_8_ideas_editremove.png)

## Publish idea

All ideas are public by default as soon as they are created. You can unpublish them by using the publication button.

![Publication button for ideas](img/catalog_8_ideas_app.png)

## See detailed information

If you want to see more detailed information about your idea, click on the information icon. 

![Information icon](img/catalog_informationicon.png)

Then you get a popup dialog where you can see metadata as well as all entities linking to or from your idea. Read more about [detailed information](detailed_information.md). 
