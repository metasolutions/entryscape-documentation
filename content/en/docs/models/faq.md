## Frequently asked questions (FAQ)

### 1. What is the difference between a vocabulary and an ontology?

We acknowledge that there are two ways of creating information models for linked data.

1. **Vocabulary approach** - Classes and properties are introduced independently with a flexibility to allow them to be reused in different settings.
2. **Ontology approach** - Classes and properties are created for a specific purpose with very precise semantics and each property is typically scoped to be useful only on a specific class.


Although EntryScape Models can in principle be used for both approaches, it is designed with the flexible vocabulary approach in mind as we believe it is more in line with linked data principles. It is also percieved as the better approach to ensure interoperability.

Unfortunately, the vocabulary approach also leads to new challenges of describing how classes and properties should be combined in new contexts. Traditionally, such descriptions has often been written down in documents with long tables inside of them and have been called application profiles, or, in short, just profiles. Unfortunately, there are still no generally accepted formalism to express profiles, although using SHACL for this purpose seems promising. In EntryScape Models we use forms and fields as a formalism for describing profiles.

In EntryScape we refer to models who only introduce classes and properties as **vocabulary models**. Complementary, models who only reuse vocabulary in new settings via **forms and fields** are called **profile models**.


### 2. How do I export something from EntryScape Models to use elsewhere?

You can **export** your models as files, for example by **creating RDForms bundles, RDF Schemas and diagrams** to export and reuse elsewhere. RDForms bundles (json-files) are possible to import in EntryScape Workbench for further implementation.

You can read more about how to export models under [Resources](resources.md).


### 3. What is a specification?

The purpose of **specifications** is to ensure **data interoperability**. Although the main focus of EntryScape Models is to support working with vocabularies and profiles, most people will not be content with reading the formal expression of a model. For example, a complementary **specification document**, a **diagram**, a **set of guidelines**, **example data** as well as **scope and requirements** that formed the model may be crucial information for other actors to get a deeper understanding of the information model. The need may also be different if you are an implementor, architect or someone that only needs a quick view of the model. 

![Example of a specification with related resources](img/models_specifications_overview.png)

For this purpose EntryScape Models offers **specifications** as a way of **enriching vocabularies and profiles with diagrams, related documents, web pages and other resources**. Each specification is expressed formally using the Profiles vocabulary and is given a unique identifier (a URI), to refer to it from other sources. E.g. a dataset can refer which specification it conforms to. Specifications can also refer to each other to indicate versioning, refinement, composition etc. To learn more about how to create specifications, go to [Specifications](specifications.md).