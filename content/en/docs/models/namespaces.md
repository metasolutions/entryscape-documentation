## Overview

A namespace is a reference (URI) to a uniquely identified set of metadata that can be used and reused to support interoperability between domains. It works mainly as an abbreviation for a URI so that it doesn't have to be repeated every time you refer to a namespace.

If you want to reuse an already **existing namespace**, click on **"Import"**. To create a **new namespace**, click on **"Create"**. 

![Create buttons for namespaces](img/models_namespaces_createbuttons.png)

## Reuse namespace

EntryScape Models offers a number of common all-purpose built-in namespaces, such as **dcat**, **dcterms**, **foaf**, **rdf**, **rdfs**, **skos**, **vcard** and more. You can choose to **reuse** one or several **existing namespaces**. Just select the ones you want to use by clicking the check boxes.

![List of reused namespaces to choose from](img/models_namespaces_importnamespaces.png)

You can use and refer to both reused namespaces and custom namespaces that you have created yourself in the same model.

## Create namespace

Click on **"Create"** and fill in the mandatory fields **Abbreviation**, **Namespace** (URL) and **Full title**.

The purpose of the abbreviation is to easily refer to the namespace when filling in information for entities belonging to this namespace, without having to write the full URL again and again.

![Create namespace](img/models_namespaces_createnamespace.png)

Click on **"Create"** so save.

## Edit or remove namespace

When you have reused or created namespaces, they are listed on the namespace overview page. The reused namespaces can't be edited, as opposed to the custom namespace you created yourself and are able to edit. 

![List of namespaces](img/models_namespaces_list.png)

If you click on the **title** of your namespace, you get to the detailed overview for that namespace. Here you can click the buttons to **edit** the namespace, **set as default** or **remove** the namespace.

![List of namespaces](img/models_namespaces_buttons.png)

The namespaces you enter here will be visible in other parts of Models, for example when creating a class. By setting a namespace as "default", it will appear as "default" when presented in a list.

![Select field with namespaces when creating a class](img/models_namespaces_classpage.png)

To remove a namespace, just click the **"Remove"** button.