## Overview

If you want to create a resource to export elsewhere, click on **"Resoures"** and then **"Create"**. Then you choose if you want to create an **RDForms bundle**, a **Diagram**, an **RDF Schema** or an undefined resource.

![Drop down menu for Create resources](img/models_resources_createresource.png)

When you have created your resource file, you can then **download** it in the next step.


## Create an RDForms bundle

To create an RDForms bundle, fill in the mandatory fields **Title** and **Target languages**. If you want, you can also write a **Description**.

You may also edit which **Forms and fields** that should be selected and included in the bundle. At the bottom you see all **dependencies** to other objects that the bundle has.

![Create bundle.](img/models_resources_createbundle.png)

When you have created your bundle, you can click the title and then the **"Download"** button to download it as a JSON file.

![Download bundle.](img/models_resources_downloadbundle.png)



## Create a Diagram

You can create a diagram for your model as a PNG or SVG image file.

![Create diagram.](img/models_resources_creatediagram.png)

When you have created your diagram, you can click the title. You get a preview of your diagram and can click **"Download"** button to download it as an image file (PNG or SVG).

![Download diagram.](img/models_resources_downloaddiagram.png)


## Create an RDF Schema

You can create an RDF Schema of your model.

![Create RDF Schema.](img/models_resources_createrdfschema.png)


## Create other resource

Option for creating unspecific resource files.

![Create other resource.](img/models_resources_createresourceentity.png)
