## Overview

With forms and fields, you can make advanced customizations of your model in great detail. Many forms and fields are designed to be **reused** in similar contexts. For instance "**publisher**" is a field for dcterms:publisher and "**Document**" is the form for foaf:Document.

The main work flow is to **create a form**, then **create the fields** you want to have in the form and finally **add the fields to the form**. Below is a preview of a form with different fields.

![Form example with different types of fields](img/models_forms_preview.png)

Click on **Form** and choose to either **create a form** from scratch or **extend an already existing form** with the buttons **"Extend"** and **"Create"**.

![Create buttons for forms](img/models_forms_createbuttons.png)

## Create form

Click on **"Create"** to start creating a new form and then choose which **Form type** you want your form to be:

* **Profile form** - A standard web form containing fields.
* **Section form** - A subform displayed as a separate section within another form.
* **Object form** - A supporting form to a profile form, dependent on a property. (Object forms can't be used as stand-alone forms.)

You can also add a **Purpose** to your form. Click on **"Create"** to save your new form.

![Create form options](img/models_forms_createform.png)


## Extend form

To adapt or enhance an **already existing form**, you can click on **"Extend"** and choose another form to start from. 
Note that extending another form does not create a copy of other form, but rather a **reference** to that form **with or without** its **fields**. 

![Extend form button](img/models_extendform_button.png)

In the next dialog, you get to specify a name and select which form you want to extend, as well as whether you want to **include the fields (Copy and override)** from the source form or just copy the form without the fields.

![Extend form button](img/models_extend_formitems.png)

Use the filter or search function to find the form you want to extend.

![Extend form button](img/models_selectform.png)

Once you have extended your form, you can change and adapt it they way you want. Editing existing field data will **override** the existing data from the original form. Adding new data in empty fields enriches your form, but keep in mind that your new form will be **dependent** on your source form and you should avoid changing core elements such as namespaces, domains or ranges.

## Edit form

If you click on the **form title** you see the **detailed overview** of the form and can enter more information by clicking on the different **edit buttons** to the right.

![Buttons Edit form](img/models_forms_editbuttons.png)

Click on **"Edit"** if you want to change the **metadata** for your form.

Click on **"Edit attributes"** if you want to change the **behaviour and appearance** of the form.

Click on **"Change type"** if you want to change form type (**Profile form, Object form, Section form**). Note that changing form type might lead to losing non supported properties.

Click on **"Form items"** if you want to **add or remove fields** to your form.

### Edit form metadata

Click on the **"Edit"** button to edit metadata for the form.

**Status:**

* **Unstable** - May change at any time (also default value)
* **Stable** - Finished, no changes expected
* **Depracated** - Outdated/replaced

![Edit form, first page](img/models_forms_editform1.png)


**RDF shape**

* **Node type** - States if the form corresponds to a **URI**, **Blank** or **Resource**

* **Cardinality** - If the form is **Mandatory**, **Recommended** or **Optional**

* **Max** - Max number of subforms of the specific form type 

* **Constraint** - Here you can either **select a class** or choose a more advanced approach with **constraining properties** as well as **constraining objects**. Constraining for a certain value for the RDF type.

![Edit form, second page](img/models_forms_editform2.png)

**Presenters and Editors**

Presenters and Editors can either see the same information, or can be shown different labels and descriptions. (The default is to show the presenter label and description for both user groups.)

Presenter:

* **Label** 
* **Description**


Editor:

* **Edit label** 
* **Edit description**


![Edit form, third page](img/models_forms_editform3.png)


Lastly, you can add **CSS classes** to your form, such as card-group (predefined from RDForms) or your own CSS classes.

![Edit form, CSS](img/models_forms_editform4.png)


### Edit attributes

You can change the **behaviour and appearance** of your form by clicking on the button **"Edit attributes"**. 

![Edit attributes](img/models_forms_editattributes.png)

### Change form type

You can change **form type** for your form, but remember that elements that belonged to the old type but is unsupported in the new type will be lost irreversably.
![Change form type](img/models_forms_changetype.png)

## Manage fields in form


You can **manage fields** in your form by clicking on the button **"Form items"**.

![Button Form items](img/models_forms_formitemsbutton.png)

### Add field


To add fields to your form, click on the **Plus button** to get a list to choose from. 


![Button for adding form items](img/models_forms_addformitems.png)

You can **select the fields** you want to add to your form from the list. It's also possible to **search** for form item titles as well as use the **filter function** to be able to use e.g. fields from **another model**, of a specific **status** and **type**, such as text fields.

![Selecting form items](img/models_forms_selectformitems.png)


### Override field

If you want to adapt your model by **changing a field** and **override** the existing field information, click on the pen icon to edit.

![Edit field to override](img/models_forms_editformitem.png)

Then click on **"Override"** below the field you want to change.

![Override field by clicking Override](img/models_forms_overridefield1.png)

The new field title will **override** the old title in this example.

![Write new information](img/models_forms_overridefield2.png)

Note that if the new information is **deleted**, there will be a **fallback to the old information**.

### Change order of fields
You can **change the order** of your fields by drag-and-drop with your cursor.

![Change order of fields](img/models_forms_changeorderoffields.png)

### Remove field
To **remove** a field from your form, click the three-point menu and choose **"Remove"**.

![Remove field](img/models_forms_removefield.png)

## Preview form

To see a **preview** of what your form will look like, click on **"Preview"**.

![Preview button for Forms](img/models_forms_previewbutton.png)

Below is an **example of a preview** of a form.

![Preview of a form with text fields](img/models_forms_preview.png)

## Remove form

If you want to remove/delete a form, click on **"Remove"**. Note that the fields contained in the removed form will not be deleted.

![Remove button for Forms](img/models_forms_removebutton.png)
