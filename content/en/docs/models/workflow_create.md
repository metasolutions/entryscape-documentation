

If you are building something that is unique to your field and doesn't already exist, you need to start by **creating a new model from scratch** and add classes and properties, and/or forms and fields.

![Create a new model](img/models_workflows_create.png)

Learn **[how to create a new model from scratch](models.md#create-a-new-model-from-scratch)** step by step in EntryScape Models.
