## What is EntryScape Models?

**EntryScape Models** is our newest and most advanced application. It's a **data modeling tool for linked data** that ensures interoperability and reusability. It is built on formats and linked data standards such as **RDFS**, **SKOS**, **PROF** and **SHACL** for sharing information models, validating data, generating specifications and much more. 

EntryScape Models has five main purposes:

1. **Manage classes and properties** according to RDFS.
2. **Reuse** classes and properties in **new contexts** using **forms and fields**.
3. **Visualize** information models as **class diagrams**.
4. **Provide specifications** where the information models play a central part.
5. **Provide configurations** for other parts of the EntryScape platform, such as **Workbench**.


## Classes, Properties, Forms and Fields

All models in EntryScape Models are based on either **classes and properties** or **forms and fields**, or all four of them combined. 

![Overview of EntryScape Models with Classes, Properties, Forms and Fields](img/models_overview_simpleoverview.png)

**Classes and properties** are often shared in world wide open-standard **metadata vocabularies**, such as Dublin Core for resources and FOAF (friend of a friend) for describing persons, relations and information. 










