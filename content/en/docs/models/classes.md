## Overview

**Classes** together with **properties** are the basic building blocks in EntryScape Models that can be combined to form information models. The classes and properties can either be **reused** from other models or **created** from scratch.

EntryScape Models is designed to primarily support the management of vocabularies not ontologies, hence classes and properties are expressed via RDFS rather than OWL. The following general principles appliy, you should:

1. reuse classes as far as possible.
2. avoid reuse if the semantics does not fit or is not sufficiently well maintained.
3. create new classes only when there is nothing suitable to reuse.
4. express new classes as refinements of more generic vocabulary when possible (subclass of).

Note that EntryScape Models allows you to provide additional metadata on reused classes or properties. This is fine if you want to provide translations or clarifications that you know to be true and inline with existing semantics. However, it is not ok to change the semantics, e.g. to change to another domain or range or by providing a definition which is more narrow than the original.




## Reuse class

In order to **reuse classes from other models**, you first need to import (or create) models containing the classes you want into EntryScape Models, such as FOAF for example. Read more about how to [import models](models.md#import-classes-and-properties-from-an-rdf-schema).


To **select a subset of classes** to **reuse** in your own profile model, go to your own model, choose Classes and click on **Reuse**. 

![Reuse classes button](img/models_classes_reusebutton.png)

Then you get to **select which classes** you want to reuse in your own model. If you click on the filter button at the top right corner you can choose to see only classes that belong to a specific model (in this case FOAF).

![Select imported classes to reuse](img/models_classes_reuseclasses.png)

In the list of classes, all reused classes originating from another source model are shown with link icons. 

![List of classes, both reused and self created](img/models_classes_listofclasses.png)

If you **click on the class title**, you get the options to **edit** or **remove** the class. However, keep in mind that your new profile model will depend on your source model and you should avoid changing core aspects of the class such as namespaces, domains or ranges for reused classes.

![Edit and Remove buttons for reused class](img/models_classes_editremovereusedclass.png)

It is useful to have a set of classes that can be reused again and again in different models.


## Create class

You can also create your own class from scratch. Click on **"Create"** and fill in the mandatory fields **Web address** and **Label**. You can either choose a namespace that you have already imported/created and enter a name for the class, or you can enter a full web address for a namespace.

![Create class](img/models_classes_createclass1.png)

If you want, you can enrich your class description by turning on optional fields and enter more information.

![Optional fields for Create class](img/models_classes_createclass2.png)


## Edit class

To edit your class, click on the **"Edit"** button. You can change URI for you class by clicking the **"Replace URI"** button.

![Buttons Edit class and Replace URI for class](img/models_classes_editbuttons.png)


![Edit class](img/models_classes_editclass.png)

## Remove class

You can remove a class by clicking on the **"Remove"** button.

![Remove class](img/models_classes_removebutton.png)