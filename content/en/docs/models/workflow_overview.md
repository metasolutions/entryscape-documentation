There are **four common workflows** in Entryscape Models: **Create**, **Import**, **Reuse** and **Adapt**. 


![Four ways to work with models](img/models_workflows_four_workflows.png)

In reality these cases can be combined and tweaked further in various ways, but here we present them in a simplified way.

We refer to models with only **classes and properties** as **vocabulary models**. Complementary, new models with **forms and fields** and models which only reuse vocabularies in new settings via forms and fields are called **profile models**.


Read more about the four workflows:

* [**Create**](workflow_create.md)

* [**Import**](workflow_import.md)

* [**Reuse**](workflow_reuse.md)

* [**Adapt**](workflow_adapt.md)





