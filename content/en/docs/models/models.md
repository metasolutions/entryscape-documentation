

## Create a new model from scratch

Start by clicking on **"Create"** and create a new model with a name, a URL and preferably a description.

![Button Create model](img/models_models_createbuttonsmall.png)

![Create model dialog](img/models_models_createmodel.png)

Once you have your empty model, you should add content to it. Start by creating (or importing) one or several [Namespaces](namespaces.md). 

Essentially, all models in EntryScape Models consists of **Classes and properties** or **Forms and fields**, or a combination of all of it. Next, you can either create [Classes](classes.md) and [Properties](properties.md) or create [Forms](forms.md) and [Fields](fields.md). 


![Menu items Namespaces, Classes and Properties, Forms and Fields](img/models_models_menuitems.png)


## Import an existing model

You **import** (copy) a vocabulary model, e.g. Dublin Core Metadata Terms, **into an empty model** into EntryScape Models. Before you import a model/classes/properties in EntryScape, **make sure that it doesn't already exist** by using the search function and search for it, to avoid making duplicates. 


### Import classes and properties from an RDF Schema

To **import classes and properties** from an existing RDF Schema, in the list of models, click on the three-point menu for your empty model and choose **"Import RDFS"**. 

![Import RDFS](img/models_models_import_rdfs.png)

Next, provide a link to or upload your RDF/XML file with classes and properties into the model.

![Import classes and properties from RDF Schema](img/models_models_import_rdfschema.png)

The result will be visible in the overview of your imported vocabulary model, with the number of imported classes and properties etc. 

![Imported vocabulary model](img/models_models_importedmodel.png)

If you click on **"Classes"**, you see all the imported classes belonging to the vocabulary model. You can click on the information icon to see detailed information about the class.

![Imported external classes](img/models_models_importedclasses.png)


### Import form templates

To import models with **form templates** and **fields**, in the list of models, click on the three-point menu for your model and choose **"Import RDForms bundle"**. 
![Import form button](img/models_models_import_rdformsbundle.png)

Next, link to or upload your form template (JSON) file with forms and fields.

![Import form template](img/models_models_importform_dialog.png)


## Edit model

You can **edit** your model information by clicking on the three-point menu anc choose "Edit".

![Edit option for Model](img/models_models_edit.png)

There you can change **Title**, **Description** and **Identifier** for your model.

![Edit Model information](img/models_models_editmodel.png)

If you want to edit **classes**, **properties**, **forms** or **fields** belonging to your model, use the left menu instead.

![Left menu](img/models_models_leftmenu.png)


## Permissions and sharing settings

In EntryScape Models, a user can be either a **group manager** or a **member**. When a user creates a model, he or she automatically becomes the group manager for that model. A group manager can **give other users access** to a model as well as **change their permissions**. If you don't have access to a model, you need to contact the group manager for that model in your organization. 


To give another user access to a model, click on the three-point menu and choose **"Sharing settings"**.

![Menu item Sharing settings](img/models_models_sharingsettingsmenu.png)

Then you can **add a user** by clicking on **"Add user to group"**. You can also **change permissions** for a user, to either a member or a group manager, by clicking on the permissions icon to the left of their email address. Only group managers can add or remove other users and change their permissions. Ordinary members can't give other users access to a model or change permissions.

![Add user and change permissions](img/models_models_permissions.png)

To **remove access** to a model for a user, click on the three-point menu and choose **"Remove"**

![Remove user access to model](img/models_models_removemember.png)


## Remove model

If you want to **remove/delete** a model, click on the three-point menu and choose **"Remove"**. 

![Remove option for Model](img/models_models_removemodel.png)

You then get to confirm that you want to remove the model.

![Confirmation dialog, remove model](img/models_models_removeproject.png)







