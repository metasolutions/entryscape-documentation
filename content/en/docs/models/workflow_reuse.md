



You can **reuse classes and properties** from existing vocabulary models in EntryScape Models. For example, the vocabularies **Dublin Core Metadata Terms** and **FOAF** may have already been imported and now you can **select a subset from one or several classes and properties** to **reuse** and build your own profile model. 

![Reuse parts of a model](img/models_workflows_reuse.png)


Learn **[how to reuse imported classes](classes.md#reuse-class)** or **[reuse imported properties](properties.md#reuse-property)** step-by-step in EntryScape Models.

