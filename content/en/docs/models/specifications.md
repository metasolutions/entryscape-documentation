## Overview

EntryScape Models offers specifications as a way of enriching vocabularies and profiles with diagrams, related documents, web pages and other resources. Each specification is formally expressed using the Profiles vocabulary and is given a unique identifier (a URI), to refer to it from other sources.

![Example of a specification with related resources](img/models_specifications_overview.png)


You can create new specifications from scratch, to refer to from other applications such as EntryScape Catalog, or share with other people.

Click on **"Create"** to start writing on a new specification.

## Create new specification
![Create new specification](img/models_specifications_createnew.png)

If you want to enter more than just the mandatory information, click on **"Recommended"** and **"Optional"** to show more input fields.

![Create new specification, recommended and optional fields](img/models_specifications_rec_opt.png)

### Mandatory fields

* **Web address** - URI for the specification

* **Specification type** - There are two types of specifications: "Normal" and "Profile". A normal specification is mainly a stand-alone, self-contained specification, while a profile specification builds upon one or several already existing specifications.

* **Publisher** - Organization responsible for making the specification available


### Recommended fields

* **Is profile of** - A specification for which this profile defines constraints, extensions, or which is uses in combination with other  or provides explanation of its usage

* **Title** - A name given to the specification

* **Description** - An account of the resource

* **Identifier** - The preferred identifier for the specification for use in circumstances when its URI can't be used

* **Contact point** - Contact information to be used for reporting errors or sending comments to

* **Has resource** - A resources that defines a particular part or feature of a specification

* **Keyword** - Keywords that describe which kind of dataset(s) the specification is relevant for

* **Date issued** - Publication date for the specification

* **Date modified** - Last date when the specification was modified


### Optional fields

* **Theme** - Indicates what kind of dataset(s) the specification is relevant for. A specification may have more than one theme.

* **Status** - The status of the specification in the context of a particular workflow process

* **Previous version** - Link to the previous version of the specification

* **Latest version** - Link to the current or latest version of the specication

* **Next version** - Link to the next version fo the specification



## Edit or Remove specification

You can **edit your specification** anytime by clicking the **Edit button**. Then you can change most of the information for your specification, except for the **Web address** which you can change if you click on **"Change link"** instead.

To remove a specification, click on **"Remove"**.

![Edit or remove specification](img/models_specifications_edit_remove.png)


## Preview specification

To get a **preview** of your specification, click on **"Preview"**.

![Preview specification](img/models_specifications_preview.png)

## Add resource

If you want to **add a resource** to your specification, click on the magnifying class next to **"Resources"**.

![Add resource to specification](img/models_specifications_addresourcebutton.png)

You can either **link to an existing resource** or **create a new resource** from scratch, to be added to your specification.

![Link or create resource to specification](img/models_specifications_linkorcreatenewresource.png)