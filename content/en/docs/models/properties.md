## Overview

**Properties** together with **classes** are the basic building blocks in EntryScape Models that can be combined to form information models. The classes and properties can either be **reused** from other models or **created** from scratch.

The following general principles appliy, you should:

1. reuse properties as far as possible.
2. avoid reuse if the semantics does not fit or is not sufficiently well maintained.
3. create new properties only when there is nothing suitable to reuse.
4. express new properties as refinements of more generic vocabulary when possible (subproperty of).
5. express the domain and range of new properties in a generic way to encourage reuse.


## Reuse property

In order to **reuse properties from other models**, you first need to import (or create) models containing the properties you want into EntryScape Models, such as FOAF for example. Read more about how to [import models](models.md#import-classes-and-properties-from-an-rdf-schema).


To **select a subset of properties** to **reuse** in your own profile model, go to your own model, choose Properties and click on **Reuse**. 

![Reuse properties button](img/models_properties_reusebutton.png)

Then you get to **select which properties** you want to reuse in your own model. If you click on the filter button at the top right corner you can choose to see only properties that belong to a specific model (in this case FOAF).

![Select imported properties to reuse](img/models_properties_reuseproperties.png)

In the list of properties, all reused properties originating from another source model are shown with link icons. 

![List of properties, both reused and self created](img/models_properties_listofproperties.png)

If you **click on the property title**, you get the options to **edit** or **remove** the property. However, keep in mind that your new profile model will depend on your source model and you should avoid changing core aspects of the property such as namespaces, domains or ranges for reused properties.

![Edit and Remove button for reused property](img/models_properties_editremovereusedproperty.png)



### Example of reuse
 Reusing the property `dcterms:publisher` on something typed as `foaf:Document` is fine since `dcterms:publisher` has the most flexible domain possible, i.e. `rdfs:Resource`. But expressing this reuse by adding the more restrictive class `foaf:Document` as domain on the `dcterms:publisher` property is bad practice since it is not what the maintainers of the property intended. Instead, this must be expressed at the profile level, i.e. by associating the corresponding field to the form at hand, see [add fields to forms](forms.md#add-fields-to-form) for how this is done.

## Create property

You can also create your own property from scratch. Click on **"Create"** and fill in the mandatory fields **Web address** and **Label**. You can either manually enter a full web address for a namespace, or you can choose a namespace that you have already imported/created and enter a name for the property.

![Create property](img/models_properties_createproperty1.png)


If you want, you can enrich your property description by turning on optional fields and enter more information.

## Edit property

To edit your property, click on the **"Edit"** button. You can change URI for you property by clicking the **"Replace URI"** button.

![Buttons Edit property and Replace URI for property](img/models_properties_editbuttons.png)


![Edit property](img/models_properties_editproperty.png)

## Remove property

You can remove a Property by clicking on the **"Remove"** button.

![Remove property](img/models_properties_removebutton.png)