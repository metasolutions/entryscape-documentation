

If you want to **use a model** or **parts of a model** from the web, an intranet or if you can upload it as a file from somewhere, you can **import it** into EntryScape Models. Once you have done this, you can either use it as it is or continue by **reusing parts** of it or **adapting** it to your own needs.

![Import a model](img/models_workflows_import.png)

Learn **[how to import a model](models.md#import-an-existing-model)** step by step in EntryScape Models.

