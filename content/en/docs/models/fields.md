## Overview

Structuring forms from an ordered set of fields is a core functionality in EntryScape Models. In most situations a field corresponds to a property and a form to a class.

The main work flow is to first **create a form**, then **create the fields** you want to have in the form and finally **add the fields to the form**.

Start by clicking on **"Fields"** and then the **"Create"** button. If you already have a form that you want to reuse to make a new form, you can click on **"Extend"** instead. 

![Createbuttons for fields](img/models_fields_createbuttons.png)


## Create field

To create a new text field, you need to specify the following:

**Field type**

* **Text field** - if you need to edit **text strings**.
* **Datatype field** - corresponding to **integers**, **dates**, **booleans** etc.
* **Select field** - corresponding to a controlled list of values, URIs or literals, e.g. **drop down lists**, a list of **checkbox alternatives**, **radiobutton alternatives** etc.
* **Lookup field** - when you need to make a request to a system to get an appropriate list of **values existing as entities**, always URIs.
* **Inline field** - when you need to use an inline subform (called an Object form) in EntryScape Models).


**Title** is mandatory for your field. 

**Purpose** is recommended to specify.

**Node type**

* **Literal** - Corresponds to plain text. The recommended option for most fields.
* **Only literal** - Used rarely.
* **Language literal** - For text strings with predefined languages.
* **URI** - When a URI is needed. (Also comes with validation.)

In most cases, "Literal" is the suitable node type.

**Property**

Here you can select a corresponding **property** to your field. 

Alternatively you can choose a **Namespace property** as well as a **local name** for your field. If you haven't declared a namespace before, you can enter a namespace URI by choosing **Manual**.

![Create field dialog](img/models_fields_createfield.png)

Save and create the new field by clicking on **"Create"** at the bottom. 

On the fields overview page, you see the **list of the fields** that you created. 

![Listing fields](img/models_fields_examples.png)


## Extend field

You can **reuse/extend** already existing fields from other forms that you have access to. Click on **"Extend"** and choose the field you want models to reuse. This is especially handy when you want to use standard metadata fields that belong to known models or vocabularies, such as "Family name".

![Extending field](img/models_fields_extendfield1.png)

![Extending field](img/models_fields_extendfield2.png)

The new labels will override all old labels from the extended field.



## Filter & search

If you have many fields, you might want to see only a subset of them, which you can do with the **Filter** function at the top. There you can filter your fields based on type.

![Filter function for fields](img/models_fields_filtersearch.png)

## Edit field

If you click on the **field title** you get to the **detailed overview** of the field. You can enter more information about the field by clicking **"Edit"**.

![Fields edit buttons](img/models_fields_editbutton.png)


When you click on **"Edit"**, you can edit information about your field:

**Title**

**Purpose**

**Status:**

* **Unstable** - May change at any time (also default value)
* **Stable** - Finished, no changes expected
* **Depracated** - Outdated/replaced 

![Edit field, first page](img/models_fields_editfield1.png)

**RDF shape**

* **Property** - Select a corresponding **property** to your field. Alternatively you can choose a **Namespace property** or enter a namespace URI manually.

* **Node type** - States if the field corresponds to a **URI**, **Blank** or **Resource**

* **Cardinality** - If the field is **Mandatory**, **Recommended** or **Optional**

* **Max** - Max number of fields of the specific type



![Edit field, second page](img/models_fields_editfield2.png)



Presenters and Editors can either see the same information, or can be shown different labels and descriptions. (The default is to show the presenter label and description for both user groups.)

Presenter:

* **Label** - Label name to be shown to end users
* **Description** - Description to be shown to end users

Editor:

* **Edit label** - Label name to be shown for editors
* **Edit description** - Descriptions to be shown for editors

![Edit field, third page](img/models_fields_editfield3.png)

* **Placeholder** - Example text in the field that disappears when the user enters information
* **Validation help** - Message shown when validation fails, such as "This must be an email adress"
* **Pattern** - Regular expression, such as  "https?://.+" for URLs
* **Value template** - For adding invisible prefixes such as "mailto:"

![Edit field, fourth page](img/models_fields_editfield4.png)
* **Depends on Field** - When the field is secondary to another field and will appear together when chosen
* **Language** - Language for the field
* **CSS class** - Predefined CSS classes (such as card-groupfrom RDForms) or your own CSS classes.


![Edit field, fifth page](img/models_fields_editfield5.png)


## Edit attributes

With the option **"Edit attributes"** you can choose how the field should be presented and behave, like for displayed as horizontal or vertical radiobuttons, a dropdown list, multiline textfield etc.

![Edit attributes button](img/models_fields_editattributesbutton.png)

![Edit field attributes](img/models_fields_editattributes.png)



## Change field type

You can change the underlying field type by clicking the button **"Change type"**. Remember though that any type specific data will be lost, if you change to a different type.

![Change field type](img/models_fields_changetype.png)



## Edit options

For Select field types, you get to add your own values. 

![Edit options button](img/models_fields_editoptionsbutton.png)

![Edit options](img/models_fields_editoptions.png)


## Add fields to form

When you are done specifying your fields, go to your **form** and [add your fields to the form](forms.md#manage-fields-in-form).

