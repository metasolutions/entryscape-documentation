




You can use EntryScape Models to **adapt an existing model** such as DCAT-AP and remodel it into a different version, by extending and overriding the existing model entities with forms and fields.

![Adapt model](img/models_workflows_adapt.png)


One example of a suitable adaptation situation could be the Swedish DCAT-AP-SE 2 model, which differs from the generic DCAT-AP2 model. For example, the Swedish DCAT-AP-SE 2 model requires a *mandatory* publisher of a catalog while in the generic model, adding a publisher is just *recommended*. Making such an adaptation of a model is possible, by importing DCAT-AP 2 and then override the fields in question with new settings.

**Adapting existing forms and fields are done in two steps:**

1. **Extend a form**, by copy and override
2. **Adapt fields** to your extended form

Learn **[how to extend a form](forms.md#extend-form)** and **[how to manage and adapt fields](forms.md#manage-fields-in-form)** in EntryScape Models.








