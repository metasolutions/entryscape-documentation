# Getting started with EntryScape

You need a user account in order to be able to use EntryScape. Depending on the configuration of your EntryScape instance you may be able to perform a sign-up yourself or an administrator needs to create an account for you. If your organisation uses Single Sign-On in EntryScape you may login with your organisation's usual credentials.  

## Login

Accessing EntryScape without being logged in redirects you automatically to the login screen where you are required to enter your credentials, see screenshot:

![Screenshot of login dialog](img/login.png)

After successfully logging in you are redirected to the start view.

## Sign-up

If you don't have an account yet, you can create one by clicking on the "Create account" link at the bottom of the login dialog. This presents you with the sign-up dialog as seen below:

![Screenshot of sign-up dialog](img/signup.png)

After entering the required information you need to confirm your e-mail address by visiting the URL that has been sent to you by e-mail in order to create your account.

## Reset your password

In case you have forgotten your password or if your administrator has created an account for you without a password, you need to perform a password reset. You can open the password reset dialog by clicking the link "Forgot your password?" in the login screen.

You are asked to provide a new password directly, but it must be confirmed by visiting the URL that is sent to the e-mail address of your account.

## Switching between modules and views

In the start view you can choose between the different EntryScape modules that are enabled for your organisation and user:

![Screenshot of start view](img/start_view.png)

The general way of working with EntryScape builds on module specific views which become visible after you enter a module and choose a manageable object (e.g. a data catalog, terminology or generic project).

The views of a module are typically divided by the type of information that is being managed.