# Welcome to the documentation for EntryScape

!!! info "This documentation is intended primarily as a guide for end users."

EntryScape is an information management platform based on open standards from the [W3C](https://www.w3.org) standards body on [Linked Data](https://www.w3.org/standards/semanticweb/). EntryScape supports the entire data management lifecycle, from import to access.

EntryScape consists of different application modules that can be used together or separately:

### [Catalog](catalog/index.md)

Manages and publishes data catalogues and datasets according to the DCAT-AP metadata standard. Datasets can be public APIs, data summaries, document collections, etc. Learn more about [Catalog](catalog/index.md).


### [Terms](terms/index.md)

Create terminologies intended for reuse according to the [SKOS standard](https://www.w3.org/2009/08/skos-reference/skos.html). Build on existing SKOS terminologies or create new ones. Learn more about [Terms](terms/index.md).


### [Workbench](workbench/index.md)

Structure and collaborate on your data models with linked data. We'll help you customize Workbench to meet your metadata needs. Learn more about [Workbench](workbench/index.md).


### [Registry](registry/index.md)

Manage data sources, harvest and convert between metadata standards. Metadata validation is automated and the data publisher receives immediate feedback to take corrective action. Learn more about [Registry](registry/index.md).


### [Models](models/index.md)

Build, reuse, adapt and share information models and specifications for linked data. A data modeling tool to ensure interoperability. Learn more about [Models](models/index.md).


### [Blocks](blocks/index.md)

Embed information stored in EntryScape into web pages or CMS using shortcodes. For example, build a data portal on a standard web page with or without a CMS using metadata from EntryScape. Blocks works with all forms of static pages and dynamic pages via CMS such as WordPress, SiteVision, EpiServer, Drupal and others. Learn more about [Blocks](blocks/index.md).


### [Admin](admin/index.md)

Administer one or more application modules with Admin, which helps you administer users, permissions and group management, among other things. Learn more about [Admin](admin/index.md).


### User support via [EntryScape Community](https://community.entryscape.com/)

The [EntryScape Community](https://community.entryscape.com/) user and support forum provides help and support for your questions from users and MetaSolutions, the main developer of EntryScape. These questions and answers in turn lead to improvements in the open user documentation.

!!! info "Please note that the EntryScape Community is a community-based forum. Anyone can get answers from the community for free. For example, if you have EntryScape Free or EntryScape Starter, you will need a support contract that includes getting answers from MetaSolutions on the forum."

***

Learn more about EntryScape and MetaSolutions offerings at [entryscape.com](https://entryscape.com).
