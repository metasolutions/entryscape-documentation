# Overview

Registry consists of several complementary parts:

![Start view of EntryScape Registry](img/start_view.png)

### Status report

Provides an overview and analysis of which organizations provide DCAT-AP metadata and a web page referring to their PSI data.

### Search

A search interface for browsing through all data catalogs and datasets that are harvested or managed by this Registry instance.

### Organizations

An interface to manage organizations and their harvesting details.

### Toolkit

A DCAT-AP toolkit with a metadata validator and other tools.