# Using the toolkit

The Registry's Toolkit is designed specifically for data catalogs that are described using DCAT-AP.

!!! info "The Toolkit operates on the currently loaded catalog source and nothing else."

## Specifying a catalog source

In the tab "Catalog source" it is possible to load DCAT-AP metadata. The DCAT-AP metadata may be provided as an uploaded file, a URL or it can be pasted as RDF/XML or RDF/JSON. For demonstration and testing purposes it is possible to load an example catalog by clicking the corresponding button at the bottom.

![Screenshot of loaded example](img/toolkit_example.png)

The catalog loaded in this tab is then used for the other tools of the Toolkit.

## Validating catalog metadata

The Registry's Validation tool operates on five of the main entity types of DCAT-AP: Catalog, Dataset, Distribution, Organization and Contact Point.

The tool provides a validation report that allows a detailed analysis of DCAT-AP metadata. Issues are flagged per statement and in addition to the top-level validation report it is possible to access validation report per entity type by clicking the corresponding link after expanding the panel.

![Screenshot of detailed validation view](img/toolkit_validation.png)

## Merging multiple catalogs

Additional DCAP-AP catalogs can be merged with the already loaded primary catalog. The datasets and all other entities (except the catalog itself) of the additionally loaded catalogs are merged into the primary catalog. The resulting metadata can be accessed through the source tab and downloaded from there.

## Exploring a catalog

Using the Explore tool the datasets of the loaded catalog can be browsed using a list of datasets. Clicking a dataset's list row shows the dataset's metadata in a side dialog from where also other linked entities may be accessed. 

![Screenshot of catalog explorer](img/toolkit_explore.png)

## Converting catalog metadata

The Convert tool can be used to convert the metadata from older DCAT-AP versions to a current one. A list of changes is presented in a table and after clicking the conversion button the catalog metadata in the source tab is updated and can be downloaded from there.

![Screenshot of conversion table](img/toolkit_conversion.png)