# Managing organizations and harvesting sources

In Registry an organization is also a harvesting source, so in order to be able to harvest a data catalog an organization has to be created.

The sections below describe how harvesting sources are created and managed.

## Adding an organization

Click the creation button <input type="button" value="+"> in the upper right corner to create an organization. Only few details are necessary: the name of the organization, a short description, the type of data catalog (DCAT, CKAN, etc) and the URL of the harvesting source.

![Screenshot of creation dialog](img/organization_creation.png)

Once the organization is created it may take some time until the URL is checked and harvested from.

By clicking the cog wheel <input type="button" value="&#9881;"> in the organization's list row you can access the configuration dialog to edit the organization and harvester details.

## Check the harvesting status

The organization's harvesting status can be accessed by clicking the organization's list row or by clicking the corresponding menu item after clicking the cog wheel <input type="button" value="&#9881;">.

The latest harvesting attempts are shown with some basic statistics such as amount of datasets harvested. If a harvesting source was found it is possible to access a validation report per harvesting attempt.

The button "Reharvest" adds the organization to the harvesting queue which will trigger a reharvesting process within a short while. 

![Screenshot of harvesting status](img/harvesting_status.png)