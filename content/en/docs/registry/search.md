# Search catalogs and datasets

The Registry's search view allows to search through all metadata that is managed and harvested by this registry instance.

The left pane lists all organizations with one catalog per organization and sorted descending by amount of successfully harvested datasets. The right pane contains a list of all datasets. 

![Screenshot of search GUI](img/search.png)

If an organization is selected only the datasets of this particular organization are shown in the right pane.

![Screenshot of organization list](img/search_detail.png)