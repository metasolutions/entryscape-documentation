# View the status report

The Registry's status report acts as a dashboard to get an overall picture of how many organizations publish Open Data and/or provide a human-readable web page about their Open Data (cf. Sweden's `/psidata` recommendation).

![Screenshot of status report at registrera.oppnadata.se](img/status_report.png)