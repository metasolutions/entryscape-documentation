# Entities

A project contains **entities** (objects) of various **entity types**, such as documents, events, artwork, photos, authors, and so on. They are listed in the project overview, along with the number of each type. The entity types listed are determined by the selected project type and are often predefined, but in some cases they can be [configured](projects.md#configure-entity-types) by the user. 

The example below shows a Tourism project with predefined entity types in the form of Activities, Accommodations, Food, Photos, Hikes, and so on.

![A tourism project with associated entities](img/workbench_entities_overview.png)


## Create new
<a id="create-new"></a>

To create a new entity (object), click on the entity type and then on the **"Create"** button. The example below shows the entity type "Accommodation" in the project "Tourism", but it could just as easily be brochures, activities, artists or anything else. Here you can fill in the metadata and create relations to other entity types.

![Create new entity](img/workbench_entities_createnewentity.png)

The information fields displayed here also depend on the selected configuration (project type with its associated entity types). 


## Describe
<a id="describe"></a>

Keep in mind that more specific descriptions of published projects and entities will make it easier for others to find and reuse the metadata. 

The input window contains input fields at three different levels: **mandatory information** (\*must be filled in), **recommended information** (good to fill in), and **voluntary information**. You can toggle which input fields are displayed using the sliders at the top. 

![Create new activity](img/workbench_entities_man_rec_opt.png)


## Edit
<a id="edit"></a>
You can go in and **change** or **fill in more information** about an entity later, e.g. add information in a new language or upload new images.
To do this, either click on the edit pencil on the far right, or click on the entity to get to the overview page and then click on the **"Edit"** button.

![Edit pen for an entity](img/workbench_entities_editentity.png)

![Edit button on the overview page of an entity](img/workbench_entities_editbutton.png)

## Manage files and links
<a id="manage-files-and-links"></a>

For entities that have associated **files**, such as images or documents, this section describes how to upload, download and replace files in EntryScape Workbench. It is also possible to **link** to files, web pages and other resources available on the web.

### Upload file
<a id="upload-file"></a>

To **upload a file**, click on **File** and then the magnifying glass icon.

![Upload file](img/workbench_entities_uploadfile.png)


### Download file
<a id="download-file"></a>

You can also **download files** that are uploaded by going to the entity's overview page and clicking on the **“Download”** button.

![Download file](img/workbench_entities_downloadfile.png)

### Replace file
<a id="replace-file"></a>
For entities that consist of **files** that you upload (e.g. image files or documents), there is a **“Replace file”** button that allows you to quickly and easily replace the file associated with the file's metadata description in a convenient way. 

![Replace file button](img/workbench_entities_replacefilebutton.png)
Locate the new file and click on **“Replace file”**.

![Replace file](img/workbench_entities_replacefile.png)

### Link to resource
<a id="link-to-resource"></a>

To **link** to a file, web page, image or other resource accessible on the web, type or **paste the URL** in the web address field and add a descriptive name.

![Edit button on the overview page of an entity](img/workbench_entities_linktoresource.png)


## Search
<a id="search"></a>

If you have a lot of entities, e.g. a large collection of images or documents and are looking for a specific document, you can use  the **search box** at the top. **Enter at least 3 characters** to get a match.

![Search box in Workbench](img/workbench_entities_search.png)

## Table view and list view
<a id=“table-view-and-list-view”></a>

The default view in EntryScape Workbench is to display data in a **list view**. However, if you want to view and edit many entities at the same time on the same page, you can use **table view**. You can switch to table view by clicking on the **table icon** at the top.

![Table icon to switch to table view](img/workbench_entities_tableview1.png)

In table view, you can also decide how many columns you want to see at once. By clicking on **“Columns”** you can choose which columns you want to see or hide.

![Column selector](img/workbench_entities_columns.png)

To edit a table cell, click on the text in the cell. Click on the **upper X** or outside the edit box to **close** it. After you have made your changes to all table cells, don't forget to click the **Save button** at the bottom.

![Table view with editing options](img/workbench_entities_tableview2.png)

To switch back to list view, click on the list icon at the top right.

![List view icon](img/workbench_entities_listview.png)


## See detailed information
<a id=“see-detailed-information”></a>

If you want to see more detailed information about a project or an entity, you can click on the information icon. 

![Information icon](img/workbench_informationicon.png)

This will open a dialog window where you can see the metadata for the entity and all other entities that link to and from your entity. Read more about it on the [detailed information page](detailed_information.md).

## Remove
<a id=“remove”></a>

To **remove** an entity, go to the entity's overview page and click on the **Remove** button.

![Menu item Remove entity](img/workbench_entities_removeentity.png)

Note that if you try to delete an entity that is **linked to** from somewhere else, e.g. a main image used to represent a hotel, you will get an error message that you must first replace the main image with another image on the hotel page before you can delete the old image.

![Error message about deleting linked entity](img/workbench_entities_removelinkedfile.png)
