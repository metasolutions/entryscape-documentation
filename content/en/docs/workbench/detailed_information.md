If you click on the **information icon**, you will open a dialog showing more **detailed metadata** for a document, an event, a work of art or other entity of your choice.

![Information icon in list](img/workbench_detailedinformation_icon1.png)

You can find the information icon both in the list of entities and on the overview page for a single entity.

![Information icon on overview page](img/workbench_detailedinformation_icon2.png)

You will see three tabs in the dialog window that pops up: **"Information"**, **"About"** and **"References"**. 

![information dialog, three tabs](img/workbench_detailedinformation_tabs.png)

## Information
<a id=“information”></a>

In the first tab, **"Information"**, you will find metadata for your chosen entity, including all other entities that **your entity links to**. 

![Information dialog, Information tab](img/workbench_detailedinformation_information.png)

## About
<a id="about"></a>

In the second tab, **"About"**, you will find the **URI** to your entity. It's a quick way to find and **copy the URI** when you need to refer to it elsewhere.

![Information dialog, About tab](img/workbench_detailedinformation_about.png)

## References
<a id="references"></a>

In the third tab, **"References"**, you will find all entities that **refer to your entity**. 

![Information dialog, tab References](img/workbench_detailedinformation_references.png)


## Languages
<a id="languages"></a>

In the top right corner there are two **language icons**, that allow you to view the information in the three tabs in **different languages**.

![Information dialog, languages](img/workbench_detailedinformation_languageicons.png)

If you click on the **globe icon** you will see all metadata about your entity presented in **all entered languages simultaneously**. To turn off all languages and return to the original one language, click on the globe icon one more time.

You may also view your metadata in **one language at a time** by clicking on **the right language icon** and choose language from a long list. 

![Information dialog, list of languages](img/workbench_detailedinformation_languagelist.png)
