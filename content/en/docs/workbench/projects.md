
Workspaces in Workbench are called **projects** and they in turn contain different types of **entities** (objects).

On the main page you can see projects that you have already created or have access to. It also shows whether the project is **public** or not, i.e. whether it is searchable by others. If you do not see a project here, you need to start by creating one, or ask for [permission](#permissions-and-sharing-settings) to the project by the group manager.

![Overview view of projects](img/workbench_projects_listprojects.png)


## Create a project
<a id="create-a-project"></a>

To **create a new project**, click on the **“Create”** button.

![Create button](img/workbench_projects_createbutton.png) 

If your organization has several different types of projects to choose from, you must first select **Project type** for your project. The project type also determines which **entity types** will be included in your project.

![Create project dialog](img/workbench_projects_createproject.png)

Next, fill in the **project name**, i.e. a title such as “Document management”. Keep in mind that the title should be short and that all the words will be searchable. Also fill in a short **project description**. 

A newly created project is always non-public. The creator of the project automatically becomes  the **group manager** for it, which means that you manage other users' access to the project. 


## Permissions and sharing settings
<a id="permissions-and-sharing-settings"></a>

In EntryScape Workbench a user can generally be either a **group manager** or a **member** with read and write permissions. The group manager can give other users access to a project and publish the project. Members can edit the metadata of the project and create and manage entities for the project, but not publish. You can be a member of one or several projects depending on your work role and how data is organized.

If needed it's possible to get a higher level of permissions in specific cases, admin permissions, if you contact the MetaSolutions support. In such cases your organization must already have established a work routine, work group and metadata management or open data work in the management organization. 

If you don't have access to the project you should work with, you need to ask the group manager (or admin) for access to that project. (The person responsible is likely the head of the department, manager or similar in your organization.) 

Group managers can change **sharing settings** for other users by clicking on the three-point menu and choose  **"Sharing settings"**. 

![Menu option Sharing settings](img/workbench_projects_menusharingsettings.png)


There you can **add** and **remove users** to the group who has access to a project. To add a user to the group, click on "Add user to group". You can also choose **which level of permissions** the user should have (group manager or member) by clicking on icon before the name.

![Permission settings for users of a catalog](img/workbench_projects_sharingsettings2.png)

To **remove** a user from the group, click on the three-point menu and choose **"Remove"**.

![Menu option Remove user from group](img/workbench_projects_sharingsettings3.png)


## Edit project
<a id="edit-project"></a>

You can edit the information about your project by clicking on the **three-dot menu** and selecting **“Edit”**. 

![Menu item Edit project](img/workbench_projects_menueditproject.png)

In edit mode you can change information or add information in **different languages**.

![Edit project](img/workbench_projects_editproject.png)


## Configure entity types
<a id="configure-entity-types"></a>

It is only in **special cases** that you need to go in and add or remove entity types, as in most cases entity types are already predefined in the selected project type for the project. If you need to **change entity types** for a project, you can click on the three-dot menu for a project and select **“Configure entity types”**.

![Menu item configure entity types](img/workbench_projects_menuconfigureentitytypes.png)

If you already have a predefined project type to start from, the entity types cannot be changed, as in the case of the project type “Tourism” below where the entity types are grayed out and thus locked.

![Configure entity types, locked](img/workbench_projects_manageentitytypes_tourism.png)

In other cases where you need to go in and change entity types, you can select which entity types you want to have in the project.

![Configure entity types, selectable](img/workbench_projects_manageentitytypes_default.png)

## Remove project
<a id="remove-project"></a>

To **remove** a project, click on the project's three-dot menu and select **“Remove”**.
![Menu item Remove project](img/workbench_projects_menuremoveproject.png)
