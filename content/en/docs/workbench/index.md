

**EntryScape Workbench** is a generic **metadata tool** that is customized to handle organisations' own information models. This could be publications, tourism data, artwork, accounts payable or other collections of data that should be published as open or shared data.

Workspaces in Workbench are organized in **projects**, and they in turn contain different **entities** (objects), which are described with metadata and relationships. 

![Project examples in Workbench](img/workbench_overview_projectexamples.png)

## Project example 

The example below shows a tourism project that contains a number of different **entity types**, such as accomodations, activities, events, etc. We have chosen to list entities of type **activities**.  

By clicking on an activity, you can edit its metadata such as name, description, webpage, etc. and connect the activity to other entities such as photos, nearby accomodations, restaurants etc. Simply linked open data. 

![EntryScape Workbench, actitivities](img/workbench_overview_editgavleborg.png)

The collected information can then be **presented on a webpage** to potential visitors in the region. In this example, various activities for visitors to the region Gävleborg are presented using our presentation library **EntryScape Blocks**. Visitors can click through from the activities to restaurants or accommodations nearby.

![Presentation of EntryScape Workbench activites using EntryScape Blocks](img/workbench_overview_presentationgavleborg.png)




