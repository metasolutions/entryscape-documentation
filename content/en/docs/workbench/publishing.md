## Publishing projects and entities
<a id=“publishing-projects-and-entities”></a>

All newly created projects are unpublished by default, so only users with the appropriate permissions can view and manage the project. If the project's publishing status is green, it means that the project's metadata is publicly readable and ready to be harvested by e.g. open data portals. 
The **Publish button** allows you to **publish** or **unpublish** your project.

![Publish button for a project](img/workbench_publishing_publishproject.png) 

If your project and entity types are configured to allow **publishing** and **unpublishing** of entities, you can set this on an entity's overview page. Click the **Publish button** for the entity to make it public. Note that the project under which the entity is located must be **published** for the entity to be published as well.

![Publish button for an entity](img/workbench_publishing_publishentity.png) 

The entities that have a **green publish icon** (globe) are published, as in the example below.

![Publishing icon for an entity](img/workbench_publishing_publishedentity.png) 

If you don't see a publish icon (or publish button) for your entities, it means that they are **automatically published/unpublished** with the project when it is published/unpublished.


## Revision history
<a id=“revision-history”></a>

Some versions of EntryScape have an automatic **revision history** of projects and entities. The version history can be found under the three-dot menu for projects and on the overview page for entities.

![Menu item Versions for a project](img/workbench_publishing_revisionsproject.png)

For entities, there is a blue button to access the revision history.

![Version button for entities](img/workbench_publishing_revisionsentities.png)

If you are a group manager or admin, you can **revert** to previous versions of metadata for the entity of your choice. Regular members have permissions to view different versions, but not revert to previous versions.

![Version history](img/workbench_publishing_revisionhistory.png)


## Publish with EntryScape Blocks
<a id=“publish-with-entryscape-blocks”></a>

Many users who enter metadata into the EntryScape Workbench also use the **EntryScape Blocks** presentation library to display the information on a web page. Contact EntryScape support for more information about Blocks or read more at [entryscape.com](https://entryscape.com/sv/produkter/blocks/).

### Example

Below is the **edit view in EntryScape Workbench** for a destination in a tourism project. Here the user has entered texts, images and other metadata about the destination.

![EntryScape Workbench editing mode, example](img/workbench_publishing_editexample.png)

The text and images are then presented in a neat layout on a web page using the **EntryScape Blocks** presentation library, with links to related organizations and related objects connected via shared data in EntryScape.

![Example of presentation using EntryScape Blocks](img/workbench_publishing_blocksexample.png)


