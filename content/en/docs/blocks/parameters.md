# Parameter scope

A central part of using blocks is to provide the right parameters. The parameters are either **block level parameters**, **page level parameters** or **global level parameters**:

## Block level parameters

Each block defines which parameters it supports, e.g. the text block supports a `property` parameter that says which RDF property to present. Here is an example of two instances of the text block presenting a person using two different values for this parameter.

    <span data-entryscape="text" data-entryscape-property="foaf:givenName"></span> 
    <span data-entryscape="text" data-entryscape-property="foaf:familyName"></span>

Which block level parameters are available depends on the block, see the [reference](reference.md) for details.

## Page level parameters

There are two main use-cases for page level parameters. First, the page level parameters can be used to load a specific entry either by using the fragment as described in [entry from page parameters](entries.md#entry-from-page-parameters) or more indirectly as described in [entry from the url of the page](entries.md#entry-from-the-url-of-the-page). Secondly, the page level parameters can be used to describe state for the blocks `searchList`, `facets`, `searchFilter` and `multiSearch` to allow linking to a specific search.

## Global level parameters

Global level parameters are parameters that may apply to all or at least several blocks in a page. A difference from page level parameters are that the global level parameters are defined at configuration time and do not change depending on the actions of the user. Global level parameters may easily be shared between pages by including them in a separate configuration file.

A good example of a global level parameter is the `entrystore` parameters which provides a base URL to the entrystore instance to communicate with. There are three options for providing global level parameters:

1. In a global `__entryscape_config` variable set in javascript (potentially set in a file shared across pages)
2. In a special `config` block
3. In any block

The last option may be confusing as it mixes global and block level parameters. However, in situations where you have a single block in a page this option makes it easier to write that page.

**Example option 1**

    <script>
      window.__entryscape_config = {
          entrystore: "http://example.com/store"
      }
    </script>

**Example option 2**
  
    <span data-entryscape="config"
          data-entryscape-entrystore="http://example.com/store"></span>
    
**Example option 3**

    <span data-entryscape="text" data-entryscape-property="foaf:givenName"
          data-entryscape-entrystore="http://example.com/store"></span>

**Note 1**: option 1 has to be written exactly like in the example above while option 2 and 3 can be written using [different syntax](syntax.md).

**Note 2**: only a few of the global level parameters can be detected on all blocks, the separate page about [available global level parameters](global.md) indicates which are detectable on all blocks.

**Note 3**: the global `__entryscape_config` variable is the same as provided in the config/local.js which is part of the
 build of EntryScape Blocks. Hence, it is possible to build a special version of EntryScape Blocks with certain parameters hardcoded, for instance which entrystore instance to connect to.
