# Blocks reference

There are some parameters that applies to a lot of blocks, and we avoid listing them per block to avoid clutter. There are basically three categories:

**Entry dependant blocks** - marked with `entry dependant`

Many blocks require an entry to work as expected. To see which options that are available on how to provide the entry see the [entries page](entries.md).

**Conditional visibility** - marked with `filter condition`

Nearly all blocks can be hidden if a search filter is present or not. The available filters are defined in the [global level parameter collections](collections.md). The filtering is enabled by providing the `if` parameter. Like this:

    <span data-entryscape="text"
          data-entryscape-if="collectionname==value"></span>
or like this if any value for that filter:

    <span data-entryscape="text"
    data-entryscape-if="collectionname"></span>

It is also possible to give multiple values by using an object or a comma separated string. Like this:

    <span data-entryscape="text"
    data-entryscape-if="collectionname,anothercollectionname"></span>

**Accessibility attributes** - marked with `aria support`

Many of the blocks support parameters that provide accessibility attributes, i.e. attributes like aria-label, aria-describedby, aria-live etc. The following table lists the parameters and how they translate to various aria attributes. Note that we also include the title attribute in this list for convenience.

| Parameter   | Value  | Translates to                                                       |
|:------------|:-------|:--------------------------------------------------------------------|
| title       | string | title                                                               |
| label       | string | aria-label                                                          |
| labelledby  | id     | aria-labelledby                                                     |
| describedby | id     | aria-describedby                                                    |
| live        | string | aria-live, the value should be one `off`, `polite` and `assertive`. |

## helloworld
This block does nothing more than print out the text "Hello world!" and is only used to make sure that the basics of blocks works in a specific setting.

## text
`entry dependant`, `filter condition`

| Parameter | Value              | Description                                                                                                                                                                                                                                                                                                                                                                                                                       |
|:----------|:-------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| property  | RDF property       | Picks the value for the given property.                                                                                                                                                                                                                                                                                                                                                                                           |
| content   | string template    | Constructs a string from the string template that may contain variables, a variable should correspond to a property by being expressed via a namespace, e.g. foaf:name. The variables title and description are built in and corresponds to well known title and description properties. If multiple values exist for title and description the best are selected based on language preferences, all others are chosen arbitrary. |
| fallback  | handlebar template | If there is no suitable value found (without content parameter) the fallback handlebar template is used. Can be a simple string like `No value found` to a more complicated handlebar template containing blocks, the current entry is passed. If the content parameter is provided the fallback must be a simple string and it will be used to replace variables that cannot be resolved.                                        |

**text example 1 - no parameter**
The block can be used without any parameters, if so it tries to find a suitable label for the current entry by relying on the default label properties, see the [global level labelProperties parameter](global.md#labelproperties) for how to configure this.

    <span data-entryscape="text"></span>

**text example 2 - content parameter**

The default behaviour can be overridden by providing the `content` parameter where anything looking like `${ns:prop}` will be replaced with the corresponding value of the property (if it exists):

    <span data-entryscape="text"
          data-entryscape-content="${vc:firstName} ${vc:lastName}"></span>

Note that unless the namespace is one of the well known it has to be declared in the [global level namespaces parameter](global.md#namespaces).

**text example 3 - property parameter**
It possible to retrieve the value of a single property as well. The example also shows the use of a fallback value:

    <span data-entryscape="text"
          data-entryscape-property="vc:firstName" data-entryscape-fallback="No name given"></span>

**text example 4 - fallback can be a template**
Below we see how the fallback value is a template which can contain other blocks, in this case the text block again.

    <span data-entryscape="text"
          data-entryscape-property="foaf:name" 
          data-entryscape-fallback='No name given for {{text property="foaf:mbox" }}'></span>

## link
`entry dependant`, `filter condition`

| Parameter  | Value           | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|:-----------|:----------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| click      | url             | The absolute or relative path to a webpage whereto the link should go. The current entry will be indicated by appending fragment parameters using the contextid and entryid unless the clickkey says otherwise. If the url contains the prefix `esb:` the rest of the url is a patterned url that will be constructed with help of the clickmask and expansion of variables, see example 6 nd 7 below.                                                                                                                  |
| namedclick | string          | Similar to url, but reuses a click definition from the [links global parameter](global.md#clicks).                                                                                                                                                                                                                                                                                                                                                                                                                      |
| property   | RDF property    | An alternative to click and namedclick. If given a link will be constructed from the value of the property in the metadata.                                                                                                                                                                                                                                                                                                                                                                                             |
| clickmask  | string          | Used together with the `esb:` prefix in click parameter (or in the global links parameter if namedclick is used) to extract part of the resourceURI. See example below.                                                                                                                                                                                                                                                                                                                                                 |
| clickkey   | string          | Specifies another parameter to use when constructing the link, replacing the default usage of contextid and entryid. Must be used in conjunction with the `clickvalue` parameter to indicate which value should be provided for the key.                                                                                                                                                                                                                                                                                |
| clickvalue | RDF property    | Indicates which property from the metadata graph to use as value for the clickkey. If "resource" is given it is interpreted to mean that the resourceURI of the entry should be used as value.                                                                                                                                                                                                                                                                                                                          |
| content    | string template | If provided it allows controlling the name of the link in the same way as for the text block. A string is constructed by replacing variables in the template. A variable should correspond to a property by being expressed via a namespace, e.g. foaf:name. The variables title and description are built in and corresponds to well known title and description properties. If multiple values exist for title and description the best are selected based on language preferences, all others are choosen arbitrary. |
| fallback   | string          | Used together with the `content` parameter by providing a default value for variables that cannot be resolved.                                                                                                                                                                                                                                                                                                                                                                                                          |

Note: several of the examples below can be accomplished by using the `template` block, both example 4 and 5 falls in this category. However, the link block does come in handy when there is a special logic in how to insert fragment parameters or advanced construction of the link based on the resourceURI.

**link example 1 - simple use of the click parameter**

    <span data-entryscape="link" data-entryscape-click="/otherpage"></span>

Remember that the link to "/otherpage" will have [page parameters](entries.md#entry-from-page-parameters) referencing the current entry appended.

**link example 2 - using a global click definition**

    <span data-entryscape="link" data-entryscape-namedclick="artist"></span>

Here the assumption is that there is a [global level click definition](global.md#clicks) for the named click `artist`. The resulting link will be the same as if the click parameter where provided directly in this block.

**link example 3 - using clickkey parameter**

Sometimes you want to provide another kind of page parameters, for instance a filter to a search oriented page or provide the lookup of an entry via another identifier than the combination of contextid and entryid.

    <span data-entryscape="link"
          data-entryscape-click="/otherpage"
          data-entryscape-clickkey='org'
          data-entryscape-clickvalue='dct:publisher'></span>

The resulting link will go to `/otherpage` with the fragment `esc_org=publisherurl` where publisherurl is discovered by taking the value of dct:publisher in the metadata.

**link example 4 - link with the resourceURI as clickvalue**

    <span data-entryscape="link"
          data-entryscape-click="/otherpage"
          data-entryscape-clickkey='org'
          data-entryscape-clickvalue='resource'></span>

Similar to example 3, but the value is the resourceURI of the entry instead, i.e. the fragment will be `esc_org=resourceURI`.

**link example 5 - link via property**

    <span data-entryscape="link"
          data-entryscape-property="rdf:seeAlso"
          data-entryscape-fallback="No link found"
          data-content="${label}"></span>

In this example the link is taken directly from the value of a specific property.

**link example 6 - patterned link**
If you want to link to a certain url where the contextid and/or entryid is provided as part of the link, not inserted in the fragment. 

    <span data-entryscape="link"
          data-entryscape-click="esb:/artist${context}_${entry}"></span>

**link example 7 - masked patterned link**

    <span data-entryscape="link"
          data-entryscape-click="esb:/artist/${1}"
          data-entryscape-clickmask="https://example.com/artist/(.*)"></span>

The clickmask always works against the resourceURI of the entry and extracts one or several values from it via regexp capturing groups (indicated with parentheses). The values can then be referenced (via the capturing group number) in the patterned url (provided in the `click` parameter).

## date
`entry dependant`, `filter condition`

| Parameter     | Value        | Description                                                                                                                                                     |
|:--------------|:-------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| property      | RDF property | May be an RDF property (namespaced or not) or one of the special values `modified` and `issued`, if nothing is provided `modified` is assumed.                  |
| format        | string       | A date formatting option according to the [moment library](https://momentjs.com/docs/#/displaying/), uses the 'l' as default corresponding to a localized date. |
| tooltipformat | string       | Same as format but used for a tooltip, uses the 'LLLL' as default corresponding to a more explicit localized date an time expression.                           |

**multiSearch example 1 - modification date of an entry**

    <span data-entryscape="date"></span>

**multiSearch example 2 - explicit date in metadata, only the year**

    <span data-entryscape="date"
          data-entryscape-property="dcterms:date"
          data-entrycape-format="YYYY"></span>

## template
`entry dependant` (for some templates), `filter condition`

| Parameter            | Value              | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|:---------------------|:-------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| template             | handlebar template | See the separate section on [handlebar templates](template.md)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| class                | string             | Adds a provided html class to the node where the template is inserted.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| relation             | RDF property       | Entry reachable via relation (requires entry given, see [section on entries](entries.md))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| relationinverse      | RDF property       | Entry inversely reachable via relation (requires entry given, see [section on entries](entries.md)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| rdftype              | RDF class          | Entry will have to be instances of this class, may be array                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| dependencyproperties | RDF property       | Properties that point to entries need to be preloaded if values are used in the template                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| constraints          | object             | Use when the rdftype is not enough, the key value pairs corresponds to predicate value pairs. Indicate that a value is a uri by surrounding it with `<` and `>`. If the value is an array the members are combined as a disjunction (or:ed together). Add a ~ before the key to negate ```"~key":"value"``` => ```NOT key:value```.  The key can also be one of the special values `'uri', 'resource' or 'query'`. 'uri' and 'resource' are used for adding those fields to the query. Setting '~uri' to the entry uri will exclude that entry from the result. The 'query' option is more advanced and lets you add a structured query object to the constraint. For correct syntax consult the documentation on [entrystore-js](https://entrystore.org/js/stable/doc/module-store_SolrQuery.html) |
| sortOrder            | string             | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |

This block allows you to write handlebar templates which may contain:

* Regular HTML
* Build in handlebar directives 
* Entryscape Blocks extra handlebar directives like `prop`, `eachprop`, `ifprop` etc..
* All the Entryscape Blocks (exposed as handlebar directives)

Note that if the template contains the use of blocks like text or link or the blocks-handlebar specific directives prop, ifProp, or eachProp it will require an entry to work. In that case, see the [entries page](entries.md) for how to specify it.

**template example 1 - simple template**

    <span data-entryscape="template"
          data-entryscape-template='{{#ifprop "dcterms:title"}}{{text}}{{/ifprop}}'></span>

**template example 2 - collect multiple blocks in a template**

    <script type="text/x-entryscape-handlebar">
        Given name: {{prop foaf:firstName}}<br>
        {{ifprop foaf:givenName}}Family name: {{prop foaf:familyName}}{{/ifprop}}
        <hr>
        {{viewMetadata rdformsid="PersonOrGroup_RDFormsId"}}
    </script>

Note that this example uses the special [script syntax](syntax.md#option-4-block-handlebar-script-use-for-large-templates) as it allows line breaks and makes the template simpler to read.

## icon

`entry dependant`

Render an icons for each value of a property
The configurable property 'icons' will decide which icon to render. It is also possible to set a `fallback` to handle when there is no matching icon.

It is possible to use fontawesome style icons `<i class="fas fa-icon"></i>` by pairing the values to fontawesome classNames and
img-icon by pairing the values to img-url. To avoid writing long urls use global parameter [namespaces](global.md#namespaces)

| Parameter  | Type                                                      | Default value                | Description                                                                                                           |
|------------|-----------------------------------------------------------|------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| property   | <code>string</code>                                       |                              | The property to use for matching the icon                                                                             |
| icons      | <code>Object</code>                                       |                              | For matching property value to icon e.g. `{map: 'fas fa-map'}`                                                        |
| [fallback] | <code>string</code>                                       |                              | Fallback icon do display when no match is found                                                                       |
| [style]    | <code>&#x27;fa&#x27;</code>  <code>&#x27;img&#x27;</code> | <code>&#x27;img&#x27;</code> | Type of icon. fa for font awesome and img for img-element. Defaults to img                                            |
| [altText]  | <code>string</code>                                       | <code>'icon'</code>          | Fallback text for "alt" on img-icon or text for screen readers for fa-icon                                            |
| [altTexts] | <code>Object</code>                                       |                              | For matching property value to "alt" text on img-icon or text for screen readers for fa-icon e.g. `{map: 'Map icon'}` |
| [limit]    | <code>string</code><code>number</code>                    | <code>10</code>              | Max number of icons to add. Defaults to 10                                                                            |
| [class]    | <code>string</code>                                       |                              | Optional extra class for image icons                                                                                  |

*Parameters surrounded by [] are optional*

**icon example - fontawesome icon matched to theme value**

    <span data-entryscape="icon"
          data-entryscape="1669"
          data-entryscape-style="fa"
          data-entryscape-property="dcat:theme"
          data-entryscape-icons='{"http://publications.europa.eu/resource/authority/data-theme/AGRI":"fas fa-tree","http://publications.europa.eu/resource/authority/data-theme/HEAL":"fas fa-heartbeat","http://publications.europa.eu/resource/authority/data-theme/ENVI":"fas fa-recycle"}'></span>


## image
`entry dependant`

| Parameter   | Value           | Description                                                                                                                               |
|:------------|:----------------|:------------------------------------------------------------------------------------------------------------------------------------------|
| property    | RDF property    | The property to use for loading the image                                                                                                 |
| fallback    | url             | A fixed URL to use as fallback if there is no value for the indicated `property` parameter                                                |
| width       | number          | A width to set on the image                                                                                                               |
| height      | number          | A height to sen on the image                                                                                                              |
| alt         | string          | A text to show as alt text                                                                                                                |
| altproperty | RDF property    | Let the alt value be drawn from the indicated property, if multiple values with different values exist only the most appropriate is shown |
| altcontent  | string template | Any values are expected to correspond to namespaced properties, alternatively the hardcoded title and description can be provided.        |

**image example 1 - an image with a fallback and width**

    <span data-entryscape="image"
          data-entryscape-property="foaf:depiction"
          data-entryscape-fallback="http://example.com/empty_picture.png"
          data-entryscape-width="300px"></span>

## slider
`entry dependant`

| Parameter       | Value              | Description                                                                                                                                                                                                                                             |
|:----------------|:-------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| property        | RDF property       | Images are detected on the current entry via this property, if caption or captionTemplate are provided the values are loaded as entries.                                                                                                                |
| src             | RDF property       | If entries are loaded based on the values in the property, the image is by default the resourceURI unless this `src` parameter is provided, in this case the image uri is detected via the `src` parameter in each loaded entry.                        |
| srcTemplate     | string template    | Template containing `${src}`, `${escapedSrc}`, `${doubleEscapedSrc}` or `${md5}`                                                                                                                                                                        |
| srcTemplateMap  | Object             | If the key matches part of the src the template will be used instead of the src e.g. `{'https://partofsomeimageurl':'${src}?download=false'}`                                                                                                           |
| fallback        | uri                | If no images are found a fallback image is shown                                                                                                                                                                                                        |
| headerTemplate  | handlebar template | This header is rendered if at least one image is found                                                                                                                                                                                                  |
| caption         | RDF property       | A caption is extracted from the metadata by using the value of the `caption` parameter as a property.                                                                                                                                                   |
| captionTemplate | handlebar template | In case the caption requires a combination of properties, then use the captionTemplate instead. If a slider-lightbox is used the text content from the caption will be used as caption for the lightbox. Read more about lightbox under [click](#click) |

The slider block allows you to show multiple images in a slide-deck like canvas. The slide-deck is also possible to open into a lightbox like popup to provide a larger viewing area.

**slider example 1 - images with captions and a fallback**

        <div data-entryscape="slider" data-entryscape-property="schema:image"
             data-entryscape-caption="dc:rights"
             data-entryscape-fallback="http://example.com/empty_picture.png"></div>

## view
`entry dependant`

Renders metadata of an entry with help of an RDForms template given by the parameter `rdformsid` (to avoid confusion with handlebar templates).


| Parameter          | Value           | Description                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|:-------------------|:----------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| rdformsid          | string          | RDForms template-id. If left empty, or the one given cannot be resolved an attempt will be made to find a template by inspecting the entry metadata. More specifically, templates will be detected according to the global parameters [type2template](global.md#type2template) and [entitypes](global.md#entitytypes). It is also possible to pass an array of ids of field level tamplates from which a larger form template will be constructed. |
| filterpredicates   | string or array | Fields to exclude from the template as indicated by a comma separated list (or single value) of properties. This is very useful when you want to have special treatment of certain fields, e.g. have the dcterms:title in the title and not in the complete metadata view.                                                                                                                                                                         |
| onecol             | boolean         | If set to true, field-names will be one row and the value beneath. The alternative (and default) is to have the first level fields on the same row as the fieldname to make the rendering more vertically compact.                                                                                                                                                                                                                                 |
| twocolbreak        | number          | The breaking width where two column view are replaced with a single column, by default this is set to 400.                                                                                                                                                                                                                                                                                                                                         |
| label              | boolean         | if set to false and the template being shown corresponds to a single field the label in front of the value will be suppressed.                                                                                                                                                                                                                                                                                                                     |
| showLanguage       | boolean         | If set to false all the language markers on free text fields will be suppressed                                                                                                                                                                                                                                                                                                                                                                    |
| filterTranslations | boolean         | Filters all multilingual fields so that only the value with the most correct language will be shown. By default this is set to true.                                                                                                                                                                                                                                                                                                               |
| popupOnLabel       | boolean         | If set to false labels will not trigger popovers with further information                                                                                                                                                                                                                                                                                                                                                                          |

**view example 1 - relying on global template config**

    <span data-entryscape="view">

**view example 2 - specifying the template**

    <span data-entryscape="view"
          data-entryscape-rdformsid="dcat:OnlyDataset"
          data-entryscape-filterpredicates="dcterms:title,dcterms:description"
          data-entryscape-onecol="true"></span>

In this example parts of the template is suppressed by providing the predicates, also forcing a one column view.

**view example 3 - constructing a template inline**

    <span data-entryscape="view"
          data-entryscape-rdformsid="foaf:givenName,foaf:familyName">

Note that the template in this example is constructed from ids of field-level templates not by identifying predicates. This can be confusing, especially when ids are named similar or even identical to the corresponding predicates like in this example.

## list
Is `entry dependant` if the relation or relationinverse properties are specified.

Renders a list of entries given a query. Here are all the parameters that list supports:

| Parameter            | Value               | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|:---------------------|:--------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| relation             | RDF property        | List entries reachable via relation (requires entry given, see [section on entries](entries.md)), see also example below                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| relationinverse      | RDF property        | List entries inversely reachable via relation (requires entry given, see [section on entries](entries.md)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| rdftype              | RDF class           | Entries in list will have to be instances of this class, may be array                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| constraints          | object              | Use when the rdftype is not enough, the key value pairs corresponds to predicate value pairs. Indicate that a value is a uri by surrounding it with `<` and `>`. If the value is an array the members are combined as a disjunction (or:ed together). Add a ~ before the key to negate ```"~key":"value"``` => ```NOT key:value```. The key can also be one of the special values `'uri', 'resource' or 'query'`. 'uri' and 'resource' are used for adding those fields to the query. Setting '~uri' to the entry uri will exclude that entry from the result. The 'query' option is more advanced and lets you add a structured query object to the constraint. For correct syntax consult the documentation on [entrystore-js](https://entrystore.org/js/stable/doc/module-store_SolrQuery.html) |
| property             | RDF property        | Use instead of rdftype, relation or relationinverse to show a list of values that are not entries (do not have a rdf:type). The type of the property values must be "uri".                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| limit                | number              | Number of entries shown before list is paginated                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| dependencyproperties | RDF property        | See explanation below                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| rdformsid            | string              | RDForms template-id for showing each entry in the list, see example below                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| click                | url                 | If provided makes each row clickable, clicking on the row will open the indicated url with the entry as parameter                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| namedclick           | string              | Similar to click, but reuses a click definition from the [links global parameter](global.md#clicks).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| pageInUrl            | boolean             | If set to true paginating to a page will add a query parameter to the url. The buttons for pagination will have links containing ?esc_page={page no}. If needed the prefix `esc` can be changed using global level parameter `urlParamsPrefix`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| clickExpand          | boolean             | If set to true clicking anywhere on the row will expand it, otherwise expand will be triggered by clicking at an arrow at the end of the row                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| expandButton         | boolean             | Controls if the expandbutton should be visible or not. By default the button will be visible if the rowexpand template is given or if an RDForms template is given via the rdformsid parameter, set to false to override this behaviour. The opposite is not true, you cannot force an expandbutton if there is nothing to show in the expanded section. To make a custom expand button / link, add the class `esbExpandButton` to the element that should respond to a click.                                                                                                                                                                                                                                                                                                                 |
| expandTooltip        | string              | Tooltip on arrow for expanding row                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| unexpandTooltip      | string              | Tooltip on arrow for un-expanding row                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| listhead             | handlebars template | Shown at the top of the list if it is not empty                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| listplaceholder      | handlebars template | Shown if the list is empty                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| rowhead              | handlebars template | Rendered for each list result (entry)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| rowexpand            | handlebars template | Rendered on expand for each row, if provided there will be an expand button at the end of each row                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| listbody             | handlebars template | Wrapper template for the rows, use the {{body}} directive to indicate where the list rows should go                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| randomize            | boolean             | If set to true the order of the search results will be randomized rather than shown according to the sorting order (default is latest).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| sortOrder            | string              | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| layout               | string              | One of two possible alternate layouts 'raw' or 'swiper. For the swiper no pagination option is available. Use a limit to adjust the number of results to present in the swipe                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| swiperSettings       | object              | Settings for the slider. Default values are `{slidesToShow: 1, slidesToScroll: 1, infinite: false}`. It is also possible to give an id for alternate placement of the swiper arrows by setting `{arrowElementId: 'yourId'}`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

**List example 1 - persons in a certain context**

Each person will be displayed using the default label, see the global [labelProperties](global.md#labelproperties) configuration.
The amount of results is capped to 8, if there are more matching results there will be pagination.

    <span data-entryscape="list"
          data-entryscape-context="1"
          data-entryscape-limit="8"
          data-entryscape-rdftype="foaf:Person"></span>

**List example 2 - all persons X knows**

If an entry (X) is given to the list, (e.g. via a page parameter, see [section on entries](entries.md)) it is possible to trigger a search based on that entry by following a relation. The following example shows all people that a given person claim to know:

    <span data-entryscape="list"
          data-entryscape-relation="foaf:knows"></span>

**List example 3 - all persons knowing X**

The people that claims to know the given person (X) can be listed via:

    <span data-entryscape="list"
          data-entryscape-relationinverse="foaf:knows" ></span>

**List example 4 - expandable list of persons**

We can see the details of a row (entry) in the list by providing a rdforms
template in the `rdformsid` parameter. This will make each row expandable and by default use the `view` block to render it on expand.

    <span data-entryscape="list"
          data-entryscape-rdftype="foaf:Person"
          data-entryscape-rdformsid="rdformsTemplateIdForFoaf"
          data-entryscape-expand-tooltip="More information"
          data-entryscape-unexpand-tooltip="Less information"></span>

**List example 5 - related entries in rowhead**

Care has to be taken when writing rowhead templates to avoid causing too many requests, e.g. if you need to load additional entries per row. The following example shows how to render a list of documents with their creators and publisher visible directly in a clever way using the parameter `dependencyproperties`. In the example below we use the special handlebar type of script to avoid complicated escaping of the template.

    <script type="text/x-entryscape-handlebar"
          data-entryscape-rdftype="foaf:Document"
          data-entryscape-dependencyproperties="dcterms:creator,dcterms:publisher"
          data-entryscape-limit="8">
      <h4>{{text}}</h4><br>
      Creator: {{text relation="dcterms:creator"}} <br>
      Publisher: {{text relation="dcterms:publisher"}}
    </script>


The dependencyproperty causes the list to look through all the retrieved entries, follow the indicated properties and load all those related entries in one request before trying to render the list. It is best to use `dependencyproperties` parameter together with a conservative limit, otherwise we may end up with to large queries (100 search results with no overlapping dependencies would in this scenario cause a query of 200 entries).

**List example 6 - custom expand button**
To make a custom expand button we need to have a list that allows each row to expand, here we do that by providing a RDForms template id, e.g. the `rdformsid` parameter. Second, we need to hide the default expand button, this is done by setting the parameter `expandButton` to false. Third and last, we need to add an element in the rowhead template that will act as button by adding the special class `esbExpandButton`. 

    <span data-entryscape="list"
          data-entryscape-rdftype="foaf:Person"
          data-entryscape-rdformsid="rdformsTemplateIdForFoaf"
          data-entryscape-expand-button="false"
          data-entryscape-rowhead="{{text}} <button class='esbExpandButton'>Expand</button>"></span>

When a row is expanded the class `esbRowExpanded` is added to the row, this is to allow specific styling on both the rowhead and the expanded section. For instance, this might be used to provide specific styling on buttons or even have two buttons and make sure only one is visible at a time, i.e. one for expanding and one for un-expanding the row.

**List example 7 - List of values that are not entries using property**
Sometimes the values you want to list are not entries (don't have a rdf:type) and `eachprop` handlebars helper is not enough. You want to make an actual list with pagination and one row per property value. It is possible to do if that value is of type "uri". This behaviour is entry dependant so there need to be an entry to start at for the block to work in this way. Each row in the list will hold that entry information but the resourceURI will be changed to the uri value of the given property. The template will have access to property values that are pointed to from the given property and to the entry property values by using a handlebar `prop` helper with `nested` set to `true` 

    <div data-entryscape="list"
          data-entryscape-property="dcat:downloadURL"
          data-entryscape-limit="10"
          data-entryscape-rowhead="One of the download links for {{prop 'dcterms:title' nested=true}} is {{resourceURI}}"</div>

## searchList
The `searchList` block is a version of the `list` block that also accepts user interactions via other search enabled blocks. Supported parameters on `searchList` include all those listed on the `list` block in addition to the following:

| Parameter | Value   | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|:----------|:--------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| facets    | boolean | Force requesting for facets when searching, necessary if using with collections with type facet, see [page on collections](collections.md). That is, if not set to true there will be no numbers indicating how many results there are per facet.                                                                                                                                                                                                                                                                                        |
| visible   | boolean | If set to false the list is deactivated. The purpose of this flag is to allow to allow several searchLists on the same page that can be switched between, e.g. list or icon mode. This is accomplished by letting the visible state be remote controlled via a `click` block that sends the `searchList-open` signal. The signal includes a value corresponding to the list name (given by the `define` parameter). If a signal is sent with a value that does not correspond to the name the searchList `visible` state is set to false. |

The `searchList` maintains a page wide search filter that is also reflected in the page parameters, allowing a search to be bookmarked and also be revisited in a browser history. The search filter can range from a free text query to a quite complex expression combining multiple search criteria in a boolean expression.
The expression is constructed by providing input in other blocks like `facets`, `simpleSearch`, `multiSearch`, `searchFilter`, `clear` or `rangeSearch`. See sections below for documentation and examples for how to use these blocks in combination with the `searchList` block. You should also read about how the global level parameter [collections](collections.md) provides a central configuration mechanism for the filter.

There should only be one `searchList` block visible at a time per page (see parameter `visible`).

## simpleSearch
`Aria support`

Provides a detached search field for a searchList that can be positioned anywhere on the page. The `simpleSearch` searches automatically on keypress (with a small delay) and is always used together with a `searchList` block. The `simpleSearch` block supports the following parameters:

| Parameter    | Value   | Description                                                                               |
|:-------------|:--------|:------------------------------------------------------------------------------------------|
| searchButton | boolean | If set to try there will be a search icon to the right of the input field                 |
| placeholder  | string  | The placeholder of the input field                                                        |
| collection   | string  | A collection that indicates that the simpleSearch is searching against a certain property |

**simpleSearch example 1 - default search**

In the simplest case the `simpleSearch` uses the [global query configuration (or its default setting)](global.md#query).

    <span data-entryscape="simpleSearch"
          data-entryscape-search-button="true"
          data-entryscape-placehoder="Search for..."></span>
    <span data-entryscape="searchList" data-entryscape-rdftype="foaf:Person"></span>

**simpleSearch example 2 - collection search**

It is possible to force the simpleSearch block to search against a certain property. This is achieved by referring to a collection that provides the definition of that property, i.e. the property and whether it is a literal or a uri.

    <span data-entryscape="simpleSearch"
          data-entryscape-collection="familyname"></span>
    <span data-entryscape="searchList" data-entryscape-rdftype="foaf:Person"></span>

## searchFilter
`Aria support`, `filter condition`

The `searchFilter` block allows you to choose values from a dropdown that helps narrow the search for entries shown in a `searchList` block. The values are drawn from a [collection](collections.md), i.e. they can be hardcoded, loaded as facets from a previous search, explicitly searched for or choices from a RDForms template.

| Parameter            | Value           | Description                                                                                                                                                                                                                              |
|:---------------------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| placeholder          | string          | The placeholder of the input field                                                                                                                                                                                                       |
| collection           | string          | A collection that indicates that the simpleSearch is searching against a certain property                                                                                                                                                |
| width                | number          | Set the width of the input field explicitly                                                                                                                                                                                              |
| allowEmptyOption     | boolean         | If true the dropdown will contain the empty value, this value is true by default                                                                                                                                                         |
| emptyLabel           | string          | If `allowEmptyOption` is true it is possible to provide a string that corresponds to the empty value, the default is the empty string                                                                                                    |
| freeText             | boolean         | If true the user is allowed to do a free text search in addition to the exact matches provided in the dropdown.                                                                                                                          |
| freeTextTemplate     | string template | If `freeText` is set to true a template can be provided that explains the free text search option, the default template is: `Search for "${term}"`                                                                                       |
| openOnFocus          | boolean         | If set to true the dropdown will open on focus rather than after some input have been provided. The default setting is false as it avoids the popup to show unnecessarily when using the keyboard to navigate the different input fields |
| invalidSearchFilter  | regexp          | Regular expression (can be provided as text) to test if the query contains characters that are forbidden. The default regular expression looks like: `[\^+\-[\]{}\\/!#~"]`                                                               |
| invalidSearchMessage | string          | If the query contains an unwanted character this message is displayed. The default message is: `Search string not allowed to contain characters ^+-[]{}\\/!#"~.`                                                                         |
| matchStartOfWord     | boolean         | If set to true the query will match the options in the dropdown from the start of words only (not necessarily the first word in an option).                                                                                              |
| limit                | number          | Limits the number of options searched for when populating the dropdown.                                                                                                                                                                  |

**searchFilter example 1 - filter documents based on publisher and topic**

Two `searchFilter` blocks are used in combination with a `searchList`:

    Publisher: <span data-entryscape="searchFilter"
                     data-entryscape-collection="publisher"></span><br>
    Topic: <span data-entryscape="searchFilter" 
                 data-entryscape-collection="topic"></span><br>
    <span data-entryscape="searchList"></span>

Note that the searchFilter references two [collections](collections.md) they could be defined like this in the global configuration:

    collections: [
      {
        type: 'search',
        name: 'publisher',
        property: 'dcterms:publisher',
        nodetype: 'uri',
        rdftype: ['foaf:Agent', 'foaf:Organization', 'foaf:Person'],
      },
      {
        type: 'search',
        name: 'topic',
        property: 'dcterms:subject',
        nodetype: 'uri',
        rdftype: 'skos:Concept',
      }
    ]

## rangeSearch
`Aria support`

The `rangeSearch` blocks allows range queries against metadata fields containing dates or integers.  

| Parameter   | Value   | Description                                                                                    |
|:------------|:--------|:-----------------------------------------------------------------------------------------------|
| collection  | string  | The collection containing information about which property the range query should use          |
| pattern     | regexp  | A regular expression that allows restricting which values are valid to trigger the range query |
| to          | boolean | If true the range query provides the upper bound of a range query                              |
| placeholder | string  | Text to show in input field when no value is yet provided                                      |

**rangeSearch example 1 - date range query**

     <label id="from">From:</label>
     <div data-entryscape="rangeSearch"
          data-entryscape-describedby="from"
          data-entryscape-collection="date"
          data-entryscape-placeholder="YYYY"
          data-entryscape-pattern="\\d\\d\\d\\d"></div>
     <label id="to">To:</label>
     <div data-entryscape="rangeSearch"
          data-entryscape-to="true"
          data-entryscape-describedby="to"
          data-entryscape-collection="date"
          data-entryscape-placeholder="YYYY"
          data-entryscape-pattern="\\d\\d\\d\\d"></div>

Where the corresponding collection is defined as:

    collections: [
       {
          type: 'range',
          name: 'date',
          nodetype: 'date',
          property: 'xsd:date',
       }
    ]

## checkboxFilter

The `checkboxFilter` blocks allows excluding entries with a specific property-value from the search results. The property, values, nodetype etc. is specified in the collection. Giving the named `collection` for the checkboxFilter is mandatory. To include the filter in the query per default without the user clicking the checkbox use the global parameter [baseFilter](https://docs.entryscape.com/en/blocks/global/#basefilter). If baseFilter is used and the checkboxType is `add` the checkbox will be checked on first load. If baseFilter is not used and the checkboxType is `add` the checkbox will be unchecked on first load.

| Parameter    | Value  | Description                                                                                                               |
|:-------------|:-------|:--------------------------------------------------------------------------------------------------------------------------|
| collection   | string | The collection containing information about which property the checkBox query should use                                  |
| label        | string | The visible label for the checkbox                                                                                        |
| checkboxType | string | Should the checkbox add or remove the collection from the query. Possible values are `add` or `remove`. Default is `add`. |
| class        | string | An optional class can be added to the checkbox and label element                                                          |

**checkboxFilter example**

     <label></label>
     <div data-entryscape="checkboxFilter"
          data-entryscape-collection="title"
          data-entryscape-checkbox-type="add"
          data-entryscape-label="Exclude The Stand from results: "></div>
Where the corresponding collection is defined as:

    collections: [
       {
          type: 'check',
          name: 'title',
          property: 'dcterms:title',
          nodetype: 'literal',
          values: "The Stand",
          modifier: 'not,

       }
    ]

## radioButtonFilter

Renders a radio button filter. The button values correspond to the collection options in a `radio` collection. The selected option will be used by the "search" component (renderSearchList) as constraints.

Giving the named `collection` for the radioButtonFilter is mandatory.

By default, the collection's first option is selected. The global parameter - [baseFilter](https://docs.entryscape.com/en/blocks/global/#basefilter), can be used if another option than first should be default.

| Parameter  | Type                | Description                                                                       |
|------------|---------------------|-----------------------------------------------------------------------------------|
| collection | <code>string</code> | Collection containing options/query's and labels for the radio button control     |
| [class]    | <code>string</code> | A class can be added to replace the bootstrap classes on input, label and wrapper |

*Parameters surrounded by [] are optional*

## multiSearch
`Aria support`

The `multiSearch` can be seen as a combination of the blocks `simpleSearch` and multiple `searchFilter` in a single input area. Typing a search term will trigger a dropdown where the first option (and default on enter) is to do a free text search. The rest of the dropdown contains filter options based on what is defined in the global [collections](collections.md) configuration. The options are divided into sections in the dropdown corresponding to the collection they originate from, the number of options show per section is defined in the collection via the limit parameter.

The `multiSearch` block accepts the following parameters:

| Parameter            | Value                    | Description                                                                                                                                               |
|:---------------------|:-------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------|
| placeholder          | string                   | The placeholder of the input field.                                                                                                                       |
| openOnFocus          | boolean                  | Opens dropdown on input field focus if true, otherwise opens after the user types something.                                                              |
| click                | string                   | Opens a new page rather than remote controlling a searchList.                                                                                             |
| namedclick           | string                   | Same as click, but look up exact page from [global level parameter clicks](global.md#clicks).                                                             |
| include              | array or comma separated | List of collectionnames to be searchable.                                                                                                                 |
| exclude              | array or comma separated | List of collectionnames to exclude when searching.                                                                                                        |
| freeSearchTemplate   | string                   | A template string containing $1 where $1 will be replaced by the value written in the search field by the user. By default the string is "Search for $1". |
| invalidSearchFilter  | regexp                   | A regular expression to warn against faulty queries, by default [\^+\-[\]{}\\/!#~"] is used.                                                              |
| invalidSearchMessage | string                   | A message that will be shown if the regular expression in the `invalidSearchFilter` parameter matches.                                                    |

The `click` and `namedclick` can either be used in combination with a `searchList` block triggering clicks (or keyboard selection) leading to another page. The parameters `click` and `namedclick` can be provided **either** on the `multiSearch` block **or** on the collection being selected. The collection setting has higher priority and may be used to direct clicks to different pages depending on which collection option being clicked (selected).

**multiSearch example 1 - together with a searchList**

    <span data-entryscape="multiSearch"></span>
    <span data-entryscape="searchList"></span>

**multiSearch example 2 - standalone, triggering opening another page**

    <span data-entryscape="multiSearch" data-entryscape-click="/otherpage"></span>
    <span data-entryscape="searchList"></span>

## facets
The `facet` block provides a list of facets divided into sections based on what is specified in the global [collections](collections.md) configuration. Each facet is nothing more than a filter with a number indicating the number of search results that would be listed if selected. The `limit` parameter provided on a collection will limit the amount of facets, if more facets are available than the limit a button will appear to allow the user to "show more".
The `facet` block must be used in conjunction with a `searchList` with the `facets` parameter set to true.

| Parameter | Value  | Description                                                                                                                                                  |
|:----------|:-------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| showmore  | string | Text on button for showing more than the fixed amount of facets indicated by the `limit` parameter in the corresponding collection. Defaults to "Show more". |
| showless  | string | Text on button for hiding facets shown by the "show more" button. Defaults to "Show less".                                                                   |
| include   | string | Comma separated list of collections (by name) to include, cannot be used together with the `exclude` parameter                                               |
| exclude   | string | Comma separated list of collections (by name) to exclude, cannot be used together with the `include` parameter                                               |
| hl        | number | The heading level to use for the facets, defaults to 3                                                                                                       |

**facets example 1 - on the side of a simpleSearch and searchList**

    <div class="sidebar">
       <span data-entryscape="facets"></span>
    </div>
    <div class="main">
       <span data-entryscape="simpleSearch"></span>
       <span data-entryscape="searchList" data-entryscape-facets="true"></span>
    </div>

## limit
`filter condition`

The `limit` block allows the user to adjust the number of results per page in a `searchList` block by adjusting the limit `query parameter`. Options in dropdown are based on the global level [limitOptions](global.md#limitoptions) parameter. The selected option triggers re-render on the list and is not reflected in the url and will not be remembered on page reload. Be careful to set limit property on other list-blocks on the page that should not be affected. The limit property on list-block override the limit option. 

| Parameter | Value  | Description                                                                                                                         |
|:----------|:-------|:------------------------------------------------------------------------------------------------------------------------------------|
| class     | string | A class that will be set on the select element.                                                                                     |
| type      | string | Possible values are 'radio' for a radiobuttons and 'select' for a dropdown interface. If no value given it will default to 'select' |

**limit example - limit options**

    <span data-entryscape="limit"></span>


## sort
`Aria support`, `filter condition`
The `sort` block allows the user to sort matches in a `searchList` block based on the global level [sortOptions](global.md#sortoptions) parameter.

| Parameter | Value  | Description                                                                                                                         |
|:----------|:-------|:------------------------------------------------------------------------------------------------------------------------------------|
| class     | string | A class that will be set on the select element, if none is provided the bootstrap class `form-control` will be set.                 |
| type      | string | Possible values are 'radio' for a radiobuttons and 'select' for a dropdown interface. If no value given it will default to 'select' |

**sort example - sort options**

    <span data-entryscape="sort"></span>

## clear
The `clear` block allows the user to clear all filters added by one or several of the blocks `simpleSearch`, `searchList`, `searchFilter`, `multiSearch` and `facets`.

| Parameter | Value  | Description                                                                                                                                                                                                                                                                                                                                                     |
|:----------|:-------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| empty     | string | Can be one of `hide`, `remove` and `disable`. Correspondingly, if there is nothing to clear the button is either hidden (`style="display:hidden"`), removed (`style="display: none;"`) or disabled (`disabled="true"`). All other values will be considered to be a class that will be added on the parent node of the block in case there is nothing to clear. |

Note that the `clear` block does not provide a button itself as it relies on being added as the child of an element that will be considered the button. The reason is that it gives more flexibility to the developer/designer that uses blocks to do a custom design of the button or button like construction.

**clear example 1 - bootstrap button with font awesome icon**

    <button class="btn btn-primary" type="button">
        <div data-entryscape="clear" data-entryscape-empty="remove"></div>
        <span>Clear filters</span>
        <i class="fas fa-times"></i>
    </button>

## results
The `results` must be used on the same page as a `list` or `searchList` and displays the number of results shown in the list.

| Parameter               | Value  | Description                                                                                                                                                            |
|:------------------------|:-------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| use                     | string | Connect to a `list` or `searchList` via the name given by the `define` parameter                                                                                       |
| template                | string | If provided this template will always be used, if not provided the parameters `templateNoFilter`, `templateFilter` and `templateFilterNoResults` must be used instead. |
| templateNoFilter        | string | Template used when no filter is provided                                                                                                                               |
| templateFilter          | string | Template used when a filter is provided and there are matching results                                                                                                 |
| templateFilterNoResults | string | Template used when a filter is provided but there are are no matching results                                                                                          |

In all the templates the following variables can be used:

| Variable    | Description                         |
|:------------|:------------------------------------|
| resultsize  | Number of results                   |
| currentpage | The page currently shown            |
| pagecount   | The total number of pages available |
| term        | The current free text search string |

**results example - integration with a `searchList` block**

    <div data-entryscape="results" data-entryscape-use="mainList"
        data-entryscape-template-filter="{{resultsize}} matching items"
        data-entryscape-template-filter-no-results="No matching items for your query"
        data-entryscape-template-no-filter="{{resultsize}} apples in stock"></div>
    <div data-entryscape="searchList" data-entryscape-define="mainList"></div>

## filterResults
The `filterResults` block must be used on the same page as a `searchList` as it shows the filter that where applied to narrow down the result list. It can be considered a simplified version of the `multiSearch` block as it shows the same
sets of facets/constraints but not inside a search bar. Hence, the `filterResults` should probably be used with `simpleSearch` a few `searchFilter` blocks or with the `facets` block rather than with `multiSearch`. The `filterResults` does provide a remove button on each facet/constraint.

**filterResults example - together with `facets`, `simpleSearch` and a `searchList` block**

    <div class="sidebar">
       <span data-entryscape="facets"></span>
    </div>
    <div class="main">
       <span data-entryscape="simpleSearch"></span>
       <span data-entryscape="filterResults" use="mainSearchList"></span>
       <span data-entryscape="searchList" data-entryscape-facets="true" define="mainSearchList"></span>
    </div>

## collectionText
The purpose of the `collectionText` block is to render information dependent on how a specific collection is used in the filter. If the filter does not have any constraint based on the specified collection the template in the `empty` parameters will be used. Otherwise, if the filter contains a constraint for the specified collection the template in the `template` parameter will be used.

**collectionText example - information for a conditional `searchFilter` block.**

    <span data-entryscape="collectionText"
          data-entryscape-empty="You must choose a document type before you can choose a category</div><br>"
          data-entryscape-template="<label>Choose a category for document type {{value}}:</label>"
          data-entryscape-collection="documenttype"></span>
    <span data-entryscape="searchFilter"
         data-entryscape-if="documenttype"
         data-entryscape-collection="subject">
    </span>

## export

The `export` block works must always work in conjunction with a `searchList` block as it allows all the matches to be exported. The export currently only support the formats application/rdf+xml or application/json. The latter requires a transformation that is guided via a projection object provided on each entity type, see the [global parameter entitytypes](global.md#entitytypes). The `export` block must always be placed inside a clickable element, e.g. a link or a button. The user will be prompted for a download on click.

| Parameter | Value  | Description                                                                |
|:----------|:-------|:---------------------------------------------------------------------------|
| use       | string | A named (via the define parameter) `resultsList` block, this is mandatory. |
| format    | string | Currently is only `rdf` or `json` supported.                               |

**Example export 1 - RDF/XML**

    <button><span data-entryscape="export"
            data-entryscape-use="listOfDocuments"
            data-entryscape-format="rdf"></span>Download as RDF/XML</button>

**Example export 2 - JSON**

    <button><span data-entryscape="export"
            data-entryscape-use="listOfDocuments"
            data-entryscape-format="json"></span>Download JSON</button>

Assuming the list presents documents typed as `foaf:Document`, a projection inside an entitytype could be:

    entitytypes : {
      document: {
         rdftype: "foaf:Document",
         projection: {
           title: "dcterms:title",
           abstract: "dcterms:description",
           "*keyword": "dcterms:subject"
         }
      }
    }

This projection would lead to the following json structure on download:

    [{
        title: "Document 1",
        abstract: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut ",
        keyword: ["keyword1", "keyword2"]
     }, ...
    ]

Note that the `*` in front of keyword in the projection forces an array, even if there is only a single value. If two values exists in the graph for a value that is not marked with an initial `*` only one of them will be picked at random and included in the json expression.

## back
The purpose with the back block is to allow a "back to search" link on a landing page that preserves the search filters you had before clicking on a search result. The block works by going back in the browser history. If the browser history is wrong, e.g. when you arrived on the landing page via a link on another page the back link can take you back to a clean search interface. E.g. if you have been sent the link in an email, clicking on the link should not take you back to the email application.

| Parameter | Value  | Description                                                                                                                                                                                                                                                                            |
|:----------|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| test      | string | Should correspond to a regular expression that will be used to test if the page you arrived via, the referrer, is the page you want to go back to. If the test parameter is not provided it is assumed that it is ok if the referrer has the same origin (domain) as the current page. |
| href      | url    | The page to go back to if the referrer does not fulfill the test                                                                                                                                                                                                                       |
| label     | string | The label of the link                                                                                                                                                                                                                                                                  |
| tooltip   | string | The tooltip (the title attribute in HTML) to set                                                                                                                                                                                                                                       |
| className | string | One or several classes to set (space separated) on the link element (`a` element)                                                                                                                                                                                                      |

Note that the `label`, `tooltip` and `className` parameters will be ignored if the block is expressed on or directly within an `a` element that provides these values. The parameters should only be used when defining custom `back` blocks that you want to use in many places, and you do not want to repeat that information inline in an `a` element. In these cases the `a` will be created for you.

**back example 1 - directly in html**

    <a href="http://example.com/search" data-entryscape="back">Back to search</a>

**back example 2 - directly in html**

    <a href="search" data-entryscape="back" data-entryscape-test="example.com/search">Back to search</a>

**back example 3 - inside an `a` element**

This is useful when you are inside handlebar templates as data attributes are not detected there.

    <script type="text/x-entryscape-handlebar" data-entryscape="template">
       <a href="http://example.com/search">{{back}}Go back to search</a>
    </script>

## tabs

`entry dependant`, `filter condition`

The `tabs` block has content sections given by `contentTemplate` and buttons for switching between what content to show. 

The tabs block needs an entry to start on but each content section can have its own entry. The entry for the content section can be one relation away or the one given from start. The entry and parameters are applied to the content template when the button is clicked which avoids unnecessary loading of content on first render.

The buttons are also customizable via the `buttonTemplate`.

In the description below a (*) indicates that the value can be overridden on each tab.

| Parameter                       | Type                                     | Default value      | Description                                                                                                 |
|---------------------------------|------------------------------------------|--------------------|-------------------------------------------------------------------------------------------------------------|
| [buttonClass]                   | <code>string</code>                      |                    | (*) A class for the buttons that will overwrite the default bootstrap class                                 |
| [relation] or [relationinverse] | <code>string</code>                      |                    | (*) Use entry/entries one relation away from the start entry for the tabs                                   |
| [constraints]                   | <code>object</code>                      |                    | OR constraints for the relation or relationinverse query. `~` will modify query with NOT for the constraint |
| [sortOrder]                     | <code>string</code>                      |                    | Sort order for the relation or relationinverse query                                                        |
| [limit]                         | <code>number</code> <code>string</code>  |                    | Limit for the relation or relationinverse queries                                                           |
| [fallback]                      | <code>string</code>                      |                    | Handlebar Template to show if no results                                                                    |
| [buttonTemplate]                | <code>HandlebarTemplate</code>           |                    | (*) Handlebar Template for the buttons defaults to the title if given                                       |
| **contentTemplate**             | <code>HandlebarTemplate</code>           |                    | (*) Handlebar Template for the container                                                                    |
| [entry]                         | <code>string</code> <code>number</code>  |                    | (*) The entry to use for the block, usually fetched from a list or the url                                  |
| [tabs]                          | <code>Array</code>                       |                    | The order of the tabs from left to right [{property: value},...] See below for details                      |
| [useNoLangSort]                 | <code>string</code> <code>boolean</code> | <code>false</code> | To sort on literals with no language value                                                                  |
| [block]                         | <code>string</code>                      |                    | Custom block name                                                                                           |

*Parameters surrounded by [] are optional*

### tabs array
Advanced option to specify the order of the tabs and override templates etc. on each tab.

| Parameter         | Type                                   | Description                                                                                                                                                                                                                                                               |
|:------------------|:---------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [relation]        | <code>string</code>                    | Override for relation                                                                                                                                                                                                                                                     |
| [relationinverse] | <code>string</code>                    | Override for relationinverse                                                                                                                                                                                                                                              |
| [buttonClass]     | <code>string</code>                    | Override for buttonClass                                                                                                                                                                                                                                                  |
| [buttonTemplate]  | <code>HandlebarTemplate</code>         | Override for buttonTemplate                                                                                                                                                                                                                                               |
| [contentTemplate] | <code>HandlebarTemplate</code>         | Override for contentTemplate                                                                                                                                                                                                                                              |
| [match]           | <code>object</code><code>string</code> | If the tabs need to be in a specific order e.g a map on the first tab and a table on the second this can be done by giving constraint(s) to match `{property: 'value'}` for the tabs. If multiple constraints are given they will be applied with 'OR' defaults to 'rest' |

*Parameters surrounded by [] are optional*

**Example tabs 1 - simple example using an entry that has visualizations**

    <div data-entryscape="tabs" 
         data-entryscape-entry="1636" 
         data-entryscape-relation="schema:diagram" 
         data-entryscape-content-template="{{visualization}}"></div>

**Example tabs 2 - setting the order of the tabs and templates for the buttons**

    <script type="text/x-entryscape-json">
    {
	"entry": "1636",
	"block": "tabs",
	"fallback": "Nothing to show!",
	"sortOrder": "title",
	"buttonTemplate": "{{iconTest}}{{prop \"dcterms:title\"}}",
	"contentTemplate": "{{prop \"dcterms:title\"}}{{visualization}}",
	"tabs": [{
		"buttonTemplate": "Overview",
		"contentTemplate": "{{prop \"dcterms:title\"}}{{view}}"
	}, {
		"relation": "schema:diagram",
		"match": {
			"escv:chartType": "pie"
		}
	},{
		"relation": "schema:diagram",
		"match": {
			"escv:chartType": "bar"
		}
	}, {
		"relation": "schema:diagram",
		"match": {
			"escv:chartType": "map"
		}
	}, {
		"relation": "schema:diagram",
		"match": "rest"
	}, {
		"buttonTemplate": "{{text}}",
		"contentTemplate": "Anny template using the starting entry"
	}]
    }
    </script>

## map
`entry dependant`

The `map` block provides a map view based on the OpenLayers library. The block can be used in two ways. Either as a way to show the geographical position of a dataset or to show the layers from a WMS service.

When used to show the position the coordinates are either detected from explicit positions in the metadata (provided via the wgs84:lat and wgs84:long, see [basic geo](https://www.w3.org/2003/01/geo/)) or via lookup of an address. (Currently the address information is hardcoded to be detected via a schema.org construction
in the form of a schema:schema property pointing to a blank node with schema:streetAddress, schema:addressRegion and schema:addressLocality properties.)

When used to show the layers from a WMS distribution the property pointing to the capabilities url must be set in the configuration

| Parameter        | Value        | Description                                                                                                                                                                                                                            |
|:-----------------|:-------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| tilelayer        | string       | A template URL for loading tiles, see example below. If nothing provided the openstreatmap will be used by default.                                                                                                                    |
| attribution      | string       | A text shown at the bottom of the map to provide attribution of the map information.                                                                                                                                                   |
| scaleLine        | boolean      | Include a scale line on the map if true. Default value is false                                                                                                                                                                        |
| maxzoom          | number       | The maximal zoom allowed for the map                                                                                                                                                                                                   |
| layers           | array        | An array of layers, each consisting of the layer name in the `tilename` attribute together with tilelayer, maxzoom and attribution per layer.                                                                                          |
| zoom             | number       | The initial zoom level of the map                                                                                                                                                                                                      |
| height           | string       | The height of the map, default is `300px`                                                                                                                                                                                              |
| popup            | string       | A template that will be rendered in a popup close to the pin on click                                                                                                                                                                  |
| popupOnload      | boolean      | If set to true the popup will show directly without requiring a click on the pin                                                                                                                                                       |
| constraints      | object       | Only render a map if any of the `{property: values}` match                                                                                                                                                                             |
| property         | RDF property | **(For wms)** property pointing to wms capabilities url                                                                                                                                                                                |
| pattern          | array        | **(For wms)** string or regex only url from property matching all will render a map                                                                                                                                                    |
| legend           | boolean      | **(For wms)** include legend from wms service                                                                                                                                                                                          |
| legendButtonText | string       | **(For wms)** alternative text for the legend button                                                                                                                                                                                   |
| noLegendText     | string       | **(For wms)** text to show if no legend is provided for a layer                                                                                                                                                                        |
| baseMap          | object       | Default basemap is osm. The base map can be set for each map block but also on a global level for all map blocks. For reference on how to set the baseMap see documentation on the global level [baseMap parameter](global.md#basemap) |

**Layers array layer object**

| Parameter    | Value   | Description                                                                            |
|:-------------|:--------|:---------------------------------------------------------------------------------------|
| type         | string  | Possible values are 'xyz', 'wmts', 'wms', 'osm'. Default value is 'osm'                |
| url          | string  | Template URL for loading tiles                                                         |
| title        | string  | A title used in the control for switching between layers                               |
| maxzoom      | number  | The maximal zoom (0-28) allowed for the layer                                          |
| attribution  | string  | A text shown at the bottom of the map to provide attribution of the layer information. |
| base         | boolean | Defaults to true                                                                       |
| capabilities | string  | Url for wms capabilities                                                               |
| layer        | string  | Specific layer to use                                                                  |

**Example map 1 - one layer only**

    <script type="text/x-entryscape-json">
    {
        "block":"map",
        "url":"https://api.lantmateriet.se/open/topowebb-ccby/v1/wmts/token/{yourkey}/1.0.0/topowebb/default/3857/{z}/{y}/{x}.png",
        "maxzoom":"15",
        "attribution":"Map data © <a href=\"https://www.lantmateriet.se/\">Lantmäteriet</a>",
        "zoom":"11",
        "height":"400px",
        "popup":"<h2>{{text property=\"dcterms:title\"}}</h2><p>{{text property=\"dcterms:description\"}}</p>"
    }
    </script>

**Example map 2 - information provided in a layers array**

    <script type="text/x-entryscape-json">
    {
        "block":"map",
        "layers":[
            {
                "title":"Lantmäteriet",
                "url":"https://api.lantmateriet.se/open/topowebb-ccby/v1/wmts/token/{yourkey}/1.0.0/topowebb/default/3857/{z}/{y}/{x}.png",
                "maxzoom":"15",
                "attribution":"Map data © <a href=\"https://www.lantmateriet.se/\">Lantmäteriet</a>"
            },
            {
                "title":"Open streetmap",
                "url":"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                "maxzoom":"18",
                "attribution":"Map data © <a href=\"https://openstreetmap.org\">OpenStreetMap</a> contributors, <a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>"
            }
        ],
        "zoom":"11",
        "height":"400px",
        "popup":"<h2>{{text property=\"dcterms:title\"}}</h2><p>{{text property=\"dcterms:description\"}}</p>"
    }
    </script>

**Example map 3 - wms layers**

    <script type="text/x-entryscape-json">
    {
        "block":"map",
        "property":"dcat:accessURL",
        "legendButtonText":"Legend",
        "noLegendText":"No legend provided",
        "legend":true,
        "pattern":[
            "wms"
        ],
        "loading":"<div style=\"margin-top: 120px\" class=\"esbLoader\"></div>"
    }
    </script>

## mediaQuery

The `mediaQuery` blocks provides a simplified way of writing css media queries using blocks. The main use case is to make it possible to provide an easy way to adapt custom blocks to different embedding circumstances, e.g. via a simple parameter control when to hide or show certain blocks.

| Parameter | Value  | Description                                                                                                          |
|:----------|:-------|:---------------------------------------------------------------------------------------------------------------------|
| media     | string | The media in the media query, e.g. `screen`                                                                          |
| max       | string | A max width upon wich the media query will apply, e.g. `1024px`                                                      |
| min       | string | A min width upon wich the media query will apply, e.g. `1024px`                                                      |
| rules     | object | A set of rules expressed in an object where each key is a selector and each value is a set of comma separated styles |
| class     | string | A css class to which the media query will apply it's styles                                                          |
| selector  | string | A full selector to which the media query will apply it's styles, use when class is not enough                        |
| style     | string | A set of styles expressed the way inline styles are expressed in html, e.g. `display: block; border-width: 1px;`.    |

Typically, you use the rules parameter or a combination of either the class and style or the selector and style parameters.

**Example hierarchy 1 - hide sidebar on small screens**

    <span data-entryscape="mediaQuery"
          data-entryscape-media="screen"
          data-entryscape-max="800px"
          data-entryscape-class="esb2Col__sidebar"
          data-entryscape-style="display: none;"></span>

## hierarchy
`entry dependant`

The `hierarchy` blocks renders a hierarchical graph (left to right) from the current entry. Each entry is represented as a square with a label according to the [labelProperties global parameter](global.md#labelproperties). The hierarchy is detected via the forward and backward properties provided per type. If an entry has children, as detected via the forward property, the square is marked and may be expanded. By default, only one level is expanded.

| Parameter      | Value   | Description                                                                                                                                                                                                                                         |
|:---------------|:--------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| relationByType | object  | A mapping between forward and backward properties per rdf:type. Default is to support SKOS, i.e. `skos:narrower` respectively `skos:broader` for `skos:Concept` and `skos:hasTopConcept` respectively `skos:topConceptOf` for `skos:ConceptScheme`. |
| boxWidth       | number  | Width of the box for each entry, default is 120                                                                                                                                                                                                     |
| boxHeight      | number  | Height of the box for each entry, default is 12                                                                                                                                                                                                     |
| maxCharacter   | number  | Number of characters before an ellipsis is shown, default is 20                                                                                                                                                                                     |
| scale          | decimal | The scale expressed as a whole number or decimal, default is 1                                                                                                                                                                                      |

**Example hierarchy 1 - default SKOS hierarchy**

    <span data-entryscape="hierarchy"
          data-entryscape-scale="2"></span>

## visualization
`entry dependant`

The `visualization` block is the only blocks that relies on special knowledge of how EntryScape express information. The entry provided must be Visualization entry which indicates the type of visualization to do, which data to load, how to treat the columns when constructing the visualization etc.

| Parameter | Value   | Description                                                                                                                                                                                                                            |
|:----------|:--------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| baseMap   | object  | Default basemap is osm. The base map can be set for each map block but also on a global level for all map blocks. For reference on how to set the baseMap see documentation on the global level [baseMap parameter](global.md#basemap) |
| scaleLine | boolean | **(For all map visualizations)** to override the selected value in Entryscape                                                                                                                                                          |
| legend    | boolean | **(For wms map visualizations)** to override the selected value in Entryscape                                                                                                                                                          |

## chart
The `chart` block displays a chart with help of the library [Chart.js](https://www.chartjs.org/) based on either inline data or data loaded from a json file.

| Parameter        | Value   | Description                                                                                                                                                                                                                                         |
|:-----------------|:--------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| data             | object  | Provide hardcoded data to be shown in the chart, see section below on accepted data-structures.                                                                                                                                                     |
| url              | url     | A url to a json file containing the data                                                                                                                                                                                                            |
| legend           | string  | A suitable legend to show below the chart                                                                                                                                                                                                           |
| type             | string  | Values `bar`, `line`, `horizontalBar`, `pie` or `doughnut` is supported and have some pre-set colors that can be overridden by adding more colors via the backgroudColor property. Other charts like `radar`, `bubble` etc. have no pre-set colors. |
| backgroundColor  | color   | Either a color like `blue` or a hexadecimal value like `#fff`                                                                                                                                                                                       |
| width            | string  | A width like `300px`                                                                                                                                                                                                                                |
| height           | string  | A height like `600px`                                                                                                                                                                                                                               |
| options          | object  | A set of options specific to each chart type, see [Chart.js](https://www.chartjs.org/) for more information on what you can provide here.                                                                                                           |
| filterZeroValues | boolean | If true zero values are removed from the chart                                                                                                                                                                                                      |
| limit            | number  | Limit the number of data points shown in the chart                                                                                                                                                                                                  |
| labelMaxLength   | number  | Shortens or break labels that ar too long                                                                                                                                                                                                           |

There are two ways to express chart data in json, they will be auto-detected.

**Data structure according to Chart.js**

1. The object must contain an array of labels in the `labels` attribute.
2. The object must contain the `datasets` attribute which is an array of datasets represented as an array of individual values.

For example:

    {
       "labels": ["Col1", "Col2"],
       "datasets": [[ 23, 45]]
    }

**Data structure according to an older format**

1. The object must contain an array of labels in the `labels` attribute (same as above).
2. The object must contain the `series` attribute which is an array of individual values.

For example:

    {
       "labels": ["Col1", "Col2"],
       "series": [ 23, 45]
    }

**chart example - a bar chart**

    <div data-entryscape="chart"
         data-entryscape-type="horizontalBar"
         data-entryscape-caption="Some example data" 
         data-entryscape-limit="7"
         data-entryscape-url="http://example.com/somechartdata.json">
    </div>

## table
`entry dependant`

The `table` block displays a CSV file in a table with an upper limit on the size of the loaded data.

| Parameter | Value        | Description                                                                                                                                                              |
|:----------|:-------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| property  | RDF property | Detect the url to the file via the given property                                                                                                                        |
| maxSize   | number       | Amount of data that should be read in bytes from the file before giving up. Use this parameter to hinder that too big files are loaded and make the browser unresponsive |

**table example - show a file for a distribution**

    <div data-entryscape="table"
         data-entryscape-property="dcat:downloadURL"
         data-entryscape-max-size="1000000"></div>

## click

The `click` block allows sending a signal, potentially with different values upon clicking or selecting among options.

| Parameter | Value              | Description                                                                                                                                                                         |
|:----------|:-------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| signal    | text               | The signal that will be sent on click or select                                                                                                                                     |
| template  | handlebar template | If a template is provided all input elements inside will be detected and all onchange events will cause a signal to be sent with the corresponding inputs elements value            |
| class     | string             | A css class that will be added to the node, useful when adding the block in a handlebar template to avoid introducing an additional surrounding element just to provide a css class |

**click example 1 - button click to open image slider**

    <span data-entryscape="click" data-entryscape-signal="slider-lightbox-open">Open</span>

The slider block listens to the special signal `slider-lightbox-open`. This allows you to open the slider on another button than the default supported click on the image itself.

**click example 2 - tabs for searchlist**

    <div data-entryscape="click"
      data-entryscape-signal="searchList-open"
      data-entryscape-template='<input type="radio" name="lt" value="rows"/>Rows&nbsp;<input type="radio" name="lt" value="cards" checked="checked"/>Cards'
    </div>
    <div data-entryscape="searchList" data-entrscape-define="cards" ...>
    <div data-entryscape="searchList" data-entrscape-define="rows" 
         data-entryscape-visible="false" ...>

In the above code the signal `searchList-open` with the value `rows` will be sent when users select `Rows` and the signal `searchList-open` with the value `cards` will be sent when the user selects `Cards`. The two lists will be listening and show or hide each list respectively, note that the value is connected to the `define` parameter of the lists.

## expand
`filter condition`

The `expand` blocks allows you to hide and show a section of the page which may or may not contain other blocks.

| Parameter       | Value              | Description                                                                                                                                                                                                                                                                                                                                                   |
|:----------------|:-------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| expandhead      | handlebar template | A section that either displays a button or (if `clickExpand` is set to true) is clickable in it entirety. To make something act as a button inside of the template add the `esbExpandButton` class. Certain parts of the template may be hidden when collapsed (marked with the `esbExpanded` class) or when expanded (marked with the `esbCollapsed` class). |
| expandbody      | handlebar template | A section that can be expanded, it may contain blocks                                                                                                                                                                                                                                                                                                         |
| clickExpand     | boolean            | If true the entire `expandhead` template is clickable                                                                                                                                                                                                                                                                                                         |
| class           | string             | A css class that will be added to the node, useful when adding the block in a handlebar template to avoid introducing an additional surrounding element just to provide a css class                                                                                                                                                                           |
| initialExpandOn | string             | The expand block will be expanded on initial render if any of the collections in this comma separated list has a value set                                                                                                                                                                                                                                     |

**expand example 1 - clickable head template**

    <div data-entryscape="expand"
         data-entryscape-click-expand="true"
         data-entryscape-expandhead="Open or close"
         data-entryscape-expandbody="{{helloworld}}"></div>

**expand example 2 - custom expand button**

    <div data-entryscape="expand"
         data-entryscape-expandhead="<button class="esbExpandButton esbCollapsed">Open</button><button class="esbExpandButton esbExpanded">Close</button>"
         data-entryscape-expandbody="{{helloworld}}"></div>

## dialog

Renders a button or a clickable area which triggers a popover dialog on click. Filter controls and state controls placed inside the dialog template will not take effect until the dialog is saved

| Parameter        | Type                            | Default value       | Description                                               |
|:-----------------|:--------------------------------|:--------------------|:----------------------------------------------------------|
| openButtonText   | <code>string</code>             | <code>Open</code>   | The text label for the button that will open the dialog   |
| openTemplate     | <code>HandlebarsTemplate</code> |                     | A template to be used as toggle control instead of button |
| saveButtonText   | <code>string</code>             | <code>Save</code>   | The text label for the save button                        |
| cancelButtonText | <code>string</code>             | <code>Cancel</code> | The text label for the cancel button                      |
| class            | <code>string</code>             |                     | A class can be added to the dialog content container      |
| headerTemplate   | <code>HandlebarsTemplate</code> |                     | Handle bar template with content for heading              |
| contentTemplate  | <code>HandlebarsTemplate</code> |                     | Handle bar template with content of dialog                |

## popover

Renders a button or a clickable area which triggers a popover on click.

| Parameter       | Type                            | Default value      | Description                                               |
|:----------------|:--------------------------------|:-------------------|:----------------------------------------------------------|
| openButtonText  | <code>string</code>             |                    | The text label for the button that will open the dialog   |
| openTemplate    | <code>HandlebarsTemplate</code> |                    | A template to be used as toggle control instead of button |
| closeButtonText | <code>string</code>             | <code>Close</code> | The text label for the cancel button                      |
| class           | <code>string</code>             |                    | A class can be added to the dialog content container      |
| headerTemplate  | <code>HandlebarsTemplate</code> |                    | Handle bar template with content for heading              |
| contentTemplate | <code>HandlebarsTemplate</code> |                    | Handle bar template with content of dialog                |

## json

`filter condition`

The `json` block is similar to the text block in the sense that it displays a simple value. The distinction is that fetches the data from a json file rather than from the metadata of an entry.

| Parameter | Value           | Description                                                                                                                                                |
|:----------|:----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| url       | url             | A URL to a json file to be loaded.                                                                                                                         |
| attribute | string          | Picks the value for the given attribute in the loaded json object.                                                                                         |
| template  | lodash template | A [lodash template](https://lodash.com/docs/4.17.15#template) allowing picking out specific attributes as well as deeper substructures of the loaded json. |
| ondata    | function        | A function that will be called on load of the json, the function is passed the json object and the html node where the result is to be inserted.           |

**json example 1 - single attribute**

    <span data-entryscape="json"
          data-entryscape-url="http://example.com/ex.json" data-entryscape-attribute="name"></span>

**json example 2 - lodash template**
Lodash templates allows bringing out multiple values from the retrieved json object like this:

    <span data-entryscape="text"
          data-entryscape-json="http://example.com/ex.json"
          data-entryscape-content="Mr. ${name} ${lastname}"></span>

## error
This block provides a way to control where error messages are shown. If this block is not provided the default error dialog is used instead. To make sure that overall initialization of the page knows that there will be an error block (and avoids initializing the error dialog) the global [level parameter `errorBlock`](global.md#errorblock) needs to be set to true.   

| Parameter    | Value   | Description                                                                                                                          |
|:-------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------|
| delay        | number  | Number of milliseconds to wait before displaying an error / warning text, default is 2000.                                           |
| close        | string  | Text that will be set as title and aria-label on the close button                                                                    |
| inProgress   | string  | Warning text that will show when requests are slow to complete                                                                       |
| generic      | string  | Generic error text that will show when something goes wrong                                                                          |
| requireEntry | boolean | If true the block will show an error if the current page does not have a focused entry                                               |
| missingEntry | string  | Error text that will show when the current page does not have a focused entry given that the parameter `requireEntry` is set to true |

## config
This block allows you to provide [global level parameters](global.md) that have no direct visual rendering at the point of insertion of the block.
The [global level parameters](global.md) can be provided in several ways, see the section on [syntax](syntax.md) for more options.

## stateControl

 Checkbox control for changing state parameter value. Works with boolean [dynamic state parameters](global.md#state)

| Parameter      | Type                 | Default value     | Description                                                            |
|:---------------|:---------------------|:------------------|:-----------------------------------------------------------------------|
| stateParameter | <code>string</code>  |                   | The parameter to control with the checkbox                             |
| label          | <code>string</code>  | stateParameter    | The text label for the checkbox. Defaults to the name of the parameter |
| checkedIf      | <code>boolean</code> | <code>true</code> | Value for the checkbox                                                 |
| class          | <code>string</code>  |                   | A class can be added to the wrapper element                            |
