# Block syntax

There are four different syntactical expressions for a block in your page. Each syntax is complete, there is nothing that
can be expressed in one syntax that cannot be expressed in another. Choosing one syntax in favour of another is a matter
of taste and readability.

Note that the use of [custom blocks](custom.md), may make it easier to use the recommended approach (option 1) as
the complexity is hidden from the expression in the page. 

## Option 1: Multiple attributes - Recommended
This is the preferred option for describing a block if the amount of parameters and their values are reasonably small.

    <span data-entryscape="text" 
          data-entryscape-entry="1"
          data-entryscape-context="1"
          data-entryscape-entrystore="http://example.com/store"></span>

## Option 2: Single attribute as JSON - Use for plugins / integrations
This option is more compact, but it can be complicated to get all the quotes right. The value has to be correct json.

    <span data-entryscape='{"block": "text", "entry": "1", "context": "1",
                            "entrystore": "http://example.com/store"}'></span>

## Option 3: Block JSON script - Use for many parameters
This option is a good approach when the block requires many or large values of the parameters. 

    <script type="text/x-entryscape-json">
      {
         "block": "text",
         "entry": "1",
         "context": "1",
         "entrystore": "http://example.com/store"
      }
    </script>

## Option 4: Block handlebar script - use for large templates
In situations where blocks takes templates as parameters the template may become large and awkward to 
maintain as data attributes or as part of a JSON expressions inside a single parameter 
(especially when trying to correctly escape quotes).
The following example shows how to use the script type `text/x-entryscape-handlebar` for the block `template`
(which is the easiest block that uses a single handlebar template).

    <script type="text/x-entryscape-handlebar" data-entryscape="template">
      <h4>{{text}}</h4>
      <div class="description">{{text content="${dcterms:description}"}}</div>
    </script>
    
Note that the template in turn uses two instances of the text block.

