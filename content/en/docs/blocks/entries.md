# Loading entries
At the core of EntryStore is the concept of an entry.
An entry consists of information (metadata) describing some entity (a resource). 
The entity may be a person, a webpage, an uploaded file etc.

In this section we describe how to specify which entry to work with by providing parameters on blocks, as page parameters / in the URL, or via global parameters. This guidance is common for many blocks like text, link, view and some configurations of list.

Note that there are a few blocks that can load data from JSON files rather than entries. However, as this is not the norm it is documented per block and not here.

## Entry from parameters
Loading an entry from parameters can be done per block, from page parameters / in the URL, or via global parameters. The follow sections will explain this in more detail, but since the parameters are the same for all situations we explain them independently first.        

There are three ways you can point to a specific entry, by entryid, via the resourceURI of the entry, or by looking up the entry via a (unique) value for a specific property. The corresponding parameters are `entry`, `uri`, and `lookup` and requires / are complemented by additional parameters, lets look at them in detail:

| Parameter     | Value  | Description                                                                                                                                                                                                                                                                                                                                         |
|:--------------|:-------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| entry         | string | An entry id, must be provided in combination with a context id.                                                                                                                                                                                                                                                                                     |
| context       | string | A context id, should be used together with `entry`, `uri`, or `lookup` parameters. Note that for some blocks, like `list` it's meaning it is used to scope the search for all entries in the list rather than to retrieve a single entry.                                                                                                           |
| uri           | string | This is the uri of a resource described by an entry. Since multiple entries may describe the same entry it is often useful to scope it to a certain context where there is a unique entry for that entry. In the terminology of an entry this is the `resourceURI` of the entry. If multiple entries match which will be returned is unpredictable. |
| lookup        | string | This parameter allows the lookup of an entry based on a specific value. If no property is given via the parameters `lookupURI` or `lookupLiteral` the value of the lookup will be assumed to be a literal pointed to via `dcterms:identifier`.                                                                                                      |
| lookupLiteral | string | Indicates the property to use in the lookup, the lookup will be treated as a literal.                                                                                                                                                                                                                                                               |
| lookupURI     | string | Indicates the property to use in the lookup, the lookup will be treated as a URI.                                                                                                                                                                                                                                                                   |

### Entry from block parameters
Sometimes you know in advance which entry to present and want to specify the entry directly in the block.
Typically, you want to tell a story where information from a certain property of a known entity should enhance the text. For instance:

    <p>In stockholm the population has grown substantially the last 50 years and
    is now estimated to be <span data-entryscape="text"
        data-entryscape-context="2"
        data-entryscape-entry="34"
        data-entryscape-content="${dbpo:populationSize"></span>.</p>
        
### Entry from page parameters
A common scenario is to have one webpage for searching and one webpage to see the details for each search result.
In this case you cannot hardcode the entry as a parameter to the block, instead it has to be detected from page parameters.
Insert page parameters like this:

    https://example.com/detailspage#esc_entry=1&esc_context=1

If you want to change to another expression you can specify this in the global `__entryscape_config` variable like this
(to avoid potential conflicts):

     __entryscape_config = {
        //  This is the default value
        hashParamsPrefix: "esc_"
     };

### Entry from the URL of the page
An alternative to providing page parameters in the fragment is to extract information from the rest of the URL. For instance, you may want to extract the entry and context id from the path or a query parameter. For example, in the URL below the entry id is given at the end of the URL while the context id is hardcoded in the configuration:

    https://data.riksarkivet.se/tora/14

How this extraction is done depends on the URL design of the site and needs a custom configuration. Such a configuration is done in the [routes global parameter](global.md#routes).

### Entry from global level parameters
Finally, an entry can be given explicitly in the global configuration, this is seldom what you want to do, but can provide a fallback alternative when testing. See [global level entry parameters](global.md#entry-parameters).

## Entry from search result inside of lists
Blocks used within rowHead or rowExpand templates (in list blocks) will be instantiated with the entry of each search result.

## Entries one step away - using relation and relationinverse
Sometimes blocks needs to retrieve entries related to the entry provided by the page parameter.
For example, a page may want to display information about both an organization and the related site 
(via org:hasSite property) it is located at. Many of the blocks support the relation and relationinverse parameters
to indicate the property to follow from the current entry, e.g.:

    Organization: <span data-entryscape='{"block": "text"}'></span><br>
    Site: <span data-entryscape='{"block": "text", relation: "org:hasSite"}'></span>
 
## Avoiding loading the same entry twice - define and use
Fetching a related entry nearly always requires a request to entrystore to be made and will cause a small delay.
If multiple blocks want to present information about a single related entry it is clearly unnecessary to request the
same entry multiple times. The "define" and "use" mechanism lets you avoid this problem by allowing one block be the main
responsible for requesting an entry and then "define" it when it is loaded.
The blocks that have stated they want to "use" an entry will get rendered first when it is available.
Let's continue with the example above, but now you want to have the name of the site in an h3 element and the rest of
the metadata below.

    Site <h3><span data-entryscape=
        '{"block": "text", "relation": "org:hasSite", "define": "site"}'></span></h3>
    <p data-entryscape=
        '{"block": "viewMetadata", "rdformsid": "org:Site", "use": "site"}'></span>
