# Blocks introduced in opendata extension
The main part of the opendata extension is the extra blocks it defines. Below we divide them into standalone and supportive blocks. In most cases it should be enough to use one of the standalone blocks with the supported parameters listed below.

## Standalone blocks - views and layouts
The following blocks are stand alone, meaning in most cases it is enough to embed a single one of these on a webpage. You will notice that many of the parameters are the same across these blocks, the reason being that they use different combinations of more underlying supportive blocks. These supportive blocks are listed in the following section.

### datasetView
Use this block on a landing page for a dataset. The dataset to show is detected from parameters in the url.

| Parameter                   | Value   | Description                                                                                                                                                        |
|:----------------------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| hl                          | number  | The heading level to start with in the block, default is 1                                                                                                         |
| pageInUrl                   | boolean | Adds a query parameter to the url to enable search-bots crawling. Also enables  navigating back to last page no                                                    |
| includeExtras               | boolean | Include showcases and ideas, default is false                                                                                                                      |
| includeVisualizations       | boolean | Include visualizations as a list after all other data, default is false                                                                                            |
| includeTabbedVisualizations | boolean | Include visualization in a tabbed view at the top, default is false                                                                                                |
| includeAutoVisualizations   | boolean | Include automatic visualizations for wms distributions (Use if no visualizations are added in Entryscape or if they are not harvested on e.g. a portal)            |
| includeDataPreview          | boolean | Include preview of tabular data, default is false                                                                                                                  |
| includeIndicators           | boolean | Shows indicators, default is false. Indicators only works as expected when run against an EntryScape Registry due to some metadata being cached differently there. |
| clickExpand                 | boolean | Click anywhere on distribution row expands the metadata, default is true                                                                                           |
| registry                    | boolean | Set to true if the block is used against harvested data in EntryScape Registry to enable improved lookup of filenames                                              |
| legend                      | boolean | Turn off/on legends in WMS visualizations                                                                                                                          |

### dataserviceView
Use this block on a landing page for a data service. Which data service to show is detected from parameters in the url.

| Parameter         | Value   | Description                                                |
|:------------------|:--------|:-----------------------------------------------------------|
| hl                | number  | The heading level to start with in the block, default is 1 |
| includeIndicators | boolean | Shows indicators, default is false                         |

### datasetListLayout
Use this block for an expandable list of datasets.

| Parameter                   | Value   | Description                                                                                                                                                        |
|:----------------------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| hl                          | number  | The heading level to start with in the block, default is 1                                                                                                         |
| pageInUrl                   | boolean | Adds a query parameter to the url to enable search-bots crawling. Also enables  navigating back to last page no                                                    |
| includeExtras               | boolean | Include showcases and ideas, default is false                                                                                                                      |
| includeVisualizations       | boolean | Include visualizations as a list after all other data, default is false                                                                                            |
| includeTabbedVisualizations | boolean | Include visualization in a tabbed view at the top, default is false                                                                                                |
| includeAutoVisualizations   | boolean | Include automatic visualizations for wms distributions (Use if no visualizations are added in Entryscape or if they are not harvested on e.g. a portal)            |
| includeDataPreview          | boolean | Include preview of tabular data, default is false                                                                                                                  |
| includeIndicators           | boolean | Shows indicators, default is false. Indicators only works as expected when run against an EntryScape Registry due to some metadata being cached differently there. |
| clickExpand                 | boolean | Click anywhere on distribution row expands the metadata, default is true                                                                                           |
| sortOrder                   | string  | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                           |
| registry                    | boolean | Set to true if the block is used against harvested data in EntryScape Registry to enable improved lookup of filenames                                              |
| legend                      | boolean | Turn off/on legends in WMS visualizations                                                                                                                          |


### datasetSimpleSearchLayout

| Parameter                   | Value   | Description                                                                                                                                             |
|:----------------------------|:--------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
| hl                          | number  | The heading level to start with in the block, default is 2                                                                                              |
| pageInUrl                   | boolean | Adds a query parameter to the url to enable search-bots crawling. Also enables  navigating back to last page no                                         |
| includeExtras               | boolean | Include showcases and ideas, default is false                                                                                                           |
| includeVisualizations       | boolean | Include visualizations as a list after all other data, default is false                                                                                 |
| includeTabbedVisualizations | boolean | Include visualization in a tabbed view at the top, default is false                                                                                     |
| includeAutoVisualizations   | boolean | Include automatic visualizations for wms distributions (Use if no visualizations are added in Entryscape or if they are not harvested on e.g. a portal) |
| includeDataPreview          | boolean | Include preview of tabular data, default is false                                                                                                       |
| includeSorting              | boolean | Include a dropdown for switching to sorting alphabetically                                                                                              |
| sortOrder                   | string  | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                |
| includeServices             | boolean | If set to true will also include datasets that are data services in the list (sv,en,de only)                                                            |

### datasetMultiSearchLayout

| Parameter                   | Value   | Description                                                                                                                                             |
|:----------------------------|:--------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
| hl                          | number  | The heading level to start with in the block, default is 2                                                                                              |
| pageInUrl                   | boolean | Adds a query parameter to the url to enable search-bots crawling. Also enables  navigating back to last page no                                         |
| includeExtras               | boolean | Include showcases and ideas, default is false                                                                                                           |
| includeVisualizations       | boolean | Include visualizations as a list after all other data, default is false                                                                                 |
| includeTabbedVisualizations | boolean | Include visualization in a tabbed view at the top, default is false                                                                                     |
| includeAutoVisualizations   | boolean | Include automatic visualizations for wms distributions (Use if no visualizations are added in Entryscape or if they are not harvested on e.g. a portal) |
| includeDataPreview          | boolean | Include preview of tabular data, default is false                                                                                                       |
| includeSorting              | boolean | Include a dropdown for switching to sorting alphabetically                                                                                              |
| sortOrder                   | string  | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                |
| legend                      | boolean | Turn off/on legends in WMS visualizations. Overrides selected option in Entryscape.                                                                     |
| includeServices             | boolean | If set to true will also include datasets that are data services in the list (sv,en,de only)                                                            |

### datasetFacetSearchLayout

| Parameter                   | Value   | Description                                                                                                                                                                                      |
|:----------------------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| hl                          | number  | The heading level to start with in the block, default is 2                                                                                                                                       |
| pageInUrl                   | boolean | Adds a query parameter to the url to enable search-bots crawling. Also enables  navigating back to last page no                                                                                  |
| includeExtras               | boolean | Include showcases and ideas, default is false                                                                                                                                                    |
| includeVisualizations       | boolean | Include visualizations as a list after all other data, default is false                                                                                                                          |
| includeTabbedVisualizations | boolean | Include visualization in a tabbed view at the top, default is false                                                                                                                              |
| includeAutoVisualizations   | boolean | Include automatic visualizations for wms distributions (Use if no visualizations are added in Entryscape or if they are not harvested on e.g. a portal)                                          |
| includeDataPreview          | boolean | Include preview of tabular data, default is false                                                                                                                                                |
| break                       | string  | A width, typically given in pixels that indicates when the two column should be transformed into a single column with a expandable filter in a responsive manner. The default value is `1024px`. |
| includeSorting              | boolean | Include a dropdown for switching to sorting alphabetically                                                                                                                                       |
| sortOrder                   | string  | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                                                         |
| hvdFilter                   | boolean | If set to true the facet filter will include HVD                                                                                                                                                 |
| typeFilter                  | boolean | If set to true the facet filter will include Type (dataset series, dataset, dataservice) (sv,en,de only)                                                                                         |
| legend                      | boolean | Turn off/on legends in WMS visualizations                                                                                                                                                        |
| includeServices             | boolean | If set to true will also include datasets that are data services in the list (sv,en,de only)                                                                                                     |

### datasetFacetSearchPortalLayout

| Parameter                 | Value   | Description                                                                                                                                                                                      |
|:--------------------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| hl                        | number  | The heading level to start with in the block, default is 2                                                                                                                                       |
| pageInUrl                 | boolean | Adds a query parameter to the url to enable search-bots crawling. Also enables  navigating back to last page no                                                                                  |
| includeDataPreview        | boolean | Include preview of tabular data, default is false                                                                                                                                                |
| includeIndicators         | boolean | Shows indicators, default is false                                                                                                                                                               |
| includeAutoVisualizations | boolean | Include automatic visualizations for wms distributions (Use if no visualizations are added in Entryscape or if they are not harvested on e.g. a portal)                                          |
| break                     | string  | A width, typically given in pixels that indicates when the two column should be transformed into a single column with a expandable filter in a responsive manner. The default value is `1024px`. |
| sortOrder                 | string  | A sort order specified according to the [Entrystore documentation on sorting](https://entrystore.org/kb/search/#sorting)                                                                         |
| hvdFilter                 | boolean | If set to true the facet filter will include HVD                                                                                                                                                 |

## HTML validation
In some cases validation tools will comment on the generated HTML structure. The structure can be slightly changed to avoid the comments. This is done by setting the global parameter `strictStandardHtml` to `true`.  
As attribute on  the data-entryscape-config tag `data-strict-standard-html = "true"`
Or in JSON format for e.g. config handlebar template or config file `strictStandarHtml: true`

## Supportive blocks - used by views and layouts
If your needs cannot be fulfilled with the standalone blocks you can use these supportive blocks to have more complete control. Note that the majority of the standalone blocks are constructed by combining the supportive blocks, for instance if we look at the handlebar template of `datasetSimpleSearchLayout`:

    {{datasetSearch}}{{datasetResults}}{{datasetList includeExtras="inherit" 
      includeVisualizations="inherit" includeDataPreview="inherit" hl="inherit"}}

We see that it uses the `datasetSearch`, `datasetResults` and `datasetList` blocks that are defined below.

**Note:** many of the blocks below extend a single existing built-in block by providing default parameters for them, if this is the case we indicate this at the start of each block definition below. This will make it easier to look up that block and see which additional parameters that are available. A few blocks are more complicated by utilizing a combination of several blocks, this is accomplished by extending the template block.

### datasetSearch
Extends simpleSearch

### datasetMultiSearch
Extends multiSearch

### datasetResults
Extends results

### datasetList
Extends searchList
Used by all standalone search layouts datasetSimpleSearchLayout, datasetMultiSearchLayout, datasetFacetSearchLayout and datasetFacetSearchPortalLayout. Defines datasetList for reuse by datasetExport and datasetResults

### facets_sv
Extends facets

### facetSearchResponsiveness
Extends template.
Adds style for media query to hide and show facets and mobile filters. The break parameter on datasetFacetSearchLayout and datasetFacetSearchPortalLayout can be used to change default `1024px` break point;

### datasetExports
Extends template.
Adds export blocks with preset format and use datasetList