import {urlParameterStore} from "./urlParameterConfig.js";

/**
 * The HTML div snippets for blocks preview and code to copy dialog.
 * The key of each object is used as attribute for the BlocksContent element on the CMS page
 *
 * @param {function} getClickString
 * @returns {{datasetSearch: {codeDialogHeading: string, visible: boolean, config: {"data-entryscape": string, "data-entryscape-strict-standard-html": string, "data-entryscape-entrystore": (string|*), "data-entryscape-context": *, "data-entryscape-clicks": string, "data-entryscape-url-query-parameters": boolean}, block: {"data-entryscape": (string|ScrollLogicalPosition|string|*), "data-entryscape-include-visualizations": boolean, "data-entryscape-include-tabbed-visualizations": boolean, "data-entryscape-include-extras": boolean, "data-entryscape-include-data-preview": boolean, "data-entryscape-include-services": string, "data-entryscape-page-in-url": boolean, "data-entryscape-include-series": boolean}}, datasetLanding: {codeDialogHeading: string, visible: boolean, config: {"data-entryscape": string, "data-entryscape-strict-standard-html": string, "data-entryscape-entrystore": (string|*), "data-entryscape-context": *}, block: {"data-entryscape": (string|string|*), "data-entryscape-include-visualizations": boolean, "data-entryscape-include-tabbed-visualizations": boolean, "data-entryscape-include-extras": boolean, "data-entryscape-include-data-preview": boolean}}}}
 */
const getBlocksSnippetConfig = (getClickString) => {
  const clickString = getClickString();
  const userConfig = urlParameterStore.getUserConfig();
  return {
    datasetSearch: {
      codeDialogHeading: 'For pasting in to your Open data page',
      visible: userConfig.storeUrl !== '',
      config: {
        'data-entryscape': 'config',
        'data-entryscape-strict-standard-html': 'true',
        'data-entryscape-entrystore': userConfig.entrystore,
        'data-entryscape-context': userConfig.context,
        'data-entryscape-clicks': clickString,
        'data-entryscape-url-query-parameters': userConfig.seo === 'true',
      },
      block: {
        'data-entryscape': userConfig.block,
        'data-entryscape-include-visualizations': (userConfig.viz === 'true' && userConfig.tabbedViz === 'false'),
        'data-entryscape-include-tabbed-visualizations': (userConfig.viz === 'true' && userConfig.tabbedViz === 'true'),
        'data-entryscape-include-extras': userConfig.extras === 'true',
        'data-entryscape-include-data-preview': userConfig.preview === 'true',
        'data-entryscape-include-services': userConfig.services === 'true',
        'data-entryscape-page-in-url': userConfig.seo === 'true',
        'data-entryscape-include-sorting': userConfig.sorting === 'true',
      }
    },
    datasetLanding: {
      codeDialogHeading: 'For pasting in to your dataset details page',
      visible: userConfig.storeUrl !== '' && userConfig.block !== 'datasetListLayout',
      config: {
        'data-entryscape': 'config',
        'data-entryscape-strict-standard-html': 'true',
        'data-entryscape-entrystore': userConfig.entrystore,
        'data-entryscape-context': userConfig.context
      },
      block: {
        'data-entryscape': userConfig.clickBlock,
        'data-entryscape-include-visualizations': (userConfig.clickViz === 'true' && userConfig.clickTabbedViz === 'false'),
        'data-entryscape-include-tabbed-visualizations': (userConfig.viz === 'true' && userConfig.tabbedViz === 'true'),
        'data-entryscape-include-extras': userConfig.extras === 'true',
        'data-entryscape-include-data-preview': userConfig.preview === 'true'
      }
    }
  }
}

/**
 *
 * @return {`{"dataset": "details/?${string}"}`}
 */
const getPreviewClickString = () => {
  const detailedPageQueryString = urlParameterStore.getQueryString();
  return `{"dataset": "details/?${detailedPageQueryString}"}`;
}

/**
 *
 * @return {`{"dataset": "${string}"}`}
 */
const getSnippetClickString = () => {
  const clickValue = urlParameterStore.getUserConfigValue('clicks');
  return `{"dataset": "${clickValue}"}`;
}


/**
 * src attribute values for the script tags
 *
 * @type {string[]}
 */
export const blocksScripts = [
  `https://static.entryscape.com/blocks-ext/1/opendata/opendata-${urlParameterStore.getUserConfigValue('lang')}.js`,
  'https://static.entryscape.com/blocks/1/app.js'];


/**
 *
 * @type {{datasetSearch: {codeDialogHeading: string, visible: boolean, config: {"data-entryscape": string, "data-entryscape-strict-standard-html": string, "data-entryscape-entrystore": (string|*), "data-entryscape-context": *, "data-entryscape-clicks": string, "data-entryscape-url-query-parameters": boolean}, block: {"data-entryscape": (string|ScrollLogicalPosition|*), "data-entryscape-include-visualizations": boolean, "data-entryscape-include-tabbed-visualizations": boolean, "data-entryscape-include-extras": boolean, "data-entryscape-include-data-preview": boolean, "data-entryscape-include-services": string, "data-entryscape-page-in-url": boolean}}, datasetLanding: {codeDialogHeading: string, visible: boolean, config: {"data-entryscape": string, "data-entryscape-strict-standard-html": string, "data-entryscape-entrystore": (string|*), "data-entryscape-context": *}, block: {"data-entryscape": (string|*), "data-entryscape-include-visualizations": boolean, "data-entryscape-include-tabbed-visualizations": boolean, "data-entryscape-include-extras": boolean, "data-entryscape-include-data-preview": boolean}}}}
 */
export const blocksPreviewConfig = getBlocksSnippetConfig(getPreviewClickString);

/**
 *
 * @type {{datasetSearch: {codeDialogHeading: string, visible: boolean, config: {"data-entryscape": string, "data-entryscape-strict-standard-html": string, "data-entryscape-entrystore": (string|*), "data-entryscape-context": *, "data-entryscape-clicks": string, "data-entryscape-url-query-parameters": boolean}, block: {"data-entryscape": (string|ScrollLogicalPosition|*), "data-entryscape-include-visualizations": boolean, "data-entryscape-include-tabbed-visualizations": boolean, "data-entryscape-include-extras": boolean, "data-entryscape-include-data-preview": boolean, "data-entryscape-include-services": string, "data-entryscape-page-in-url": boolean}}, datasetLanding: {codeDialogHeading: string, visible: boolean, config: {"data-entryscape": string, "data-entryscape-strict-standard-html": string, "data-entryscape-entrystore": (string|*), "data-entryscape-context": *}, block: {"data-entryscape": (string|*), "data-entryscape-include-visualizations": boolean, "data-entryscape-include-tabbed-visualizations": boolean, "data-entryscape-include-extras": boolean, "data-entryscape-include-data-preview": boolean}}}}
 */
export const copyCodeDialogConfig = getBlocksSnippetConfig(getSnippetClickString);
