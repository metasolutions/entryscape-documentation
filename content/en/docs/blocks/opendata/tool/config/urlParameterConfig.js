import {UrlParameterStore} from "../util/UrlParameterStore.js";

/**
 * Url parameters to use for creating the snippets. Values are used for the urlParameters in configDialogConfig
 *
 * @type {{lang: string, storeUrl: string, block: string, viz: string, tabbedViz: string, extras: string, preview: string, seo: string, clicks: string, clickBlock: string, clickViz: string, clickTabbedViz: string, clickExtras: string, clickPreview: string}}
 */
const defaultUrlParameterConfig = {
  lang: 'sv',
  storeUrl: '',
  block: 'datasetListLayout',
  viz: 'false',
  tabbedViz: 'false',
  extras: 'false',
  preview: 'false',
  services: 'false',
  seo: 'true',
  sorting: 'false',
  clicks: '',
  clickBlock: 'datasetView',
  clickViz: 'false',
  clickTabbedViz: 'false',
  clickExtras: 'false',
  clickPreview: 'false',
};

export const urlParameterStore = new UrlParameterStore(defaultUrlParameterConfig);
