import {urlParameterStore} from "./urlParameterConfig.js";


const _config = urlParameterStore.getUserConfig();

export const configDialogConfig = [
  {
    type: 'section',
    form: [
      {
        urlParameter: 'lang',
        id: 'lang',
        label: 'Language',
        help: 'Language of the embedding. Data will be shown in this language if available',
        type: 'select',
        options: [{value: 'sv', text: 'sv'}, {value: 'en', text: 'en'}, {value: 'de', text: 'de'}, {
          value: 'it',
          text: 'it'
        }],
        wide: true
      },
      {
        urlParameter: 'storeUrl',
        id: 'storeUrl',
        label: 'URL to your catalog in Entryscape',
        help: 'Copy the url of one specific published catalog to show only that one. Or show all published catalogues by using the url to the list of all catalogues in Entryscape',
        type: 'input',
        value: _config.storeUrl,
        wide: true
      },
      {
        urlParameter: 'block',
        id: 'blockType',
        label: 'Type of layout',
        help: 'The "List" layout option is the simplest one and can be embedded on a single page on your site',
        type: 'select',
        options: [
          {value: 'datasetListLayout', text: 'List'},
          {value: 'datasetSimpleSearchLayout', text: 'Searchable list'},
          {value: 'datasetMultiSearchLayout', text: 'Typeahead search list'},
          {value: 'datasetFacetSearchLayout', text: 'Typeahead search list + facets'}
        ],
        wide: true
      },
      {
        urlParameter: 'sorting', //TODO! Brakes the layout
        visible: (_config.block !== 'datasetListLayout' && _config.block !== undefined),
        toggle: {id: 'blockType', value: ['datasetSimpleSearchLayout', 'datasetMultiSearchLayout', 'datasetFacetSearchLayout']},
        id: 'sorting',
        label: 'Include sorting option',
        help: 'Includes a dropdown for sorting the list',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'viz',
        id: 'visualisations',
        label: 'Include visualizations',
        help: 'Visualizations created inside Entryscape can be included at the top of each dataset or as a list after it',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'tabbedViz',
        id: 'tabbedViz',
        toggle: {id: 'visualisations', value: ['true']},
        visible: _config.viz === 'true',
        type: 'select',
        options: [
          {value: 'false', text: 'In a list beneath the datasets'},
          {value: 'true', text: 'Above dataset in tabs'}
        ],
        wide: false
      },
      {
        urlParameter: 'services',
        id: 'services',
        label: 'Include dataset services',
        help: 'If you have dataset services that are not just distributions but standalone datasets this should be activated',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'extras',
        id: 'extras',
        label: 'Include showcases och ideas',
        help: 'Use only if you have Showcases or Ideas in your project',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'preview',
        id: 'preview',
        label: 'Include preview of CSV data',
        help: 'A preview will be added inside expanded views of csv distributions that support it (Not same as visualizations)',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'seo',
        id: 'seo',
        label: 'Use indexable pages',
        help: 'Set this to false if you don\'t want detailed pages to be indexable by e.g. google or if your site is incompatible with the parameters used',
        type: 'select',
        options: [
          {value: 'true', text: 'Yes'},
          {value: 'false', text: 'No'}
        ],
        wide: false
      },
    ]
  },
  {
    type: 'section',
    visible: (_config.block !== 'datasetListLayout' && _config.block !== undefined),
    id: 'conditionalConfig',
    toggle: {id: 'blockType', value: ['datasetSimpleSearchLayout','datasetMultiSearchLayout',
        'datasetFacetSearchLayout']},
    form: [
      {
        type: 'heading',
        text: 'For the details page',
        help: 'All layout options except "List" requires an extra page for displaying dataset-details. Add the page in the CMS and give the relative URL below'
      },
      {
        urlParameter: 'clicks',
        id: 'clicks',
        label: 'Relative URL for dataset details page',
        help: 'Use the relative url e.g. /details/ or full path to the detailed page e.g. https://example.com/details/',
        type: 'input',
        value: _config.clicks,
        wide: true
      },
      {
        urlParameter: 'clickViz',
        id: 'clickVisualisations',
        label: 'Include visualizations',
        help: 'Visualizations created inside Entryscape can be included at the top of each dataset or as a list after it',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'clickTabbedViz',
        id: 'clickTabbedViz',
        toggle: {id: 'clickVisualisations', value: ['true']},
        visible: _config.clickViz === 'true',
        type: 'select',
        options: [
          {value: 'false', text: 'In a list beneath the datasets'},
          {value: 'true', text: 'Above dataset in tabs'}
        ],
        wide: false
      },
      {
        urlParameter: 'clickExtras',
        id: 'clickExtras',
        label: 'Include showcases och ideas',
        help: 'Use only if you have Showcases or Ideas in your project',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
      {
        urlParameter: 'clickPreview',
        id: 'clickPreview',
        label: 'Include preview of CSV data',
        help: 'A preview will be added inside expanded views of csv distributions that support it (Not same as visualizations)',
        type: 'select',
        options: [
          {value: 'false', text: 'No'},
          {value: 'true', text: 'Yes'}
        ],
        wide: false
      },
    ]
  }
];
