import {blocksPreviewConfig, blocksScripts} from "../config/blocksSnippetConfig.js";
import createHTML from "../util/HTML.js";
import {urlParameterStore} from "../config/urlParameterConfig.js";


export class BlocksContent extends HTMLElement {
  constructor() {
    super();
    this._pageName = this.getAttribute('page');
    this._entrystore = urlParameterStore.getUserConfigValue('entrystore');
  }
  static get observedAttributes() {
    return ['page'];
  }

  connectedCallback() {
    this.redrawItAll();
  }

  redrawItAll() {
    if (!this._entrystore) return;

    const blocksContent = [
      {
        tagName: 'style',
        text: '*,*::before,*::after{box-sizing:border-box}'
      },
      {
        tagName: 'div',
        attributes: blocksPreviewConfig[this._pageName].config,
      },
      {
        tagName: 'div',
        attributes: blocksPreviewConfig[this._pageName].block,
      }
    ];
    blocksContent.forEach((content) => this.appendChild(createHTML(content)));

    blocksScripts.forEach((script) => {
      if (script !== '') {
        const scriptElement = document.createElement('script');
        scriptElement.src = script;
        scriptElement.async = false;
        this.appendChild(scriptElement);
      }
    });
  }
}

window.customElements.define('blocks-content', BlocksContent);
