export class BlocksEditPanel extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({
      mode: 'open',
    });
    // this._parameterStore = new UrlParameters();
    this._configDialog = null;
    this._codeDialog = null;
  }

  connectedCallback() {
    const template = `
<style>
*,
*::before,
*::after{box-sizing:border-box;}
.control {
    display: flex;
    width: 100%;
    justify-content: center;
}
.control button {
    font: inherit; 
    display: block;
    font-size: 1em;
    padding: 0.5em;
    margin: 0.5em;
    border-width: 0;
    border-radius: 0.4em;
    cursor: pointer;
    -webkit-appearance: none; 
    -moz-appearance: none;
}
button:hover {
   background-color: #e3f6fc;
}
button:active {
    background-color: white;
</style>
    <div class="control">
        <button id="configButton">Configure</button>
        <button id="codeButton">Show code to copy...</button>
    </div>
        `;
    this.shadow.innerHTML = template;
    const configButton = this.shadowRoot.getElementById('configButton');
    configButton.addEventListener('click', (e) => {
      if (this._configDialog !== null) {
        this._configDialog.remove();
        this._configDialog = null;
      }
      this._configDialog = document.createElement('config-dialog');
      document.querySelector('article').appendChild(this._configDialog);
      this._configDialog.addEventListener('hideModal', () => {
        this._configDialog.remove();
      });
    });
    this.shadowRoot.getElementById('codeButton').addEventListener('click', (e) => {
      if (this._codeDialog !== null) {
        this._codeDialog.remove();
        this._codeDialog = null;
      }
      this._codeDialog = document.createElement('code-dialog');
      document.querySelector('article').appendChild(this._codeDialog);
      this._codeDialog.addEventListener('hideModal', () => {
        this._codeDialog.remove();
      });
    });
  }
}

window.customElements.define('blocks-edit-panel', BlocksEditPanel);
