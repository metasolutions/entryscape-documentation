import createHTML from "../util/HTML.js";
import {getStructureFromConfigDialogConfig, getConfigDialogToggles} from "../util/configDialogUtils.js";
import {configDialogConfig} from "../config/configDialogConfig.js";

import {urlParameterStore} from "../config/urlParameterConfig.js";

export class ConfigDialog extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({
      mode: 'open',
    });
    this._dialogConfig = configDialogConfig;
    this._config = urlParameterStore.getUserConfig();
    this._toggles = getConfigDialogToggles(configDialogConfig);
    this._style = `
    *,
*::before,
*::after{box-sizing:border-box;}
details summary::-webkit-details-marker {
  display:none;
}
.modalHead--grid {
    display: grid;
    grid-template-columns: 1fr 4rem;
    align-items: center;
}
.modalHead--grid h2 {
    justify-self: center;
}
.esbModal--backDrop {
    background-color: #00000080;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
.esbModal__container {
    max-height: 90vh;
    overflow-y: auto;
    position: fixed; 
    z-index: 100;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    display: grid;
    box-shadow: 0 0 .2rem rgba(0, 0, 0, .1), 0 .2rem .4rem rgba(0, 0, 0, .2);
    grid-template-columns: minmax(95vw, 1fr);
    grid-gap: 0.7em;
    padding: 0.5em;
    background-color: #fff;
}
.esbModalContent {
    width: auto;
}
summary {
    margin-left: 10px;
}
.configForm--grid {
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: start;
  align-items: center;
  grid-gap: 0.5em;
  padding: 0.5em;
}
.configForm--grid details {
    display: inline;
}
.configForm--grid label, .configForm--grid h3 {
    justify-self: end;
    margin-right: 1em;
    text-align: right;
}
.configForm__field--wide {
    width: 90%;
    max-width: 90%;
}
input, select {
    font: inherit;
    height: 1.5rem;
}
.hidden {
    display: none;
}
.subChoice {
    grid-column-start: 2;
}
.configForm__saveContainer {
  display: grid;
  width: 100%;
}
button {
    font: inherit; 
    display: block;
    font-size: 1em;
    padding: 0.5em;
    margin: 0.5em;
    border-width: 0;
    border-radius: 0.4em;
    cursor: pointer;
    -webkit-appearance: none; 
    -moz-appearance: none;
}
button.close {
    justify-self: end;
    width: 2.5em;
}
button.save {
    justify-self: end;   
}
button:hover {
   background-color: #e3f6fc;
}
button:active {
    background-color: white;
}
details > summary {
    cursor: pointer;
    width: 1.2em;
    height: 1.2em;
    font-size: 1em;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    border: 1px solid black;
    border-radius: 0.6em;
}

details > p {
    position: absolute;
    background-color: white;
    margin: 1em;
    max-width: 100%;
    padding: 0.5em;
    border: 1px solid #c1c1c1;
    box-shadow: 0px 0px 13px 0px #C1C1C1;
}
@media screen and (min-width: 465px) {
.esbModal__container {
    grid-template-columns: minmax(30em, 1fr);
}`;
  }

  /**
   *
   * @returns {Array<Object>}
   */
  getFormStructure() {
    return this._dialogConfig
      .map((config) => (getStructureFromConfigDialogConfig(config)))
      .flat();
  }

  addContent() {
    const configDialogContainerStructure = [
      {tagName: 'style', text: this._style},
      {tagName: 'div', attributes: {class: 'esbModal--backDrop', id: 'backDrop'}},
      {
        tagName: 'div', attributes: {class: 'esbModal__container'}, children: [
          {
            tagName: 'div', attributes: {class: 'esbModalHead--grid'}, children: [
              {tagName: 'h2', text: 'Config for Blocks extension'},
              {
                tagName: 'button',
                text: '\u2716',
                attributes: {class: 'button close', id: 'close', 'area-label': 'Close Info dialogue', type: 'button'}
              },
            ],
          },
          {
            tagName: 'div', attributes: {class: 'esbModalContent'} , children: this.getFormStructure()},
          {
            tagName: 'div', attributes: {class: 'configForm__saveContainer'}, children: [
              {
                tagName: 'button',
                text: 'Save',
                attributes: {class: 'save', id: 'save', 'area-label': 'Save Info dialogue', type: 'button'}
              },
            ]
          }
        ]
      }
    ];

    configDialogContainerStructure.forEach(structure => this.shadow.appendChild(createHTML(structure)));
  }

  redrawItAll() {
    this.addContent();
    this.setFormValues();
    this.addListeners();
  }


  setFormValues() {
    this.shadow.querySelectorAll('select').forEach((select) => {
      select.querySelectorAll('option').forEach((option) => {
        if (option.value === this._config[select.name]) {
          option.setAttribute('selected', 'true');
        }
      });
    });
    this.shadow.querySelectorAll('input').forEach((input) => {
      input.value = this._config[input.name] ? this._config[input.name] : '';
    });
  }

  updateUrl() {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);
    Object.keys(this._config).forEach((name) => {
      params.set(name, this._config[name]);
    });
    url.search = params.toString();
    window.history.replaceState({}, document.title, url);
  }

  addListeners() {
    // Change in form
    this.shadow.addEventListener('change', (e) => {
      if (e.target.tagName !== 'INPUT' && e.target.tagName !== 'SELECT') return;
      this._config[e.target.name] = e.target.value;
    });

    // Save and close modal
    this.shadowRoot.getElementById('save').addEventListener('click', (e) => {
      this.updateUrl();
      location.reload();
    });
    document.addEventListener('keyup', (e) => {
      if (event.keyCode === 13) {
        this.updateUrl();
        location.reload();
      }
    });

    // Dispatch events
    this.shadow.getElementById('backDrop').addEventListener('click', (e) => {
      this.dispatchEvent(new CustomEvent('hideModal', {
        bubbles: true,
        composed: true,
      }));
    });
    this.shadowRoot.getElementById('close').addEventListener('click', (e) => {
      this.dispatchEvent(new CustomEvent('hideModal', {
        bubbles: true,
        composed: true,
      }));
    });

    // Toggle visible/hidden parts
    this._toggles.forEach(({id, value, targetId}) => {
      this.shadow.getElementById(id).addEventListener('change', (e) => {
        const toggleTargetElement = this.shadow.getElementById(targetId);
        const toggleTargetLable = this.shadow.querySelector(`[for="${targetId}"]`);
        if (value.includes(e.target.value)) {
          toggleTargetElement.classList.replace('hidden', 'visible');
          toggleTargetLable?.classList.replace('hidden', 'visible');
        } else {
          toggleTargetElement.classList.replace('visible', 'hidden');
          toggleTargetLable?.classList.replace('visible', 'hidden');
        }
      });
    })

    // Toggle help text
    const infoDetails = this.shadow.querySelectorAll('details');
    infoDetails.forEach((targetDetail) => {
      targetDetail.addEventListener('toggle', (e) => {
        if (targetDetail.open) {
          infoDetails.forEach((detail) => {
            if (detail !== targetDetail) {
              detail.open = false;
            }
          });
        }
      });
    });
  }

  connectedCallback() {
    this.redrawItAll();
  }
}

window.customElements.define('config-dialog', ConfigDialog);
