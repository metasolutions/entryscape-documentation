import {blocksScripts, copyCodeDialogConfig} from "../config/blocksSnippetConfig.js";
import createHTML from "../util/HTML.js";
import {urlParameterStore} from "../config/urlParameterConfig.js";

export class CodeDialog extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({
      mode: 'open',
    });
    this.codeDialogueConfig = copyCodeDialogConfig;
    this._config = urlParameterStore.getUserConfig();
    this._style = `
*,
*::before,
*::after{box-sizing:border-box;}
.modalHead--grid {
    display: grid;
    grid-template-columns: 1fr 4rem;
    align-items: center;
}
.modalHead--grid h2 {
    justify-self: center;
}
.esbModal--backDrop {
    background-color: #00000080;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
.esbModal__container {
    max-height: 90vh;
    overflow-y: auto;
    position: fixed; 
    z-index: 100;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    display: grid;
    box-shadow: 0 0 .2rem rgba(0, 0, 0, .1), 0 .2rem .4rem rgba(0, 0, 0, .2);
    grid-template-columns: minmax(100px, 1fr);
    gap: 0.5em;
    padding: 1em;
    background-color: #fff;
}
configForm--grid {
    display: grid;
    justify-content: space-around;
}
.codeContainer {
    width: 100%;
    background-color: white;
    border: 1px solid #c1c1c1;
    border-radius: 6px;
    padding: 0.5em;
}
.hidden {
    display: none;
}
button {
    font: inherit; 
    display: block;
    font-size: 1em;
    padding: 0.5em;
    margin: 0.5em;
    border-width: 0;
    border-radius: 0.4em;
    cursor: pointer;
    -webkit-appearance: none; 
    -moz-appearance: none;
}
button.close {
    justify-self: end;
    width: 2.5em;
}
button.copy {
    float: right;
    line-height: 1;
}
button.copy.copied {
    background-color: #00b0ff;
}
button.copy svg {
  width: 20px;
  height: 20px;
}
}
button:hover {
   background-color: #e3f6fc;
}
button:active {
    background-color: white;
}
code {
    display: inline-block;
    white-space: normal;
    max-width: 100%;
    word-break:break-all;
}`;
  }

  getCopyCodeDialogContent() {
    const visibleContent = ([pageKey, pageObject]) => (pageObject.visible);
    const getStructuredDialogueContent = ([pageKey, pageObject]) => {
      return {
        tagName: 'div',
        children: [
          {tagName: 'h3', text: pageObject.codeDialogHeading},
          {tagName: 'button', text: 'copy code', attributes: {class: 'copy', title: 'copy snippet'}},
          {
            tagName: 'div', attributes: {class: 'codeContainer'}, children: [
              {
                tagName: 'code', text: this.getBlocksSnippetString(pageObject)
              }
            ]
          }
        ]
      };
    }

    return Object.entries(this.codeDialogueConfig)
      .filter(visibleContent)
      .map(getStructuredDialogueContent);
  }

  getBlocksSnippetString({config, block}) {
    const codeContentStructure = [
      {
        tagName: 'div',
        attributes: config
      },
      {
        tagName: 'div',
        attributes: block
      }
    ];
    let code = codeContentStructure.map((structure) => (createHTML(structure).outerHTML)).join('');
    code += blocksScripts.map((script) => (`<script src="${script}"></script>`)).join('');
    return code;
  }

  addContent() {
    const dialogStructure = [
      {
        tagName: 'style',
        text: this._style,
      },
      {
        tagName: 'div',
        attributes: {class: 'esbModal--backDrop', id: 'backDrop'},
      },
      {
        tagName: 'div',
        attributes: {class: 'esbModal__container'},
        children: [
          {
            tagName: 'div',
            attributes: {class: 'modalHead--grid'},
            children: [
              {
                tagName: 'h2',
                text: 'Opendata blocks config'
              },
              {
                tagName: 'button',
                text: '\u2716',
                attributes: {
                  id: 'close', 'area-label': 'Close the info dialogue', class: 'button close'
                }
              },
            ],
          },
          {
            tagName: 'div',
            attributes: {class: 'configForm--grid'},
            children: this.getCopyCodeDialogContent()
          }
        ]
      }
    ];
    dialogStructure.forEach(structure => {
      this.shadow.appendChild(createHTML(structure));
    });
  }

  addListeners() {
    this.shadow.querySelectorAll('button.copy').forEach((button) => {
      button.addEventListener('click', (e) => {
        const buttonParent = e.target.parentElement;
        const codeElement = buttonParent.querySelector('code');
        const mainPageHTMLCode = codeElement.innerText;
        navigator.clipboard.writeText(mainPageHTMLCode).then(() => {
          this.classList.add('copied');
          setTimeout(() => {
            this.classList.remove('copied')
          }, 1000);
        });
      })
    })


    this.shadowRoot.getElementById('backDrop').addEventListener('click', () => {
      this.dispatchEvent(new CustomEvent('hideModal', {
        bubbles: true,
        composed: true,
      }));
    });
    this.shadowRoot.getElementById('close').addEventListener('click', () => {
      this.dispatchEvent(new CustomEvent('hideModal', {
        bubbles: true,
        composed: true,
      }));
    });
  }

  redrawItAll() {
    this.addContent();
    this.addListeners();
  }

  connectedCallback() {
    this.redrawItAll();
  }

}

window.customElements.define('code-dialog', CodeDialog);
