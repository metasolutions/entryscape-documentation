This page shows a preview for the dataset detailed page.

The area beneath is a preview of the embedding. **If the area is empty go back to the [Tool page](index.md) to start the guide**. 

The content will change depending on the configuration you make. The content is stylable and will also change apperance depending on the page it is surrounded by.

<style>
    .blocks-container{border:2px dashed #838383;border-radius:4px;min-height:50vh;padding:10px;}.entryscape h3{font-size:1rem}.entryscape h2{font-size:1rem}.entryscape h1{font-size:1.5rem}.md-typeset .entryscape ul.pagination:not([hidden]){display:flex}[dir=ltr] .md-typeset .entryscape ul li{margin:0}[dir=ltr] .md-typeset .entryscape dd{margin:0}.entryscape .esbRowControl .btn-sm{font-size: 0.5rem;padding:0.25rem 0.25rem}.md-sidebar--secondary:not([hidden]){display:none}
</style>

<div class="blocks-container">
    <blocks-content page="datasetLanding">
    </blocks-content>
</div>

<script src="../main.js" type="module">
</script>
