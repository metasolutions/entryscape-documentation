import { BlocksEditPanel } from './modules/BlocksEditPanel.js';
import { ConfigDialog } from './modules/ConfigDialog.js';
import { BlocksContent } from './modules/BlocksContent.js';
import { CodeDialog } from './modules/CodeDialog.js';
