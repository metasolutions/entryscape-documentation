This tool is a help for configuring the embedding of data from Entryscape Catalog.

This guide will help you do som basic settings for your embedding like type of layout, language and some options for display. There are advance options in the [opendata reference](../reference.md) but the code snippet you get here is sufficient for most cases.

When you are done with configuring you can copy-paste the code into your own page. You find the code by clicking "Show code to copy" below.

The List layout is the most basic one and in your webpage you only need one page to make it work. The others need a separate landing page so you will need to create a page for that too. e.g. exempel.se/opendata/ and exempel.se/dataset/

Click "Configure to get started"

<blocks-edit-panel></blocks-edit-panel>

The area below is a preview of the embedding. The content will change depending on the configuration you make. The content is stylable and will also change appearance depending on the page it is surrounded by. It is mobile friendly and accessible.

<style>
    .blocks-container{border:2px dashed #838383;border-radius:4px;min-height:50vh;padding:10px;}.entryscape h3{font-size:1rem}.entryscape h2{font-size:1rem}.entryscape h1{font-size:1.5rem}.md-typeset .entryscape ul.pagination:not([hidden]){display:flex}[dir=ltr] .md-typeset .entryscape ul li{margin:0}[dir=ltr] .md-typeset .entryscape dd{margin:0}.entryscape .esbRowControl .btn-sm{font-size: 0.5rem;padding:0.25rem 0.25rem}.md-sidebar--secondary:not([hidden]){display:none}.entryscape .btn{font-size: 1em}.esbTabs__container{height: 450px}
</style>
<div class="backDrop"></div>
<div class="blocks-container">
    <blocks-content page="datasetSearch" ></blocks-content>
</div>

<script src="main.js" type='module'>
</script>
