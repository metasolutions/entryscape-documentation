const _whitelistedAttributes = ['id', 'class', 'for', 'value', 'name', 'role', 'area-label'];

const _isSafe = (attribute) => {
  return _whitelistedAttributes.includes(attribute) || attribute.startsWith('data-entryscape');
}
/**
 * HTML attributes
 *
 * @typedef {object} Attributes
 * @property {string|undefined} [id]
 * @property {string|undefined} [class]
 * @property {string|undefined} [for]
 * @property {string|undefined} [value]
 * @property {string|undefined} [name]
 * @property {string|undefined} [data-entryscape*] - A custom data attribute starting with "data-entryscape"
 */

/**
 * @param {HTMLElement} element
 * @param {Attributes} attributes
 */
const addAttributes = (element, attributes={}) => {
  if (Object.keys(attributes).length === 0) return;
  Object.entries(attributes)
    .filter(([attribute, value]) => (_isSafe(attribute) && value))
    .forEach(([attribute, value]) => element.setAttribute(attribute, value));
}

/**
 * @param {string} tagName
 * @param {string} [text='']
 * @param {Array<object>} [children=[]]
 * @param {Attributes} [attributes={}]
 * @returns {HTMLElement}
 */

const createHTML = ({tagName, text='', attributes={}, children=[]}) => {
  let element = document.createElement(tagName);
  element.innerText = text;
  addAttributes(element, attributes);
  children.forEach(childObject => {
    const childElement = createHTML(childObject);
    element.appendChild(childElement);
  });
  return element;
}

export default createHTML
