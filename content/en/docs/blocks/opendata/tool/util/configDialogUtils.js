/**
 * @typedef {object} ConfigDialogObject
 * @property {'section' | 'heading' | 'select' | 'input'} type
 * @property {string} [urlParameter]
 * @property {string} [id]
 * @property {string} [label]
 * @property {string} [help]
 * @property {Array<object>} [options]
 * @property {boolean} [wide=false]
 * @property {object} [toggle]
 * @property {boolean} [visible=true]
 * @property {string} [text]
 * @property {Array<ConfigDialogObject>} [form=[]]
 */

/**
 * @typedef {object} ConfigDialogToggleObject
 * @property {string} id
 * @property {Array<string>} value
 * @property {string} targetId
 */

/**
 *
 * @param {string} id
 * @param {Array<object>} form
 * @param {boolean} visible
 * @returns {object}
 */
const getSectionStructure = (id, form, visible) => {
  return {
    tagName: 'div',
    attributes: {
      id,
      class: `configForm--grid ${visible ? 'visible' : 'hidden'}`
    },
    children: form.map((formObject) => (getStructureFromConfigDialogConfig(formObject))).flat()
  }
}

/**
 *
 * @param {Array<object>} options
 * @returns {object}
 */
const getOptionsStructure = (options) => {
  return options.map(({value, text}) => {
    return {
      tagName: 'option',
      text: text,
      attributes: {
        value: value
      }
    }
  });
}

/**
 *
 * @param {string} help
 * @returns {Array<object>}
 */
const getHelpStructure = (help) => {
  if (!help) return [];
  return [{
    tagName: 'details',
    children: [
      {tagName: 'summary', text: '?'},
      {tagName: 'p', text: help},
    ],
  }];
}

/**
 *
 * @param {string} id
 * @param {string} label
 * @param {string} help
 * @returns {object}
 */
const getLabelStructure = (id, label, help, visible) => {
  return {
    tagName: 'label',
    text: label,
    attributes: {for: id, class: `${visible ? 'visible' : 'hidden'}`},
    children: getHelpStructure(help)
  }
}

/**
 *
 * @param {string} type
 * @param {string} [id]
 * @param {string} [urlParameter]
 * @param {Array<object>} [options]
 * @param {boolean} [wide=false]
 * @param {boolean} [visible=true]
 * @param {object} [toggle]
 * @returns {object}
 */
const getFieldsStructure = (type, id, urlParameter, options, toggle, wide = false, visible = true) => {
  const subChoiceVisibleClassNames = visible ? ' subChoice visible' : ' subChoice hidden';
  return {
    tagName: type,
    attributes: {
      id: id,
      name: urlParameter,
      class: `${wide ? 'configForm__field--wide' : ''}${toggle ? subChoiceVisibleClassNames : ''}`
    },
    children: options ? getOptionsStructure(options) : [],
  }
}

/**
 *
 * @param {string} text
 * @param {string} [help]
 * @returns {Array<object>}
 */
const getHeaderStructure = (text, help) => {
  return [{
    tagName: 'h3',
    text,
  },
    ...getHelpStructure(help)
  ];
}

/**
 *
 * @param {ConfigDialogObject} config
 * @returns {Array<object>}
 */
const getStructureFromConfigDialogConfig = ({
                                   urlParameter,
                                   id,
                                   label,
                                   help,
                                   type,
                                   options,
                                   toggle,
                                   wide = false,
                                   visible = true,
                                   text,
                                   form = []
                                 }) => {
  const structure = [];
  switch (type) {
    case 'section':
      structure.push(getSectionStructure(id, form, visible));
      break;
    case 'heading':
      structure.push(...getHeaderStructure(text, help));
      break;
    case 'select':
    case 'input':
      if (label) {
        structure.push(getLabelStructure(id, label, help, visible));
      }
      structure.push(getFieldsStructure(type, id, urlParameter, options, toggle, wide, visible));
      break;
    default:
      break;
  }
  return structure;
};

/**
 *
 * @param {Array<ConfigDialogObject>} configs
 * @returns {Array<ConfigDialogToggleObject>}
 */
const getConfigDialogToggles = (configs) => {
  let toggles = [];
  configs.forEach((config) => {
    if (config.toggle) toggles.push({...config.toggle, targetId: config.id});
    if (!config.form || !Array.isArray(config.form)) return;
    toggles = [...toggles, ...getConfigDialogToggles(config.form)];
  });
  return toggles;
}

export {getStructureFromConfigDialogConfig, getConfigDialogToggles}
