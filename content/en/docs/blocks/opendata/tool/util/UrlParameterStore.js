/**
 * Used url parameters
 *
 * @typedef {object} UrlParametersObject
 * @property {string} lang Primary language to use
 * @property {string} storeUrl Url to the Entrystore
 * @property {string} block Name of the block for search page
 * @property {string} viz Should include visualisation block
 * @property {string} tabbedViz Show visualisations using tabbed layout
 * @property {string} extras Should include extras
 * @property {string} preview Should include table preview of CSV
 * @property {string} seo Should use query parameters
 * @property {string} clicks Relative path to the dataset page
 * @property {string} clickBlock Name of the block for dataset page
 * @property {string} clickViz Should include visualisation block on dataset page
 * @property {string} clickTabbedViz Show visualisations using tabbed layout on dataset page
 * @property {string} clickExtras Should include extras on dataset page
 * @property {string} clickPreview Should include table preview of CSV on datset page
 * @property {string} [context] Specific context to use for list
 */

/**
 * Entrystore parameters
 *
 * @typedef {object} EntrystoreParametersObject
 * @property {string} entrystore
 * @property {string} context
 */

/**
 * Complete parameters
 *
 * @typedef {UrlParametersObject & EntrystoreParametersObject} ParametersObject
 */


export class UrlParameterStore {
  /**
   *
   * @param {UrlParametersObject} defaultParameterValues
   */
  constructor(defaultParameterValues) {
    this._defaultParameterValues = defaultParameterValues;
    this._parameterValues = this._getParameterValues();
    this.getUserConfig = () => {
      return this._parameterValues;
    };
    this.getUserConfigValue = (parameterName) => {
      return this._parameterValues[parameterName];
    };
    this.getQueryString = () => {
      const configUrlParameters = this._getConfigFromUrlParams();
      const searchParams = new URLSearchParams(Object.entries(configUrlParameters));
      return searchParams.toString();
    };
  }

  /**
   * Get config set in url parameters
   *
   * @returns {ParametersObject}
   */
  _getConfigFromUrlParams() {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);
    return { ...this._defaultParameterValues, ...Object.fromEntries(params) };
  }


  /**
   * Get config parameters
   *
   * @returns {ParametersObject}
   */
  _getParameterValues() {
    const configFromUrlParams = this._getConfigFromUrlParams();
    if (configFromUrlParams.storeUrl === '') return configFromUrlParams;
    try {
      const url = new URL(configFromUrlParams.storeUrl);
      const contextUrlMatch = url.pathname.match(/.*\/(?:catalog|)\/([0-9a-zA-Z]+)\/?.*/) || [];
      return {
        ...configFromUrlParams,
        entrystore: `${url.origin}/store/`,
        context: contextUrlMatch[1],
      };
    } catch (e) {
      console.error(e);
    }
  }
}
