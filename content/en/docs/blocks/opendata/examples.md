# Examples of how to use the extension

Below are a few examples on how to embed the opendata extension blocks into one or several webpages.

**Note 1:** In each of the examples below you have to provide the URL to the EntryStore API, hence you need to replace the variable `_ES_API_` with something like `https://demo.entryscape.com/store`.

**Note 2:** To make the examples more readable we exclude the script inclusion. Hence, you always need to include the following in every page you have added blocks:

    <script src="https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js"></script>
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

## Example 1 - list of datasets
This case is suitable for a data provider that have between 0 and 20 datasets, i.e. the there is little need for making the datasets searchable.

    <div data-entryscape="datasetListLayout" 
         data-entryscape-entrystore="_ES_API_"></div>

## Example 2 - A landing page per dataset
Just like in example 1 we have a list of datasets, but, in addition to the list we also have a landing page for each dataset. Hence, we need to prepare two pages, one for the search page and one for the landing page.

**Search page**

    <div data-entryscape="datasetList"></div>
    <div data-entryscape="config"
         data-entryscape-entrystore="_ES_API_"    
         data-entryscape-clicks='{"dataset": "_PATH_TO_LANDINGPAGE_"}'></div>

Note that you will have to provide either a relative or absolute path to the landing page in place of the variable `_PATH_TO_LANDINGPAGE_"`.

**Landing page**

    <div data-entryscape="datasetView"></div>
    <div data-entryscape="config"
         data-entryscape-entrystore="_ES_API_"></div>

## Example 3 - A searchable list with landing page per dataset
This example is similar to example 2 with the difference that the block in the search page is `datasetSimpleSearchLayout` instead of `datasetList`:

    <div data-entryscape="datasetSimpleSearchLayout"></div>
    <div data-entryscape="config"
         data-entryscape-entrystore="_ES_API_"    
         data-entryscape-clicks='{"dataset": "_PATH_TO_LANDINGPAGE_"}'></div>

Note in this example, as well in examples below we make use of blocks that ends with `Layout`. The reason for these blocks to exist is to provide a combination of blocks to make the embedding easier. In this case the block combined are `datasetSearch`, `datasetResults` and `datasetList`.

## Example 4 - A searchable list with typeahead and a landing page per dataset

This example is similar to both example 2 and 3 with the difference that the search input field is more capable with typeahead towards filters such as keywords and themes.

    <div data-entryscape="datasetMultiSearchLayout"></div>
    <div data-entryscape="config"
         data-entryscape-entrystore="_ES_API_"
         data-entryscape-clicks='{"dataset": "_PATH_TO_LANDINGPAGE_"}'></div>

## Example 5 - A searchable list via facets, typeahead and a landing page per dataset

This is the crown jewel, this is the most capable search inteface allowing users to drill down via various filters (faceted search).

    <div data-entryscape="datasetFacetSearchLayout"></div>
    <div data-entryscape="config"
         data-entryscape-entrystore="_ES_API_"
         data-entryscape-clicks='{"dataset": "_PATH_TO_LANDINGPAGE_"}'></div>
