# Opendata extension

This extension introduces a range of specialized blocks that simplify the creation of presentations of datasets according to the DCAT standard. With the blocks you can list, search or filter datasets as well as provide a landing page for each dataset. The use cases ranges from creating a web for a single data provider with a few datasets to a full-fledged regional or national dataportal with thousands of datasets. In addition, there is also a few blocks that supports presenting ideas and showcases connected to datasets.

To learn about the extension you can take a look at the [examples](examples.md) and also read the [reference](reference.md) for all the specialized extension blocks to see which configuration options they provide.

## Versions
The extension is available in four localized versions,

1. [English open data extension](https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-en.js)
2. [Italian open data extension](https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-it.js)
3. [Swedish open data extension](https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js)
4. [German open data extension](https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-de.js)

## Getting started
Using the extension involves three steps:

###Step 1 - deciding on a blocks configuration
First, check the ready-made [examples](examples.md) to find a suitable starting point. Second, you will always need to provide a basic configuration containing:

1. Which data catalog to show (provided by pointing to your instance of the Entrystore API).
2. In case you want to have a landing page for each dataset you have to indicate the path to the page in the `clicks` configuration. 

Finally, you may want to turn on or off some functionality of your blocks via their parameters. See the [reference section](reference.md) for details of what is possible.

The result of this step is that you have one or two snippets of HTML code ready to be inserted into one or two webpages. Below we see [example 1](examples.md#example-1-list-of-datasets):

    <div data-entryscape="config" data-entryscape-entrystore="_ES_API_">
    <div data-entryscape="datasetStandaloneListLayout"></div>
    <script src="https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js"></script>
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

**Note** that you would have to replace the text `_EX_API_` with a link to a real EntryStore API.

###Step 2 - Search Engine Optimization
If you are using a layout without dataset landing page you can skipp this part.

By default, urls for dataset landing pages are made up of the path to the landing page + #-anchor fragments which are not considered for indexing by search engines. To make the landingpages "crawlable" by search engine robots there is a parameter called `urlQueryParameters` that you can set to `true` by modifying the snippet from above to

    <div data-entryscape="config" data-entryscape-entrystore="_ES_API_" data-entryscape-url-query-parameters="true"></div>
    <div data-entryscape="datasetStandaloneListLayout"></div>
    <script src="https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js"></script>
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

However only the first 10 results (visible in the dataset list) will be available to the robots so to make all results available the parameter `pageInUrl` must also be set. Do it by adding it like this to the snippet

    <div data-entryscape="config" data-entryscape-entrystore="_ES_API_" data-entryscape-url-query-parameters="true"></div>
    <div data-entryscape="datasetStandaloneListLayout" data-entryscape-page-in-url="true"></div>
    <script src="https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js"></script>
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

###Step 3 - integrating the blocks into your web
How this step is realized depends on which mechanism you use for maintaining your website. The most common scenario is that you utilize a [CMS](https://en.wikipedia.org/wiki/Content_management_system) (e.g. Wordpress, Drupal, EpiServer) where you can edit your pages.

When you edit a webpage in a CMS you are most likely presented with an authoring environment that looks like a word-processor. That means that you can write text without having to care about how the content you produce are expressed as HTML. However, to embed the blocks you will need to insert raw HTML code, more specifically a few div elements and two scripts. Luckily, in many cases the authoring environment provides a way for you to do this, e.g. by allowing you to add a section / component / block of raw HTML code as a part of the webpage.

If there is no way to inser raw HTML code, or you cannot find it, we recommend that you contact someone in your organization or an external consultant that knows how to do this. It might be that the CMS configuration needs to be changed slightly or even that someone need to do some small development (in most cases this should not be needed).

###Step 4 - Adjusting the design
After the integration you should see the result in your webpage(s), but it might need some tweaks from a design perspective. A typical scenario is that your web has a style guide that should be adhered to.

Adjusting the design is done by providing specific CSS rules that tweak fonts, colors, spacings etc. If you use a CMS there might be a possibility to provide the CSS rules as part of the webpage. Another alternative is to provide an external file with CSS rules that you point to as part of the raw HTML code you inserted in step 2.

Disregarding of the method you choose, you need to have knowledge of how to write these CSS rules. As an integral part of writing the CSS rules are how to make them apply to the right thing in the blocks driven user interface. This is accomplished by using the right classes. Currently, there is no extensive documentation on which css classes to use, instead we encourage to use the developer mode of the browser and inspect the corresponding user interface. All classes that start with `esb`, like `esbRow`, `esbRowMain`, `esbRowExpand` etc. are stable and are safe to use in CSS rules.

| Class                | Use                                                                                                                     | Default                                                                                                                                             | 
|:---------------------|:------------------------------------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------|
| esbNormalButtonWidth | In formatBadge that is Not used                                                                                         | -                                                                                                                                                   |
| esbBadge             | Format-badge spans (ODE + Blocks)                                                                                       | padding: 3px 10px; margin-right: 5px; line-height: 30px; text-transform: uppercase; border-radius: 5px; white-space: normal;                        |
| esbRow               | Rows in list of results (ODE + Blocks)                                                                                  | margin-bottom: 10px; box-shadow: 0px 3px 6px -1px rgba(0, 0, 0, 0.3);                                                                               |
| esbRowAlign          | Head of item in Lists of data resources (ODE)                                                                           | display: flex; justify-content: space-between; align-items: center;                                                                                 |
| esbRowAlignPrimary   | Title of data resource (ODE)                                                                                            | flex-grow: 1;                                                                                                                                       |
| esbRowAlignSecondary | Link to data resource (ODE)                                                                                             | -                                                                                                                                                   |
| esbRowAlignOther     | Format badges container (ODE)                                                                                           | -                                                                                                                                                   |
| esbCollapsed         | Expandable facets and expandable dataviews (ODE + Blocks)                                                               | inside esbExpanded display: none;                                                                                                                   |
| esbExpanded          | Expandable facets and expandable dataviews (ODE + Blocks)                                                               | inside esbCollapsed display: none;                                                                                                                  |
| esbMobileFacets      | Expandable facets (ODE)                                                                                                 | -                                                                                                                                                   |
| esbClickExpand       | Expandable facets (Blocks)                                                                                              | cursor: pointer;                                                                                                                                    |
| esbView              | Dataset view and dataservice view (ODE)                                                                                 | -                                                                                                                                                   |
| esbLabel             | Hard-coded labels in dataset view and dataservice view (ODE)                                                            | font-weight: normal; color: black; padding-right: 10px;                                                                                             |
| esbTag               | Keywords, themes and types in dataset view and dataservice view (ODE)                                                   | padding: 3px 10px; border: 1px solid lightgrey; background: white; margin-right: 5px; line-height: 30px;                                            |
| esbIndent            | Container for viewMetadata (ODE)                                                                                        | margin: 0px 15px;                                                                                                                                   |
| esbDescription       | Div for description (ODE)                                                                                               | white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 782px; (Inside esbRowExpanded white-space: pre-line; overflow: visible;) |
| esbIndicators        | Container for architectureIndicator, costIndicator, accessRightsIndicator, periodicityIndicator, licenseIndicator (ODE) | -                                                                                                                                                   |
| esbIndicator         | Icon for indicator (ODE)                                                                                                | margin-right: 15px;                                                                                                                                 |
| esbIndicatorLabel    | Label for indicator (ODE)                                                                                               | padding-left: 5px; text-transform: lowercase;                                                                                                       |
| esbDatasetName       | Heading for dataset in list (ODE)                                                                                       | -                                                                                                                                                   |
| esbBackButton        | Navigation back to list (ODE)                                                                                           | complement to bootstrap btn btn-primary                                                                                                             |


