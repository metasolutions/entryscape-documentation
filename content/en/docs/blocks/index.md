# Introduction to EntryScape Blocks

EntryScape Blocks allows you to embed information from EntryScape in a custom user interface without writing any code.

As is indicated from the name, EntryScape Blocks provides a range of building blocks ranging from presentation of an individual metadata field to more interactive blocks like faceted search. Each block takes a set of parameters controlling appearance and interaction.

EntryScape Blocks is a standalone JavaScript library that can be embedded into any existing webpage or CMS. In most cases some care has to be taken to tweak the appearance so the blocks blend with the rest of the site, done via CSS. The library's own CSS is carefully scoped to avoid affecting the rest of the site by mistake.

The blocks can be divided into three categories, blocks that work with:

1. individual entries.
2. searches or search results of entries.
3. other data sources.

## Hello World

Add the following to any webpage:

    <span data-entryscape="helloworld"></span>
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

You should see a simple output at the same position in the page indicating that it works:

    Hello world!

The example shows that we have pointed to an entrystore instance, as well as an individual entry by providing an identifier for both the entry, and it's context. We need to do the latter since all entries belong to a single context. Read more in a separate chapter about [how to provide parameters](parameters.md) and [how to address entries](entries.md).

## EntryStore instance

As stated above, you need to connect to an EntryStore instance, how to install an entrystore instance is out of scope for this documentation, the reader is advised to take a look at [entrystore.org](https://entrystore.org) instead. If you don't have an entrystore installation but still want to test EntryScape Blocks you can use [free.entryscape.com](https://free.entryscape.com). The entrystore instance address for it is: `https://free.entryscape.com/store`.

## Features

1. Blocks provides texts, links, lists, search, facets, charts, maps etc.
2. Integration via a single script and data attributes on regular html elements
3. Many blocks can be configured via handlebar templates that in turn allow other blocks enabling nested constructions.
4. Supports definition of custom blocks to keep your pages DRY
5. Present complex metadata via RDForms templates
