# Using handlebar templates in blocks
Handlebar templates are used in the `list`, `searchList` and the `template` blocks. The templates may contain various directives, some are provided by handlebars by default and some are specific to EntryScape blocks. Below we explain both how to use handlebars templates in general and those specific directives.

Note that parameters are in general given to handlebars directives explicitly as key="value". However, for some of the directives (like `ifprop`, `prop` and `eachprop`) there is a special first parameter which is given without the key, when such a parameter is listed in a table it is indicated with a `-`.

## Small templates
Small templates can be provided inline, e.g. the `template` block expects a template parameter,
and it can look like this:

    <span data-entryscape="template"
          data-entryscape-template="{{prop \"dcat:theme\" render=\"label\"}}"></span>

## Large templates
Larger templates are easier to provide in special script tag of type `text/x-entryscape-handlebar`, this also avoids the awkward escaping of quotes. For this kind of scripts the block will be assumed to be `template` unless something else is specified.

    <script type="text/x-entryscape-handlebar">
        {{prop "dcat:theme" render="label"}}
    </script>

## Conditionals using ifprop
Conditional check via `ifprop` is true if a certain property exists, e.g.:

    {{#ifprop "foaf:firstName"}}
      {{prop "foaf:firstName"}} {{prop "foaf:lastName"}}
    {{/ifprop}}

Multiple properties are separated by comma and will be true if one of them exist, e.g.:

    {{#ifprop "dcterms:title,dcterms:description"}}
       This dataset has a neme or a description
     {{/ifprop}}

It is possible to check for a certain value of the property, here a literal:

    {{#ifprop "foaf:name" literal="John"}}
       This is John, he is a special person.
     {{/ifprop}}

As well as invert the check, here is an example of a check that is true if there is a triple with the property `rdf:type` where the object can be any uri except `foaf:Person`:

    {{#ifprop "rdf:type" uri="foaf:Person" invert=true}}
      This is Not a person.
    {{/ifprop}}

## Loops using eachprop
The `eachprop` iterates over triples in the metadata on the current entry and renders the inner template once per matched triple with a specific set of directives available. Below we go through first all parameters that `eachprop` supports and after that the special directives available within the inner template. Let's first look at an example:


## Accessing data using prop
The `prop` block finds a triple for a specific predicate and renders the object. The main difference from the `eachprop` block described below is that it only renders a single triple and uses the `render` parameter to control the output rather than an inner template. If multiple triples match the condition the triple shown is chosen at random. Here are the supported parameters:

| Parameter | Value                       | Description                                                                                              |
|:----------|:----------------------------|:---------------------------------------------------------------------------------------------------------|
| -         | RDF property                | This is a mandatory parameter, it forces matching only triples with this property in predicate position. |
| nested    | boolean                     | If false or not provided only triples with the entry's resourceURI in subject position will match.       |
| nodetype  | uri&#124;literal&#124;blank | Indicates what kind of object should be matched, if left out all objects valid matches.                  |
| fallback  | string                      | If no label can be found for a uri the following text will act as a fallback for the `label` directive.  |
| regexp    | Regular expression          | Allows extracting part of a value via the `regexp` directive.                                            |
| render    | see below                   | Specifies what should be rendered, e.g. the value (literal/uri) or a label for it.                       |

For the render parameter the following options are supported:

| Option      | Description                                                                                                                     |
|:------------|:--------------------------------------------------------------------------------------------------------------------------------|
| value       | This is the default, it renders the uri or literal depending on which triple matched.                                           |
| md5         | This is a md5 hash of the value, useful for generating relatively short unique identifiers for the value                        |
| language    | This is the language of a literal, e.g. 'sv' or 'en'.                                                                           |
| datatype    | This is the datatype, e.g. 'xsd:date'. Note that the value is not namespaced.                                                   |
| type        | This is the type of object, i.e. `uri`, `literal` or `blank`.                                                                   |
| label       | A nice label for the value, see below on how it works.                                                                          |
| description | A description from a matching choice in a loaded RDForms template (if any).                                                     |
| regexp      | The result of applying the regular expression provided as parameter                                                             |
| optionvalue | If the value is matched to an option (that may match several different values) in a collection this provides the option's value |
| date        | Only available for values of dataType `dateTime` or `date`                                                                      |

The render-option `label` is detected in the following order:

1. From an option, but only if the property corresponds to a collection where the triple's value can be matched to one of the options.
2. The value is one of the explicitly named values in the configuration, see the [global named parameter](global.md#named).
3. The value can be matched to a choice in identifiable RDForms template item (the item has to have an identifier)
4. The value is a uri, and it corresponds to the resourceURI of an already loaded entry, if so the label is detected via the [global labelProperties parameter](global.md#labelproperties).
5. The fallback label provided as a parameter.
6. An empty string.
 
The render-option `description` is detected only via the steps 3 and 4.

**Prop example 1 - a name**

    {{prop "foaf:name"}}

**Prop example 2 - different renderings**

    <span class='theme_{{prop "dcat:theme" render="md5" nodetype="uri"}}'>{{prop "dcat:theme"
    nodetype="uri" render="label"}}</span>

The example uses the `prop` block twice to render both a stable css class for the value (using a md5 hash) and a nice label.

**Prop example 3 - pick part of a value with regexp**

    {{prop "dcterms:date regexp="^\\d*"}}
    
The regexp picks out the year from an ISO 8601 date.

## Loops using eachprop
The `eachprop` iterates over triples in the metadata on the current entry and renders the inner template once per matched triple. Below we go through first all parameters that `eachprop` supports and after that the special directives available within the inner template.

The `eachprop` block supports all the parameters that `prop` supports except the render parameter. In addition, it supports the following parameters:

| Parameter      | Value   | Description                                                                                                               |
|:---------------|:--------|:--------------------------------------------------------------------------------------------------------------------------|
| separator      | string  | Text to be used by the `separator` directive, if not provided a komma with an empty space after (", ") is the fallback.   |
| limit          | integer | Number of matching triples that will be rendered before breaking and adding a expand button by default there is no limit. |
| expandellipsis | string  | Text to be used just before the expand button to indicate that there are more matched values, the default is "…".         |
| expandbutton   | string  | Text on expand button, "Show more" is default.                                                                            |
| unexpandbutton | string  | Text on unexpand button, "Show less" is default.                                                                          |

The inner template supports 10 directives, 9 are the same as the rendering options described for the `prop` block, see above. The only extra directive is the `separator` directive. The `label` and `description` directive is detected in the same way as the corresponding label and description render option.


**eachprop example 1 - multiple keywords**
  
    {{#eachprop "dcat:keyword"}}<span class="keyword">{{value}}</span>{{/eachprop}}

**eachprop example 1 - multiple keywords with separators**
  
    {{#eachprop "dcat:keyword" separator=", "}}{{value}}{{separator}}{{/eachprop}}

Note that the separator parameter to the eachprop block is actually not needed in this case as what is specifies is the default. However, the use of the separator directive inside the inner template is crucial, without it the keywords will be written together in one long string.

**eachprop example 3 - multiple themes**

    {{#eachprop "dcat:theme" nodetype="uri"}}<a class="tema" title="{{description}}" href="{{value}}">{{label}}</a>{{/eachprop}}

Here eachprop is used to show multiple `dcat:theme` properties where the object are uris. Each theme is rendered with a label, a tooltip and a link to the value (the uri).

## entry URI of an entry using entryURI

Provides the URI to the current entry, something like `https://example.com/example/15/entry/23`. For instance:

    Download <a href="{{ metadataURI }}?format=text/turtle" target="_blank">meta-metadata</a>.

## Resource URI of an entry using resourceURI

Provides the URI to the resource of the current entry, the resource can be something like a uploaded pdf or a link to an external site. Hence, depending on what the entry represents you may want to use the directive for different purposes. In the following example we assume the entry corresponds to a uploaded profile picture of a person. (That is why we combine it with a `view` block to show the metadata.)

    <img src="{{resourceURI}}" style="float:right">{{view rdformsid="foaf:Person"}}

## Metadata URI of an entry using metadataURI

Provides the URI to the metadata of the current entry, something like `https://example.com/example/15/metadata/23`. For instance:

    Download metadata in: <a href="{{ metadataURI }}?format=text/turtle" target="_blank">TURTLE</a>

## Accessing the resource URI
Apart from props, it's possible to access the resource URI using one of entryURI, resourceURI or metadataURI, e.g.:

  {{entryURI}}

## Blocks in templates
Note that all EntryScape blocks are made available as helpers within the handlebar template. That means that it is possible to have blocks inside of blocks.

    {{#ifprop foaf:firstName}}
      {{text content="${foaf:firstName} ${foaf:lastName"}}
    {{/ifprop}}

Note that this example provides the same result as the first example in the section [Conditionals using ifprop](#conditionals_using_ifprop).
Hence, it can be argued that the content parameter of the text block is superfluous as the same
thing can be achieved with the prop directive inside a template block. Although this is true the text block is in many cases more convenient and less complex. The text block was also implemented first and keeping it is a question of being backwards compatible.

## Blocks with multiple templates
For the `list` and `searchList` block up to 4 different handlebar templates may be provided.
There are two main options for providing these templates:

### Using multiple parameters
Depending on the complexity of the templates it may be best to use a script with json for the expression,
see [syntax option 3](syntax.md#option-3-block-json-script-use-for-many-parameters):

    <script type="text/x-entryscape-json">
      {
        "block": "searchList",
        "listplaceholder": "<h4>No matching results</h4>",
        "rowhead": "<h4>{{text}}</h4><div class="description">{{text content="${dcterms:description}"}}</div>"
      }
    </script>

### Encoding multiple templates inside a single template
There are situations when using separate parameters is not an option, e.g. when the templates have to be
provided inside another template. (Strictly speaking it is possible, but the amount of
escaping for correct formatting makes it complicated and error-prone.)

For this purpose there is a special solution that relies on encoding the templates using the 'raw'
blocks mechanism in handlebars. A raw block is achieved by using four curly braces, in the
example below you see the listplaceholder and rowhead templates provided in this way.
(Here we use a handlebar script, see [syntax option 4](syntax.md##option-4-block-handlebar-script-use-for-large-templates))

    <script type="text/x-entryscape-handlebar">
      {{#ifprop rdf:type uri="dcterms:Collection"}}
        {{list relation="dcterms:hasPart"}}
          {{{{listplaceholder}}}}
            <h4>No matching results</h4>
          {{{{/listplaceholder}}}}
          {{{{rowhead}}}}
            <h4>{{text}}</h4>
            <div class="description">{{text content="${dcterms:description}"}}</div>
          {{{{/rowhead}}}}
        {{/list}}
      {{/ifprop}}
    </script>

**Note:** in the example above the justification for having the list within a template is a conditional check, but it can just as well be something else such as a need to have lists within lists.
