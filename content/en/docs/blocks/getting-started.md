# Getting Started

## How it works

Blocks needs to fetch information from an EntryStore instance or other data source
Start by providing a common configuration that points to a Entrystore instance and a context. In the following examples we use the demo-store. The context is often a number which can be found in the url when clicking on the catalogue or project in entryscape.

    <div data-entryscape="config" data-entryscape-context="93"
         data-entryscape-entrystore="https://demo.entryscape.com/store"></div>

    <!-- Put your blocks here -->
 
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

<div data-entryscape="config" data-entryscape-context="93"
     data-entryscape-entrystore="https://demo.entryscape.com/store"></div>

Note that you need to load the blocks library after the block declarations.

## text
To show a text from a single entry you can use a `text` block. Unless given a `property` or `content` it will try to 
display a suitable label for the specified entry by relying on default label properties.
Read about [Loading entries](entries.md) and [labelProperties](global.md#labelProperties)

    <div data-entryscape="text" data-entryscape-entry="660"></div>
<div class="escEx">
  <div data-entryscape="text" data-entryscape-entry="660"></div>
</div>


## list
To show a list of entry's in the given context use the `list` block

    <div data-entryscape="list" data-entryscape-rdftype="foaf:Agent"
         data-entryscape-limit="3"></div>
<div class="escEx">
  <div data-entryscape="list" data-entryscape-rdftype="dcat:Dataset" data-entryscape-limit="3"></div>
</div>

The tags above will generate a list of maximum 3 entries that are of `rdftype = "foaf:Agent"`. 

To generate a list with expandable drop-down you add a template reference by giving a `rdformsid`.

    <div data-entryscape="list" data-entryscape-rdftype="foaf:Agent"
         data-entryscape-rdformsid="foaf:Agent"
         data-entryscape-limit="3"></div>
<div class="escEx">
  <div data-entryscape="list" data-entryscape-rdftype="foaf:Agent" data-entryscape-rdformsid="foaf:Agent" data-entryscape-limit="3"></div>
</div>

For more parameters that can be used to change lists, take a look at the [Blocks Reference](reference.md/#list)

## Search

The `searchList` block is a version of the `list` block that also accepts user interactions via other search enabled
blocks. In your code just change list to searchList in your code, add `data-entryscape-facets= "true"`. 
Then and a `simpleSearch` block on the same page. The search button is optional, and it is possible to use the field 
without it. You are now able to search among all the entry's in your list.

    <div data-entryscape="simpleSearch"
      data-entryscape-search-button="true"
      data-entryscape-placehoder="Search for..."></div>    
    <div data-entryscape="searchList" data-entryscape-rdftype="foaf:Agent"
         data-entryscape-rdformsid="foaf:Agent"
         data-entryscape-facets= "true" 
         data-entryscape-limit="3"></div>
<div class="escEx">
  <div data-entryscape="simpleSearch" data-entryscape-search-button="true" data-entryscape-placehoder="Search for..."></div>
  <div data-entryscape="searchList" data-entryscape-rdftype="foaf:Agent" data-entryscape-rdformsid="foaf:Agent" data-entryscape-facets= "true" data-entryscape-limit="3"></div>
</div>

## Next step
You can continue from here by adding blocks from the library using [Blocks Reference](reference.md) or use one of the 
extension blocks [Opendata extension](opendata/intro.md). The extension blocks are styled collections of blocks and using 
them is the quickest and easiest path to getting a user interface up. Adding blocks by hand is moore advanced but gives 
you the opportunity to customize the user interface more.

## Opendata Extension - datasetListLayout
You can test the most basic opendata extension - datasetListLayout by replacing your blocks from the examples
above with the `datasetListLayout` block and the extension-library tag.

    <div data-entryscape="config" data-entryscape-context="93"
         data-entryscape-entrystore="https://demo.entryscape.com/store"></div>
    <div data-entryscape="datasetListLayout" data-entryscape-limit="3"></div>
    <script src="https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js">
    </script>
<div class="escEx">
    <div data-entryscape="config" data-entryscape-context="93"
             data-entryscape-entrystore="https://demo.entryscape.com/store"></div>
    <div data-entryscape="datasetListLayout" data-entryscape-limit="3"></div>
</div>
<script src="https://static.cdn.entryscape.com/blocks-ext/1/opendata/opendata-sv.js">
</script>
<script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>
<style>
   .esbRowHead {
      max-height: none;
   }
   .escEx ul li:last-child {
      margin-bottom: 0.5em;
   }
   .escEx {
      border: 10px solid whitesmoke;
      border-top-width: 0;
      margin-top: -17px;
   }
   .escEx>.entryscape {
     background: white;
     padding: 10px;
   }
   .md-typeset .entryscape ul:not([hidden]) {
     display: flex;
   }
   .entryscape .pagination li.page-item {
     margin-left: initial;
   }
</style>
