# Examples of blocks

To avoid duplicating information in the examples we provide a common configurations that points to the
Entrystore instance and a context.
    
    <div data-entryscape="config" data-entryscape-context="93"
         data-entryscape-entrystore="https://demo.entryscape.com/store"></div>

<div data-entryscape="config" data-entryscape-context="93"
     data-entryscape-entrystore="https://demo.entryscape.com/store"></div>

At the end of the examples we need to load the blocks library:

    // Include the blocks library after all of the block declarations:
    <script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>

## helloworld

    <div data-entryscape="helloworld"></div>
<div class="escEx">
  <div data-entryscape="helloworld"></div>
</div>
## text

    <div data-entryscape="text" data-entryscape-entry="31"></div>
<div class="escEx">
  <div data-entryscape="text" data-entryscape-entry="31"></div>
</div>

## list

    <div data-entryscape="list" data-entryscape-rdftype="foaf:Agent"
         data-entryscape-rdformsid="foaf:Agent"
         data-entryscape-limit="3"></div>
<div class="escEx">
  <div data-entryscape="list" data-entryscape-rdftype="foaf:Agent" data-entryscape-rdformsid="dcat:foaf:Agent" data-entryscape-limit="3"></div>
</div>

## template

    <script type="text/x-entryscape-handlebar" data-entryscape="template"
            data-entryscape-entry="256">
      <strong>{{text}}</strong><br>
      {{#eachprop "dcat:theme"}}
         <a href="{{value}}" class="esbTag">{{label}}</a>
      {{/eachprop}}            
    </script>
<div class="escEx">    
  <script type="text/x-entryscape-handlebar" data-entryscape="template" data-entryscape-entry="256">
        <strong>{{text}}</strong><br>
        {{#eachprop "dcat:theme"}}<a href="{{value}}" class="esbTag">{{label}}</a>{{/eachprop}}
  </script>
</div>
    
<script src="https://static.cdn.entryscape.com/blocks/1/app.js"></script>
<style>
   .escEx {
      border: 10px solid whitesmoke;
      border-top-width: 0;
      margin-top: -17px;
   }
   .escEx>.entryscape {
     background: white;
     padding: 10px;
   }
   .md-typeset .entryscape ul:not([hidden]) {
     display: flex;
   }
   .entryscape .pagination li.page-item {
     margin-left: initial;
   }
</style>
