# Available global level parameters

Configuring blocks is about providing global level parameters.
The parameters `entrystore`, `namespaces`, `labelProperties`, `bundles`, `entitytypes`
and `type2template` will be detected when expressed on any top-level block (not when used within handlebar templates).
The rest of the global level parameters have to be expressed in a `config` block or in the `__entryscape_config` expression like this:

    <script>
      window.__entryscape_config = {
          some_parameter: "some value"
      }
    </script>
 
For more options on how global level parameters can be expressed see the pages on [parameters](parameters.md) and [syntax](syntax.md).

## entrystore

The `entrystore` parameter tells which instance to connect to, exemplified above.

    entrystore: "http://example.com/store"

If left out the application will try to fall back to the page domain name as entrystore url.

## page_language
Forces all blocks to render in a specific language, values should be a two-letter ISO 639-1 language code like `en`.

## routes
Provides a way to extract the parameters from the current URL by providing a set of routes. For example:

    routes: [
      {
        regex: /^https:\/\/data.riksarkivet.se\/resource\/(\d+)_(\d+)$/,
        entry: 2,
        context: 1
      },
      {
        regex: /^https:\/\/data.riksarkivet.se\/tora\/(\w+)$/,
        entry: 1,
        context: "61"
      },
      {
        regex: /^https:\/\/data.riksarkivet.se\/id\/(\w+)$/,
        lookup: 1,
        lookupLiteral: 'dcterms:identifier'
      },
      {
        regex: /\/sv\/.*?.*uri=(.+)/,
        uri: 1,
        page_language: 'sv'
      },
      {
        regex: /\/sv\/.*?.*uri=(.+)/,
        uri: 1,
        page_language: 'en'
      }
    ],

In this example the first routes matches URLs containing both the entry id and context id separated via an underscore, note how the entry and context values are integers referring to the order of the regular expression capturing groups. The second route matches another set of URLs with a single entry id in it and providing a fixed context, this is done by providing a string for the context parameter in the route. In the third route the entry is detected via a lookup procedure using `dcterms:identifier`. The two last route matches a query string for the uri parameter as well as provides an override for the page language. The uri parameter is used to look up entries by their resource URI.

You can read a bit more about the various parameters in the routes in the [how to specify an entry section](entries.md#entry-from-parameters).

## context

Restricts all blocks (unless overridden) to make all requests in the entrystore instance within 
the specified context. An individual context may be given or an array of multiple contexts.
A context is indicated via its context id, in most cases this is a number provided as a string.

    context: "3"

**Note:** if you do not want to restrict all blocks to this/these contexts you have to provide it per context instead, most
blocks that work with entries accept this parameter. 

## entry
A specific entry can be indicated on the global level, however, in most situations this only makes sense as a fallback for testing.
If so, the entry is indicated by providing the `entry` parameter in conjunction with the `context` parameter (the latter has a wider use as described above). Alternatively it is possible to provide the `uri` or the `lookup` parameter potentially in conjunction with parameters `context`, `lookupURI` or `lookupLiteral`, read more about how to use these parameters in the sections [entry from parameters](entries.md#entry-from-parameters).

## namespaces
This is an object that provides a mapping from the abbreviation to the full namespace. The mapping will be registered
centrally and make it possible to express other parameters less verbose (both global and block level).
For example:

    "namespaces": {
        "ex": "http://example.com/"
    }
  
Observe that a bunch of common namespaces are already registered like: *rdf*, *rdfs*, *dc*, *dcterms*, *foaf*, *skos*, *dcat*, *xsd*, *ical*, *vcard*, *owl*, *wot*, *org*, *vs*, *gn*, *schema*.
You can look them up using [prefix.cc](http://prefix.cc).

## labelProperties
To render an entry (or any resource in a RDF graph) there must be a way to construct a suitable label. For instance, 
for the `text` block this can be achieved by specifying the parameter `property`. Another possibility is to rely on the 
labelProperties fallback which provides a list of properties that are checked in order to construct a suitable label.
The default labelProperties looks like:

```JSON
    [
      "foaf:name",
      ["foaf:firstName", "foaf:lastName"],
      ["foaf:givenName", "foaf:familyName"],
      "vcard:fn",
      "skos:prefLabel",
      "dcterms:title",
      "dc:title",
      "rdfs:label",
      "skos:altLabel",
      "vcard:hasEmail"
    ] 
```

## bundles
In addition to the RDForms template bundles provided by default in blocks (includes support for dcat, dcterms, foaf)
you can indicate additional template bundles to be loaded like this:
  
    "bundles": [
       "https://example.com/my/template/bundle.js"
    ],

## relativeBundlePath
If the full url path is given for the bundles this parameter has no effect but if only the bundle names are used as in the following example the value will determine where the application will look for them.

    "bundles": [
        "bundle1",
        "bundle2"
    ],

Default is true which results in the application first looking in the current folder for a json file with the bundle name before retrying by requesting it from https://static.cdn.entryscape.com/suite/latest/templates/.
If the parameter is set to false instead the behaviour is to first try fetching it from /theme/templates/ folder of the given entrystore before retrying by requesting it from https://static.cdn.entryscape.com/suite/latest/templates/ as before.

## collections
For more advanced search there is a need for facets and filters to work, this is achieved by defining collections.
The collection parameter is quite advanced with many options and demands [its own section](collections.md).

## facetLimit facetMissing and facetMinCount
Options that are applied to all facetFields in the query.

facetLimit - The maximum amount of facet results to be report on. Default is 100, values up to 1000 is allowed.

facetMissing - If matches with no match against a facet should be reported as well.

facetMinCount - The minimum amount of facet results to be included in the response. Default is 1.

## clicks
This is a global level parameter that is used for the `link` and `multiSearch` blocks to make it possible to make 
blocks extensions reusable for different organizations. See more documentation on the `link` block. Let's look at how two example `clicks` configurations affect the links from the following simple link block:

    <div data-entryscape="link" data-entryscape-namedclick="art"></div>

**Example with fragment identifiers to static html file**

    clicks: {
      artist: '/artist.html',
      art: '/art.html
    }

the links will be expressed as:

    http://some_domain.com/art.html?#esc_context=3&esc_entry=5

**Example with parameters to routed sited**

    clicks: {
      artist: '/artist/${context}/${entry}',
      art: '/art/${context}/${entry}
    },

the links will be expressed as:

    http://some_domain.com/art/3/5

**Note: the parametrized clicks should in most cases be used in conjunction with a route expression to allow detecting the correct entry on the target webpage (assuming it is also driven by blocks):**

    routes: [
      {
        regex: /art\/(\w+)\/(\w+)$/,
        context: 1,
        entry: 2,
      },
    ],

## linkBehaviour

The linkBehaviour setting controls how rdforms view blocks will treat related entries. If linkBehaviour is explicitly set to "link" all metadata renderings will use links when encountering a related entity.
If not set, a dialog will be utilized and an appropriate RDForms template will attempt to be detected via the
type2template mapping below. If linkBehaviour is set to "dialogWithResourceLink" the resource link is added to the dialog.
The default is to use a dialog without a resource link.
 
## type2template
When using rdforms templates to show metadata it is sometimes necessary to detect which template to use for an entity
(RDF resource in an RDF graph). For this purpose it is possible to provide a mapping between type of the entity and which
RDForms template to use:
  
    "type2template": {
        "foaf:Agent": "idOfTemplateForAgent"
    }
  
## entitytypes 
In some situations you need more complicated ways of matching entities to templates, e.g. when a simple type is not
unique enough. There are two ways of doing this. One is to re-use the entitytypes from Entryscape configuration. The other one is a more compact version of that declaration and is described in the following example.
In the example both countries and continents are represented as gn:FeatureCode,
but we can make the distinction based on the gn:featureCode property instead.
For the country we need to capture a range of different featureCodes since they all correspond to countries in different
legal states, this is done by providing an array for the property, it is interpreted as a disjunction (or:ed together) when searching.
Since the simple map form cannot be used we use the entitytypes attribute instead like this:
  
    "entitytypes" : {
      "country": {
        constraints: {
          "rdf:type": "gn:Feature",
          "gn:featureCode": [
             "gn:A.PCLD", "gn:A.PCLI","gn:A.PCLIX", "gn:A.PCLH", 
             "gn:A.TERR", "gn:A.PCLS", "gn:A.PCLF", "gn:A.PCL"]
        },
        template: "countryTemplateId",
      },
      "continent": {
        constraints: {
          "rdf:type": "gn:Feature",
          "gn:featureCode": ["gn:L.CONT"]
        },
        template: "continentTemplateId",
      },
    }

Indicate that a value is a uri by surrounding it with < and >. Placing a ~ before the key will negate ```"~key": "value"``` => ```NOT key:value```.

## blocks
The `blocks` global level parameter is a way of introducing new blocks, in most cases by extending another,
for instance the following defines a new block called `name`.

    blocks: [
      {
         block: 'name',
         extends: 'text',
         content: '${foaf:givenName} ${foaf:familyName}'
      }
    ]

See more in the [custom blocks](custom.md) section for how to define new blocks.

## minimumSearchLength
By default, the minimum search length in the Solr index of EntryStore is three, however, there are ways to bypass this
limitation by using more verbose and more expensive queries (e.g. by using wildcards). Hence, switching to a shorter
minimum search length should not be done without careful considerations of the performance impact. 

## query
By default, the `searchList` uses the `title` field in the [EntryStore solr index](https://entrystore.org/kb/search/) when a search term is typed in to the simple search.
This can be changed by the global level `query` paramer, here is an example of how it can look like:

    query: {
      'metadata.predicate.literal_t.893797ba': 'term', // foaf:name
      'metadata.predicate.literal_t.3f2ae919': 'term',  // dcterms:title
    },

This means we will only search for foaf:name and dcterms:title. So, if we are searching for "train" the query
will be expanded to:
 
     metadata.predicate.literal_t.893797ba:train+OR+
     related.metadata.predicate.literal_t.893797ba:train

See the [EntryStore solr index documentation](https://entrystore.org/kb/search/) on how the hashed solr fields are constructed.
As a side note, here is how you can get the 8 character string for a specific property using bash:

    > echo -n http://purl.org/dc/terms/title | md5sum | cut -c1-8
    3f2ae919

Since we are using the literal_t in the search term we will match whole words, but we will not get a match
against a title (or foaf:name) "trainwreck". To achieve this we have to either remove the _t or instruct the query builder
to append a wildcard. This is achieved by changing the `term` to a `term+` in the query above. Now, by searching for "trai*" will
work and match both "train" and "trainwreck".

Now, it could be argued that it would be better to remove rely on the text_ngram option, i.e. to remove the _t from the
literal_t expressions in the query above. In most cases this works just fine, however for longer or shorter queries this
may not work as the default ngram length in EntryStore is 3 characters.
So the approach above is useful when setting the global level parameter `minimumSearchLength` to 1 or 2. 

The expressions above also allow for boosting certain search fields by appending an integer like this:

    query: {
      'metadata.predicate.literal_t.893797ba': 'term,10', // foaf:name
      'metadata.predicate.literal_t.3f2ae919': 'term,5',  // dcterms:title
    },

Boosting is relevant when relying on the score based sorting (relevancy).

It is also possible to include and exclude fields based on matching the search term with a regular expression. For these moore advanced query's we use another declaration e.g.

    query: {
        'metadata.predicate.literal_t.3f2ae919': { // dcterms:title
            type: 'term+',
            boost: 10,
        },
        'metadata.predicate.literal_t.a6424133': { // dcat:keyword
            type: 'term+',
            excludes: '^\\d+$',
            boost: 5,
        },
        'metadata.predicate.literal_t.c86e1300': { // dcterms:issued
            type: 'term',
            includes: '^\\d+$',
        },
    },

This will give a query for `dcterms:title` or `dcat:keyword` matching with wildcard at the end unless the term is only digits. In that case the query will be for `dcterms:title` and `dcterms:issued`. It is unlikely that you would use issued in this way but sometimes there is an id number that you would like to look for if the search term matches a regular expression.

Restricting the query like this is usefully when you want to avoid getting irrelevant results and when the query would get too long if all fields are included.

When searching for `literal_s` the search term must match exactly. So searching `Svensson` or `bo svensson` will not render results for Bo Svensson. In the rare case that exakt matching with wildcard is required use `metadata.predicate.literal_s` with wildcards e.g. to match surname Svensson but not svensson.

    query: {
        'metadata.predicate.literal_s.893797ba': { // fof:name
            type: '+term',
        },
    },

## limitOptions

The user may want to control the number of returned results, this is achieved via the `limit` block. However, the options are defined on the global level like this:

    limitOptions: [10, 20, 30],

Max limit is 100. The first number will be default for all list-blocks. To override set property limit on list. 

## sortOptions

The user may want to control the sort order, this is achieved via the `sort` block. However, the options are defined on the global level like this:

    sortOptions: [
      {
        label: 'Relevancy',
        value: 'score+desc,modified+desc',
      },
      {
        label: 'Latest',
        value: 'modified+desc',
      },
      {
        label: 'Alfabetical',
        value: 'title.sv+desc',
      },
    ],

## named

In some situations you have values that are URIs without there being a suitable label expressed in the metadata graph at hand.
In these cases it is possible to provide a lookup table that provides names for URIs. For instance:

    named: {
      'https://creativecommons.org/publicdomain/mark/1.0/': {
         en: 'Public Domain Mark 1.0'
      }
    }

This lookup table is respected in multiple blocks like `text`, `prop` and `facets`. Se documentation on each block for
more information.

## jsonp
In many scenarios blocks will be included on webpages which have another domain than the EntryStore API. The browser will then make cross domain requests which in modern browsers requires CORS to be enabled. This is enabled per default on your EntryStore API endpoint. If, for some reason, there is a need to use jsonp the global level parameter can be set to `true`

## credentials
By default, credentials are not sent along with requests. Sending along credentials with cross domain requests is not accepted by the endpoint and will result in an error. Sometimes the blocks site is on the same domain as the endpoint or credentials are allowed. To activate sending credentials set `credentials` to `true`

## requestCache 
By default, the response from the endpoint is not cached. To optimize load-time, allow client and endpoint to cache requests by setting `requestCache` to `true`. The cache will persist for one hour and during that time changes done in the backend will not be visible in the lists.

## urlQueryParameters + urlParamsPrefix
By default, lists using click property will create an url made up of the path, given by the click value + #-anchor. This type of links are normally not indexed by search engine crawlers because they are not considered to be unique links. To make the landingpages "crawlable" by search engine robots set the global parameter `urlQueryParameters` to `true`. This setting will generate links ending with `?esc_entry=0&esc_context=0`. 

The prefix `esc` can be changed by setting `urlParamsPrefix` to some other string.

Also consider setting the local list parameter `pageInUrl` to `true` to make all pages of a list available to the crawlers. 

## filter
This global level parameter is used to complement the search in the list and searchList blocks. The parameter may either be a function being passed the solrQuery object from EntryStore.js to make custom tweaks or an object being passed to the or method of the same instance.

## baseFilter
The base filter global level parameter allows for one or several values from collections to be preselected without being visible in the URL as parameters. The preselected values will be shown as selected facets, as selected values in searchFilter blocks, as values in a multiSearch block and will affect checked/unchecked state of the checkboxFilter. If a value is deselected it will appear in a negative manner in the URL.

    baseFilter: {
        nameOfCollection: 'value'
    }

## forcePublicRequests
By default, all solr queries sent to the EntryStore API endpoint include the public:true setting. This means that any ACL checks are avoided even if the user happens to be authenticated to the EntryStore API. This behaviour is enforced by default to not fool developers thinking that the public read-only user interface somehow now shows protected information just because they happen to be signed-in in another tab. The only case when it makes sense to set`forcePublicRequest to false is if you want to build a user interface for signed-in users of EntryScape.  

## baseMap
By default, the base map used for all maps in blocks is OSM (Open Street Map). This can be overridden by adding the `baseMap` parameter object. The base map can be of type `wms` or `xyz`. For example:

    baseMap: {
        type: 'wms',
        title: 'Simple base map',
        attributions: 'Attribution text</br>',
        name: 'layer-name',
        base: true,
        url: '{WMS-url}',
    }

## state

Global parameters that are available in all blocks templates. Can be `string`, `number` or `boolean`. If `dynamic` is set to `true` a change will trigger re-rendering of blocks that have that parameter set in `subscribe`. For now only boolean parameters can be changed.

| Parameter  | Type                    | Default value  | Description                                                                                                   |
|:-----------|:------------------------|:---------------|:--------------------------------------------------------------------------------------------------------------|
| type       | string                  | boolean        | Type of value                                                                                                 |
| key        | string                  | stateParameter | A key to use in url instead of parameter name                                                                 |
| value      | string, number, boolean |                | Parameter value                                                                                               |
| dynamic    | boolean                 | false          | Should it be possible to change (only possible for boolean values)                                            |
| persistent | string                  | 'none'         | For dynamic state parameters the current value can be reflected in the url or not. Values are 'none' or 'url' |


    state: {
        includeHeader: {
          type: 'boolean',
          key: 'incHead',
          value: true,
          dynamic: true,
          persistent: 'url',
        },
        heart: {
            type: 'string',
            value: '❤️',
        },
    }

The state parameters are used by writing state.stateParameter in the template. Example ande moore information can be found in the section [Using global level state parameters](custom.md#using-global-level-state-parameters).

## errorBlock
Blocks comes with two distinct solutions for presenting errors. The modal dialog and the error block. By default, the modal dialog is activated. To suppress the modal dialog and let an error block handle the errors instead you have to set the `errorBlock` global level parameter to true. (Unfortunately, automatically detecting the presence of an error block cannot be done in time to handle potential errors from other blocks, hence it has to be signalled explicitly on the global level.)

## style

It is possible to add style to a page by adding it in the configuration as a string
