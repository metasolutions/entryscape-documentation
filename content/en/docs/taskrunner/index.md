# Taskrunner API


The [Taskrunner API](https://swagger.entryscape.com/?url=https://docs.entryscape.com/en/taskrunner/swagger.json#/info). is a simplified API for carrying out tasks which otherwise would require requires multiple API calls.

An authentication cookie is required, then tasks can be created. The tasks are placed in a queue, after which the steps of the task will be performed at the server side. It is possible to check the task status during the process.

## Authentication

Authentication is required in order to create tasks. An authentication cookie can be obtained from the [EntryStore API](https://entrystore.org/api/#/auth/loginCookie), like so:


``` Javascript
curl --location --request POST 'https://demo.entryscape.com/store/auth/cookie' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'auth_username=example@metasolutions.se' \
--data-urlencode 'auth_password=***'
```

If the call is successful, the response will contain the authentication cookie in a Set-Cookie header:

    Set-Cookie: auth_token=*** Max-Age=31536000; SameSite=None; HttpOnly; Secure; Path=/store/

**Note:** The cookie contains a path-attribute, Path=/store/, which is specific to EntryStore API. If passing the cookie with this attribute, the cookie will not be recognized by the Taskrunner API. For instance, this is the case if handling the cookie by the curl cookie engine. Therefore, only pass the auth_token-attribute of the cookie when making calls to Taskrunner API.

## Tasks

### Add a file to a distribution

This [task](https://swagger.entryscape.com/?url=https://docs.entryscape.com/en/taskrunner/swagger.json#/files/post_distribution_addFile) allows you to add an individual file to a [distribution](https://docs.entryscape.com/en/catalog/datasets/#distributions). It will also update the connected API distribution if there is one. The API will continue to work during the processing time and if successful, it will contain the data of the new file.

Identify the resource URI of the _distribution_ which you want to add a file to: 

1. Go to the the overview of the dataset
2. Chose the information icon of the distribution
3. In the 'About' section, the resource URI is found under 'Resource'

Pass this as a query parameter along with the file and authentication. An example of such a call is:

``` Javascript
curl --location 
--request POST 'https://demo.entryscape.com/taskrunner/v1/distribution/addFile?resourceURI=https://demo.entryscape.com/store/34/resource/5' \
--header 'Cookie: auth_token=*** \
--form 'file=@"/home/example.csv"'
```

If a successful call was made, use the provided jobId key for checking the task status. A prototype URI is also provided, which is where the file will reside if the job is successfully carried out.

### Replace a file of a distribution

This [task](https://swagger.entryscape.com/?url=https://docs.entryscape.com/en/taskrunner/swagger.json#/files/post_distribution_replaceFile) allows you to replace an individual file of a [distribution](https://docs.entryscape.com/en/catalog/datasets/#distributions). It will also update the connected API distribution if there is one. The API will continue to work during the processing time and if successful, it should contain the replacing file's data.
	
Identify the resource URI of the _file_ which you want to replace:

1. Go to the overview of the dataset
2. Choose the information icon of the distribution. 
3. In the 'Information' section, the resource URI is found under 'Web address for access' of the file to be replaced

Pass this as a query parameter along with the file and authentication. An example of such a call is:

``` Javascript
curl --location 
--request POST 'https://demo.entryscape.com/taskrunner/v1/distribution/replaceFile?resourceURI=https://demo.entryscape.com/store/34/resource/5' \
--header 'Cookie: auth_token=*** \
--form 'file=@"/home/example.csv"'
```

If a successful call was made, use the provided jobId key for checking the task status.

### Status

After successfully activating a task via the Taskrunner API, it is possible to check the status of the task. This is done by utilizing the jobId key which is received when when activating the task. Pass this job id as a parameter, in the following example the jobId is 12:

``` Javascript

curl --location --request GET 'https://demo.entryscape.com/taskrunner/v1/job/12'

```

At a minumum, the response object consists of the id of the job and its status. If the job has terminated, it will also contain info on the results of the job.
