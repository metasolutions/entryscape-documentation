# Subcontractors

MetaSolutions AB uses the following subcontractors.

## Cloud DNS Ltd

Sofia 1000, ul. Iskar 4, Bulgaria<br/>
VAT BG202743734

Use: DNS<br/>
Personal data processing: IP-addresses

## GleSYS AB

Box 134, 311 22 Falkenberg, Sweden<br/>
VAT SE556647924101

Use: operation, DNS, email<br/>
Personal data processing: customer data, IP-addresses

## KeyCDN / proinity LLC

Faerberstrasse 9, 8832 Wollerau, Switzerland<br/>
VAT CHE-459.847.656

Use: CDN<br/>
Personal data processing: IP-addresses

## Hetzner Online GmbH

Industriestr. 25, 91710 Gunzenhausen, Germany<br/>
VAT DE812871812

Use: operation<br/>
Personal data processing: customer data, IP-addresses

## Loopia AB

Kopparbergsvägen 8, 722 13 Västerås, Sweden<br/>
VAT SE556633930401

Use: DNS<br/>
Personal data processing: IP-addresses

## Scaleway SAS

BP 438, 75366 - Paris CEDEX 08, France<br/>
VAT FR35433115904

Use: backup<br/>
Personal data processing: customer data (encrypted), IP-addresses
