# Visa statusrapporten

Statusrapporten fungerar som en instrumentpanel för att få en övergripande bild över hur många organisationer som publicerar öppna data och tillhandahåller en webbsida om deras öppna data enligt den svenska rekommendationen angående en `/psidata`-sida.

![Skärmdump av statusrapporten på registrera.oppnadata.se](img/status_report.png)