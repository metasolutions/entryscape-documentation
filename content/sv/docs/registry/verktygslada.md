# Använd verktygslådan

EntryScape Registry Toolkit är framtagen för datakataloger som är beskrivna enligt DCAT-AP.

!!! info "Verktygslådan arbetar med den för närvarande laddade datakatalogen och ingenting annat."

## Ange en katalogkälla

Under fliken "Katalogkälla" är det möjligt att ladda DCAT-AP-metadata. DCAT-AP-metadata kan tillhandahållas som en uppladdad fil, en URL eller den kan klistras in i formaten RDF/XML och RDF/JSON. För att testa valideringen är det möjligt att ladda ett exempel genom att klicka på den motsvarande knappen längst ner i dialogen.

![Skärmdump av laddad exempel](img/toolkit_example.png)

Katalogen som laddas i denna flik används sedan för de andra verktygen i verktygslådan.

## Validera katalogens metadata

Registrys valideringsverktyg ger återkoppling på fem av de huvudsakliga entiterna i DCAT-AP: katalog, datamängd, distribution, organisation och kontaktpunkt.

Verktyget skapar en valideringsrapport som möjliggör en detaljerad analys av DCAT-AP-metadata. Eventuella problem flaggas per uttryck. Förutom valideringsrapporten på toppnivå är det även möjligt att få fördjupa sig per entitetsyp genom att klicka på motsvarande länk (eventuellt behöver panelen expanderas först).

![Skärmdump av valideringsvyn](img/toolkit_validation.png)

## Slå ihop flera kataloger

Ytterligare DCAP-AP-kataloger kan slås ihop med den redan laddade primära katalogen. Datamängder och alla andra entiteter (förutom själva katalogen) av de tillagda katalogerna läggs till primärkatalogen. Den resulterande metadatan kan nås via fliken "Katalogkälla" och hämtas därifrån.

## Utforska en katalog

Med hjälp av utforskaren kan datamängderna i den laddade katalogen inspekteras. Genom att klicka på en datamängd visas dess metadata i en sidodialog. Därifrån går det att bläddra vidare till andra länkade entiteter. 

![Skärmdump av katalogutforskaren](img/toolkit_explore.png)

## Konvertera katalogens metadata

Konverteringsverktyget används för att migrera metadata från äldre DCAT-AP-versioner till en aktuell. En lista över nödvändiga ändringar presenteras i en tabell. Genom att klicka på konverteringsknappen uppdateras katalogens metadata i källfliken och kan hämtas därifrån.

![Skärmdump av konverteringstabellen](img/toolkit_conversion.png)