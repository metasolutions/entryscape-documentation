# Hantera organisationer och skördningskällor

I EntryScape Registry är varje organisation också en skördningskälla. Det innebär att en organisation behöver skapas först för att kunna skörda datakatalog.

Detta kapitel beskriver hur skördningskällor skapas och hanteras.

## Lägga till en organisation

Klicka på skapningsknappen <input type="button" value="+"> i övre högra hörnet för att skapa en organisation. Endast några få detaljer är nödvändiga: organisationens namn, en kort beskrivning, vilken typ av datakatalog det handlar om (DCAT, CKAN, etc) och skördningskällans webbadress.

![Skärmdump av skapningsdialogen](img/organization_creation.png)

Efter att organisationen har skapats kan det ta lite tid tills webbadressen är kontrollerad och skördad ifrån.

Genom att klicka på kugghjulet <input type="button" value= "⚙"> i organisationens listrad kan du komma åt konfigurationsdialogen för att redigera organisationen och skördningsdetaljerna.

## Kontrollera skördningsstatus

En organisations skördningstatus kan nås genom att klicka på organisationens listrad eller genom att klicka på motsvarande menyalternativ som nås via kugghjulet <input type="button" value="⚙">.

De senaste skördningsförsöken visas med viss grundläggande statistik, t.ex. mängd datamängder som har skördats. Om en skördningskälla hittades är det möjligt att få tillgång till en valideringsrapport per skördningsförsök.

Knappen "Skörda om" lägger till organisationen i skördningskön och ett nytt skördningsförsök kommer att genomföras inom en kort stund.

![Skärmdump av skördningsstatus](img/harvesting_status.png)