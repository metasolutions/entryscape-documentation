# Overview

EntryScape Registry består av flera delar:

![Startvy EntryScape Registry](img/start_view.png)

### Statusrapport

Ger en översikt över vilka organisationer som tillhandahåller DCAT-AP-metadata och en webbsida som hänvisar till organisationens PSI-data.

### Sök

Ett sökgränssnitt för att bläddra igenom alla datakataloger och datamängder som skördas eller hanteras av denna Registry instans.

### Organisationer

Hanterar organisationer och deras skördningsinställningar.

### Verktygslåda

En DCAT-AP-verktygslåda med innehållandes bl.a. en metadata validator.