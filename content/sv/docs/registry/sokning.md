# Sök bland datakataloger och datamängder

Sökgränssnittet ger möjlighet att söka igenom all metadata som hanteras och skördas av den här Registry-instansen.

I gränssnittets vänstra del listas alla organisationer (en katalog per organisation) som är sorterade fallande efter antalet framgångsrikt skördade datamängder. Den högra listan innehåller alla datamängder.

![Skärmdump av sökgränssnittet](img/search.png)

Om en organisation selekteras så visas endast organisationens datamängder i resultatlistan.

![Skärmdump av organisationslistan](img/search_detail.png)