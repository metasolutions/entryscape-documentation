# Vanliga frågor

<!-- Läs på om vanliga frågor (FAQ) på [EntryScape Community](https://community.entryscape.com/tag/faq) forum. -->


### 1. Var hittar jag vår organisations EntryScape? 

EntryScape är ett webbaserat system som ligger uppe på er organisaitons webb.

1. Registrera dig på **EntryScapes användarforum** [https://community.entryscape.com](https://community.entryscape.com) och **logga in** där.
2. Sök på din **organisations namn**, t.ex. "Hedestads kommun" med hjälp av **förstoringsglaset** uppe till höger.
![Sökrutan på forumet](img/faq_sok.png)
3. Klicka på sökresultatet **"Om kategorin** X **kommun internt"**.
4. Läs instruktionerna och **klicka på länken** till er EntryScape-instans.
5. **Registrera ett användarkonto på ert EntryScape** med din e-post från din organisation. Ett bekräftelsemail kommer skickas till din e-postadress, där du måste bekräfta ditt konto genom att klicka på knappen.
6. Nu kan du logga in i EntryScape!

Obs! Om du trots ovanstående steg inte hittar adressen till er organisations EntryScape, kolla ert avtal eller hör efter internt inom organisationen.


---

### 2. Hur skapar jag ett konto i EntryScape?

Du som behöver ett användarkonto ska gå in på din organisations instans av EntryScape och klicka på ”Skapa konto” längst ner till höger.

![Inloggning med länk till skapa konto](img/faq_skapa_konto.png)

Fyll sedan i **namn, e-postadress, önskat lösenord** och bocka för rutan för användningsvillkor. Klicka sedan på **Skapa konto**. Då skickas ett **mail** till din e-postadress. **Klicka på den gröna bekräftelse-knappen i mailet**, så skapas ditt konto.

![Mail med knapp för att bekräfta användare](img/faq_mail_bekrafta_anvandare.png)

Hittar du inget bekräftelsemail? Kolla i Skräppost-mappen.

---

### 3. Jag har glömt lösenordet.

Klicka på **”Glömt lösenordet?”** längst ner till vänster i inloggningsrutan till EntryScape.

![Inloggning med länk till glömt lösenord](img/faq_glomt_losenordet.png)

Fyll sen i din e-postadress och **hitta på ett nytt lösenord** som du vill byta till. Kom ihåg att det måste vara **minst 10 tecken** långt och innehålla **minst en siffra** samt **STORA och små** bokstäver.

![Inmatningsfält för nytt lösenord](img/faq_nytt_losenord.png)

Slutligen behöver du bekräfta ditt lösenordsbyte genom att **klicka på den gröna knappen i mailet** som skickats till ditt e-postkonto. Klart!

![Mail med knapp för att bekräfta återställning av lösenord](img/faq_mail_aterstall_losenord.png)

Hittar du inget bekräftelsemail? Kolla i Skräppost-mappen.

---

### 4. Jag har skapat ett konto, men kan inte se min kollegas arbete i EntryScape Catalog.

Den som är **gruppansvarig** (oftast den som har skapat katalogen eller är systemansvarig) behöver **ge dig behörighet till katalogen** i EntryScape Catalog. Det görs under trepunktsmenyn för katalogen och menyalternativet **Delningsinställningar**.

![Menyalternativet Delningsinställningar för en katalog](img/faq_delningsinstallningar.png)

Du kan läsa mer i vår användarmanual om [hur man lägger till användare till en katalog](https://docs.entryscape.com/sv/catalog/#behorigheter-och-delningsinstallningar).


För att man ska kunna förbereda och beskriva datamängder etc. behöver man har behörigheten **medlem**. Om man ska kunna publicera behöver man ha behörigheten **gruppansvarig**.

---

### 5. Hur tar jag bort/inaktiverar jag ett användarkonto?

Kontakta [EntryScape Support Helpdesk](https://support.entryscape.com/).

---

### 6. Hur stora CSV-filer kan jag lägga upp i EntryScape Catalog?

Maxgränsen för uppladdade filer i EntryScape ligger på **24 MB**. Är filen större än så rekommenderar vi att du **delar upp datat på flera filer**, till exempel en fil för varje år eller månad.

![Skapa distribution](img/faq_skapadistribution.png)

Observera att din organisation kan ha ett undantag för denna begränsning om ni använder EntryScape Catalog Tailored med skräddarsydda anpassningar.

---

### 7. Hur gör jag om CSV-filer till API så att det fungerar?

Om du har försökt ladda upp en CSV-fil och det inte fungerar, så kan det vara så att CSV-filen innehåller felaktigheter. Gör då så här:

1. Kontrollera att all data följer exakt samma tabellstruktur.
2. Se till att det inte finns data i kolumner utanför tabellstrukturen.
3. Kontrollera att det inte förekommer vanliga kommatecken. Ta bort dessa kommatecken.
4. Se även till att ha sparat filen med UTF-8 kodning.
5. Läs i manualen om hur man [skapar ett API från tabelldata (csv)](https://docs.entryscape.com/sv/catalog/distributioner/#skapa-ett-api-fran-tabelldata-csv).

---

### 8. Hur tar jag bort/ersätter jag filer i EntryScape Catalog?

Under **trepunktsmenyn** för en distribution hittar du menyalternativet **"Hantera filer"**.

![Menyalternativet Hantera filer för en distribution](img/faq_hanterafiler.png)

Där kommer du åt dina filer och kan lägga till, redigera, ersätta, ladda ner och ta bort filer.

![Hantera filer](img/faq_hanterafiler2.png)

I vår användarmanual så kan du läsa mer om hur man gör för att **[hantera filer i EntryScape Catalog](https://docs.entryscape.com/sv/catalog/forvaltning/#uppdatera-filer-i-en-distribution)**.

---

### 9. Vad innebär publiceringknapparna i EntryScape?

Alla nyskapade kataloger är opublicerade som standard och därför kan endast användare som tilldelats rättigheter se katalogen. De som har behörigheten **gruppansvarig** kan **publicera** och **avpublicera** en katalog med **publiceringsknappen**. Om katalogens publiceringsstatus är grön (publik) så betyder det att **katalogens metadata och data är publikt läsbar** och redo att skördas av till exempel dataportaler. Alla datamängder kommer inte nödvändigtvis att skördas till dataportaler, se förklaring nedan.

![Publiceringsknappen för kataloger](img/catalog_5_publicering_publicerakatalog.png)

För att något ska synas så måste det finnas en **datamängd** eller **datatjänst** i katalogen vars **status** antingen är **publicerad** (syns i listorna) eller att knappen **Extern** är aktiverad för dataskördning till andra dataportaler. Det ställer du in på nästa nivå, d.v.s. nere på datamängden. Klicka på **publiceringsknappen för datamängden** för att göra datamängden publikt listad. Om knappen **Extern** är inaktiverad så kan datamängden synas på till exempel en egen dataportal inbäddad på extern webbsida eller intranät-sida med hjälp av EntryScape Blocks - men inte andra dataportaler.

![Publiceringsknappen för datamängder](img/catalog_5_publicering_publiceradatamangd.png)

---

### 10. Vilka sorters öppna data kan kommuner publicera?

Här under finns exempel på lämplig data för en kommun att publicera som öppna data:

* Alkohol/servering/danstillstånd
* Arkitektskisser nybyggnation
* Avfall
* Badplatser
* Bullerkarta
* Bygglov
* Byggnadspolygoner
* Cykelparkeringar
* Cykelpooler
* Cykelvägar
* Detaljplaner
* Evenemang
* Event inom skola/omsorg
* Fastighetsinformation
* Fastighetspolygoner
* Flygfoton/Satellitbilder
* Grundläggande geografisk platsinformation
* Grundläggande information om skolor
* Grundläggande information om vård- och omsorgsverksamheter
* Idrotts-/fritidsanläggningar
* Information för funktionshindrade
* Kulturklassade hus
* Kvalitetstal på skolor/förskolor
* Kvalitetstal för vård- och omsorgsverksamheter
* Laddstationer
* Lekplatser
* Leverantörsfakturor
* Litteratur på bibliotek
* Livsmedelskontroller
* Luftkvalitet
* Markanvisningar
* Miljöfarlig verksamhet
* Nämndhandlingar
* Offentliga toaletter
* Parkeringar
* Skolmatsedel
* Skolval
* Snöröjning
* Stadsbyggnadsprojekt + nyproduktion
* Terräng och topografi
* Trafikflöden cykeltrafik
* Trafikflöden fordonstrafik
* Trafikomfördelningar
* Trafikstörningar/vägarbeten
* Trängsel- och parkeringsavgifter
* Turiststatistik
* Upphandlingsdokument
* Utegym
* Vattenkvalitet
* Vägnät
* Återvinningsstationer



## Hittar du inte det du letar efter?

Registrera dig på **EntryScapes användarforum** [https://community.entryscape.com](https://community.entryscape.com) och **logga in** där, så hittar du många fler frågor och svar.