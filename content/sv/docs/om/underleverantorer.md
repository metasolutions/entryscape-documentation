# Underleverantörer

MetaSolutions AB använder följande underleverantörer.

## Cloud DNS Ltd

Sofia 1000, ul. Iskar 4, Bulgarien<br/>
VAT BG202743734

Användning: DNS<br/>
Personuppgiftsbehandling: IP-adresser

## GleSYS AB

Box 134, 311 22 Falkenberg, Sverige<br/>
VAT SE556647924101

Användning: DNS, e-post<br/>
Personuppgiftsbehandling: IP-adresser, e-post meddelanden

## KeyCDN / proinity LLC

Faerberstrasse 9, 8832 Wollerau, Schweiz<br/>
VAT CHE-459.847.656

Användning: CDN<br/>
Personuppgiftsbehandling: IP-adresser

## Hetzner Online GmbH

Industriestr. 25, 91710 Gunzenhausen, Tyskland<br/>
VAT DE812871812

Användning: drift<br/>
Personuppgiftsbehandling: kunddata, IP-adresser

## Loopia AB

Kopparbergsvägen 8, 722 13 Västerås, Sverige<br/>
VAT SE556633930401

Användning: DNS<br/>
Personuppgiftsbehandling: IP-adresser

## Scaleway SAS

BP 438, 75366 - Paris CEDEX 08, Frankrike<br/>
VAT FR35433115904

Användning: backup<br/>
Personuppgiftsbehandling: kunddata (krypterat), IP-adresser
