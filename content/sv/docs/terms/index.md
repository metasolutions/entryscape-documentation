# EntryScape Terms

Med **EntryScape Terms** hanterar du **termer och begrepp** kopplat till öppna och delade data. Här kan du lägga in eller importera terminologier och taxonomier som kan användas av andra applikationer. 

 Du som är informationsansvarig/dataägare har också nytta av att läsa igenom delar av denna dokumentation. Innan du börjar arbeta med EntryScape Terms, ta reda på vad som gäller för tillgängliggörande av information i din organisation, via er policy eller rutin för vidareutnyttjande av information.

## Hur begrepp ordnas i EntryScape Terms

En **terminologi** är en mängd termer och begrepp inom ett specifikt fackområde. I EntryScape Terms samlar man begrepp som hör ihop inom en viss terminologi och strukturerar upp dem i **begreppshierarkier**. Man kan också skapa **samlingar** där man samlar ett urval av begrepp från en eller flera terminologier.

Här under är ett **exempel på en terminologi**, "Hundraser", där begreppet "Sällskapshundar" är det överordnade begreppet för ett antal underordnade begrepp i en hierarkisk struktur där man kan klicka sig ner i flera nivåer.

![Exempel på hierarkisk terminologi](img/terms_begreppshierarki.png)


Man kan också gruppera existerande begrepp i **samlingar**. En samling är en utvald **delmängd begrepp** ur en eller flera terminologier för ett specfikt ändamål. 

Här under är ett **exempel på en samling** "Allergivänliga katter och hundar" som samlar och pekar på en delmängd av begrepp ifrån de redan existerande terminologierna "Hundraser" och "Kattraser". 

![Exempel på samling](img/terms_samling.png)