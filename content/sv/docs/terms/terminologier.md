# Terminologier

## Skapa terminologier

När du har loggat in i EntryScape så kommer du först till den generella startsidan. För att hantera terminologier och begrepp så klicka på **"Terms"**, antingen i vänstermenyn eller huvudvyn.

![Startsidan med vänstermenyn och huvudvyn, Terms](img/terms_startsidan.png)

Då kommer du till **översiktsvyn** över de **terminologier** som har lagts in i EntryScape Terms. Här listas alla redan inlagda terminologier och du kan söka (efter namn på terminologin), importera befintliga terminologier (RDF/XML-filer) eller skapa nya terminologier. Om du inte ser någon lista med terminologier på din sida så finns inga inlagda än, eller så har din organisation inte delat dem med dig än. Under [Behörigheter](behorigheter.md) kan du läsa mer om delningsinställningar och användarbehörigheter.

![Översiktsvyn över terminologier](img/terms_oversiktterminologier.png)



Det finns två sätt att skapa terminologier i EntryScape Terms. Antingen **importerar** du en befintlig terminologi eller så **skapar** du en terminologi själv från grunden, med hjälp av knapparna "Importera" och "Skapa" uppe till höger.

![Knapparna Importera och Skapa](img/terms_knapparimporteraskapa.png)


### Importera terminologi

Du kan importera en befintlig terminologi genom att klicka på knappen **"Importera"**. Då öppnas ett fönster där du kan välja att antingen **länka** till en fil som ligger på webben eller **ladda upp** en fil som du har sparad lokalt. Terminologifiler ska vara av formatet RDF/XML, som är standardformatet för länkade data.

![Dialogfönster Importera terminologi via länk](img/terms_importeraterminologilank.png)

En viktig sak som du måste bestämma när du importerar en terminologi är om du har tänkt att: 

1) enbart **kopiera** en terminologi från en källa som **någon annan underhåller/uppdaterar** (och som du kan återimportera med deras uppdateringar)

eller

2) den nya importerade terminologin ska vara den **nya källan**, vilket innebär att **du/din organisation ansvarar för uppdateringar och underhåll** av terminologin.

Om du kör på **alternativ 1** och bara vill kopiera en befintlig terminologi och bara återimportera nya kopior/slippa göra egna ändringar i den ska du klicka i rutan **"Begränsa terminologin till att endast tillåta utökningar av importerade begrepp"**.

![Begränsad import](img/terms_begransaterminologin.png)

Om du istället kör på **alternativ 2** och där den importerade terminolgin ska **vara den nya källan**, klicka **inte** i rutan "Begränsa terminologin..."

### Importera via länk

Om du vill **importera en terminologi från webben**, klistra in en **länk**  till en fil i formatet RDF/XML. När du sedan klickar på knappen "Importera" startas processen med att ladda upp, analysera och importera terminologi-filen i EntryScape Terms. Om det är många begrepp så kan det ta en stund innan det är färdigt. (Om du behöver avbryta processen, klicka bara på uppdatera-knappen i din webbläsare.)

![Statusfönster för importen](img/terms_importstatus.png)

### Importera via lokal fil

Du kan också ladda upp en fil som ligger lokalt. Klicka i så fall på **"Fil"** och sedan ikonen med förstoringsglaset för att välja en fil med formatet RDF/XML.

![Dialogfönster Importera terminologi från fil](img/terms_importeraterminologifil.png)


### Uppdatera en begränsad terminologi

Terminologier som har blivit importerade som "begränsade" och måste uppdateras från en annan källa, kan uppdateras genom att klicka på trepunktsmenyn, välja **"Uppdatera från källan"** och klistra in länk till källan eller välja källfil (RDF/XML format). Både gruppansvariga och medlemmar kan uppdatera en begränsad terminologi från en källa.

![Uppdatera terminologi från källa](img/terms_uppdaterafrankallan.png)


### Skapa ny terminologi 
För att skapa en **helt ny terminologi** från grunden, klicka på knappen **“Skapa”**. Då får du upp ett inmatningsfönster där du skriver in ett namn på terminologin samt en beskrivning. När du har skapat din terminologi kan du börja lägga in begrepp.

![Dialogfönster Skapa terminologi](img/terms_skapaterminologi.png)


## Redigera terminologi

Gruppansvariga kan enkelt gå in och **redigera informationen/metadata** om en terminologi genom att gå tillbaka till listan med terminologier (klicka på "Terms" i vänstermenyn). Där klickar du sedan på **trepunktsmenyn** och väljer **"Redigera"**.

![Trepunktsmenyn Redigera terminologi](img/terms_redigeraterminologi1.png)

Då får du upp ett dialogfönster där du kan redigera alla inmatningsfält för din terminologi: Titel, beskrivning, utgivare och nyckelord.

![Dialogfönster Redigera terminologi](img/terms_redigeraterminologi2.png)


## Begrepp

### Lägga in nya begrepp

Om du går in på din nyskapade terminologi så kan du **lägga in egna begrepp**, genom att klicka på **"Skapa"**.

![Knapp Skapa begrepp](img/terms_skapabegrepp1.png)

Då får du upp ett fönster där du kan fylla i begreppets namn och sen lägga till det till din terminologi.

![Dialogfönster Skapa begrepp](img/terms_skapabegrepp2.png)

Notera att det inte går att skapa nya begrepp i begränsade terminologier som enbart kan uppdateras från en annan källa. Däremot kan du lägga till mer information till existerande begrepp, så som t ex översättningar till andra språk.

### Ordna begrepp

Begreppen kommer att automatiskt sortera sig i bokstavsordning. För icke-begränsade terminologier är det också lätt att skapa **underordnade begrepp** och flytta runt begrepp. Du skapar ett underordnat begrepp genom att först markera det överordnade begreppet och sen klicka på knappen "Skapa".

![Skapa underordnat begrepp, markera överordnat begrepp](img/terms_skapaunderbegrepp1.png)

Då läggs det nya begreppet (blått) till under det markerade begreppet (svart).

![Nyskapat underordnat begrepp](img/terms_skapaunderbegrepp2.png)

Ännu enklare är det att **flytta begrepp** genom att **dra och släppa** dem där man vill ha dem. Klicka på det begrepp du vill flytta med muspekaren och håll inne knappen. Sedan pekar du dit du vill flytta det. En liten svart pil i vänsterkanten indikerar var begreppet läggs när du släpper musknappen.

![Flytta begrepp genom dra och släpp](img/terms_flyttabegrepp.png)

Notera att det inte går att flytta begrepp i begränsade terminologier som bara kan uppdateras från källan.


### Redigera begrepp

Om du vill gå in och **redigera** ett begrepp så **klicka på begreppet** i begreppshierarkin och klicka sedan på **pennikonen** uppe till höger.


![Redigera begrepp](img/terms_redigerabegrepp1.png)


Då får du upp ett inmatningsfönster där du kan fylla i mer information om begreppet. Med de gröna reglageknapparna högst upp kan du välja om du vill se både **Rekommenderade fält** och **Frivilliga fält**.

![Redigera begrepp](img/terms_redigerabegrepp2.png)


### Rekommenderade fält
**Föredragen term:** Huvudbegreppet. Ett begrepp får ha max en föredragen term per språk.

**Alternativ term:** Ett begrepp kan ha flera alternativa termer.

**Dold term:** Man kan också ha dolda termer för begreppet, t ex förlegade begrepp som ska vara sökbara men inte visas upp.

**Definition:** En kortfattad text som beskriver begreppet. Viktigt att man beskriver så att begreppet inte kan blandas ihop med likadana ord som har annan innebörd.

**Användningsanmärkning:** Hjälptext som beskriver kontext/användningsområde.

**Exempel:** Ett konkret exempel på vad som avses med begreppet.

**Historisk anmärkning:** En text som beskriver hur begreppet har vuxit fram och eventuellt om betydelsen har ändrats över tid.

**Är relaterat med:** Begrepp som ligger på olika ställen i en hierarki men ändå har relaterade egenskaper som behöver uttryckas.

**Har överordnat begrepp:** Ett överordnat, bredare begrepp inom samma terminologi.

**Har underordnat begrepp:** Ett underordnat, smalare begrepp inom samma terminologi.

**Ingår i modell:** Terminologin/modellen som begreppet tillhör.

**Huvudbegrepp i modellen:** Är huvudbegrepp i terminologin/modellen.

**Har exakt motsvarande begrepp:**  Mappning mot begrepp som har samma definition och avgränsning men ligger i en annan terminologi

**Har snarlikt motsvarande begrepp:**  Mappning mot begrepp som i stora drag motsvarar ett annat begrepp i en annan terminologi

**Har relaterat motsvarande begrepp:**  Begrepp med samma användningsområde, fast i en annan terminologi



### Frivilliga fält
**Redaktionell anmärkning:**  Information som riktar sig till den som underhåller begreppet.

**Förändringsanmärkning:** Redaktionell anmärkning som motsvarar redaktörens ändringar över tid.

**Anmärkning:** Extra fält för anmärkningar som inte passar i andra fält.

**Har överordnat motsvarande begrepp:** Ett överordnat, bredare begrepp som befinner sig i en annan terminologi.

**Har underordnat motsvarande begrepp:** Ett underordnat, smalare begrepp som befinner sig i en annan terminologi.


### Ta bort begrepp

Om du vill ta bort ett begrepp helt och hållet, markera begreppet i hierarkin, klicka på trepunktsmenyn uppe till höger och välj **"Ta bort"**. Borttagna begrepp kan inte återskapas.

![Ta bort begrepp](img/terms_tabortbegrepp.png)


## Publicera terminologi

När du känner att din terminologi är **redo för publicering** så går till din lista med terminologier (klicka på "Terms" i vänstermenyn) och publicerar  den med den gröna **publiceringsknappen**. Du kan lika lätt avpublicera den med samma knapp. Bara gruppansvariga har rättigheter att publicera och avpublicera terminologier.

![Publiceringsknapp](img/terms_publiceringsknapp.png)


## Ladda ner terminologi


Du kan **ladda ner din terminologi** för att spara en lokal kopia, genom att navigera till listan över terminologier, klicka på trepunktsmenyn till höger om terminologin och välj **"Ladda ner terminologi"**. 

 Notera att den nedladdade filen kommer att **innehålla alla begrepp** som hör till terminologin, men **inga samlingar**.

![Menyalternativ Ladda ner terminologi](img/terms_laddanerterminologi1.png)

 Därefter väljer du format för din nedladdning: RDF/XML, Turtle, N-Triples eller JSON-LD. Klicka sedan på **"Ladda ner terminologi"**.

![Välj filformat för nedladdning](img/terms_laddanerterminologi2.png)

## Ta bort terminologi

Det är lätt att ta bort en terminologi, genom att klicka på trepunktsmenyn och välj **"Ta bort"**. Observera att en borttagen terminologi inte går att återskapa. Bara gruppansvariga kan ta bort en terminologi.

![Knapp Ta bort terminologi](img/terms_tabortterminologi.png)











