# Behörigheter och delningsinställningar

I EntryScape Terms kan en användare i regel vara antingen **gruppansvarig** för en terminologi eller **medlem** med begränsade rättigheter. Du kan vara medlem eller gruppansvarig i en eller flera terminologier. Den som är **gruppansvarig** kan **ge andra användare behörighet**, **redigera****, publicera/avpublicera**, **ladda ner** eller **ta bort** en terminologi samt **lägga till begrepp** till en terminologi och hantera **samlingar**. När du skapar eller importerar en terminologi för första gången blir du automatiskt gruppamsvarig för den terminologin.

**Medlemmar** kan **ladda ner** en terminologi, **uppdatera en begränsad terminologi från en källa** samt **skapa begrepp och samlingar** mm för en terminologi. Däremot kan medlemmar **inte publicera/avpublicera**, **redigera metadatat** för eller **ta bort** terminologier. 

I de fall där din organisation upprättat arbetsrutin, arbetsgrupp och fått in metadatahantering eller öppna data-arbete i förvaltningsorganisation, eller motsvarande, så finns det möjlighet att få en högre behörighet än gruppansvarig om det behövs, admin, i dialog med MetaSolutions support.

Om du inte har tillgång till den terminologi du vill komma åt, så behöver du begära tillgång till den av den som är gruppansvarig (eller admin) i er organisation. (Den som är gruppansvarig är troligtvis motsvarande avdelnings- eller enhetschef.) 

**Gruppansvariga** kan **ändra behörigheter** genom att gå in under trepunktsmenyn och välja **”Delningsinställningar”**. 

![Menyalternativ Delningsinställningar](img/terms_delningsinstallningar1.png)


Där kan du **lägga till och ta bort användare** till den grupp som har behörighet till terminologin. För att lägga till en användare till en grupp så klickar du på **”Lägg till användare i gruppen”**. Du kan också välja vilken sorts **behörighet** användaren ska få, **gruppansvarig eller medlem**, genom att klicka på ikonen framför namnet.

![Behörighetsinställningar för användare](img/terms_delningsinstallningar2.png)

För att **ta bort** en användare från en grupp så klickar du på trepunktsmenyn och väljer **”Ta bort”**.

![Menyalternativ Ta bort användare från grupp](img/terms_delningsinstallningar3.png)