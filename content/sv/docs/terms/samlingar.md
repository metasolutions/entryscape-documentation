# Samlingar

Om man vill använda en **delmängd** av begrepp i en terminologi och **kombinera begrepp från olika terminologier** så kan man skapa **samlingar**. 

![Schematisk bild av en samling](img/samling.png)

## Skapa samling

Klicka på **"Samlingar"** ute i vänstermenyn och sedan **"Skapa"** för att skapa en ny samling. Fyll i **Titel** (obligatorisk) och **Beskrivning** av samlingen, gärna på fler än ett språk. 

![Dialogfönstret Skapa samling](img/terms_skapasamling.png)

Klicka sedan på "Skapa"-knappen längst ner till höger för att spara. Då ser du din samling i en lista. Nollan i svarta cirkeln visar att den än så länge saknar begrepp. Klicka på titeln på din samling för att komma till samlingens översiktsvy, där du kan lägga in begrepp.

![Lista med samlingar](img/terms_listamedsamlingar.png)

På översiktssidan kan du klicka på **pennknappen** bredvid **Begrepp** för att lägga till begrepp till din samling. 

![Pennknapp för att lägga till begrepp](img/terms_laggtillbegrepp1.png)

Då får du upp ett dialogfönster där du antingen kan **söka efter begrepp** (via sökrutan) eller **bläddra bland begrepp** i de terminologier du har åtkomst till. 

Du väljer vilka begrepp du vill lägga till din samling genom att **bocka för rutorna** i högerkanten av begreppen.

Uppe till höger kan du välja vilken **källa** du vill söka i och om du vill välja begrepp från en **specifik terminologi**. 


![Dialogfönster för att hitta och lägga till begrepp](img/terms_laggtillbegrepp2.png)

Längst ner kan du se **hur många begrepp** du söker bland (baserat på den källa och de/den terminologi du valde) och **blädda framåt** i terminolgin. Om du vill se fler begrepp än fem begrepp per sida så kan du ändra det längst ner med **"Rader per sida"**.

När du har lagt till dina begrepp så läggs de i en lista under din samling.

![Lista med valda begrepp i samling](img/terms_listadebegreppisamling.png)

## Redigera samling

Om du vill **redigera titeln** eller **beskrivningen** av din samling så klickar du på knappen **"Redigera"** högst upp till höger.

Om du däremot vill **lägga till begrepp** till samlingen så använder du **pennknappen** bredvid "Begrepp" längre ner. För att **ta bort begrepp** ur samlingen så kan du klicka på trepunktsmenyn till höger om begreppet och välja **"Ta bort"**. Begreppet tas då bort från samlingen, men ligger kvar i sin terminologi.

![Redigera samling](img/terms_redigerasamling.png)


## Ta bort samling

För att ta bort en hel samling så klickar du på knappen "Ta bort". En borttagen samling kan inte tas tillbaka, men det går naturligtvis att återskapa den genom att skapa en ny samling och lägga till begreppen igen, eftersom begreppen ligger kvar under sina terminologier.

![Ta bort samling](img/terms_tabortsamling.png)
