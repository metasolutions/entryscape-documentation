# Välkommen till dokumentationen för EntryScape

!!! info "Denna dokumentation är främst avsedd som vägledning för slutanvändare."

EntryScape är en informationshanteringsplattform som bygger på öppna standarder från standardiseringsorganet [W3C](https://www.w3.org) kring [Länkade Data (Linked Data)](https://www.w3.org/standards/semanticweb/). EntryScape stödjer hela livscykeln för datahantering, från import till åtkomst.

EntryScape består av olika applikationsmoduler som kan användas tillsammans eller separat var för sig:

### [Catalog](catalog/index.md)

Hantera och publicera datakataloger och datamängder enligt metadatastandarden DCAT-AP. Datamängder kan vara publika API:er, datadumpar, dokumentsamlingar, etc. Lär dig mer om [Catalog](catalog/index.md).

### [Terms](terms/index.md)

Skapa terminologier avsedda för återanvändning enligt [SKOS-standarden](https://www.w3.org/2009/08/skos-reference/skos.html). Bygg vidare på befintliga terminologier enligt SKOS eller skapa nya. Lär dig mer om [Terms](terms/index.md).

### [Workbench](workbench/index.md)

Strukturera upp och samarbeta kring era datamodeller med hjälp av länkade data. Vi hjälper er att anpassa Workbench efter era metadatabehov. Lär dig mer om [Workbench](workbench/index.md).

### [Registry](registry/index.md)

Hantera datakällor, skörda och konvertera mellan metadatastandarder. Metadatavalideringen är automatiserad och datautgivaren får omedelbar återkoppling för att vidta korrigerande åtgärder. Lär dig mer om [Registry](registry/index.md).

### [Blocks](blocks/index.md)

Bädda in information som är lagrad i EntryScape i webbsidor eller CMS med hjälp av kortkoder (shortcodes). T.ex. bygg en dataportal på en standard webbsida med eller utan CMS med metadata från EntryScape. Blocks fungerar med alla former av statiska sidor och dynamiska sidor via CMS som t.ex. WordPress, SiteVision, EpiServer och Drupal med flera. Lär dig mer om [Blocks](blocks/index.md).

### [Admin](admin/index.md)

Administrera en eller flera applikationsmoduler med Admin som bland annat hjälper dig att administrera användare, behörigheter och grupphantering. Lär dig mer om [Admin](admin/index.md).

### Användarsupport via [EntryScape Community](https://community.entryscape.com/)

På användar- och supportforumet [EntryScape Community](https://community.entryscape.com/) får du hjälp och support kring dina frågor av användare och MetaSolutions som är huvudutvecklare av EntryScape. Dessa frågor och svar leder i sin tur till förbättringar av den öppna användardokumentationen.

!!! info "Observera att EntryScape Community gemenskapsbaserat forum. Alla kan få svar från gemenskapen gratis. Du som t.ex. har EntryScape Free eller EntryScape Starter behöver ett supportavtal där det ingår att få svar från MetaSolutions på forumet."

***

Läs mer om EntryScape och MetaSolutions erbjudanden på [entryscape.com](https://entryscape.com).
