## Översikt
För att kunna lägga till **utgivare** för en katalog eller datamängd, så behöver du lägga in **organisationer** i EntryScape. Tillhandahållande organisationer och kontakter hanteras som egna separata entiteter i EntryScape. Du kan lägga till flera organisationer, vilket är användbart till exempel om din organisation ska publicera datamängder åt en annan organisation. 


## Organisationer
För att skapa en organisation, gå in på din katalog, välj Organisationer i vänstermenyn och klicka sen på ”Skapa”.

![Huvudmeny Kontakter och organisationer](img/catalog_6_orgkont_menyorganisationer.png)

Då får du fylla i **namn** på organisationen. Rekommenderat är att du också fyller i vilken **typ av organisation** det är och om du vill så kan du även fylla i hemsida och e-postadress (frivilligt).

![Dialogfönster Skapa tillhandhållande organisation](img/catalog_6_orgkont_skapaorganisation.png)

Det går naturligtvis bra att gå in och fylla i extra information i efterhand. I så fall klickar du på redigera-knappen längst ut till höger i listan över organisationer. 

![Menyalternativ Redigera organisation](img/catalog_6_orgkont_redigeraorganisation.png)

Eller så klickar du dig in på organisationen där du kan både redigera och ta bort med knapparna.

![Knappar Redigera och Ta bort organisation](img/catalog_6_orgkont_tabortorganisation.png)


## Kontakter
En **kontakt** anges som en funktionsadress till en datamängd. Det är en ansvarig funktion för datamängdens förvaltning, med **ansvar att hålla metadata uppdaterad och korrekt**. Använd sökfältet för att hitta den kontakt som du vill ange i din datamängd, eller skapa en ny kontakt under Kontakter i vänstermenyn, genom att klicka på ”Skapa”.

![Huvudmeny Kontakter](img/catalog_6_orgkont_menykontakter.png)

Välj först om kontakten är en **individ** eller en **organisation**. Därefter fyller du i **namn** och **e-postadress**. Om du vill kan du slå på de rekommenderade fälten och fylla i telefon och postadress också.

![Dialogfönster för att skapa en ny kontakt](img/catalog_6_orgkont_skapakontakt.png)

Om du går in på kontakten så kan du redigera uppgifter eller ta bort kontakten med hjälp av knapparna till höger.

![Knappar Redigera och ta bort kontakter](img/catalog_6_orgkont_redigerakontakt.png)

## Se detaljerad information

Om du vill se mer detaljerad information om en organisation eller kontakt kan du klicka på informationsikonen. 

![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata som typ av organisation eller mailadress till kontakten samt alla entiteter som länkar till och från organisationen eller kontakten. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).