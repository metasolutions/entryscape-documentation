

Om du klickar på **informationsikonen**, så får du fram ett dialogfönster som visar mer **detaljerad metadata** om t ex en datamängd, kontakt, distribution eller annan entitet. 

![Informationsikonen](img/catalog_13_detaljeradinformation_ikon1.png)

Du hittar både ikonen på i listan med entiteter samt på översiktssidan för entiteten.

![Informationsikonen](img/catalog_13_detaljeradinformation_ikon2.png)

Det fönster som öppnas har tre flikar: **"Information"**, **"Om"** och **"Referenser"**. 

![Informationsdialogfönstret, tre flikar](img/catalog_13_detaljeradinformation_flikar.png)

## Information

I den första fliken, **"Information"**, kan du se metadata för den datamängd eller annan entitet som du har klickat på, samt länkar till alla entiteter som **din entitet refererar till**, som t ex utgivare och kontakt.

![Informationsdialogfönstret, fliken Information](img/catalog_13_detaljeradinformation_information.png)

## Om

I den andra fliken, **"Om"**, ser du bland annat **URI:n** till din entitet. Det är ett snabbt sätt att få fram och kunna **kopiera URI:n** när du behöver referera till den i andra sammanhang.

![Informationsdialogfönstret, fliken Om](img/catalog_13_detaljeradinformation_om.png)

## Referenser

I den tredje fliken, **"Referenser"**, ser du alla entiteter som **refererar till din entitet**.

![Informationsdialogfönstret, fliken Referenser](img/catalog_13_detaljeradinformation_referenser.png)


## Språkval

Uppe till höger finns **två språkikoner** för att se informationen i flikarna på **olika språk**.

![Informationsdialogfönstret, språkikonerna](img/catalog_13_detaljeradinformation_sprakikoner.png)

Om du klickar på **jordgloben** så ser du all metadata om din entitet på **alla inmatade språk samtidigt**. För att gå tillbaka till ursprungsläget klickar du på jordgloben en gång till.


Du kan också se metadatan på **ett språk i taget** genom att klicka på **den högra språkikonen** och välja språk från en lång lista. 

![Informationsdialogfönstret, lista med språk](img/catalog_13_detaljeradinformation_spraklista.png)



