## Översikt
Under **showcases** kan du visa upp **konkreta exempel** (t.ex. en webbsida eller en mobilapp) som har byggts baserat på en eller flera publicerade datamängder i en katalog. Det kan vara användbart för att dokumentera hur en organisations öppna data har nyttiggjorts genom andra aktörer. Till skillnad från idéer så är ett showcase alltså något som redan finns och går att visa upp.

![Översiktsbild över showcases](img/catalog_9_showcases_oversikt.png)

## Skapa showcase

För att lägga upp ett showcase, gå in på Showcases och klicka på ”Skapa”. Fyll sedan i de obligatoriska fälten **titel**, **beskrivning** och vilken eller vilka **datamängder** som showcaset är baserat på.

![Dialogfönster för att skapa ett showcase](img/catalog_9_showcases_skapashowcase.png)

## Redigera eller ta bort

Och vill du gå in och plocka bort showcaset eller redigera beskrivningen i efterhand så kan du klicka på knapparna ”Redigera” eller ”Ta bort” inne på showcaset.

![Knapparna Redigera och Ta bort för showcases](img/catalog_9_showcases_redigeratabort.png)

## Publicera showcase

Nyskapade showcases har som standardinställning att vara publika direkt när de skapats. De går att avpublicera via publiceringsknappen.

![Publiceringsknappar för showcases](img/catalog_9_showcases_publiceringsknappar.png)

## Se detaljerad information

Om du vill se mer detaljerad information om ett showcase kan du klicka på informationsikonen. 
![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata samt alla entiteter som länkar till och från ditt showcase. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).
