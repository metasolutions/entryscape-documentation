## Översikt

En **datatjänst** är ett sätt att beskriva och publicera ett eller flera **API:er**. I EntryScape Catalog kan du lägga till en datatjänst som hör till en datamängd. 

## Skapa datatjänst

För att skapa en datatjänst, gå in på Datatjänster i menyn och klicka på ”Skapa”.

![Huvudmeny Datatjänster](img/catalog_7_datatjanster_meny.png)

Då får du fylla i **titel** och **åtkomstadress** för datatjänsten. För fältet åtkomstadress ska du peka på den mest generella öppna åtkomstadressen till din datatjänst. Undvik att länka djupare direkt till API:et. 

![Dialogfönster för att skapa datatjänst](img/catalog_7_datatjanster_skapadatatjanst.png)

Om du dessutom klickar i att du vill se även **rekommenderade** och **frivilliga fält** så får du fylla i ytterligare information om datatjänsten, som till exempel licens m.m. Klicka sedan på ”Skapa”.

Det går att spara beskrivningen av datatjänsten även om man inte har fyllt i alla obligatoriska fält, men då som ett ofärdigt utkast. Datatjänsten visas då med en gul ikon som visar att den saknar viss obligatorisk information och det går inte att publicera datatjänsten förrän alla obligatoriska fält är ifyllda.

![Gul ikon visar att datatjänsten saknar viss obligatorisk information](img/catalog_utkastikon.png)


## Redigera eller ta bort datatjänst

Du kan redigera informationen om din datatjänst eller ta bort med knapparna **"Redigera"** och **"Ta bort"** på översiktssidan. Där kan du även se en lista med den eller de **Datamängder** som datatjänster försörjer.

![Knapparna Redigera och Ta bort](img/catalog_7_datatjanster_redigeratabortknappar.png)


## Se detaljerad information

Om du vill se mer detaljerad information om en datatjänst kan du klicka på informationsikonen. 
![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata som åtkomstadress, vilken datamängd datatjänsten försörjer samt alla entiteter som länkar till och från datatjänsten. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).

## Publicera datatjänst

När du har beskrivit din datatjänst tillräckligt så kan du **publicera** den, genom att klicka på publiceraknappen på listan med datatjänster. Då blir knappen grön.

![Publiceringsknappen för datatjänster](img/catalog_7_datatjanster_publiceradatatjanst.png)