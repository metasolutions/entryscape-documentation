## Översikt
Med den här funktionen kan organisationer lägga upp **idéer på hur publicerad öppen data kan användas**, baserade på en eller flera datamängder i en katalog. Det kan till exempel vara bra när du planerar hackatons eller vill uppmuntra till en viss typ av användning av öppna data där du ser ett stort behov. Till skillnad från showcases så är en idé något som inte redan är konkretiserat. 

## Skapa idé

För att lägga upp en idé, gå in på Idéer och klicka på ”Skapa”.

![Huvudmeny Idéer](img/catalog_8_ideer_meny.png)

**Titel** och **beskrivning** är obligatoriskt att fylla i, men det är bra att fylla i vilken **datamängd** som du har i åtanke att idén ska basera sig på.

![Dialogfönster för att skapa idé](img/catalog_8_ideer_skapaide.png)

## Redigera eller ta bort idé

Du kan fylla i mer information om idén, genom att gå in på idéns sida och klicka på knappen Redigera. Där kan du också ta bort idén om du vill med knappen Ta bort.

![Knappar Redigera och Ta bort för ideer](img/catalog_8_ideer_redigeratabort.png)


## Publicera idé

Nyskapade idéer har som standardinställning att vara publika direkt när de skapats. De går att avpublicera via publiceringsknappen.

![Publiceringsknapp för idéer](img/catalog_8_ideer_app.png)

## Se detaljerad information

Om du vill se mer detaljerad information om en idé kan du klicka på informationsikonen. 

![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata samt alla entiteter som länkar till och från din idé. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).
