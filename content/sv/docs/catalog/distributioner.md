## Översikt
En distribution är en **representation** av en datamängd. T ex kan en och samma datamängd både finnas tillgänglig via API och som en CSV-fil, d.v.s. datamängden har två distributioner. Det finns ingen gräns för antalet distributioner per datamängd, men det ska inte finnas **flera distributioner med samma innehåll och filformat** för en datamängd. En distribution kan dock bestå av **flera filer** i samma format, vid till exempel tidsserier som regelbundet uppdateras/utökas.


## Skapa distribution
För att skapa en distribution, gå in på datamängdens översiktssida och klicka på plusknappen vid Distributioner.

![Plusknappen för att skapa en distribution](img/catalog_4_distributioner_plusknapp.png)

Sedan får du välja om du vill **fylla i en länk** (webbadress) eller **ladda upp en fil**. 


## Ladda upp fil
För att **ladda upp en fil**, klicka på Fil och sedan ikonen med förstoringsglaset.

![Dialogfönster för att ladda upp en fil](img/catalog_4_distributioner_laddauppfil.png)

Om du slår på rekommenderade och frivilliga fält så får du möjlighet att fylla i ytterligare information om distributionen. 

![Dialogfönster för att fylla i information om distributionen](img/catalog_4_distributioner_skapadistribution_fil.png)

Här nedan förklaras ett urval av de rekommenderade och frivilliga fälten. Om du klickar på rubriken för varje inmatningsfält så får du också upp en informationsruta med tips om vilken information som förväntas i fältet.

### Rekommenderade fält
**Titel:** Distributionens namn. För att lägga till en titel på ett annat språk, klicka på **+ Titel**. Då kommer en ny rad med inmatningsfält upp där du kan välja andra språk.

**Beskrivning:** Utförligare beskrivning av distributionen.

**Format:** Beskriv vilket format din distribution har. Du kan välja i listorna bland "Vanliga mediatyper” som till exempel CSV och JSON, eller "Geografiska mediatyper” som till exempel WMS eller TIFF.

**Tillgänglighet:** Tillgänglighet anger hur länge datat kommer att finnas åtkomligt. 
”Experimentell” innebär att datat ligger kvar på kort sikt. 
”Stabil” innebär att datat ligger kvar för lång tid framöver. 
”Temporär” innebär att datat kan försvinna när som helst.
”Tillgänglig” innebär att datat ligger kvar på medelång sikt (några år).

**Licens:** Välj licensen som gäller för distributionen även om du också angivit den för datamängden. Creative Commons Zero Public Domain 1.0 (CC0 1.0) är ofta standard för öppna data och organisationer. Om du är osäker på vilken licens som organisationen har valt att arbeta med, så finns den ofta förklarad i organisationens dokument Rutin för vidareutnyttjande av information.

### Frivilliga fält	

**Tidsupplösning:** Den minsta urskiljbara tidsperioden i datamängden, till exempel ”Månader: 3”.

**Utgivningsdatum:** Det datumet då distributionen skapades. Med den högra datummenyn kan du välja om du vill fylla i bara årtal, bara datum eller datum och tidpunkt. Sedan fyller du i önskat år/datum/klockslag i de vänstra fälten.

**Tillämplig lagstiftning:** Om distributionen tillhör en värdefull datamängd eller ej. Under själva datamängden som distributionen tillhör ska du välja vilken kategori av värdefull datamängd den tillhör.

**Dokumentation:** En sida eller ett dokument om den beskrivna distributionen. Om du vill koppla distributionen till ett redan existerande dokument i EntryScape Catalog, måste du se till att det är av dokumenttypen "Dokumentation". Under [Dokument](dokument.md) kan du läsa mer om hur du skapar och kopplar dokument till distributioner.

Under distributionens trepunktsmeny kan du redigera och hantera filer som hör till den (d.v.s. ladda ner, ersätta och ta bort). Du kan också skapa ett API utifrån en befintlig CSV-fil, se nedan.

![Menyalternativ för distributioner](img/catalog_4_distributioner_trepunktsmeny.png)


## Se detaljerad information

Om du vill se mer detaljerad information om en distribution kan du klicka på informationsikonen. 
![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata som webbadress för åtkomst, format mm för distributionen samt alla entiteter som länkar till och från din distribution. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).

## Ta bort, ersätta eller uppdatera fil

För att komma åt och hantera de filer som hör till en distribution så går du in under **Distributioner**, klickar på distributionens trepunktsmeny och väljer **”Hantera filer”**.

![Menyalternativet Hantera filer för en distribution](img/catalog_12_forvaltning_meny_hanterafiler.png)

Då kommer du till översiktsvyn för de filer som hör till distributionen. Där kan du välja att  **ersätta** eller **ta bort** den fil som redan ligger där. Du kan också **lägga till en ny fil** (data för ett nytt år till exempel).

![Knappen Lägg till fil samt menyalternativ Ersätt filt samt Ta bort fil](img/catalog_12_forvaltning_hanterafiler.png)

Glöm inte sen att även **hålla den beskrivande metadatan uppdaterad**, så att den stämmer överens med de nya filer som ligger uppladdade. T ex **”Ändringsdatum”** för distributionen, som är ett frivilligt informationsfält. Du når den via distributionens trepunktsmeny och alternativet ”Redigera”.

![Menyalternativet Redigera för en distribution](img/catalog_12_forvaltning_redigeradistribution.png)

Du kan också gå in under Datamängder, välja ”Redigera” och ändra det rekommenderade fältet **”Tidsperiod”** för att passa datat i den nya filen. 

![Redigera tidsperiod för en datamängd](img/catalog_12_forvaltning_tidsperiod.png)


## Skapa ett API från tabelldata (CSV)
Du kan automatiskt generera ett API från en distribution med tabelldata (för närvarande CSV-filer). För att det ska fungera måste följande villkor vara uppfyllda:

- Tabellens första rad bör innehålla **korta namn** för varje kolumn eftersom de används som variabelbeteckningar i API:et. Kolumntitlarna blir förkortade och konverteras till små bokstäver vid import.
- Textsträngar som kolumntitlar eller annat textinnehåll i cellerna får endast innehålla **Unicode-tecken**.
- **Endast kommatecken** (`,`) bör användas som kolumnavgränsare. (Det finns detektering för CSV-filer som använder semikolon (`;`) som separator, men det är kommatecken som rekommenderas.
- Alla citattecken måste vara **dubbla citattecken** (`"`).
- Istället för Escape så måste dubbel backslash användas (`\\`)
- Ny rad skrivs som backslash + n (d.v.s. `\n`) och Enter + ny rad skrivs som backslash + r + backslash + n (`\r\n`).

Ett automatiskt genererat API är tillgängligt via dess REST-gränssnitt och är ett enkelt webbgränssnitt som kan nås med en webbläsare. Webbgränssnittet innehåller länkar till mer detaljerad Swagger-baserad API-dokumentation.

Om du har laddat upp en fil som distribution kan du aktivera den för användning genom API. Detta gör du genom att stå i översiktsvyn för datamängd och klicka på trepunktsmenyn till höger vid din distribution och sen klickar “Aktivera API”.

![Menyalternativ Aktivera API](img/catalog_4_distributioner_aktiveraapi.png)

Om inte datamängden är publicerad kommer följande varning om att datamängden kommer att publiceras när du påbörjar aktivering av API-generering. Är du OK med detta så klicka på "Ja".

![Bekräftelse för att aktivera API](img/catalog_4_distributioner_bekraftelseaktiveraapi.png)

Då påbörjas API-genereringen.

![Initialisering av API](img/catalog_4_distributioner_initialiseratapi.png)

Om inga felmeddelanden dyker upp, så klickar du på ”Stäng” när processen är klar.

![API:et har skapats](img/catalog_4_distributioner_apiharskapats.png)

Ditt nyskapade API finns nu som en egen distribution döpt till ”Automatiskt skapat API”, i formatet JSON. Via trepunktsmenyn kan du gå in och se API-information, som till exempel API:ets URL. Du kan också redigera, ta bort och - om du gjort förändringar i grunddistributionen (CSV-filen) -  välja att uppdatera API:et för att få med de nya förändringarna i ditt skapade API.

![Menyalternativ för nyskapat API](img/catalog_4_distributioner_automatiskt.png)


## Länka till externa API:n eller filer
Om det redan finns ett **externt API** eller en extern fil som hör till den datamängd som är beskriven i EntryScape, så kan du **länka** från din datamängd till den filen eller det API:et, som en extern distribution. (Det systemet ansvarar för uppdateringar av sina egna filer och API:n, till skillnad från om du skulle skapa en distribution genom uppladdning av en fil i EntryScape.)

För att länka till ett externt API eller en extern fil, klicka på plus-tecknet bredvid Distributioner.

![Plusknapp för att skapa distribution](img/catalog_4_distributioner_plusknapp_liten.png)

Sedan fyller du i den externa webbadressen, d.v.s. klistrar in länk som börjar med ”http” eller ”https” och slutar på till exempel .json. Sedan klickar du på ”Skapa”.

![Skapa distribution genom en länk](img/catalog_4_distributioner_skapadistribution_lank.png)

Då skapas en extern distribution med det tillfälliga namnet ”Åtkomstpunkt”.

![Menyalternativ för den nyskapade externa distributionen](img/catalog_4_distributioner_atkomstpunkt.png)

Gå sedan in via trepunktsmenyn för att redigera informationen om distributionen och ge den ett bättre beskrivande namn som gör den lätt att skilja från andra distributioner. Fyll i titel, beskrivning och helst även formatet på filen, tillgänglighet och licens.


## Visualiseringar
Du kan även skapa visualiseringar av ditt data i EntryScape i form av **stapeldiagram**, **cirkeldiagram**, **linjediagram**, **kartor** och **tabeller**. Om du dessutom använder visualiseringar i en extern visning med EntryScape Blocks så kommer dina visualiseringar att automatiskt läggas till eller justeras efter dina ändringar.

![Exempelkarta över bra tak för solceller i Gamla stan](img/catalog_4_distributioner_solkarta.png)

Exempelkarta: Gamla stan i Stockholm. Tak med lämplig solinstrålning för att sätta upp solceller.

För att kunna skapa en visualisering så måste det finnas en distribution med en uppladdad CSV-fil eller en Web Map Service (WMS).
Klicka på plusknappen bredvid Visualiseringar för att skapa en ny visualisering.

![Plusknapp för att skapa en visualisering](img/catalog_4_distributioner_visualiseringsknapp.png)

Sedan väljer du vilken distribution som du vill skapa visualiseringen ifrån. 

![Välj distribution att generera visualisering ifrån](img/catalog_4_distributioner_skapavisualisering1.png)

Efter det fyller du i de obligatoriska fälten **Titel**, **Diagramtyp** och **Diagramaxlar**. För att skapa en kartvisualisering krävs det att det finns geografiska koordinater i din datafil eller en WMS-tjänst. 

![Inmatningsfält för visualisering](img/catalog_4_distributioner_skapavisualisering2.png)

När du har fyllt i de obligatoriska fälten så kan du se en förhandvisning av visualiseringen längst ner i samma fönster.

![Förhandsvisning av en kartvisualisering](img/catalog_4_distributioner_forhandsvisning.png)




!!! info "**Obs!** Om det visas konstiga tecken i din visualisering så behöver du se över så att filens teckenkod stämmer överens med den angivna teckenkoden i EntryScape. Prova att ändra teckenkod tills det ser bra ut. Om det är en stor fil kan ett felaktigt tecken finnas på en annan sida än den första så bläddra gärna några sidor framåt och kontrollera."

Du kan redigera, förhandsvisa och ta bort visualiseringen via trepunktsmenyn.

![Menyalternativ för en visualisering](img/catalog_4_distributioner_meny_visualisering.png)