## Sök datamängd
<a id="search-for-dataset"></a>

Innan du skapar en datamängd är det bra att först kontrollera att den inte redan finns. Du kan använda **sökfunktionen** för att söka bland tillgängliga datamängder i katalogen. Skriv i så fall in minst tre tecken för att kunna hitta matchande sökresultat.

![Sökrutan för datamängder](img/catalog_3_datamangder_sok.png)

Om du klickar dig in på en datamängd så kommer du in på datamängdens översiktsvy. Där kan du välja att redigera datamängden genom att klicka på **Redigera-knappen**.

![Redigeraknappen för en datamängd](img/catalog_3_datamangder_redigeraknapp.png)

Om datamängden inte redan finns, så kan du skapa den.

## Skapa datamängd från mall
<a id="create-dataset-from-template"></a>

Det finns två sätt att skapa en datamängd, antingen kan du **skapa den ifrån en mall** som redan finns eller så kan du **skapa den från grunden**. 

![Menyalternativ Skapa datamängd från mall](img/catalog_3_datamangder_skapafranmall.png)

För att skapa en datamängd från en mall, klicka på **”Skapa"** och sedan **"Datamängd från mall”** och välj sedan en passande katalog (till exempel ”Kommunala datamallar”) och eventuellt kategori (till exempel ”Miljö”) där du tror att det kan finnas en existerande mall för den datamängd du vill beskriva.

![Dialogfönster för Skapa från datamängdsmall](img/catalog_3_datamangder_skapafranmall1.png)

För att granska hur datamängden är beskriven, klicka på den runda informationssymbolen. Om du vill använda de valda mallen, klicka på ”Skapa”.

![Översiktsvy över datamängdsmallar](img/catalog_3_datamangder_skapafranmall2.png)

Då får du upp en sida med mallens förifyllda fält som du kan redigera så att det passar din datamängd.

![Exempel på förifylld datamängdsmall](img/catalog_3_datamangder_skapafranmall3.png)

Genom att **slå på rekommenderade respektive frivilliga inmatningsfält** så kan du redigera beskrivningen ytterligare. För mer information om respektive inmatningsfält, se [Redigera datamängd](#redigera-datamangd) här nedanför.

![Knappar för att slå på rekommenderade och frivilliga inmatningsfält](img/catalog_3_datamangder_obl_rek_fri.png)

Det går bara att skapa en datamängd ifrån en och samma mall, men om du vill skapa flera liknande datamängder, se [Kopiera datamängd](#kopiera-datamangd).


## Skapa datamängd från grunden
<a id="create-a-dataset-from-scratch"></a>

Om du vill **skapa en datamängd från grunden** istället för från en mall, så klickar du på knappen **”Skapa”** och väljer sedan **"Datamängd"**.

![Menyalternativ Skapa Datamängd](img/catalog_3_datamangder_skapadatamangd.png)

Börja med att **välja den profil** som passar din datamängd, innan du börjar fylla i inmatningsfälten. Profilen du väljer bestämmer vilka uppsättningar med inmatningsfält som visas för din datamängd. Den vanligaste profilen brukar vara förinställd som standard. Finns det ingen Profil-knapp, så är profilen redan förvald åt dig.

![Profilknapp](img/catalog_3_datamangder_profilknapp.png)

Organisationen kan publicera enligt olika applikationsprofiler, t.ex. för PSI kan det vara DCAT-AP-SE och för Inspire-direktivet kan det vara en kombination av NMDP och DCAT-AP-SE. Arbetar du med geodata bör du använda Inspire-profilen.


## Beskriva datamängd
<a id="describing-a-dataset"></a>

När du har valt profil kan du börja beskriva din datamängd genom att fylla i inmatningsfälten. Som default visas alla **obligatoriska** och **rekommenderade inmatningsfält**. Du kan själv välja att **visa** eller **dölja** de rekommenderade och de frivilliga inmatningsfälten i redigeringsvyn genom reglageknapparna högst upp. Ifyllda fält visas alltid.

Längst ut till höger ser du snabblänkar till alla inmatningsfält. Om du klickar på rubriken för varje inmatningsfält så får du upp en informationsruta med tips om vilken information som förväntas i fältet.

De tre fälten: **Titel**, **Beskrivning** och **Utgivare** är **obligatoriska fält** för en datamängd. Tänk på att det kan vara obligatoriskt för din organisation att fylla i obligatoriska metadatafält på **flera språk**. För att lägga till en titel på ett annat språk, klicka på **+ Titel**. Då kommer en ny rad med inmatningsfält upp där du kan välja andra språk. Om du ångrar dig och vill ta bort en rad så klickar du på minus-symbolen till höger.

![Fält för att fylla i titel](img/catalog_3_datamangder_titel.png)

Det går att spara beskrivningen av datamängden även om man inte har fyllt i alla obligatoriska fält, men då som ett ofärdigt utkast. Datamängden visas då i listan med en **gul utkastsikon** som visar att den saknar viss obligatorisk information och det går inte att publicera datatjänsten förrän alla obligatoriska fält är ifyllda.

![Gul ikon visar att datatjänsten saknar viss obligatorisk information](img/catalog_utkastikon.png)

Obs! När en **extern** datavärd förvaltar datamängd på uppdrag av organisationen ska **den organisationen** stå som utgivare av datamängden. Detta är för att organisationen endast kan ställa krav på hur den externa datavärden ska förvalta datamängden men i praktiken inte kan utöva kontroll, t.ex. myndighetskontroll utöver den.

![Fältet utgivare](img/catalog_3_datamangder_utgivare.png)

Nedan listas några av de rekommenderade fälten respektive frivilliga fälten för DCAT-AP-SE. Mer information om hur de olika fälten ska fyllas i hittar du i rekommendationerna kring metadatadatafält på [dataportal.se](http://dataportal.se).

### Rekommenderade fält
**Kontaktuppgift:** En datamängd behöver en kontakt, oftast dataägaren, för att organisationen ska kunna hålla koll på vilken avdelning eller person som uppdaterar och underhåller datamängden. Detta kan vara en individ eller organisation (avdelning eller enhet) inom organisationen med funktionsadress. Välj en kontakt ur listan eller skapa en genom att trycka på förstoringsglaset. För mer information om hur du skapar nya kontakter, se [Skapa kontaktuppgift](#skapa-kontaktuppgift).

![Fältet kontaktuppgift](img/catalog_3_datamangder_kontaktuppgift.png)

**Nyckelord:** För att andra ska kunna söka efter och lätt hitta datamängden, så behöver du beskriva den genom att lägga till nyckelord. Det är viktigt både för de som söker i din katalog, men även de som söker i kataloger som skördar data ifrån din katalog (till exempel dataportal.se).

**Special: Anpassade fält**
I vissa fall kan det finnas anpassade fält som din organisation har i sin anpassade applikationsprofil. Detta finns endast i egen instans av EntryScape och inte i EntryScape Free.
Exempelvis kan din organisation ha valt att importera kontrollerade ämnesord från ordlistan GEMET (GEneral Multilingual Environmental Thesaurus) till EntryScape. GEMET är ett en etablerad europeisk tesaurus med över 40 teman (översta nivån) och innehåller totalt ca 5000 begrepp. Då det finns en så stor mängd begrepp att välja bland kan du enklast välja lämpliga ämnesord genom att använda sökfunktionen.

**Anpassat fält från applikationsprofil Inspire**
Om din organisation har en anpassad applikationsprofil för t.ex. INSPIRE.
Om du valt profilen Inspire syns även fälten Inspire-tema och Ämneskategori för att beskriva datamängden.

**Utgivningsdatum:** Datamängdens formella publiceringsdatum. Med den högra datummenyn kan du välja om du vill fylla i bara årtal, bara datum eller datum och tidpunkt. Sedan fyller du i önskat år/datum/klockslag i de vänstra fälten.

**Namngivet geografiskt område:** De eller det geografiska område(n) som omfattas av datamängden. Klicka på förstoringsglaset för att välja till exempel land, region, kommun eller ort.

**Geografiskt område, Omskrivande rektangel:** Välj område genom att zooma in och klicka på kartan eller fyll i koordinater själv, i referenssystemet WGS 84 (latitud och longitud). Zooma in och ut med plus och minus-ikonerna i vänsterkanten. 

![Zoomknappar för kartan](img/catalog_3_datamangder_kartzoom.png)

Välj platsmarkörsymbolen för att placera en punkt på kartan, eller välj rektangelikonen för att välja att rita en rektangel runt det aktuella området för datamängden. 

![Positionsknapp för att välja en punkt och rutknapp för att välja en rektangel i kartan ](img/catalog_3_datamangder_kartnal_kartruta.png)

För att rita ut en rektangel, klicka en gång för att rita ut det övre vänstra hörnet och en gång till för att avsluta rektangeln i nedre högra hörnet. Koordinaterna uppdateras automatiskt i fälten till höger om kartan.

**Tidsperiod, Start och Slut:** Datamängdens giltighetstid, med start- och slutdatum. Du kan välja att lägga till flera giltighetsperioder. Med den högra datummenyn väljer du om du vill fylla i bara årtal, bara datum eller datum och tidpunkt. Sedan fyller du i önskat år/datum/klockslag i de vänstra fälten.

**Åtkomsträttigheter:** Klassificering som talar om ifall datamängden innehåller öppna data, data med åtkomstrestriktioner eller icke offentlig data, som till exempel känsliga personuppgifter. Observera att beskrivande metadata kan publiceras men att distributioner inte bör skapas till de datamängder som är icke offentliga.

### Frivilliga fält

**Särskilt värdefull datamängd, kategori:** Om datamängden klassas som särskilt värdefull (t ex "Kulturhistoriska lämningar") och vilken/vilka av de särskilt värdefulla kategorierna den tillhör.

**Uppfyller:** Den tillämpningsföreskrift eller specifikation som gäller för datamängden. Det går att hänvisa till något du importerat eller laddat upp i funktionen [Dokument](dokument.md) eller välja en specifikation som du har importerat från en dataportal. Observera att dokumentet måste vara av dokumenttypen "Standard/rekommendation/specifikation" i Catalog för att synas.

**Tidsupplösning:** Den minsta urskiljbara tidsperioden i datamängden, till exempel ”Månader: 3”.

**Avgift:** Om dataåtkomsten är belagd med avgift.	

**Dokumentation:** En sida eller ett dokument om den beskrivna datamängden. Om du vill koppla datamängden till ett redan existerande dokument i EntryScape Catalog, måste du se till att det är av dokumenttypen "Dokumentation". Under [Dokument](dokument.md) kan du läsa mer om hur du skapar och kopplar dokument till datamängder.


!!! info "**Obs!** Det kan vara frestande att jobba med EntryScape med flera flikar uppe samtidigt i webbläsaren, men det är inget som rekommenderas om du vill redigera dina datamängder. Om du av misstag råkar redigera samma datamängd i två olika flikar så är det lätt att du råkar skriva över dina egna ändringar på fel sätt och orsakar felmeddelanden i EntryScape."



## Datamängdsserier
I den nya metadatastandarden DCAT-AP 3 finns det stöd för **datamängdsserier**, något som också finns implementerat i EntryScape 3.14. Datamängdsserier kan användas för att gruppera data som hör ihop, i t ex tidsserier. Läs mer om hur datamängdsserier fungerar i [Dataportalens rekommendationer](https://docs.dataportal.se/dcat/docs/recommendations/#15-datamangdsseriers-dimensioner).

För att skapa en datamängdsserie, gå in på datamängder, klicka på **"Skapa"** och sedan **"Datamängdsserie"**. Fyll sedan i **Titel**, **Beskrivning** och **Utgivare** och eventuell annan information och klicka på "Skapa".

![Skapa Datamängdsserie](img/catalog_3_datamangder_skapadatamangdsserie.png)

I din lista med datamängder ser du datamängdsserien, utmärkt med en serieikon. 

![Datamängdsserieikonen](img/catalog_3_datamangder_datamangdsserieikon.png)

Samma ikon finns också på datamängdsseriens översiktssida. Där kan du **lägga till datamängder** till serien genom att klicka på knappen med förstoringsglaset bredvid "Datamängder". Och under trepunktsmenyn för en datamängd kan du välja att **ta bort** den från serien, eller att ta bort den helt och hållet.

![Lägga till datamängder till datamängdsserie](img/catalog_3_datamangder_datamangdsserie.png)


## Skapa kontaktuppgift
<a id="create-contact-point"></a>

När du redigerar information om en datamängd, kan du också skapa en ny **kontaktuppgift** genom att klicka på förstoringsglaset.

![Fältet Kontaktuppgift med förstoringsglas](img/catalog_3_datamangder_kontaktuppgift.png)

Du väljer först om du vill ange en organisation (till exempel enhet/avdelning) eller en individ som kontaktuppgift. Namn och e-post är obligatoriska uppgifter, men om du väljer att slå på visningen av **rekommenderade** fält (överst) så kan du fylla i fler uppgifter som telefon och adress. Klicka sedan på **Skapa-knappen** längst ner till höger.

![Dialogfönster för att skapa en ny kontakt](img/catalog_3_datamangder_skapakontaktuppgift.png)

Notera att du inte kan redigera eller ta bort en redan inmatad kontaktuppgift här. Det gör du istället under **Kontakter** ute i vänstermenyn. Där har du full redigeringskontroll över alla inlagda kontakter.

![Knapparna Redigera och Ta bort kontakt](img/catalog_3_datamangder_redigeratabortkontakt.png)


## Kopiera datamängd
<a id="copy-dataset"></a>

Om du redan har en datamängdsbeskrivning som du vill kopiera så gå in på datamängdens översiktssida och klicka på **”Kopiera”**.

![Kopieraknappen för en datamängd](img/catalog_3_datamangder_kopieradatamangd.png)

Notera att du då skapar en kopia av själva datamängdsbeskrivningen, d.v.s. inte tillhörande distributioner, visualiseringar och kommentarer som kan finnas länkade till originalet. Det förtydligas genom en kontrollfråga om du vill fortsätta.

![Kontrollfråga för att kopiera datamängd](img/catalog_3_datamangder_kopieradatamangd2.png)


## Tabellvy och listvy
<a id="table-view-and-list-view"></a>

Defaultvyn i EntryScape Catalog är att visa data i en **listvy**. Om du däremot vill se och redigera många förslag eller datamängder samtidigt på samma sida så kan du använda dig av **tabellvyn**. Du kan byta till tabellvyn genom att klicka på **tabell-ikonen** högst upp.

![Tabellikonen för att byta till tabellvy](img/catalog_3_datamangder_tabellvy1.png)

I tabellvyn kan du också bestämma hur många kolumner du vill se samtidigt. Genom att klicka på **”Kolumner”** så kan du välja vilka kolumner du vill se resp. dölja.

![Kolumnväljare](img/catalog_3_datamangder_kolumner.png)

För att redigera en tabellcell så klickar du på texten i cellen. Klicka på det **övre krysset** eller utanför redigeringsrutan för att stänga den. Efter att du har gjort dina ändringar i alla tabellceller, så glöm inte att klicka på **Spara-knappen** längst ner.

![Tabellvy med redigeringsmöjligheter](img/catalog_3_datamangder_tabellvy2.png)

För att byta tillbaka till listvy, klicka på list-ikonen högst uppe till höger.

![Listvy-ikonen](img/catalog_3_datamangder_listvy.png)

## Se detaljerad information
<a id="see-detailed-information"></a>

Om du vill se mer detaljerad information om en datamängd kan du klicka på informationsikonen. 
![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata som utgivare, URI mm för datamängden samt alla entiteter som länkar till och från din datamängd. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).

## Ta bort datamängd
<a id="remove-dataset"></a>

För att ta bort en datamängd, gå till datamängdens översiktssida och klicka på knappen **"Ta bort"**. Observera att du bara kan ta bort datamängder som är opublicerade/ avpublicerade, så om du vill ta bort en publicerad datamängd måste du avpublicera den först genom att trycka på den gröna reglageknappen.

![Ta bort datamängd](img/catalog_3_datamangder_tabort.png)

