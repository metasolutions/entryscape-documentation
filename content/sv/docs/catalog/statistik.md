## Översikt
I EntryScape kan du se **statistik över användningen** av det öppna data som du har publicerat. Det kan vara användbart i till exempel rapporterings- och utvecklingsarbete.

På din katalogs översiktssida hittar du en förenklad **förhandsvisning** av användningsstatistiken ute i högerkanten. Där kan du se antal filnedladdningar och API-anrop de senaste 7 dagarna.

![Förhandsvisning av användningsstatistik](img/catalog_11_statistik_oversikt.png)

För **mer detaljerad statistik**, gå in på **Statistik** längst ner i vänstermenyn. Beroende på hur stora datamängder som statistiken beräknas på så kan det ta några sekunder för sidan att ladda.

![Huvudmeny Statistik](img/catalog_11_statistik_huvudmeny.png)

Den detaljerade statistiken visar **antal åtkomster/nedladdningar av API:er och filer** för olika datamängder under en viss tidsperiod.

![Detaljerad statistikvy](img/catalog_11_statistik_detaljeradvy.png)

Du kan **filtrera statistiken** på flera olika sätt. Du **söka på titel** på din datamängd i sökrutan överst. Du kan också välja vilket **tidsintervall** du vill se (till exempel igår, förra månaden, innevarande år m.m.) och så kan du bocka för vilken eller vilka **typer av data** du vill se (filer, dokument eller API-anrop). 

![Filtereringsalternativ för detaljerad statistik](img/catalog_11_statistik_filter.png)


Under diagrammet visas också **varje entitet som en egen rad**. Klickar du på raden så visas bara den entiteten i diagrammet. Klicka på raden en gång till för att avmarkera och återgå till att visa alla entiteter.

Statistiken går också att **ladda ner** som en CSV-fil, eller **skriva ut**, med knapparna högst uppe till höger.

![Knapparna ladda ner CSV och Skriv ut](img/catalog_11_statistik_laddaner.png)