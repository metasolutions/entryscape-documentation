## Förslag på datamängder
I EntryScape Catalog kan du skapa datamängder på flera olika sätt. T ex kan du börja med ett **förslag** på datamängd, d.v.s. ett utkast på en kommande datamängd. Det rekommenderas att du jobbar med förslag om du ännu inte har all information om datamängden eller om detaljer behöver kontrolleras innan den kan publiceras.

Först av allt är det bra att kontrollera att datamängden eller förslaget som du vill skapa inte redan finns eller har påbörjats av någon annan. Söker gör du med **sökfunktionen**, som finns under både Förslag och Datamängder. Skriv in minst 3 tecken för att få upp resultat som matchar titeln på datamängden.

![Sökrutan för datamängder och förslag](img/catalog_2_forslag_sokruta.png)


## Skapa förslag
Under Förslag så ser du en lista över de påbörjade förslag på datamängder som du redan har tillgång till. Längst till vänster så syns en statusbar som talar om hur många krav som har uppfyllts i checklistan för att förslaget ska få uppgraderas till en publicerbar datamängd. Du kan läsa mer om checklistan under ”Klarmarkera förslag genom checklista” längre ner.

![Översiktsvy över förslag, med statusbar](img/catalog_2_forslag_oversikt.png)

Nya förslag kan skapas på två olika sätt: Antingen så skapar du ett eget förslag från grunden, eller så skapar du (kopierar) ett förslag utifrån en mall som redan finns.

Klicka på ”Skapa” för att skapa ett helt nytt förslag på datamängd eller ”Skapa från mall” för att kopiera ett redan befintligt datamängdskoncept.

![Knapparna Skapa från mall och Skapa](img/catalog_2_forslag_skapaknappar.png)

När du skapar ett helt nytt förslag från grunden så visas följande vy:

![Dialogfönster för skapa nytt förslag](img/catalog_2_forslag_skapanyttforslag.png)


Här beskriver du ditt förslag på datamängd. Det obligatoriska fältet **Titel** kommer vara samma som för datamängden. Förslagsvis ger du den samma titel som själva informationsmängden eller en delmängd av informationsmängden. Tänk på att titeln bör vara koncis och specifik, så att den blir lätt för andra att söka efter. Ange titel både på svenska och engelska.Därefter fyller du i det rekommenderade fältet **Beskrivning**. Beskrivningen ska göra det tydligt för användare att förstå vad det är för datamängd. Lägg även till en engelsk beskrivning.

**Förfrågans URL** används sällan i nuläget, men kan komma att spela större roll i framtiden. Om det finns till exempel en begäran om datamängd från länk till diarium så ange den länken här i "Förfrågans URL". 

När du har angett titel och beskrivning så klickar du på knappen ”Skapa” längst ned.

Om du istället vill skapa ett förslag utifrån en befintlig **mall**, klicka på ”Skapa från mall” så får du upp en lista över alla befintliga mallar som du kan använda dig av. Du kan se mer **detaljerad information** om en mall genom att klicka på informationsikonen. (Läs mer om det på sidan [detaljerad information](detaljerad_information.md).) 

![Informationsikonen](img/catalog_informationsikon.png)

När du har hittat en passande mall är det bara att klicka på ”Skapa” för att skapa ett nytt förslag baserat på vald mall.

![Dialogfönster Skapa förslag från mall](img/catalog_2_forslag_skapaforslagfranmall.png)


## Klarmarkera förslag genom checklista
För att du ska kunna skapa en riktig datamängd som går att publicera, så bör förslaget klarmarkeras genom att uppfylla några olika villkor. EntryScape Catalog har en checklista där några av villkoren för den föreslagna datamängden är starkt rekommenderade att uppfyllas innan datat publiceras, till exempel frågan om dataägare och upphovsrätt. I listan med förslag så finns det en liten statusbar framför varje förslag. Den indikerar hur många av villkoren som är uppfyllda. För att gå in och jobba med checklistan, klicka på statusbaren under "Checklista”.

![Statusbar för checklista](img/catalog_2_forslag_checklista1.png)

De tre översta villkoren i checklistan är märkta med röda utropstecken, vilket innebär att de **bör klarmarkeras** innan datamängden ska få publiceras. Den första punkten intygar att **inga personuppgifter eller sekretessbelagda uppgifter** finns i datamängden. Den andra punkten intygar att din organisation har **rätten att äga datamängden** (eller förvalta den åt en annan organisation som äger datamängden). Den tredje punkten **identifierar dataägaren** av datamängden i organisationen.

![Checklistan](img/catalog_2_forslag_checklista2.png)

Ytterligare information om datamängden är också värdefullt att kunna kryssa för i checklistan. Det handlar bl a om ifall det finns en underhållsplan för datamängden. När du har klickat på spara så visar statusbaren för checklistan hur många av villkoren som är uppfyllda. Om de tre översta, viktigaste villkoren i checklistan är uppfyllda så visas statusbaren som grön.

![Grön statusbar för checklistan](img/catalog_2_forslag_checklista3.png)


## Kommentarer
Om du behöver avvakta med att fylla i information om ditt förslag, till exempel för att undersöka något närmare, kan du lägga till kommentarer. Det kan vara till extra hjälp för dig själv eller kollegor som ska fortsätta att bereda förslaget till datamängd. Klicka på kommentarssymbolen längst ut till höger för att lägga till kommentarer till förslaget/datamängden.

![Ikonen för ommentarer](img/catalog_2_forslag_kommentarer.png)

Där kan du ange ämne och beskrivning till din kommentar. 

Antal kommentarer på förslagen indikeras genom en blå cirkel på kommentarsikonen i förslagslistan. Kommentarer kan lätt redigeras eller tas bort i kommentarsvyn.

![Blå cirkel visar antal kommentarer på förslaget](img/catalog_2_forslag_kommentarer2.png)


## Skapa datamängd av ett förslag
Om du har beskrivit ditt förslag korrekt och helst bockat för de viktigaste villkoren i checklistan, så kan du **skapa en datamängd av ditt förslag** som sedan kan publiceras. Gå in på själva förslaget och klicka på knappen med förstoringsglaset bredvid "Datamängder". 

![Knapp med förstoringsglas för att skapa datamängd](img/catalog_2_forslag_skapadatamangd.png)



Nu kan du välja att skapa en datamängd baserat på ditt förslag geonom att klicka på "Skapa". Du kan också söka efter och länka till en redan existerande datamängd från ditt förslag.

![Skapa eller länka till datamängd](img/catalog_2_forslag_skapaellerlankatilldatamangd.png)

Därefter kan du se över titel, beskrivning och utgivare samt välja att fylla i mer rekommenderad eller frivillig information om din datamängd. När du klickar på ”Skapa” så adderas den nya datamängden automatiskt till sidan med datamängder, men förslaget ligger ändå kvar under förslagslistan eftersom det går att skapa flera olika datamängder baserat på samma förslag. 

På översiktssidan för ditt förslag kan du se en lista över länkade datamängder längst ner.


![Listan med länkade datamängder på förslagets översiktssida](img/catalog_2_forslag_lankadedatamangder.png)


Om du vill ta bort länken mellan en datamängd och ett förslag så klickar du på trepunktsmenyn och väljer "Ta bort länken".

![Menyalternativ Ta bort länken mellan datamängd och förslag](img/catalog_2_forslag_tabortlanken.png)


## Arkivera eller ta bort förslag
Förslag som inte är aktuella kan antingen **arkiveras** för senare användning eller **tas bort** helt. Du arkiverar ett förslag genom att klicka på knappen ”Arkivera”. Med knappen "Ta bort" så kan du också ta bort ett förslag helt. Borttagna förslag kan inte tas tillbaka.

![Menyalternativen Arkivera och Ta bort förslag](img/catalog_2_forslag_arkiveratabortforslag.png)

Arkiverade förslag hittar du under filterknappen på toppen av sidan, om du väljer att visa "Alla" eller "Arkiverade" förslag. 

![Filterknappen](img/catalog_2_forslag_filter.png)

Det är lätt att avarkivera ett förslag, genom att klicka på knappen "Avarkivera".

![Knappen Avarkivera](img/catalog_2_forslag_avarkivera.png)
