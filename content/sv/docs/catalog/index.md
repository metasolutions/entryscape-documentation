## Översikt
<a id="overview"></a>

**EntryScape Catalog** hanterar och publicerar kataloger med datamängder, datatjänster och distributioner i enlighet med de metadatastandader som finns, till exempel DCAT-AP-SE. Det möjliggör för alla organisationer oavsett sort eller sektor att beskriva samma sorts data på samma sätt och att dela med sig av data på ett standardiserat sätt som gör den lätt för andra att söka, hitta och använda.



![Hierarkisk översiktsbild över EntryScape Catalog](img/catalog_1_katalog_hierarkiskoversikt.png)
*Exempel på katalog med datamängder, distrubutioner, API:n, filer och visualiseringar.*

På startsidan syns de kataloger som du redan har skapat eller har fått tillgång till. Där syns också om katalogen är publik eller ej, d.v.s. om den är sökbar för andra. Om du inte ser någon katalog här, behöver du börja med att skapa en, eller be om behörighet till katalogen av gruppansvarig (se nedan).



![Översiktsvy över kataloger](img/catalog_1_katalog_demokatalog.png)




## Skapa en katalog
<a id="create-a-new-catalog"></a>

För att skapa en ny katalog klickar du på knappen Skapa.



![Skapaknappen](img/catalog_1_katalog_skapaknapp.png)

Om er organisation har behov av att välja bland flera olika typer av projekt, får du börja med att ange vilken **projekttyp** (till exempel en metadatastandard) som katalogen ska följa. I Sverige används i huvudsak standarden DCAT-AP-SE för de flesta metadata, med undantag för geografiska data som använder sig av GeoDCAT-AP.

![Dialogfönster för att skapa en katalog](img/catalog_1_katalog_skapa_katalog_popup.png)

Därefter fyller du i **namn** på katalogen, d.v.s. en titel som till exempel ”Hedestads kommunkatalog”. Tänk på att titeln bör vara kort och att alla orden blir sökbara. Fyll också i en kort **beskrivning** samt **tillhandahållande organisation/utgivare**, till exempel ”Hedestads kommun”. 

En nyskapad katalog är alltid icke publik. Den som har skapat katalogen blir automatiskt **gruppansvarig** för den, vilket betyder att man administrerar andra användares behörighet till katalogen. 

Många dataportaler förväntar sig en enda datakatalog per organisation, men i vissa sammanhang kan det vara bra om olika avdelningar/enheter skapar och ansvarar för sina egna kataloger.




## Behörigheter och delningsinställningar
<a id="permissions-and-sharing-settings"></a>

I EntryScape Catalog kan en användare i regel vara antingen **gruppansvarig** för en katalog eller **medlem** med läs- och skrivrättigheter. Den som är gruppansvarig kan ge andra användare behörighet samt göra katalogen publik. Medlemmar kan redigera katalogens metadata samt skapa och tillhandahålla datamängder, distributioner och förslag m.m. i katalogen. Däremot kan medlemmar inte publicera katalogen. Du kan vara medlem i en eller flera kataloger beroende på din arbetsroll och hur data organiseras.

I de fall där din organisation upprättat arbetsrutin, arbetsgrupp och fått in metadatahantering eller öppna data-arbete i förvaltningsorganisation, eller motsvarande, så finns det möjlighet att få en högre behörighet än gruppansvarig om det behövs, admin, i dialog med MetaSolutions support.

Om du inte har tillgång till den katalog du behöver, så behöver du begära tillgång till katalogen av den som är gruppansvarig (eller admin) i er organisation. (Den som är gruppansvarig är troligtvis motsvarande avdelnings- eller enhetschef.) 

Gruppansvariga kan **ändra behörigheter** genom att gå in under trepunktsmenyn och välja **”Delningsinställningar”**. 

![Menyalternativ Delningsinställningar](img/catalog_1_katalog_delningsinstallningar1.png)


Där kan du lägga till och ta bort användare till den grupp som har behörighet till katalogen. För att lägga till en användare till en grupp så klickar du på ”Lägg till användare i gruppen”. Du kan också välja vilken sorts behörighet användaren ska få (gruppansvarig eller medlem) genom att klicka på ikonen framför namnet.

![Behörighetsinställningar för användare](img/catalog_1_katalog_delningsinstallningar2.png)

För att ta bort en användare från en grupp så klickar du på trepunktsmenyn och väljer ”Ta bort”.

![Menyalternativ Ta bort användare från grupp](img/catalog_1_katalog_delningsinstallningar3.png)




## Fyll i mer information om katalogen
<a id="enter-more-information-about-the-catalog"></a>

När du har skapat en katalog så är det bra om du fyller i mer information om din katalog. Det gör du genom att klicka på **trepunktsmenyn** och välja **"Redigera"**. 

![Menyalternativ Redigera katalog](img/catalog_1_katalog_redigerakatalog.png)

Ju mer specifik beskrivning du gör av det metadata som du publicerar, desto lättare blir den för andra att hitta och återanvända. 

Inmatningsfönstret som visas innehåller inmatningsfält på tre olika nivåer: obligatorisk information (* måste fyllas i), rekommenderad information (bra om det fylls i) och frivillig information. Du kan slå på och stänga av vilka inmatningsfält som ska visas med hjälp av reglagen högst upp. 

De **obligatoriska** fälten **Titel**, **Beskrivning** och **Utgivare** har du redan fyllt i när du skapade datakatalogen. Beroende på vilken profil som är vald så visas olika informationsfält. Här nedan visas några exempel på rekommenderade fält som hör till den svenska standardprofilen DCAT-AP-SE. Du kan också klicka på rubrikerna för de olika fälten för att få upp förklarande hjälptext.

![Knappar för att slå på rekommenderade och frivilliga inmatningsfält](img/catalog_1_katalog_obl_rek_fri.png)

### Exempel på rekommenderade fält enligt DCAT-AP-SE
**Utgivningsdatum:** Det datumet då katalogen skapas. Oftast är det mer intressant med att sätta utgivningsdatum för katalogens olika delar, så som datamängder m.m. Med den högra datummenyn kan du välja om du vill fylla i bara årtal, bara datum eller datum och tidpunkt. Sedan fyller du i önskat år/datum/klockslag i de vänstra fälten.

**Namngivet geografiskt område:** Namn på län, kommun eller ort. Du kan söka fram rätt område med hjälp av förstoringsglaset. Du kan enkelt lägga till flera geografiska områden.

**Licens:** Vilken eller vilka licenser som gäller för andra som ska använda eller återanvända katalogen.

**Taxonomi för tema:** Den standardlista med teman som används för att klassificera katalogens datamängder. (Innehåller bl a Befolkning och samhälle, Miljö och Transport m.m.)

## Se detaljerad information
<a id="see-detailed-information"></a>

Om du vill se mer detaljerad information om din katalog kan du klicka på informationsikonen. 
![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata som utgivare, URI mm för din katalog samt alla entiteter som länkar till och från din katalog. Läs mer om dialogfönstret på sidan [detaljerad information](detaljerad_information.md).

## Ta bort katalog
<a id="remove-catalog"></a>

För att ta bort en katalog, klicka på katalogens trepunktsmeny och välj ”Ta bort”.

![Menyalternativ Ta bort katalog](img/catalog_1_katalog_tabortkatalog.png)