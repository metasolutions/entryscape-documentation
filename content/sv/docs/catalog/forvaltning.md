## Översikt
Beroende på vad du har beskrivit i fältet **“Uppdateringsfrekvens” för din datamängd** så bör du hålla den uppdaterad så ofta som du har angivit. Förslagsvis kan du sätta påminnelser i din kalender eller i ditt projektledningsverktyg för att komma ihåg att uppdatera. En datamängd som inte är uppdaterad enligt angiven uppdateringsfrekvens kan leda till att användare av datat kontaktar dig eller din organisation och frågar varför datamängden inte underhålls. Då är det bättre att uppdatera proaktivt enligt uppdateringsfrekvensen.

![Uppdateringsfrekvens för en datamängd](img/catalog_12_forvaltning_uppdateringsfrekvens.png)


## Uppdatera filer i en distribution
För att komma åt och hantera de filer som hör till en distribution så går du in under **Distributioner**, klickar på distributionens trepunktsmeny och väljer **”Hantera filer”**.

![Menyalternativet Hantera filer för en distribution](img/catalog_12_forvaltning_meny_hanterafiler.png)

Då kommer du till översiktsvyn för de filer som hör till distributionen. Där kan du välja att till exempel **lägga till en ny fil** (data för ett nytt år till exempel) eller **ersätta** resp. **ta bort** den fil som redan ligger där.

![Knappen Lägg till fil samt menyalternativ Ersätt filt samt Ta bort fil](img/catalog_12_forvaltning_hanterafiler.png)

Glöm inte sen att även **hålla den beskrivande metadatan uppdaterad**, så att den stämmer överens med de nya filer som ligger uppladdade. T ex **”Ändringsdatum”** för distributionen, som är ett frivilligt informationsfält. Du når den via distributionens trepunktsmeny och alternativet ”Redigera”.

![Menyalternativet Redigera för en distribution](img/catalog_12_forvaltning_redigeradistribution.png)

Du kan också gå in under Datamängder, välja ”Redigera” och ändra det rekommenderade fältet **”Tidsperiod”** för att passa datat i den nya filen. 

![Redigera tidsperiod för en datamängd](img/catalog_12_forvaltning_tidsperiod.png)


## Ladda ner
Det finns flera sätt att **ladda hem data** i EntryScape Catalog. Du kan ladda hem **enskilda filer** som hör till distributioner under ”Hantera filer”.

![Menyalternativ Hantera filer](img/catalog_12_forvaltning_meny_hanterafiler.png)

![Menyalternativ Ladda ner fil](img/catalog_12_forvaltning_laddanerfil.png)

Du kan också ladda ner **hela din katalog** (som en fil), genom att högerklicka på katalogens trepunktsmeny och välja ”Ladda ner”. Observera att den nedladdade katalogen bara innehåller *metadata*, *inte* de filer eller visualiseringar som hör till dess distributioner.

![Menyalternativ Ladda ner för katalog](img/catalog_12_forvaltning_laddanerkatalog.png)

Sedan får du välja format för din fil: RDF/XML, Turtle, N-Triples eller JSON-LD. Klicka slutligen på ”Ladda ned”.

![Dialogfönster för att välja nedladdningsformat](img/catalog_12_forvaltning_laddanerpopup.png)

Efter det kan du klicka på ”Stäng” för att stänga fönstret.