## Översikt
Under Dokument kan du importera, **ladda upp eller länka till filer** som ska kopplas till en datamängd, som till exempel en specifikation eller ett licensavtal. För närvarande stöds tre olika dokumenttyper: **Dokumentation**, **Licens** och **Standard/Rekommendation/Specifikation**. Välj först om du vill **importera en extern specifikation** eller om du vill **skapa ett dokument**, genom att länka till eller ladda upp som en fil.

![Knapparna Importera och Skapa](img/catalog_10_dokument_skapaknappar.png)


## Importera extern specifikation
Om du klickar på ”Importera” så får du upp en lista med **specifikationer** som finns tillgängliga via **dataportal.se**. 

**En specifikation är en guide eller en **instruktion** för hur en viss sorts datamängd bör se ut.** Följer du specifikationen så blir datat kompatibelt med samma sorts data från andra leverantörer.

Klicka på ”Importera” på den specifikation du vill välja och sedan ”Stäng” för att stänga fönstret.

![Lista över valbara specifikationer att importera](img/catalog_10_dokument_importeraspecifikation.png)


## Skapa dokument
För att **skapa/lägga till ett nytt dokument**, gå in på Dokument och klicka på ”Skapa”.

Om dokumentet finns tillgängligt via webben så fyll i webbadressen (URL), annars klicka på ”Fil” och ladda upp filen. Det är obligatoriskt att fylla i **Dokumenttyp** och **Titel**, men det rekommenderas också att du fyller i en **beskrivning** av dokumentet. Klicka sedan på ”Skapa” längst ner till höger.

![Inmatningsfält för att skapa ett nytt dokument](img/catalog_10_dokument_skapanyttdokument.png)

Dokumentet är nu skapat men ligger i EntryScape Catalog som ett löst dokument som inte hör ihop med något annat än. Se nedan hur du kan göra för att länka ihop dokumentet med en datamängd.


## Länka till dokument från en datamängd eller distribution
I beskrivningen för en datamängd finns två frivilliga fält där du kan **länka till ett dokument** som du har laddat upp eller länkat till: 

**”Uppfyller”** hänvisar till en specifikation eller liknande som datamängden uppfyller. Observera att dokumentet du länkar till måste vara av dokumenttypen "Standard/rekommendation/specifikation" i Catalog för att synas.

**"Dokumentation”** hänvisar till en dokumentationsfil som hör ihop med datamängden. För att länka till dokument, gå in på Datamängder och välj ”Redigera”. Observera att dokumentet måste vara av dokumenttypen "Dokumentation" i Catalog för att synas.

![Fältet Uppfyller för en datamängd](img/catalog_10_dokument_redigerauppfyller.png)

![Fältet Dokumention för en datamängd](img/catalog_10_dokument_redigeradokumentation.png)

Du kan använda sökrutan och datamängdens innehållsförteckningen i högerkanten för att snabbare hitta till inmatningsfälten ”Uppfyller” och ”Dokumentation”.

Du kan också hänvisa till ett dokument ifrån en **distribution**. Det gör du under redigeringsvyn för en distribution, med frivilliga fält påslaget. Observera att dokumentet måste vara av dokumenttypen "Dokumentation" i Catalog för att synas.

![Redigera en distribution](img/catalog_10_dokument_redigeradistribution.png)

## Se detaljerad information

Om du vill se mer detaljerad information om ett dokument kan du klicka på informationsikonen. 
![Informationsikonen](img/catalog_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata samt alla entiteter som länkar till och från ditt dokument. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).


## Redigera, ersätt eller ta bort dokument

Du kan redigera informationen om ditt dokument, ersätta det eller ta bort det med knapparna **"Redigera"**, **"Ersätta"** och **"Ta bort"** på översiktssidan.

![Knapparna Redigera, Ersätta och Ta bort](img/catalog_10_dokument_redigeratabort.png)

## Ladda ner

Du kan ladda ner ett dokument genom att klicka på knappen **"Ladda ner"**.


![Knappen Ladda ner](img/catalog_10_dokument_laddaner.png)