## Publicera kataloger och datamängder
Alla nyskapade kataloger är opublicerade som standard och därför kan endast användare som tilldelats rättigheter se och hantera katalogen. Om katalogens publiceringsstatus är grön betyder det att katalogens metadata är publikt läsbar och redo att skördas av t.ex. öppna data-portaler. Med **publiceringsknappen** kan du **publicera** respektive **avpublicera** din katalog.

![Publiceringsknappen för en katalog](img/catalog_5_publicering_publicerakatalog.png)

För att något ska synas så måste det dock finnas en **datamängd** i katalogen vars **status** antingen är **publik** (syns i listorna) eller att **extern dataskördning** är aktiverad (till andra dataportaler). Det ställer du in på nästa nivå, d.v.s. nere på datamängden. Klicka på **publiceringsknappen** för datamängden för att göra den publikt listad.

![Publiceringsknappen för en datamängd](img/catalog_5_publicering_publiceradatamangd.png)




## Versionshistorik
Vissa versioner av EntryScape har automatisk **versionshistorik** av kataloger, datamängder och distributioner. Versionshistoriken hittas under trepunktsmenyn för kataloger och distributioner.

![Menyalternativ Versioner för en katalog](img/catalog_5_publicering_versionerkatalog.png)

För datamängder finns det en blå knapp för nå versionshistoriken.

![Versionsknapp för datamängder](img/catalog_5_publicering_versionerdatamangd.png)

Om du är gruppansvarig eller admin, så kan du återställa tidigare versioner av metadata för entiteten du väljer.

Om till exempel någon har lagt till eller tagit bort en datamängd i en katalog så kan du se det i versionshistoriken, med datum, tidpunkt och vilken användare som gjorde förändringen. Vanliga medlemmar har behörighet att se olika versioner, men inte återställa tidigare versioner.

![Versionshistorik](img/catalog_5_publicering_versionshistorik.png)


## Inbäddning
När du har en eller flera publicerade datamängder i en katalog så kan du om du vill **bädda in den på en hemsida**, till exempel i ett blogginlägg, pressmeddelande eller annan webbsida. En lista över publicerade datamängder kan visas på externa webbsidor genom att bädda in en JavaScript-snutt som pekar på en specifik katalog. 

Gå tillbaka till vyn över Kataloger. Klicka på trepunktsmenyn för din valda katalog och välj ”Inbäddning”.

![Menyalternativ Inbäddning](img/catalog_5_publicering_trepunktsmeny_inbaddning.png)


Då får du upp ett fönster med en kodsnutt och möjlighet att välja dels temaformat och dels om länkarna till datamängderna i din katalog ska öppnas i ett nytt fönster. Temaformatet bestämmer vilka ikoner som dina datamängder kommer få som symbol. 

![Dialogfönster med kod och temaformat](img/catalog_5_publicering_inbaddning1.png)

Om du klickar på knappen ”Förhandsvisa” så kan du se hur den inbäddade katalogvisningen kommer att se ut på din hemsida och jämföra vilken uppsättning med ikoner som ser bäst ut för dina datamängder. JavaScript-koden innehåller i övrigt enbart minimal styling, vilket betyder att tabell och fonter ska ärva sitt visuella utseende från den externt omgivande webbsidan.

![Förhandsvisning av valda ikoner](img/catalog_5_publicering_inbaddning2.png)

Klicka på Stäng. **Markera** och **kopiera sedan javascriptkoden** i rutan som du anpassat efter dina val och **klistra in** i redigeraren för din hemsida.

![Dialogfönster med javascriptkod för kopiering](img/catalog_5_publicering_inbaddning3.png)

Du kan också bädda in en katalogvisningen på intranätet, vilket kan vara praktiskt för data som behöver delas men inte bör publiceras som öppna data. Följ bara samma steg som ovan.


!!! info "Det finns mer avancerade sätt att bygga webbsidor med data från EntryScape genom att använda EntryScape Blocks. Ta kontakt med EntryScape support för mer information om Blocks eller läs mer på [entryscape.com](http://entryscape.com)"