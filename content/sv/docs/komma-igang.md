# Komma igång med EntryScape Catalog

Du behöver ett användarkonto för att kunna använda EntryScape. Beroende på konfigurationen av din EntryScape-instans kan du själv skapa en användare eller så behöver en administratör skapa ett konto åt dig. Om din organisation använder Single Sign-On kan du logga in med dina vanliga uppgifter.

## Logga in

Om du försöker nå EntryScape utan att vara inloggad så omdirigeras du automatiskt till inloggningsdialogen där du måste ange dina uppgifter, se skärmdump:

![Skärmdump av login dialogen](img/login.png)

Efter att du har loggat in dig omdirigeras du till startvyn.

## Logga in med SSO

Om du ska logga in på EntryScape och din organisation har konfigurerat inloggning för Single Sign On klickar du på "Använd Single Sign-On (SSO).

### Video: Logga in med SSO eller användarnamn och lösenord

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/NEIbgEy04Fo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Undantagsfall med vitlistning vid SSO

Om din organisation har aktiverat SSO går det att göra undantag. Detta görs genom att vitlista (så kallad whitelisting) specifika e-postadresser. Detta behövs ifall din organisation ska ha gäster med andra domäner t.ex. konsulter i er instans av EntryScape.

Se till att ha god framförhållning, förslagsvis 1 eller 2 veckor i förväg, i kontakt med din administratör av EntryScape för att vitlista e-postadresser och ordna nya konton. Detta är viktigt så att det kan testas innan ett evenemang, t.ex. ett internt hackathon, då gäster ska använda din organisations EntryScape t.ex. publicera datamängder. Observera att extern användning av API vid ett hackathon eller övrig tid behövs inga API-nycklar eller liknande för den som ska konsumera öppna data via EntryScape API. 

# Registrering och inloggning

Första gången du loggar in så behöver du gå igenom ett par steg som följer här (se adress nedanför).

## Skapa konto

Om du inte redan har en användare kan du skapa en ny genom att klicka på länken "Skapa konto" längst ner i inloggningsdialogen. Detta leder dig till registreringsdialogen:

![Skärmdump av kontoskapningsdialogen](img/signup.png)

Efter att du har fyllt i all nödvändig information behöver du bekräfta din e-postadress genom att besöka den webbadress som har skickats till dig via e-post.

Du behöver ett användarkonto för att kunna komma in i plattformen EntryScape. Detta går att registrera själv på den URL som finns för din instans av EntryScape.

Kontot består av din e-postadress och ett lösenord som du väljer. Kontot är personligt. När du loggat in finns flera val och beroende på behörighet visas olika funktioner. Det kan vara till exempel Katalog, Termer, Sök, Import, Workbench och Admin. De flesta användare går vidare till Katalog.

## Växla mellan moduler och vyer

I startvyn kan du välja mellan olika EntryScape-moduler som är aktiverade för din organisation och användare:

![Skärmdump av start-vyn](img/start_view.png)

Det allmänna sättet att arbeta med EntryScape bygger på modulspecifika vyer som blir synliga efter att du har valt en modul samt ett hanterbart objekt (t.ex. en datakatalog, en terminologi eller ett generiskt projekt).

## Återställning av lösenord

Om du har glömt ditt lösenord eller om administratören har skapat en användare för dig utan lösenord måste du genomföra en lösenordsåterställning. Du kan öppna dialogrutan för återställning av lösenord genom att klicka på länken "Glömt lösenordet?" på inloggningsskärmen.

Du ombeds att ange ett nytt lösenord direkt, men det måste bekräftas genom att besöka den webbadress som skickas till e-postadressen som är kopplad till din användare.

Det du behöver göra är att återställa lösenord och sen använda det lösenord du väljer för att kunna logga in. Det gör du genom att:

* Gå till din instans av EntryScape. I det här exemplet är det [https://free.entryscape.com/signin/](https://free.entryscape.com/signin/).
* Klicka på "Glömt lösenordet?" ("Forgot your password?")
* Skriv in e-postadressen du registrerat med
* Skriv in nytt säkert lösenord
* Bekräfta nytt lösenord genom att skriva in det en gång till.
* Verifiera genom reCaptcha-utmaningen om ni ser den.
* Klicka "Återställa lösenord" ("Reset password")
* Vänta på e-postmeddelandet som kommer till din e-post
* Öppna e-postmeddelandet med ämnet "Bekräfta ändring av ditt lösenord"
* Klicka "Återställ ditt lösenord".
* Nu kan du logga in!

## Inställningar

Uppe i högra hörnet hittar du en ikon för en person. Klicka på denna för att komma till dina användarinställningar.

### Användarnamn

Ditt användarnamn är din e-post. Kontakta administratören av din instans för att ändra användarnamn.

### Förnamn

Här kan du ställa in ditt förnamn.

### Efternamn

Här kan du ställa in ditt efternamn.

### Språkinställningar

Du kan ställa in språk för ditt konto till svenska, engelska eller tyska. Du ändrar språkinställningen för ditt konto till det du önskar antingen under inloggningsskärmen uppe till höger eller under Inställningar när du är inloggad.

### Ändra lösenord

Klicka på Ändra lösenord för att få upp en ny ruta där du skriver in ditt nya lösenord under 'Nytt lösenord'. Bekräfta sedan detta under "Bekräfta lösenord". Klicka på ikonen med ögat om du vill dubbelkolla det nya lösenordet du anger. Klicka sedan på "Spara nytt lösenord" för att aktivera ändringen eller "Avbryt" för att behålla ditt nuvarande lösenord.
