# Användare

## Skapa användare, grupper eller projekt

Skapa-knappen <input type="button" value="+ Skapa"> högst upp till höger visar en dialog som efterfrågar information om objektet som ska skapas.

Alla andra funktioner är tillgängliga via listan och är specifika för objektstyperna. En knapp med kugghjul <input type="button" value="&#9881;"> visas när du sveper över ett listobjekt.

## Lägg till användare

![Skärmdump Överblick klicka på Admin](img/admin_users_create_user_click.png)

### Användarhantering för gruppansvariga
Du som är gruppansvarig för en katalog lägger till användare och ger dem rättigheter för katalogen. Du kan tilldela två roller: medlem eller gruppansvarig. Till gruppansvar hör två rättigheter, dels att kunna publicera en datamängd dels att lägga till andra användare. Se instruktioner för att hantera användare i katalog under sidan för [EntryScape Catalog](../catalog/index.md).

## Redigera

Klicka på objektet i listan för att ändrar användarens metadata (standardaktivitet)

![Skärmdump Klicka på användare](img/admin_users_edit_user_info_click.png)

Du kan även använda alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka sedan på redigera.

![Skärmdump Klicka på Redigera användare](img/admin_menu_users_edit_click.png)

Skriv in ytterligare metadata.

![Skärmdump Redigera användare](img/admin_users_edit_user_info.png)

Klicka sedan på "Spara ändringar".

![Skärmdump Redigera användare](img/admin_users_save_user_info.png)

Detta tar dig tillbaka till listan över användare och metadata är uppdaterad.

## Versioner

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka sedan på "Versioner".

![Skärmdump Klicka på Versioner](img/admin_menu_users_versions_click.png)

Klicka på pil intill version för att expandera information om version.

![Skärmdump Klicka för att Expandera Version](img/admin_users_versions_expand.png)

Klicka på "Återställ" för att återställa version.

![Skärmdump Klicka på Återställ för att Återställa Version](img/admin_users_versions_reset.png)

Klicka på "Återställ" för att bekräfta återställning av version.

![Skärmdump Klicka på Återställ för att bekräfta återställning](img/admin_users_versions_confirm_reset.png)

Detta tar dig tillbaka till listan över användare och ser att informationen om användaren återställts till vald version.

### Ändra användarnamn

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka sedan på "Ändra användarnamn".

![Skärmdump Klicka på Ändra användarnamn](img/admin_menu_users_change_usernames_click.png)

Skriv in nytt användarnamn.

![Skärmdump Skriv in nytt användarnamn](img/admin_users_change_usernames_type.png)

Klicka på spara nytt användarnamn.

![Skärmdump Spara nytt användarnamn](img/admin_users_change_usernames_save.png)

Se nya användarnamnet i listan.

![Skärmdump Bekräfta nytt användarnamn](img/admin_users_change_usernames_confirmed.png)

### Ändra lösenord

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka sedan på "Ändra lösenord".

![Skärmdump Ändra lösenord](img/admin_menu_users_change_password_click.png)

Skriv in nytt lösenord och bekräfta det på raden under.

![Skärmdump Ändra lösenord](img/admin_users_change_password_type_confirm.png)

Klicka på Ögon-ikonerna om du tillfälligt vill se och bekräfta vad du skrivit in.

![Skärmdump Ändra lösenord](img/admin_users_change_password_show.png)

Klicka sedan på "Spara".

![Skärmdump Ändra lösenord](img/admin_users_change_password_save.png)

## Gruppmedlemskap

Ändrar användarens medlemskap i grupper.

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka på "Gruppmedlemskap".

![Skärmdump Klicka på Gruppmedlemskap](img/admin_menu_users_groupmemberships_click.png)

Här inne kan du hantera vilka grupper användaren är medlem i.

!!! info "Observera att du det behöver finnas olika grupper som användaren kan läggas in i eller tas bort ifrån."

### Lägg till användare i grupp

Klicka sedan på "Lägg till användare i gruppen".

![Skärmdump Klicka på Lägg till användare i gruppenp](img/admin_users_groupmemberships_add_to_group.png)

Välj sedan i listan de grupper du vill lägga till användaren i. Grupper kan vara t.ex. kataloger, projekt eller terminologier.

Du kan även söka fram specifika grupper med hjälp av titeln på deras beskrivning. Klicka sedan på välj.

![Skärmdump Sök och klicka Välj](img/admin_users_groupmemberships_search_select.png)

Sedan ser du gruppen som användaren är tillagd i.

![Skärmdump Användare tillagd i grupp](img/admin_users_groupmemberships_added_group.png)

### Detaljerad information

För att se detaljerad information om en användare klicka på trepunktsmenyn.

![Skärmdump Trepunktsmeny för Detaljerad information](img/admin_users_groupmemberships_menu.png)

Klicka sedan på "Detaljerad information".

![Skärmdump Klicka Detaljerad information i trepunktsmeny](img/admin_users_groupmemberships_detailed_info_click.png)

Sedan kan du expandera mer information.

![Skärmdump Expandera Detaljerad information](img/admin_users_groupmemberships_detailed_info_expand.png)

Du kan även ladda ned metadata i olika format. Klicka på valfritt format för att ladda ned.

![Skärmdump Ladda ned metadata från Detaljerad information](img/admin_users_groupmemberships_detailed_info_download.png)

Klicka sedan på "Stäng" för att stänga rutan.

![Skärmdump Stäng ruta för Detaljerad information](img/admin_users_groupmemberships_detailed_info_close.png)

### Ta bort

Du kan ta bort användare från en grupp. Klicka på trepunktsmenyn.

![Skärmdump Klicka trepunktsmeny](img/admin_users_groupmemberships_menu.png)

Klicka sedan på "Ta bort".

![Skärmdump Ta bort användaren från grupp](img/admin_users_groupmemberships_remove_click.png)

Bekräfta sedan genom att klicka på "Ta bort". Eller klicka på "Avbryt" om du vill behålla användaren i gruppen.

![Skärmdump Bekräfta borttagning](img/admin_users_groupmemberships_remove_confirm.png)

Efter det så syns inte gruppen i listan.

![Skärmdump Grupper för användaren](img/admin_users_groupmemberships_remove_remaining.png)

## Inaktivera användaren

Inaktiverar användaren. En inaktiverad användare kan inte logga in, men användaren och användarens data finns kvar i systemet. Detta är till exempel användbart att göra när någon slutar arbeta i organisationen, innan kontot bör raderas.

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka på "Inaktivera användaren".

![Skärmdump Klicka på Inaktivera användare](img/admin_menu_users_inactivate_user_click.png)

Bekräfta sedan att du vill inaktivera användaren.

![Skärmdump Bekräfta på Inaktivering av användare](img/admin_users_inactivate_user_confirm.png)

Du ser sedan i användarlistan att användaren är inaktiverad.

![Skärmdump Se att användare är inaktiverad](img/admin_users_inactivate_user_deactivated.png)

### Återaktivera användare

Vill du sedan återaktivera en användare återupprepar du processen för en inaktiverad användare.

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka på "Aktivera användare".

![Skärmdump Bekräfta på Inaktivering av användare](img/admin_menu_users_activate_user_click.png)

Om du är säker på att du vill återaktivera användaren så klicka på "Aktivera".

![Skärmdump Bekräfta på Inaktivering av användare](img/admin_activate_user_activate.png)

Sen är användaren aktiverad.

## Ta bort

!!! info "Observera att användaren som ska tas bort inte får vara kopplad till datamängder som t.ex. kontaktperson eller vara Gruppansvarig i katalog(er)."

Klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_menu_users_click.png)

Klicka på ta bort. 

![Skärmdump Ta bort användare](img/admin_menu_users_remove_user_click.png)

Bekräfta sedan att ta bort användaren genom att klicka på "Ta bort".

![Skärmdump Bekräfta att Ta bort användare](img/admin_users_remove_user_confirm.png)

Detta tar bort användaren, men användarens data (e.g. projekt) finns kvar i systemet.
