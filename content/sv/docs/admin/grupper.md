# Grupper

Börja med att klicka på Admin.

![Skärmdump Klicka på Admin](img/admin_index_click_admin.png)

Klicka sedan på "Grupper" för att gå dit.

![Skärmdump Klicka på Grupper](img/admin_groups_click_groups.png)

## Funktioner som är specifika för grupper

Börja med att klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_groups_click_menu.png)

### Redigera

Du kan redigera metadata om en grupp. Skriv in minst tre tecken i gruppens titel för att söka fram den. Denna funktion ändrar gruppens metadata, en standardaktivitet när man klickar på objektet i listan. 

Efter du klickat så klickar du på Redigera.

![Skärmdump Klicka på Redigera](img/admin_groups_click_menu_edit.png)

Du kan även klicka på gruppen i listan du vill redigera.

![Skärmdump Klicka på Redigera](img/admin_groups_click_edit.png)

### Versioner

Ändrar åtkomsträttigheter för gruppen. Klicka på "Versioner" i alternativmenyn.

![Skärmdump Klicka på Versioner](img/admin_groups_versions_menu.png)

Klicka på pil intill version för att expandera information om version.

![Skärmdump Klicka för att Expandera Version](img/admin_users_versions_expand.png)

Klicka på "Återställ" för att återställa version.

![Skärmdump Klicka på Återställ för att Återställa Version](img/admin_users_versions_reset.png)

Klicka på "Återställ" för att bekräfta återställning av version.

![Skärmdump Klicka på Återställ för att bekräfta återställning](img/admin_users_versions_confirm_reset.png)

Detta tar dig tillbaka till listan över grupper och ser att informationen om gruppen återställts till vald version.

### Medlemmar

Funktionen tillåter dig ändra gruppens medlemmar. I alternativmenyn klicka på "Medlemmar".

![Skärmdump I alternativmeny klicka Medlemmar](img/admin_groups_click_members.png)

#### Lägg till medlem

Klicka sedan på "Lägg till användare i gruppen".

![Skärmdump Lägg till användare](img/admin_groups_members_add_member.png)

Välj sedan medlem att lägga in genom att söka fram användaren genom namn och sen klicka "Välj".

![Skärmdump Välj användare att lägga till](img/admin_groups_members_select_user.png)

Sedan är användaren tillagd och syns i listan.

![Skärmdump Se användare tillagd](img/admin_groups_members_user_added.png)

#### Detaljerad information

Klicka på "Medlemmar" i alternativmenyn.

![Skärmdump Klicka på Medlemmar i alternativmeny](img/admin_groups_click_members.png)

Klicka sedan på alternativmenyn till den användare du vill se information om.

![Skärmdump Klicka på användares alternativmeny](img/admin_groups_members_menu.png)

Klicka på "Detaljerad information".

![Skärmdump Klicka på Detaljerad information](img/admin_groups_members_detailed_info.png)

Expandera sedan information för olika format om du vill ladda ned metadata.

![Skärmdump Klicka på pil för att expandera detaljerad information](img/admin_groups_members_expand.png)

Välj sedan format för nedladdning.

![Skärmdump Klicka på format för att välja metadataformat att ladda ned](img/admin_groups_members_choose_format.png)

Sedan anväder du webbläsaren för att spara filen antingen med högerklick på format och "Spara som" eller genom dialogruta.

![Skärmdump Klicka på Ladda ned metadata](img/admin_groups_members_download_metadata.png)

#### Ta bort

Klicka på alternativmenyn till den användare du vill ta bort från gruppen.

![Skärmdump Klicka på Ta bort i alternativmeny](img/admin_groups_members_menu.png)

Klicka sedan på "Ta bort".

![Skärmdump Klicka på Ta bort för att ta bort medlem](img/admin_groups_members_remove.png)

Bekräfta sedan genom att klicka på "Ta bort" eller ångra genom att klicka "Avbryt".

![Skärmdump Klicka på Ta Bort och bekräfta borttagning](img/admin_groups_members_confirm_removal.png)

### Ta bort

Tar bort gruppen, men gruppens data (e.g. projekt) finns kvar i systemet.

Efter du klickat på alternativmenyn klickar du på "Ta bort".

![Skärmdump Klicka Ta bort i alternativmeny](img/admin_groups_remove_menu.png)

Klicka sedan på "Ta bort" för att bekräfta eller "Avbryt" om du ångrar dig.

![Skärmdump Klicka för att bekräfta ta bort](img/admin_groups_remove_confirm.png)

Därefter syns inte gruppen längre i listan.