# Projekt

I Admin-vy kan du redigera administrativa inställningar för alla projekt i din instans av EntryScape.

Börja med att klicka på "Projekt" under "Grupper".

![Skärmdump Klicka på Projekt](img/admin_projects_menu_click.png)

Sök sedan fram det projekt du vill ändra med minst tre tecken i sökfältet.

![Skärmdump Sök efter projekt](img/admin_projects_search_project.png)

## Redigera

Som administratör kan du redigera varje projekt. Klicka på ett projekt för att ändra projektets metadata. Detta är en standardaktivitet när man klickar på ett objekt i en listan.

![Skärmdump Klicka på Projekt att redigera](img/admin_projects_edit_project.png)

Du kan även klicka på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_projects_menu_alternatives_click.png)

Sedan klicka "Redigera".

![Skärmdump Klicka på Redigera](img/admin_projects_menu_edit.png)

Redigera sedan metadata för det projekt du vill ändra på. Lägg till exempel till beskrivning på ett frivilligt fält.

![Skärmdump Beskriv ändringar](img/admin_projects_edit_project_describe.png)

Spara sedan det uppdaterade projektet genom att klicka på "Spara ändringar".

![Skärmdump Klicka på Spara ändringar](img/admin_projects_edit_project_save.png)

Sedan ser du en uppdaterad tidsstämpel för projektet i projektlistan.

![Skärmdump Se uppdaterat projekt i lista](img/admin_projects_edit_updated_project.png)

## Versioner

Du kan se versioner och återställa mellan gamla och nya versioner.

Klicka först på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_projects_menu_alternatives_click.png)

Klicka sedan på "Versioner".

![Skärmdump Klicka på Versioner](img/admin_projects_versions_menu.png)

Klicka sedan på pilen till en version du vill se mer information om och återställa.

![Skärmdump Klicka för att expandera](img/admin_projects_versions_expand_version.png)

Klicka sedan på återställ.

![Skärmdump Klicka på Återställ](img/admin_projects_versions_reset_selected.png)

Bekräfta sedan genom att klicka på Återställ.

![Skärmdump Klicka på att bekräfta Återställa](img/admin_projects_versions_confirm_reset.png)

Sedan har ditt projekt uppdaterats till vald version och syns i listan.

![Skärmdump Uppdaterat projekt i lista](img/admin_projects_versions_version_reset_confirmed.png)

## Omindexera

Du kan omindexera ett projekt i instansen. Klicka först på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_projects_menu_alternatives_click.png)

Du kan även omindexera projektet.

![Skärmdump Klicka på Omindexera i alternativmeny](img/admin_projects_menu_index.png)

Klicka sedan på Omindexera eller avbryt.

![Skärmdump Uppdaterat projekt i lista](img/admin_projects_reindex_project.png)

## Ta bort

Klicka först på alternativmenyn.

![Skärmdump Klicka på Alternativmenyn](img/admin_projects_menu_alternatives_click.png)

Klicka på "Ta bort".

![Skärmdump Klicka på Ta bort](img/admin_projects_menu_remove_project.png)

Bekräfta att ta bort projektet inklusive data eller klicka på avbryt.

![Skärmdump Bekräfta att Ta bort](img/admin_projects_remove_project.png)

Sen ser du uppdaterade projekt i listan där projektet har tagits bort ifråga.