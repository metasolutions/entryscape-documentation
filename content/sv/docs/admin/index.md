# Admin-modulen

Admin-modulen hanterar användare, grupper och projekt. Dessa objekt kan hittas i separata flikar som innehåller ett sökfält och en lista med matchande objekt.

## Förstå åtkomstkontroll

### Objekttyper
EntryScape hanterar två objekttyper som är relevanta för åtkomstkontrollen: användare och grupper. Grupper kan tilldelas åtkomsträttigheter på samma sätt som användare. Grupper kan innehålla användare men inga andra grupper (det kan komma att ändras i framtiden när funktionalitet för grupper vidareutvecklas).

!!! info "Varje objekt som hanteras i EntryScape består av administrativ information (ett s.k. "entry") och en eller flera metadata-grafer. Ett entry länkar samman metadata och en resurs som kan vara en uppladdad fil, en URI som pekar på en extern plats, eller en identifierare för ett abstrakt ting. EntryScape kan hantera åtkomsträttigheter för var och en av de nämnda delarna, men exponerar den avancerade funktionaliteten endast i Admin-modulen. Modulerna Catalog, Terms och Workbench tillämpar ett mer användarvänligt tillvägagångssätt och skiljer endast mellan "medlem" och "gruppansvarig". Medlemmar får visa, redigera och skapa objekt, medan gruppansvariga har full kontroll över en datakatalog, en terminologi eller ett projekt."

Den avancerade funktionaliteten som exponeras av Admin-modulen behövs endast för avancerade användningsfall, därför rekommenderas det att använda modulernas delningsinställningar istället.

!!! info "Enbart medlemmar av `admin`-gruppen kan se och använda Admin-modulen"

Annan funktionalitet som skapning av användare, tilldelning av gruppmedlemskap, återställning av lösenord, m.m., måste utföras med hjälp av Admin-modulen, eftersom sådan funktion inte ingår i de övriga moduler.

## Börja med Admin 

När du står i översikten för EntryScape klickar du på "Admin" för att komma till admin-modulen.

![Skärmdump Överblick klicka på Admin](img/admin_index_click_admin.png)

## Användare

Detta tar dig in till sidan för att hantera användare. Läs mer på sidan [användare](anvandare.md) för dokumentation om vad du kan göra med användare.

## Grupper

Du kan även hantera Grupper. Se [grupper](grupper.md) för instruktioner.

## Projekt

I Admin-modulen kan du hantera Projekt. Se [projekt](projekt.md) för dokumentation.
