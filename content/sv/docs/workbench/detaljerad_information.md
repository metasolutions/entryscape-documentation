

Om du klickar på **informationsikonen**, så får du fram ett dialogfönster som visar mer **detaljerad metadata** om t ex ett dokument, evenemang, konstverk eller annan entitet. 

![Informationsikonen](img/workbench_detaljeradinformation_ikon1.png)

Du hittar både ikonen på i listan med entiteter samt på översiktssidan för entiteten.

![Informationsikonen](img/workbench_detaljeradinformation_ikon2.png)

Det fönster som öppnas har tre flikar: **"Information"**, **"Om"** och **"Referenser"**. 

![Informationsdialogfönstret, tre flikar](img/workbench_detaljeradinformation_flikar.png)

## Information
<a id=“information”></a>

I den första fliken, **"Information"**, kan du se metadata för den entitet som du har klickat på, samt ev länkar till alla entiteter som **din entitet refererar till**.

![Informationsdialogfönstret, fliken Information](img/workbench_detaljeradinformation_information.png)

## Om
<a id="about"></a>

I den andra fliken, **"Om"**, ser du bland annat **URI:n** till din entitet. Det är ett snabbt sätt att få fram och kunna **kopiera URI:n** när du behöver referera till den i andra sammanhang.

![Informationsdialogfönstret, fliken Om](img/workbench_detaljeradinformation_om.png)

## Referenser
<a id="references"></a>

I den tredje fliken, **"Referenser"**, ser du alla entiteter som **refererar till din entitet**. I det här fallet är det ett utflyktsmål som använder sig av bilden.

![Informationsdialogfönstret, fliken Referenser](img/workbench_detaljeradinformation_referenser.png)


## Språkval
<a id="languages"></a>

Uppe till höger finns **två språkikoner** för att se informationen i flikarna på **olika språk**.

![Informationsdialogfönstret, språkikonerna](img/workbench_detaljeradinformation_sprakikoner.png)

Om du klickar på **jordgloben** så ser du all metadata om din entitet på **alla inmatade språk samtidigt**. För att gå tillbaka till ursprungsläget klickar du på jordgloben en gång till.


Du kan också se metadatan på **ett språk i taget** genom att klicka på **den högra språkikonen** och välja språk från en lång lista. 

![Informationsdialogfönstret, lista med språk](img/workbench_detaljeradinformation_spraklista.png)



