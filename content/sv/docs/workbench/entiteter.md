# Entiteter

Ett projekt innehåller **entiteter** (objekt) av olika **entitetstyper**, t ex dokument, evenemang, konstverk, foton, upphovsmän etc. De listas i översikten för projektet, ihop med antalet av varje typ. Vilka entitetstyper som finns bestäms av den valda projekttypen och är ofta förutbestämda, men kan i vissa fall [konfigureras](projekt.md#konfigurera-entitetstyper) av användaren. 

Exemplet nedan visar ett turism-projekt med förutbestämda entitetstyper i form av aktiviteter, boende, matställen, foton, vandringsleder mm.


![Ett Turismprojekt med tillhörande enteteter](img/workbench_entiteter_oversikt.png)


## Skapa ny
<a id="create-new"></a>

För att skapa en ny entitet (objekt), klicka på entitetstypen och sedan på knappen **"Skapa"**. Exemplet nedan visar entitetstypen "Boende" i projektet "Turism", men hade lika gärna kunnat vara broschyrer, aktiviteter, konstnärer eller något annat. Här får du fylla i metadata och skapa relationer till andra entiteter.

![Skapa ny entitet](img/workbench_entiteter_skapanyentitet.png)

De informationsfält som visas här beror också på den valda konfigurationen (projekttypen med dess tillhörande entitetstyper). 

## Beskriv
<a id="describe"></a>

Tänk på att ju mer specifik beskrivning du gör av de projekt och entiteter som du publicerar, desto lättare blir den för andra att hitta och återanvända metadatat. 

Inmatningsfönstret som visas innehåller inmatningsfält på tre olika nivåer: **obligatorisk information** (\* måste fyllas i), **rekommenderad information** (bra om det fylls i) och **frivillig information**. Du kan slå på och stänga av vilka inmatningsfält som ska visas med hjälp av reglagen högst upp. 

![Skapa ny aktivitet](img/workbench_entiteter_obl_rek_fri.png)


## Redigera
<a id="edit"></a>

Du kan gå in och **ändra** eller **fylla mer information** om en entitet senare, t ex lägga till information på ett nytt språk eller ladda upp nya bilder.
Klicka då antingen på redigeringspennan ute till höger, eller klicka dig in på översiktssidan för entiteten och klicka på knappen **"Redigera"**.

![Redigeringspennan för en entitet](img/workbench_entiteter_redigeraentitet.png)

![Redigeraknappen på översiktssidan för en entitet](img/workbench_entiteter_redigeraknapp.png)


## Hantera filer och länkar
<a id="manage-files-and-links"></a>

För entiteter som har tillhörande **filer**, som t ex bilder eller dokument, så beskrivs här hur du laddar upp, laddar ner samt ersätter filer i EntryScape Workbench. Det går också att **länka** till filer, webbsidor och annat som finns tillgängliga på webben.

### Ladda upp fil
<a id="upload-file"></a>

För att **ladda upp en fil**, klicka på **Fil** och sedan ikonen med förstoringsglaset.

![Ladda upp fil](img/workbench_entiteter_laddauppfil.png)



### Ladda ner fil
<a id="download-file"></a>

Du kan också **ladda ner filer** som ligger upplagda genom att gå in på entitetens översiktssida och klicka på knappen **"Ladda ner"**.

![Ladda ner fil](img/workbench_entiteter_laddanerfil.png)

### Ersätt fil
<a id="replace-file"></a>
För entiteter som består av **filer** som du laddar upp (t ex bildfiler eller dokument), så finns det en knapp **"Ersätt fil"** som gör att du snabbt och lätt kan byta ut filen som hör till filens metadatabeskrivning på ett smidigt sätt. 

![Knappen Ersätt fil](img/workbench_entiteter_ersattfilknapp.png)

Leta upp den nya filen och klicka på **"Ersätt fil"**.


![Ersätt fil](img/workbench_entiteter_ersattfil.png)

### Länka till resurs
<a id="link-to-resource"></a>

För att **länka till** en fil, webbsida, bild eller annat som finns åtkomligt via webben, skriv eller **klistra in webbadressen** och lägg till ett beskrivande namn.

![Redigeraknappen på översiktssidan för en entitet](img/workbench_entiteter_lankatillfil.png)


## Sök
<a id="search"></a>

Om du har väldigt många entiteter, t ex en stor samling av bilder eller dokument och letar efter ett specifikt dokument, kan du använda **sökrutan** som ligger överst. **Skriv in minst 3 tecken** för att få en matchning.

![Sökrutan i Workbench](img/workbench_entiteter_sok.png)

## Tabellvy och listvy
<a id="table-view-and-list-view"></a>

Defaultvyn i EntryScape Workbench är att visa data i en **listvy**. Om du däremot vill se och redigera många entiteter samtidigt på samma sida så kan du använda dig av **tabellvyn**. Du kan byta till tabellvyn genom att klicka på **tabell-ikonen** högst upp.

![Tabellikonen för att byta till tabellvy](img/workbench_entiteter_tabellvy1.png)

I tabellvyn kan du också bestämma hur många kolumner du vill se samtidigt. Genom att klicka på **”Kolumner”** så kan du välja vilka kolumner du vill se resp. dölja.

![Kolumnväljare](img/workbench_entiteter_kolumner.png)

För att redigera en tabellcell så klickar du på texten i cellen. Klicka på det **övre krysset** eller utanför redigeringsrutan för att **stänga** den. Efter att du har gjort dina ändringar i alla tabellceller, så glöm inte att klicka på **Spara-knappen** längst ner.

![Tabellvy med redigeringsmöjligheter](img/workbench_entiteter_tabellvy2.png)

För att byta tillbaka till listvy, klicka på list-ikonen högst uppe till höger.

![Listvy-ikonen](img/workbench_entiteter_listvy.png)

## Se detaljerad information
<a id="see-detailed-information"></a>

Om du vill se mer detaljerad information om ett projekt eller en entitet kan du klicka på informationsikonen. 

![Informationsikonen](img/workbench_informationsikon.png)

Då får du upp ett dialogfönster där du kan se metadata för entiteten samt alla andra entiteter som länkar till och från din entitet. Läs mer om det på sidan [detaljerad information](detaljerad_information.md).

## Ta bort
<a id=“remove”></a>

För att **ta bort** en entitet, gå in på entitetens översiktssida och klicka på knappen **”Ta bort”**.

![Menyalternativ Ta bort entitet](img/workbench_entiteter_tabortentitet.png)

Observera att om du försöker ta bort en entitet som är **länkad till** från ett annat ställe, t ex en huvudbild som används för att representera ett hotell, så kommer du få ett felmeddelande om att du först måste ersätta huvudbilden med en annan bild på hotellsidan innan du kan ta bort den gamla bilden.

![Felmeddelande om borttagning av länkad entitet](img/workbench_entiteter_tabortlankadfil.png)
