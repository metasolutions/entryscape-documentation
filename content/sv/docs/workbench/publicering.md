## Publicera projekt och entiteter
<a id=“publishing-projects-and-entities”></a>

Alla nyskapade projekt är opublicerade som standard och därför kan endast användare som tilldelats rättigheter se och hantera projektet. Om projektets publiceringsstatus är grön betyder det att projeketets metadata är publikt läsbar och redo att skördas av t.ex. öppna data-portaler. Med **publiceringsknappen** kan du **publicera** respektive **avpublicera** ditt projekt.

![Publiceringsknappen för ett projekt](img/workbench_publicering_publiceraprojekt.png) 

Om ert projekt och era entitetstyper är konfigurerade så att även **entiteter** kan publiceras och avpubliceras, kan du ställa in det på översiktssidan för en entitet. Klicka på **publiceringsknappen** för entiteten för att göra den publikt listad. Observera att **projektet** som entiteten ligger under **måste vara publicerat**, för att även entiteten ska kunna publiceras. 

![Publiceringsknappen för en entitet](img/workbench_publicering_publiceraentitet.png) 

De entiteter som har en **grön publiceringsikon** (jordglob) är publika, som i exemplet här under.

![Publiceringssymbol för en entitet](img/workbench_publicering_publiceradentitet.png) 

Om du inte ser någon publiceringsikon (eller publiceringsknapp) för dina entiteter, så betyder det att de **publiceras/avpubliceras automatiskt tillsammans med projektet** när det publiceras/avpubliceras.


<!-- **extern dataskördning** är aktiverad (till andra dataportaler). Det ställer du in på nästa nivå, d.v.s. inne på entiteten.  -->

## Versionshistorik
<a id=“revision-history”></a>

Vissa versioner av EntryScape har automatisk **versionshistorik** av projekt och entiteter. Versionshistoriken hittas under trepunktsmenyn för projekt och på översiktssidan för entiteter.

![Menyalternativ Versioner för ett projekt](img/workbench_publicering_versionshistorikprojekt.png)

För entiteter finns det en blå knapp för nå versionshistoriken.

![Versionsknapp för entiteter](img/workbench_publicering_versionshistorikentiteter.png)

Om du är gruppansvarig eller admin, så kan du **återställa** tidigare versioner av metadata för entiteten du väljer. Vanliga medlemmar har behörighet att se olika versioner, men inte återställa tidigare versioner.

![Versionshistorik](img/workbench_publicering_versionshistorik.png)


## Publicera med EntryScape Blocks
<a id=“publish-with-entryscape-blocks”></a>

Många som lägger in metadata i EntryScape Workbench använder också presentationsbiblioteket **EntryScape Blocks** för att visa informationen på en hemsida. Ta kontakt med EntryScape support för mer information om Blocks eller läs mer på [entryscape.com](https://entryscape.com/sv/produkter/blocks/).


### Exempel på visning

Nedan visas **redigeringsvyn i EntryScape Workbench** för ett besöksmål i ett turismprojekt. Här har användaren matat in texter, bilder och annan metadata om besöksmålet.

![Redigeringsläget i EntryScape Workbench, exempel](img/workbench_publicering_redigeringsexempel.png)

Samma text och bilder presenteras sedan med snygg layout på en hemsida med hjälp av presentationsbiblioteket **EntryScape Blocks**, med länkar till relaterade organisationer och närliggande objekt som kopplats ihop via delade data i EntryScape.

![Exempel på presentation med EntryScape Blocks](img/workbench_publicering_blocksexempel.png)
