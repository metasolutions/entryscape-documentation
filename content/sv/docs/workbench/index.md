

**EntryScape Workbench** är ett generellt **metadataverktyg** som anpassas till organisationers egna informationsstrukturer. Det kan t ex vara publikationer, turismdata, konstverk, leverantörsreskontra eller andra samlingar av data som ska publiceras som öppna eller delade data.

Arbetsytorna i Workbench kallas för **projekt** och de i sin tur innehåller olika **entiteter** (objekt), som man beskriver med metadata och relationer. 

![Projektexempel i Workbench](img/workbench_oversikt_projektexempel.png)

## Exempel på projekt

Exemplet nedan visar ett turismprojekt som innehåller en mängd olika **entitetstyper**, som aktiviteter, boende, evenemang mm. Vi har valt att lista inlagda entiteter av typen **aktiviteter**. Klickar man på respektive aktivitet kan man redigera metadata som namn, beskrivning, hemsida mm och koppla samman aktiviteten med andra entiteter som t ex foton, närliggande boenden, matställen mm. Länkade öppna data helt enkelt.

![EntryScape Workbench, inlagda aktiviteter](img/workbench_oversikt_redigeragavleborg.png)

Den insamlade informationen kan sedan **presenteras på en webbsida** för potentiella besökare i regionen. I exemplet listas olika aktiviteter för besökare i Gävleborg, med hjälp av presentationsbiblioteket **EntryScape Blocks**. Besökare kan klicka sig vidare från aktiviteterna till andra relaterade företeelser i närheten.

![Blockspresentation av aktiviteter från EntryScape Workbench ](img/workbench_oversikt_presenteragavleborg.png)




