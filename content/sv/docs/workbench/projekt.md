
Arbetsytorna i Workbench kallas för **projekt** och de i sin tur innehåller olika typer av **entiteter** (objekt).

På startsidan syns de projekt som du redan har skapat eller har fått tillgång till. Där syns också om projektet är **publikt** eller ej, d.v.s. om det är sökbart för andra. Om du inte ser något projekt här, behöver du börja med att skapa ett, eller be om [behörighet](#behörigheter-och-delningsinställningar) till projektet av gruppansvarig.

![Översiktsvy över projekt](img/workbench_projekt_listaprojekt.png)


## Skapa ett projekt
<a id="create-a-project"></a>

För att **skapa ett nytt projekt** klickar du på knappen **"Skapa"**.

![Skapaknappen](img/workbench_projekt_skapaknapp.png) 

Om er organisation har behov av att välja bland flera olika typer av projekt, får du börja med att ange vilken **projekttyp** som projektet ska ha. Projekttypen bestämmer också vilka **entitetstyper** som kan ingå i ditt projekt.

![Dialogfönster för att skapa ett projekt](img/workbench_projekt_skapaprojekt.png)

Därefter fyller du i **projektnamn**, d.v.s. en titel som till exempel ”Dokumenthantering”. Tänk på att titeln bör vara kort och att alla orden blir sökbara. Fyll också i en kort **projektbeskrivning**. 

Ett nyskapat projekt är alltid icke publikt. Den som har skapat projektet blir automatiskt **gruppansvarig** för det, vilket betyder att man administrerar andra användares behörighet till projektet. 


## Behörigheter och delningsinställningar
<a id="permissions-and-sharing-settings"></a>

I EntryScape Workbench kan en användare i regel vara antingen **gruppansvarig** för ett projekt eller **medlem** med läs- och skrivrättigheter. Den som är gruppansvarig kan ge andra användare behörighet samt göra projektet publikt. Medlemmar kan redigera projektets metadata samt skapa och tillhandahålla entiteter i projektet. Däremot kan medlemmar inte publicera projekt. Du kan vara medlem i ett eller flera projekt beroende på din arbetsroll och hur data organiseras.

I de fall där din organisation upprättat arbetsrutin, arbetsgrupp och fått in metadatahantering eller öppna data-arbete i förvaltningsorganisation, eller motsvarande, så finns det möjlighet att få en högre behörighet än gruppansvarig om det behövs, admin, i dialog med MetaSolutions support.

Om du inte har tillgång till det projekt du ska jobba med, så behöver du begära tillgång till projektet av den som är gruppansvarig (eller admin) i er organisation. (Den som är gruppansvarig är troligtvis motsvarande avdelnings- eller enhetschef.) 

Gruppansvariga kan **ändra behörigheter** genom att gå in under trepunktsmenyn och välja **”Delningsinställningar”**. 

![Menyalternativ Delningsinställningar](img/workbench_projekt_menydelningsinstallningar.png)


Där kan du **lägga till** och **ta bort användare** till den grupp som har behörighet till projektet. För att lägga till en användare till en grupp så klickar du på ”Lägg till användare i gruppen”. Du kan också välja **vilken sorts behörighet** användaren ska få (gruppansvarig eller medlem) genom att klicka på ikonen framför namnet.

![Behörighetsinställningar för användare](img/workbench_projekt_delningsinstallningar1.png)

För att **ta bort** en användare från en grupp så klickar du på trepunktsmenyn och väljer **”Ta bort”**.

![Menyalternativ Ta bort användare från grupp](img/workbench_projekt_delningsinstallningar2.png)



## Redigera projekt
<a id="edit-project"></a>

Du kan redigera informationen om ditt projekt genom att klicka på **trepunktsmenyn** och välja **"Redigera"**. 

![Menyalternativ Redigera projekt](img/workbench_projekt_menyredigeraprojekt.png)

I redigeringsläget kan du ändra information eller lägga till information på **olika språk**.

![Redigera projekt](img/workbench_projekt_redigeraprojekt.png)

## Konfigurera entitetstyper
<a id="configure-entity-types"></a>

Det är endast i **undantagsfall** som man behöver gå in och lägga till eller ta bort entitetstyper, eftersom de i de allra flesta fall redan är fördefinierade i den valda projekttypen för projektet. Om man behöver **ändra entitetstyper** för ett projekt så kan man klicka på trepunktsmenyn för ett projekt och välja **"Konfigurera entitetstyper"**.

![Menyalternativ konfigurera entitetstyper](img/workbench_projekt_menykonfigureraentitetstyper.png)

Har man redan en färdigbestämd projekttyp att utgå ifrån så går entitetstyperna inte att ändra, som i fallet med projekttypen "Turism" här under där entitetstyperna är utgråade och därmed låsta.

![Konfigurera entitetstyper, låsta](img/workbench_projekt_konfigureraentitetstyper_turism.png)

I andra fall där man behöver gå in och ändra entitetstyper så kan man klicka i och ur vilka entitetstyper man vill ska finnas i projektet.

![Konfigurera entitetstyper, valbara](img/workbench_projekt_konfigureraentitetstyper_standard.png)


## Ta bort projekt
<a id="remove-project"></a>

För att **ta bort** ett projekt, klicka på projektets trepunktsmeny och välj **”Ta bort”**.

![Menyalternativ Ta bort projekt](img/workbench_projekt_menytabortprojekt.png)
