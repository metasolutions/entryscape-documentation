#!/bin/bash

if [ -z "$1" ]; then
  echo "Parameter missing: directory to deploy to"
  exit 1
fi

DEPLOYDIR=`readlink -f $1`
if [ -z "$DEPLOYDIR" ];
then
  DEPLOYDIR=`greadlink -f $1`
fi

if [ -z "$DEPLOYDIR" ];
then
  echo "If you are a MacOS user you need to install coreutils. Exiting."
  exit 1
fi

for lang in content/*/
do
  langdir=${lang%*/}
  lang=${langdir##*/}

  echo "Building $lang ..."

  cp mkdocs_base.yml $langdir/mkdocs.yml
  cp -r vendor $langdir/docs/

  pushd $langdir > /dev/null
  cat config.yml >> mkdocs.yml
  #/usr/local/bin/mkdocs build --site-dir $DEPLOYDIR/$lang/ --config-file mkdocs.yml
  MKDOCSDIR=`readlink -f .`
  if [ -z "$MKDOCSDIR" ];
  then
    MKDOCSDIR=`greadlink -f .`
  fi
  docker run --platform linux/amd64 --rm -v $MKDOCSDIR:/mkdocs/ -v $DEPLOYDIR/$lang:/mkdocs/site/ metasolutions/mkdocs mkdocs build
  DOCKER_RESULT=$?
  rm mkdocs.yml
  rm -r docs/vendor
  popd > /dev/null
  if (( $DOCKER_RESULT != 0 )); then
    exit $DOCKER_RESULT
  fi
done
